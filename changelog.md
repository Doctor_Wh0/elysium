0.7.3.38
-
#### Changes
- Renamed gems
- Created bard songs
- Created Anvil REI Display 
- Blocked drop egg chicken
- Setted damage arrow
- Added towered contract 
- Reimplemented monocle
- Created status command
- Created first-login screen
- Created F10 Panel
- Created essence block
- Changed food hunger
- Removed all damage source in claims

0.7.3.36
-
#### Changes
- Prevent arts from being destroyed
- Implemented REI Recipe Serializer for Cauldrons, Oven, Refining, Barbeque and Altar
- Refactored Armor Config
- Implemented crops and seeds
- Disabled tooltip Booze, Juices and Wine
- Refactor claim system, implemented carpenter contract (to finish) and expires system
- New Carlo effects
- Resetted all hunger and saturation of food (filter with tag food from t0 to t5)
- Removed Empty Bottle when drink potion

0.7.3.28
-
#### Changes
- Created Tin Block
- Changed claim system
- [DEACTIVED] shield and weapons edit
- Refactored armor edit
- REI for Custom Machinery machines
- Disabled shear sheep
- Disabled dyable sheep
- [TODO] Implemented Dummy Lang, to refactor
- Registered all items of kubejs -> item.js 
- Implemented tooltip of rank and type of restricted items
- Fixed scoreboard nametags
- Injected default materials vanilla and magistuarmory
- Refactor contract
- Refactor sacrifice status effect
- Disabled death recall effect


0.7.3.22
-
#### Changes
- Disabled change max hp wolf when tamed
- Changed mining level of diamond and gold

0.7.3.21
-
#### Changes
- Disabled change hp wolf when tamed

0.7.3.20
-
#### Changes
- Added new resources (classes and spell books)
- Blocked travel to nether with portal
- Fix (to test) mesmerized and awaken effect
- Created dummy block for mining dimension
- Added new lang
- Registered new spell books with pre-bound spell effects
- Added saw

0.7.3.18
-
#### Changes
- Integrated Paintlog mod
- Custom modification of armor/weapons with specific file
- New status effect (first implementation): Awaken, Clumsy, DeathLoc and others.
- Removed unused powers
- Removed crit damage
- Fixed coffee

0.7.3.10
-
#### Changes
- Re-added all assets and moved item creation from KubeJS to mod
- Created Bientity Condition elysium:owner
- Created Entity Condition elysium:owned
- Coffee can now grow on dirt/grass dirt in overworld
- Fixed cooking pot and cooking stove (the last slot was not consumed)
- Test Stats (implemented bow damage)
- Modify Item Attribute Modifiers Event
- Item Tooltip Event

#### Dependencies
- origins
- trinkets
- arrp
- numismatic-overhaul
- archers-arsenal
- supplementaries
- goml-reserved
- vanish
- spell-power
#### Breaks
- memoryleakfix: `1.1.1`


0.7.3.7
-
#### Changes
- Cleanser effect with amplifier 1

#### Dependencies
- origins
- trinkets
- arrp
- numismatic-overhaul
- archers-arsenal
- supplementaries
- goml-reserved
- vanish
- spell-power
#### Breaks
- memoryleakfix: `1.1.1`


0.7.3.6
-
#### Changes
- Contract
- Fixed ScreenRegistry

#### Dependencies
- origins
- trinkets
- arrp
- numismatic-overhaul
- archers-arsenal
- supplementaries
- goml-reserved
- vanish
- spell-power
#### Breaks
- memoryleakfix: `1.1.1`



0.7.3.5
-
#### Changes
- Added ghost claim when the original expire (with all the logic)
- Added year to info claim
- Enchanted claim contract
- Added spawns count to mob spawner NBT
- Disabled unused things (elysium balance, eldebug)

#### Dependencies
- origins
- trinkets
- arrp
- numismatic-overhaul
- archers-arsenal
- supplementaries
- goml-reserved
- vanish
- spell-power
#### Breaks
- memoryleakfix: `1.1.1`

0.7.3
-
#### Changes
- Showcase partial fix
- Picked Up Event log
- New command /elysium untrustall

- Removed animal_bait and fish_bait (removed class BaitItem)

- Composter block output replaced with dung_ball (and fixed in REI)

- New items Claim Contract and Dung Ball

- Changed time claim from Timestamp to World Time

- Changed time claim display (showing the date and not time remaining)

- Conversion system from old claims with timestamp to new claim with world time




#### Dependencies
- origins
- trinkets
- arrp
- numismatic-overhaul
- archers-arsenal
- supplementaries
- goml-reserved
- vanish
- spell-power
#### Breaks
- memoryleakfix: `1.1.1`


0.7
-
#### Changes
- Replaced claim when times run out with admin claim
-  Replaced claim when a player is banned
-  Removed text when placed admin claim anchor
-  Fixed Aura Augment (now not working when time of anchor is 0)
-  Added command elysium info name_player
-  Removed class PotionRegistry (not used)
-  Added potion bed spawn, world spawn and death return
-  Changed duration spell power potion (from 30 seconds to 8 minutes)
-  Obtained UUID/name from minecraft servers
-  Changed milk effect color and category

#### Dependencies
- origins
- trinkets
- arrp
- numismatic-overhaul
- archers-arsenal
- supplementaries
- goml-reserved
- vanish
- spell-power
#### Breaks
- memoryleakfix: `1.1.1`

0.6.1.1
-
#### Changes
- [DISABLED] Added potion for cleanser effect
- Added subcommand to get a player's last saved anchor time
- Angelic Aura claim argument now doesn't give regen to untrusted players
- Made ALL crafting screens obey the `elysium:remove_recipes` power
- Make admin claims able to override each other if overlapping
- Update `mixin-extras` to `0.2.0-rc.5`
- Add potions for the SpellPower status effects
- New contributor @DoctorWho

#### Dependencies
- origins
- trinkets
- arrp
- numismatic-overhaul
- archers-arsenal
- supplementaries
- goml-reserved
- vanish
- spell-power
#### Breaks
- memoryleakfix: `1.1.1`

0.6.0
-
#### Changes
- Replaced `spawnRadius` with `distanceRadiusBetween` and renamed `spawnCenterPosition` to `spawnPosition`
- Removed linear tax algorithm and related options
- Removed unused `summon-random` command
- Small bait item rework
- Made anti-cheat a bit smarter
- Add log when an anchor is missing time data
- Fix dimensional gamemode not changing upon death
- Small internal refactor
- Merge changes from DreamingCodes's branch
- Block unwanted block interactions on the client rather than the server
- Log `goml escape`
- Fix claims losing timestamp data and add backup system
- Log when claims drop for tax causes

#### Dependencies
- origins
- trinkets
- arrp
- numismatic-overhaul
- archers-arsenal
- supplementaries
- goml-reserved
- vanish

0.5.10
-
#### Changes
- Make origin reload not invoke power events
- Make Admin Claim immune to modifications
- Make distance between claims configurable

#### Dependencies
- origins
- trinkets
- arrp
- numismatic-overhaul
- archers-arsenal
- supplementaries
- goml-reserved
- vanish

0.5.9
-
#### Changes
- Fix system messages not being disabled
- Unify debug commands ( crash, broadcast )
- Update MixinExtras
- Fix claims being too close to each other
- Fix vanished players getting hidden system messages
- Replace Wildfire female gender mod's player list screen with player's settings screen
- Added missing power translations
- Remove doors mixins used to fix crawling
- Send current (~10b) claim box to client
- Fix `elysium:longer_potions` stacking
- Make anchors drop when empty and a configurable time is passed ( defaults to 2 days )
- Remove broken feature: "Allow players to break empty anchors"
- Fix time reset on anchor upgrading

#### Dependencies
- origins
- trinkets
- arrp
- numismatic-overhaul
- archers-arsenal
- supplementaries
- goml-reserved
- vanish ( new )
- female-gender ( new )

package com.enderzombi102.elysium;


import codes.dreaming.noBadPlayers.NoBadPlayers;
import codes.dreaming.noBadPlayers.util.StateSaverAndLoader;
import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.event.ModifyItemAttributeModifierEvent;
import com.enderzombi102.elysium.feature.AntiCheatFeature;
import com.enderzombi102.elysium.feature.ClaimFeature;
import com.enderzombi102.elysium.feature.DimensionalGamemodeFeature;
import com.enderzombi102.elysium.recipe.*;
import com.enderzombi102.elysium.registry.*;
import com.enderzombi102.elysium.util.Const;
import io.github.apace100.origins.origin.Origin;
import io.github.apace100.origins.origin.OriginRegistry;
import io.github.apace100.origins.registry.ModComponents;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;
import net.fabricmc.fabric.api.entity.event.v1.ServerPlayerEvents;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.fabricmc.fabric.api.item.v1.ModifyItemAttributeModifiersCallback;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.block.Block;
import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.resource.LifecycledResourceManager;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tag.TagKey;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.enderzombi102.elysium.util.Const.*;

public class Elysium implements ModInitializer {
	public static final Logger LOGGER = LoggerFactory.getLogger( "Elysium" );
	public static final TagKey< EntityType< ? > > ELYSIUM_OWNER = TagKey.of( Registry.ENTITY_TYPE_KEY, new Identifier( ID, "owner" ) );
	public static final TagKey< Block > BLOCK_REMOVE = TagKey.of( Registry.BLOCK_KEY, new Identifier( ID, "remove_block" ) );
	public static final TagKey< Item > ITEM_TOOLTIP = TagKey.of( Registry.ITEM_KEY, new Identifier( ID, "remove_tooltip" ) );
	public static final TagKey< EntityType< ? > > ENTITY_REMOVE = TagKey.of( Registry.ENTITY_TYPE_KEY, new Identifier( ID, "remove_entity" ) );
	public static final Identifier QUERY_ID = getId( "query_nick" );
	public static final Identifier VIDEO_ID = getId( "video" );
	public static @Nullable MinecraftServer server = null;
	public static boolean reloading = false;

	private static void onServerReloadEnd( MinecraftServer server, LifecycledResourceManager manager, boolean success ) {
		if ( !success )
			return;
		var players = server.getPlayerManager().getPlayerList();
		Config.loadFromDisk( true );

		LOGGER.info( "[Elysium] Sending config update to {} players", players.size() );
		for ( var player : players ) {
			ServerPlayNetworking.send( player, CONFIG_SYNC_ID, Config.getPacketBufData() );
		}
		if ( Config.getData().features.reloadOrigins ) {
			for ( var player : players ) {
				var component = player.getComponent( ModComponents.ORIGIN );
				var layers = new ArrayList<>( component.getOrigins().keySet() );
				var origins = new ArrayList<>( component.getOrigins().values() );

				for ( var i = 0; i < layers.size(); i += 1 ) {
					component.setOrigin( layers.get( i ), Origin.EMPTY );
					component.setOrigin( layers.get( i ), OriginRegistry.get( origins.get( i ).getIdentifier() ) );
				}

				component.sync();
			}

			LOGGER.info( "[Elysium] Reloaded origins from disk" );
			server.getPlayerManager().broadcast( Text.literal( "[Elysium] Reloaded origins from disk" ), false );
		}

		reloading = false;
	}

	public void onInitialize() {
		ServerLifecycleEvents.END_DATA_PACK_RELOAD.register( Elysium::onServerReloadEnd );
		ServerLifecycleEvents.START_DATA_PACK_RELOAD.register( ( server, resourceManager ) -> reloading = true );
		CommandRegistrationCallback.EVENT.register( CommandRegistry::register );
		ModifyItemAttributeModifiersCallback.EVENT.register( ModifyItemAttributeModifierEvent::register );
		ServerLifecycleEvents.SERVER_STARTED.register( mc -> server = mc );
		ServerPlayNetworking.registerGlobalReceiver( QUERY_ID, ( server, player, handler, buf, responseSender ) -> {
			if(server.isDedicated()) {
				String name = StateSaverAndLoader.playerNames.getOrDefault( handler.player.getUuid(), null );
				String state = buf.readString();
				PacketByteBuf packetByteBuf = PacketByteBufs.create();
				if ( state.equalsIgnoreCase( "choose" ) && name == null ) {
					String newName = buf.readString();
					newName = StringUtils.lowerCase( newName );
					newName = WordUtils.capitalize( newName );
					newName = newName.replace( " ", "" );
					Pattern p = Pattern.compile( "[^a-z]", Pattern.CASE_INSENSITIVE );
					Matcher m = p.matcher( newName );
					boolean c1 = !newName.isEmpty();
					boolean c2 = !m.find();
					String finalNewName = newName;

					if ( StateSaverAndLoader.playerNames.values().stream().noneMatch( ( playerName ) -> {
						return playerName.equalsIgnoreCase( finalNewName );
					} ) && c1 && c2 && newName.length()<17 && !StateSaverAndLoader.playerNames.containsKey( handler.player.getUuid() )) {
						StateSaverAndLoader.playerNames.put( handler.player.getUuid(), newName );
						NoBadPlayers.stateSaverAndLoader.markDirty();
						packetByteBuf.writeString( "choose" );
						packetByteBuf.writeBoolean( true );
					} else {
						packetByteBuf.writeString( "choose" );
						packetByteBuf.writeBoolean( false );
					}
				} else if ( state.equalsIgnoreCase( "check" ) ) {
					packetByteBuf.writeString( "check" );
					packetByteBuf.writeBoolean( StateSaverAndLoader.playerNames.containsKey( handler.player.getUuid() ) );
				}
				responseSender.sendPacket( QUERY_ID, packetByteBuf );
			}
		} );
		ServerPlayNetworking.registerGlobalReceiver( VIDEO_ID, ( server, player, handler, buf, responseSender ) -> {
			if(buf.readBoolean()){
				player.teleport( 100,100,100 );
				player.sendMessage( Text.literal("Congratulations, you can now play on Elysiumcraft 2! Have fun!").formatted( Formatting.GREEN ) );

			}
		} );
		ServerPlayerEvents.AFTER_RESPAWN.register( Const.getId("respawn_health"), (
			oldPlayer, newPlayer, alive  )->{
			newPlayer.playerTick();
		} );
		// registries
		PowerRegistry.register();
		BlockConditionRegistry.register();
		StatusEffectRegistry.register();
		ItemRegistry.register();
		BlockRegistry.register();
		ScreenRegistry.register();
		GameruleRegistry.register();
		CustomStatRegistry.register();
		// features
		ClaimFeature.register();
		DimensionalGamemodeFeature.register();
		AntiCheatFeature.register();
		Registry.register( Registry.RECIPE_SERIALIZER, new Identifier( ID, TerracottaCauldronRecipe.Serializer.ID ), TerracottaCauldronRecipe.Serializer.INSTANCE );
		Registry.register( Registry.RECIPE_TYPE, new Identifier( ID, TerracottaCauldronRecipe.Type.ID ), TerracottaCauldronRecipe.Type.INSTANCE );
		Registry.register( Registry.RECIPE_SERIALIZER, new Identifier( ID, CopperCauldronRecipe.Serializer.ID ), CopperCauldronRecipe.Serializer.INSTANCE );
		Registry.register( Registry.RECIPE_TYPE, new Identifier( ID, CopperCauldronRecipe.Type.ID ), CopperCauldronRecipe.Type.INSTANCE );
		Registry.register( Registry.RECIPE_SERIALIZER, new Identifier( ID, IronCauldronRecipe.Serializer.ID ), IronCauldronRecipe.Serializer.INSTANCE );
		Registry.register( Registry.RECIPE_TYPE, new Identifier( ID, IronCauldronRecipe.Type.ID ), IronCauldronRecipe.Type.INSTANCE );
		Registry.register( Registry.RECIPE_SERIALIZER, new Identifier( ID, BarbequeRecipe.Serializer.ID ), BarbequeRecipe.Serializer.INSTANCE );
		Registry.register( Registry.RECIPE_TYPE, new Identifier( ID, BarbequeRecipe.Type.ID ), BarbequeRecipe.Type.INSTANCE );
		Registry.register( Registry.RECIPE_SERIALIZER, new Identifier( ID, OvenRecipe.Serializer.ID ), OvenRecipe.Serializer.INSTANCE );
		Registry.register( Registry.RECIPE_TYPE, new Identifier( ID, OvenRecipe.Type.ID ), OvenRecipe.Type.INSTANCE );
		Registry.register( Registry.RECIPE_SERIALIZER, new Identifier( ID, AltarRecipe.Serializer.ID ), AltarRecipe.Serializer.INSTANCE );
		Registry.register( Registry.RECIPE_TYPE, new Identifier( ID, AltarRecipe.Type.ID ), AltarRecipe.Type.INSTANCE );
		Registry.register( Registry.RECIPE_SERIALIZER, new Identifier( ID, AnvilRecipe.Serializer.ID ), AnvilRecipe.Serializer.INSTANCE );
		Registry.register( Registry.RECIPE_TYPE, new Identifier( ID, AnvilRecipe.Type.ID ), AnvilRecipe.Type.INSTANCE );
		Registry.register( Registry.RECIPE_SERIALIZER, new Identifier( ID, RefiningStationRecipe.Serializer.ID ), RefiningStationRecipe.Serializer.INSTANCE );
		Registry.register( Registry.RECIPE_TYPE, new Identifier( ID, RefiningStationRecipe.Type.ID ), RefiningStationRecipe.Type.INSTANCE );
		Registry.register( Registry.RECIPE_SERIALIZER, new Identifier( ID, FurnishRecipe.Serializer.ID ), FurnishRecipe.Serializer.INSTANCE );
		Registry.register( Registry.RECIPE_TYPE, new Identifier( ID, FurnishRecipe.Type.ID ), FurnishRecipe.Type.INSTANCE );
	}
}

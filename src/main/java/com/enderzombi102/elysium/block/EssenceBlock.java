package com.enderzombi102.elysium.block;

import com.enderzombi102.elysium.registry.ItemRegistry;
import io.wispforest.owo.nbt.NbtKey;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.AbstractFurnaceBlockEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.passive.ChickenEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.IntProperty;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

public class EssenceBlock extends Block {
	public static final IntProperty CUSTOM_MODEL_DATA = IntProperty.of( "custom_model_data",0,5 );

	public EssenceBlock( Settings settings ) {
		super( settings );
		this.setDefaultState(this.getDefaultState().with(CUSTOM_MODEL_DATA, 0));
	}

	@Override
	protected void appendProperties( StateManager.Builder< Block, BlockState > builder ) {
		builder.add( CUSTOM_MODEL_DATA );
	}

	@Override
	public void onPlaced( World world, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack itemStack ) {
		if(itemStack.getNbt()!=null && itemStack.getNbt().contains( "CustomModelData" ) && itemStack.getNbt().getInt( "CustomModelData" )<6 && itemStack.getNbt().getInt( "CustomModelData" )>0) {
			state=state.with( CUSTOM_MODEL_DATA, itemStack.getNbt().getInt( "CustomModelData" ) );
		}else{
			state=state.with( CUSTOM_MODEL_DATA, 0 );
		}
		world.setBlockState( pos, state );
	}

	@Override
	public void onBreak( World world, BlockPos pos, BlockState state, PlayerEntity player ) {
		// Useless work, no LT
		// ItemStack i=state.getBlock().asItem().getDefaultStack();
		//NbtCompound nbtCompound=new NbtCompound();
		//nbtCompound.putInt("CustomModelData",state.get( CUSTOM_MODEL_DATA ));
		//i.setNbt(nbtCompound  );
		//world.spawnEntity( new ItemEntity( world,pos.getX(),pos.getY(),pos.getZ(),i ));
		super.onBreak( world, pos, state, player );
	}
}

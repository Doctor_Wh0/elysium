package com.enderzombi102.elysium.client;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.effect.StunSpawner;
import com.enderzombi102.elysium.event.ItemTooltipCallbackEvent;
import com.enderzombi102.elysium.feature.AntiCheatFeature;
import com.enderzombi102.elysium.feature.ClaimFeature;
import com.enderzombi102.elysium.registry.BlockRegistry;
import com.enderzombi102.elysium.registry.ScreenRegistry;
import com.enderzombi102.elysium.registry.StatusEffectRegistry;
import com.enderzombi102.elysium.screen.FirstLoginScreen;
import com.enderzombi102.elysium.screen.ImageScreen;
import com.enderzombi102.elysium.screen.NicknameScreen;
import com.github.NGoedix.videoplayer.client.gui.VideoScreen;
import com.minecraftserverzone.weaponmaster.gui.WeaponMasterScreen;
import com.minecraftserverzone.weaponmaster.setup.Helper;
import net.devtech.arrp.api.RRPCallback;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.item.v1.ItemTooltipCallback;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayConnectionEvents;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.client.screen.v1.ScreenEvents;
import net.fabricmc.fabric.api.client.screen.v1.Screens;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.ingame.InventoryScreen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.util.InputUtil;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.spell_engine.api.effect.CustomParticleStatusEffect;

import java.awt.event.KeyEvent;

import static com.enderzombi102.elysium.Elysium.QUERY_ID;
import static com.enderzombi102.elysium.util.Const.CONFIG_SYNC_ID;
import static dev.latvian.mods.kubejs.client.NotificationToast.ICONS;


public class ElysiumClient implements ClientModInitializer {
	public static KeyBinding SCREEN_IMG;
	@Override
	public void onInitializeClient() {
		ItemTooltipCallback.EVENT.register( ItemTooltipCallbackEvent::register );
		// registries
		ScreenRegistry.registerClient();

		// features
		ClaimFeature.registerClient();
		AntiCheatFeature.registerClient();


		// resource packs
		RRPCallback.AFTER_VANILLA.register( resources -> {
			resources.add( new ElysiumResourcePack( "ElysiumCraft resources", "resourcepack" ) );
		} );

		// config sync
		ClientPlayNetworking.registerGlobalReceiver( CONFIG_SYNC_ID, ( client, networkHandler, data, sender ) -> {
			var version = data.readString();
			var config = data.readString();

			client.execute( () -> Config.loadFromPacketData( version, config ) );
		} );
		CustomParticleStatusEffect.register( StatusEffectRegistry.MESMERIZED, new StunSpawner() );
		BlockRenderLayerMap.INSTANCE.putBlock( BlockRegistry.BAMBOO_CROP_BLOCK, RenderLayer.getCutout() );
		BlockRenderLayerMap.INSTANCE.putBlock( BlockRegistry.SUGAR_CANE_CROP_BLOCK, RenderLayer.getCutout() );
		BlockRenderLayerMap.INSTANCE.putBlock( BlockRegistry.PUMPKIN_CROP_BLOCK, RenderLayer.getCutout() );
		BlockRenderLayerMap.INSTANCE.putBlock( BlockRegistry.RED_MUSHROOM_CROP_BLOCK, RenderLayer.getCutout() );
		BlockRenderLayerMap.INSTANCE.putBlock( BlockRegistry.BROWN_MUSHROOM_CROP_BLOCK, RenderLayer.getCutout() );
		BlockRenderLayerMap.INSTANCE.putBlock( BlockRegistry.MELON_CROP_BLOCK, RenderLayer.getCutout() );
		BlockRenderLayerMap.INSTANCE.putBlock( BlockRegistry.CORN_CROP_BLOCK, RenderLayer.getCutout() );
		BlockRenderLayerMap.INSTANCE.putBlock( BlockRegistry.CACTUS_CROP_BLOCK, RenderLayer.getCutout() );
		BlockRenderLayerMap.INSTANCE.putBlock( BlockRegistry.KELP_CROP_BLOCK, RenderLayer.getCutout() );
		BlockRenderLayerMap.INSTANCE.putBlock( BlockRegistry.RICE_CROP_BLOCK, RenderLayer.getCutout() );
		BlockRenderLayerMap.INSTANCE.putBlock( BlockRegistry.FLAX_CROP_BLOCK, RenderLayer.getCutout() );
		BlockRenderLayerMap.INSTANCE.putBlock( BlockRegistry.COCOA_CROP_BLOCK, RenderLayer.getCutout() );
		BlockRenderLayerMap.INSTANCE.putBlock( BlockRegistry.POTATO_CROP_BLOCK, RenderLayer.getCutout() );
		BlockRenderLayerMap.INSTANCE.putBlock( BlockRegistry.SWEET_BERRY_CROP_BLOCK, RenderLayer.getCutout() );
		BlockRenderLayerMap.INSTANCE.putBlock( BlockRegistry.CARROT_CROP_BLOCK, RenderLayer.getCutout() );
		Helper.setData();
		ClientPlayConnectionEvents.JOIN.register( ( ( handler, sender, server ) -> {
			PacketByteBuf buf = PacketByteBufs.create();
			buf.writeString( "check" );
			sender.sendPacket( QUERY_ID, buf );
		} ) );
		ClientPlayNetworking.registerGlobalReceiver( QUERY_ID, ( client, handler, buf, responseSender ) -> {
			String state = buf.readString();
			boolean result = buf.readBoolean();
			if ( state.equalsIgnoreCase( "check" ) ) {
				if(!result){
					client.execute( () ->{
						MinecraftClient.getInstance().setScreen(new FirstLoginScreen() );} );
				}
			} else if ( state.equalsIgnoreCase( "choose" ) ) {
				if(result){
					if(MinecraftClient.getInstance().currentScreen != null) {
						client.execute(() -> {
							client.setScreen(new VideoScreen("https://youtu.be/dQw4w9WgXcQ", 101, true));
						});
					}
				}else{
					if(MinecraftClient.getInstance().currentScreen instanceof NicknameScreen ){
						NicknameScreen.setOutput( "Error, name already taken" );
						NicknameScreen.setMessage( "Confirm" );
					}
				}
			}
		} );
		SCREEN_IMG = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.elysium.img", InputUtil.Type.KEYSYM, KeyEvent.VK_F10, "key.elysium.category"));
		ClientTickEvents.END_CLIENT_TICK.register(( client) -> {
			while(SCREEN_IMG.wasPressed()) {
				MinecraftClient.getInstance().setScreen(new ImageScreen());
			}

		});
		/*ScreenEvents.AFTER_INIT.register(( client, screen, scaledWidth, scaledHeight) -> {
			TODO:FINISH

			if (screen instanceof InventoryScreen inventoryScreen) {
				Screens.getButtons(inventoryScreen).add(new ButtonWidget( 20, 18, 30, 30, Text.literal("Show inventory"),
					(button) -> Elysium.LOGGER.info("CLICCATO")));
			}
		});*/
	}


}

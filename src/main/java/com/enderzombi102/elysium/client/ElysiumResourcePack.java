package com.enderzombi102.elysium.client;

import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.util.Util;
import net.minecraft.resource.AbstractFileResourcePack;
import net.minecraft.resource.ResourceNotFoundException;
import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;
import net.minecraft.util.InvalidIdentifierException;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.enderlib.SafeUtils.doSafely;

public class ElysiumResourcePack extends AbstractFileResourcePack {
	private final @NotNull String root;
	private final @NotNull String name;

	public ElysiumResourcePack( @NotNull String name, @NotNull String root ) {
		super( Util.getJarPath().toFile() );
		this.name = name;
		this.root = root;
	}

	@Override
	protected InputStream openFile( String filename ) throws IOException {
		if ( Config.getData().logging.virtualResourcePack )
			LOGGER.info( "[Elysium] Minecraft is opening \"" + filename + "\"" );

		final var resource = this.getResourceURL( filename );
		if ( resource == null )
			throw new ResourceNotFoundException( base, filename );

		return resource.openStream();
	}

	@Override
	public @NotNull String getName() {
		return this.name;
	}

	@Override
	public void close() {
	}

	@Override
	protected boolean containsFile( String filename ) {
		final var hasFile = this.getResourceURL( filename ) != null;

		if ( Config.getData().logging.virtualResourcePack )
			LOGGER.info( "[Elysium] Minecraft is searching for \"{}\" (found: {})", filename, hasFile );

		return hasFile;
	}

	@Override
	public Collection< Identifier > findResources( ResourceType type, String namespace, String prefix, Predicate< Identifier > allowedPathPredicate ) {
		var ids = new ArrayList< Identifier >();
		var ns = this.getResourceURL( type.getDirectory() + "/" + namespace );

		if ( ns == null )
			return ids;

		var namespacePath = Path.of( doSafely( ns::toURI ) ).toAbsolutePath().normalize();

		try {
			Files.walk( namespacePath )
				.filter( Files::isRegularFile )
				// needed as making the path relative basing off `namespacePath` will remove `$type/$namespace`
				.map( namespacePath::relativize )
				.map( Path::toString )
				.map( it -> it.replace( "\\", "/" ) )
				.filter( it -> it.startsWith( prefix ) )
				.filter( it -> allowedPathPredicate.test( new Identifier( namespace, it ) ) )
				.forEach( s -> {
					try {
						ids.add( new Identifier( namespace, s ) );
					} catch ( InvalidIdentifierException e ) {
						LOGGER.warn( e.getMessage() );
					}
				} );
		} catch ( IOException e ) {
			LOGGER.warn( "[Elysium] findResources at `" + namespacePath + "` in namespace `" + namespace + "`, root `" + this.root + "` failed!", e );
		}

		return ids;
	}

	@Override
	public Set< String > getNamespaces( ResourceType type ) {
		final var set = new HashSet< String >();

		var url = this.getResourceURL( type.getDirectory() );

		if ( url == null )
			return set;

		var path = Path.of( doSafely( url::toURI ) ).toAbsolutePath().normalize();

		try {
			Files.walk( path, 2 )
				.filter( Files::isDirectory )
				.map( path::relativize )
				.filter( it -> !it.toString().isEmpty() )
				.filter( it -> !it.toString().contains( File.separator ) )
				.forEach( it -> {
					var namespace = it.toString();
					if ( StringUtils.isAllLowerCase( namespace ) )
						set.add( namespace );
					else
						warnNonLowerCaseNamespace( namespace );
				} );
		} catch ( IOException e ) {
			throw new RuntimeException( e );
		}

		return set;
	}

	private @Nullable URL getResourceURL( @NotNull String filename ) {
		return ElysiumResourcePack.class.getResource( "/" + this.root + "/" + filename );
	}
}

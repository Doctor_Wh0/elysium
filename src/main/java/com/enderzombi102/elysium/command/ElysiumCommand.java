package com.enderzombi102.elysium.command;

import codes.dreaming.noBadPlayers.NoBadPlayers;
import codes.dreaming.noBadPlayers.util.StateSaverAndLoader;
import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.component.interfaces.ImperiumLevelComponent;
import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.imixin.ElysiumClaimBlockEntity;
import com.enderzombi102.elysium.registry.CompRegistry;
import com.google.common.collect.Multimap;
import com.jamieswhiteshirt.rtree3i.Entry;
import com.jamieswhiteshirt.rtree3i.Selection;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import draylar.goml.GetOffMyLawn;
import draylar.goml.api.Claim;
import draylar.goml.api.ClaimBox;
import draylar.goml.api.ClaimUtils;
import draylar.goml.block.entity.ClaimAnchorBlockEntity;
import me.lucko.fabric.api.permissions.v0.Permissions;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.command.argument.BlockPosArgumentType;
import net.minecraft.command.argument.EntityArgumentType;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.item.*;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.tag.ItemTags;
import net.minecraft.text.Text;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.spell_engine.api.item.weapon.StaffItem;
import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import static com.enderzombi102.elysium.util.Const.CONFIG_SYNC_ID;
import static com.enderzombi102.elysium.util.Util.position;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.literal;

public class ElysiumCommand {
	public static LiteralArgumentBuilder< ServerCommandSource > command() {
		return literal( "elysium" )
			.requires( it -> Permissions.check( it, "elysium.elysium", 2 ) )
			//.then( literal( "output" ).executes( ElysiumCommand::output ) )
			//.then(literal("output2").executes( ElysiumCommand::output2 ))
			//.then(literal("output3").executes( ElysiumCommand::output3 ))
			//.then(literal("output4").executes( ElysiumCommand::output4 ))
			//.then(literal("output5").executes( ElysiumCommand::output5 ))
			.then( literal( "config" ).then( literal( "reload" ).executes( ElysiumCommand::configReload ) )
				.then( literal( "spawn" ).then( argument( "pos", BlockPosArgumentType.blockPos() )
					.executes( ElysiumCommand::configSpawnSet ) ).executes( ElysiumCommand::configSpawn ) ) )
			.then( literal( "no-command" ).executes( ElysiumCommand::toggleNoCommandFeedback ) )
			.then( literal( "info" )
				.then( argument( "owner", StringArgumentType.string() )
					.executes( ElysiumCommand::claimGetPosPlayer ) ) )

			.then( literal( "balance" ).then( literal( "imperium" )
				.executes( ElysiumCommand::balanceImperiumDisplay )
				.then( literal( "add" )
					.then( argument( "amount", IntegerArgumentType.integer( 0 ) )
						.executes( ElysiumCommand::balanceImperiumAdd ) ) ).then( literal( "remove" )
					.then( argument( "amount", IntegerArgumentType.integer( 0 ) )
						.executes( ElysiumCommand::balanceImperiumRemove ) ) ) ) )
			.then( literal( "untrustall" )
				.executes( ElysiumCommand::untrustall ) );

	}

	private static int output5( CommandContext< ServerCommandSource > serverCommandSourceCommandContext ) {
		System.out.println( "INIT BOWS" );
		List< Item > items = Registry.ITEM.stream().filter( item -> item instanceof RangedWeaponItem ).toList();
		items.forEach( new Consumer< Item >() {
			@Override
			public void accept( Item item ) {
				RangedWeaponItem i = (RangedWeaponItem) item;
				System.out.println( i.kjs$getId().split( ":" )[ 0 ] + "," +
					i.kjs$getId() + "," +
					i.getRange() + "," +
					i.getMaxDamage() );
			}
		} );
		List< Item > arrow = Registry.ITEM.stream().filter( item -> item.getDefaultStack().isIn( ItemTags.ARROWS ) ).toList();
		arrow.forEach( new Consumer< Item >() {
			@Override
			public void accept( Item item ) {
				ArrowItem projectileEntity = (ArrowItem) item;
				PersistentProjectileEntity persistentProjectileEntity = projectileEntity.createArrow( serverCommandSourceCommandContext.getSource().getWorld(), item.getDefaultStack(), serverCommandSourceCommandContext.getSource().getPlayer() );
				System.out.println( item.kjs$getId().split( ":" )[ 0 ] + "," +
					item.kjs$getId() + "," +
					persistentProjectileEntity.getDamage() + "," );
			}
		} );
		List< Item > staff = Registry.ITEM.stream().filter( item -> item instanceof StaffItem ).toList();
		staff.forEach( new Consumer< Item >() {
			@Override
			public void accept( Item i ) {
				StaffItem staffItem = (StaffItem) i;
				Multimap< EntityAttribute, EntityAttributeModifier > map = staffItem.getAttributeModifiers( EquipmentSlot.MAINHAND );
				try {
					if ( map.containsKey( EntityAttributes.GENERIC_ATTACK_DAMAGE ) ) {
						AtomicReference< Double > damage = new AtomicReference<>( (double) 0 );
						map.get( EntityAttributes.GENERIC_ATTACK_DAMAGE ).forEach( entityAttributeModifier -> {
							if ( entityAttributeModifier.getOperation().equals( EntityAttributeModifier.Operation.ADDITION ) )
								damage.updateAndGet( v -> ( (double) ( v + entityAttributeModifier.getValue() ) ) );
							else
								System.out.println( (Item) i + " contains multiply damage" );
						} );
						AtomicReference< Double > speed = new AtomicReference<>( (double) 0 );
						map.get( EntityAttributes.GENERIC_ATTACK_SPEED ).forEach( entityAttributeModifier -> {
							if ( entityAttributeModifier.getOperation().equals( EntityAttributeModifier.Operation.ADDITION ) )
								speed.updateAndGet( v -> ( (double) ( v + entityAttributeModifier.getValue() ) ) );
							else
								System.out.println( (Item) i + " contains multiply speed" );
						} );
						System.out.println( ( (Item) i ).kjs$getId().split( ":" )[ 0 ] + "," +
							( (Item) i ).kjs$getId() + "," +
							damage + "," + speed + "," +
							( (Item) i ).getMaxDamage() );
					}
				} catch ( Exception ignored ) {

				}
			}
		} );
		return 1;
	}

	private static int output4( CommandContext< ServerCommandSource > serverCommandSourceCommandContext ) {
		serverCommandSourceCommandContext.getSource().getPlayer().getAttributeBaseValue( EntityAttributes.GENERIC_ATTACK_SPEED );
		List< Item > items = Registry.ITEM.stream().filter( item -> item instanceof Item ).toList();
		System.out.println( "INIT Items" );
		DecimalFormat df = new DecimalFormat( "0.00" );
		for ( Item i : items ) {
			try {
				Multimap< EntityAttribute, EntityAttributeModifier > map = ( (Item) i ).getAttributeModifiers( EquipmentSlot.MAINHAND );
				try {
					if ( map.containsKey( EntityAttributes.GENERIC_ATTACK_DAMAGE ) ) {
						AtomicReference< Double > damage = new AtomicReference<>( (double) 0 );
						map.get( EntityAttributes.GENERIC_ATTACK_DAMAGE ).forEach( entityAttributeModifier -> {
							if ( entityAttributeModifier.getOperation().equals( EntityAttributeModifier.Operation.ADDITION ) )
								damage.updateAndGet( v -> ( (double) ( v + entityAttributeModifier.getValue() ) ) );
							else
								System.out.println( (Item) i + " contains multiply damage" );
						} );
						AtomicReference< Double > speed = new AtomicReference<>( (double) 0 );
						map.get( EntityAttributes.GENERIC_ATTACK_SPEED ).forEach( entityAttributeModifier -> {
							if ( entityAttributeModifier.getOperation().equals( EntityAttributeModifier.Operation.ADDITION ) )
								speed.updateAndGet( v -> ( (double) ( v + entityAttributeModifier.getValue() ) ) );
							else
								System.out.println( (Item) i + " contains multiply speed" );
						} );
						System.out.println( ( (Item) i ).kjs$getId().split( ":" )[ 0 ] + "," +
							( (Item) i ).kjs$getId() + "," +
							damage + "," + speed + "," +
							( (Item) i ).getMaxDamage() );
					}
				} catch ( Exception ignored ) {

				}
				/*String attack_speed="";
				String damage="";
				String durability="";
				try{
					ItemInput itemInput = Config.getWeapon().weapon.get( i.kjs$getId() );
					ArrayList< Object > t= itemInput.getModifiers().getOrDefault( "generic.attack_damage", new ArrayList<>() );
					if(!t.isEmpty())
						damage=t.get( 0 ).toString();
					t= itemInput.getModifiers().getOrDefault( "generic.attack_speed", new ArrayList<>() );
					if(!t.isEmpty())
						attack_speed=t.get( 0 ).toString();
					t= itemInput.getModifiers().getOrDefault( "generic.durability", new ArrayList<>() );
					if(!t.isEmpty())
						durability=t.get( 0 ).toString();
				}catch(Exception ignored){
					//ignored.printStackTrace();
					System.out.println("PROBLEM WITH "+i.getName().getString() );
				}
				System.out.println( swordItem.kjs$getId().split(":")[0]+","+
					swordItem.kjs$getId()+","+
					swordItem.getEnchantability()+","+
					swordItem.getMaterial().toString()+","+
					swordItem.getAttackDamage()+","+damage+","+
					df.format(swordItem.getAttributeModifiers( EquipmentSlot.MAINHAND).get( EntityAttributes.GENERIC_ATTACK_SPEED ).stream().toList().get( 0 ).getValue())+","+attack_speed+","+
					swordItem.getMaxDamage()+","+durability );*/
				/*System.out.println( staffItem.kjs$getId().split( ":" )[ 0 ] + "," +
					staffItem.kjs$getId() + "," +
					staffItem.get.getAttackDamage() + "," +
					df.format( swordItem.getAttributeModifiers( EquipmentSlot.MAINHAND ).get( EntityAttributes.GENERIC_ATTACK_SPEED ).stream().toList().get( 0 ).getValue() ) + "," +
					swordItem.getMaxDamage() );*/
			} catch ( Exception e ) {
				System.out.println( i.getName() + " with no hunger" );
			}
		}
		return 1;
	}

	private static int output2( CommandContext< ServerCommandSource > serverCommandSourceCommandContext ) {
		serverCommandSourceCommandContext.getSource().getPlayer().getAttributeBaseValue( EntityAttributes.GENERIC_ATTACK_SPEED );
		List< Item > items = Registry.ITEM.stream().filter( item -> item instanceof SwordItem ).toList();
		System.out.println( "INIT WEAPON" );
		DecimalFormat df = new DecimalFormat( "0.00" );
		for ( Item i : items ) {
			try {
				SwordItem swordItem = (SwordItem) i;
				/*String attack_speed="";
				String damage="";
				String durability="";
				try{
					ItemInput itemInput = Config.getWeapon().weapon.get( i.kjs$getId() );
					ArrayList< Object > t= itemInput.getModifiers().getOrDefault( "generic.attack_damage", new ArrayList<>() );
					if(!t.isEmpty())
						damage=t.get( 0 ).toString();
					t= itemInput.getModifiers().getOrDefault( "generic.attack_speed", new ArrayList<>() );
					if(!t.isEmpty())
						attack_speed=t.get( 0 ).toString();
					t= itemInput.getModifiers().getOrDefault( "generic.durability", new ArrayList<>() );
					if(!t.isEmpty())
						durability=t.get( 0 ).toString();
				}catch(Exception ignored){
					//ignored.printStackTrace();
					System.out.println("PROBLEM WITH "+i.getName().getString() );
				}
				System.out.println( swordItem.kjs$getId().split(":")[0]+","+
					swordItem.kjs$getId()+","+
					swordItem.getEnchantability()+","+
					swordItem.getMaterial().toString()+","+
					swordItem.getAttackDamage()+","+damage+","+
					df.format(swordItem.getAttributeModifiers( EquipmentSlot.MAINHAND).get( EntityAttributes.GENERIC_ATTACK_SPEED ).stream().toList().get( 0 ).getValue())+","+attack_speed+","+
					swordItem.getMaxDamage()+","+durability );*/
				System.out.println( swordItem.kjs$getId().split( ":" )[ 0 ] + "," +
					swordItem.kjs$getId() + "," +
					swordItem.getAttackDamage() + "," +
					df.format( swordItem.getAttributeModifiers( EquipmentSlot.MAINHAND ).get( EntityAttributes.GENERIC_ATTACK_SPEED ).stream().toList().get( 0 ).getValue() ) + "," +
					swordItem.getMaxDamage() );
			} catch ( Exception e ) {
				System.out.println( i.getName() + " with no hunger" );
			}
		}
		return 1;
	}

	private static int output3( CommandContext< ServerCommandSource > serverCommandSourceCommandContext ) {
		List< Item > items = Registry.ITEM.stream().filter( item -> item instanceof ToolItem || item instanceof MiningToolItem ).toList();
		System.out.println( "INIT TOOLS" );
		for ( Item i : items ) {
			try {
				MiningToolItem toolItem = (MiningToolItem) i;
				/*String attack_speed="";
				String damage="";
				String durability="";
				try{
					ItemInput itemInput = Config.getWeapon().weapon.get( i.kjs$getId() );
					ArrayList< Object > t= itemInput.getModifiers().getOrDefault( "generic.attack_damage", new ArrayList<>() );
					if(!t.isEmpty())
						damage=t.get( 0 ).toString();
					t= itemInput.getModifiers().getOrDefault( "generic.attack_speed", new ArrayList<>() );
					if(!t.isEmpty())
						attack_speed=t.get( 0 ).toString();
					t= itemInput.getModifiers().getOrDefault( "generic.durability", new ArrayList<>() );
					if(!t.isEmpty())
						durability=t.get( 0 ).toString();
				}catch(Exception ignored){
					//ignored.printStackTrace();
					System.out.println("PROBLEM WITH "+i.getName().getString() );
				}*/
				System.out.println( toolItem.kjs$getId().split( ":" )[ 0 ] + "," +
					toolItem.kjs$getId() + "," +
					toolItem.getEnchantability() + "," +
					toolItem.getMaterial().toString() + "," +
					toolItem.getAttackDamage() + "," +
					toolItem.getMaxDamage() );
			} catch ( Exception e ) {
				e.printStackTrace();
				System.out.println( i.getName() + " error" );
			}
		}
		return 1;
	}

	private static int output( CommandContext< ServerCommandSource > serverCommandSourceCommandContext ) {
		List< Item > items = Registry.ITEM.stream().toList();
		System.out.println( "Armor" );
		try {
			items.forEach( new Consumer< Item >() {
				@Override
				public void accept( Item i ) {
					if(i.kjs$getId().contains( "_concavehalberd" )){
						Elysium.LOGGER.info( i.kjs$getMod() + "," + i.kjs$getId() + "," + i.getMaxDamage() );
						Multimap< EntityAttribute, EntityAttributeModifier > map = i.getAttributeModifiers( EquipmentSlot.MAINHAND );
						try {
							map.forEach( new BiConsumer< EntityAttribute, EntityAttributeModifier >() {
								@Override
								public void accept( EntityAttribute entityAttribute, EntityAttributeModifier entityAttributeModifier ) {
									Elysium.LOGGER.info( "," + entityAttribute.getTranslationKey() + ":" + entityAttributeModifier.getValue() + "," + entityAttributeModifier.getOperation() );
								}
							} );

						} catch ( Exception ignored ) {

						}
					}
					/*if ( i instanceof ArmorItem armorItem ) {
						if(i.kjs$getId().contains("paladins") || i.kjs$getId().contains("wizards")) {
							Elysium.LOGGER.info( armorItem.kjs$getMod() + "," + armorItem.kjs$getId() + "," + armorItem.getMaxDamage() );
							Multimap< EntityAttribute, EntityAttributeModifier > map = i.getAttributeModifiers( armorItem.getSlotType() );
							try {
								map.forEach( new BiConsumer< EntityAttribute, EntityAttributeModifier >() {
									@Override
									public void accept( EntityAttribute entityAttribute, EntityAttributeModifier entityAttributeModifier ) {
										Elysium.LOGGER.info( "," + entityAttribute.getTranslationKey() + ":" + entityAttributeModifier.getValue() + "," + entityAttributeModifier.getOperation() );
									}
								} );

							} catch ( Exception ignored ) {

							}
						}

					}*/

				}
			} );
				/*}else if( i instanceof ToolItem toolItem ){
					//Elysium.LOGGER.info(toolItem.kjs$getMod()+","+toolItem.kjs$getId()+","+toolItem.getMaterial().getRepairIngredient().kjs$getFirst().getItem().kjs$getId());
				}else if( i instanceof Models.ShieldEnum rangedWeaponItem){

				}*/

				/*if(i instanceof MutableMultiMap mutableMultiMap){
					try{
						Elysium.LOGGER.info(i.kjs$getMod()+","+i.kjs$getId()+" "+mutableMultiMap.getArmorMaterial().getRepairIngredient().kjs$getFirst().getItem().kjs$getId());
					}catch(Exception ignored){
					}
					try{
						Elysium.LOGGER.info(i.kjs$getMod()+","+i.kjs$getId()+" "+mutableMultiMap.getToolMaterial().getRepairIngredient().kjs$getFirst().getItem().kjs$getId());
					}catch(Exception ignored){
					}
				}*/
				/*List< Item > staff = Registry.ITEM.stream().filter( item -> item instanceof StaffItem ).toList();
				staff.forEach( new Consumer< Item >() {
					@Override
					public void accept( Item i ) {
						StaffItem staffItem= (StaffItem) i;
						Multimap< EntityAttribute, EntityAttributeModifier > map= staffItem.getAttributeModifiers( EquipmentSlot.MAINHAND );
						try {
							Elysium.LOGGER.info(((Item) i).kjs$getId().split(":")[0]+","+
									((Item) i).kjs$getId()+","+((Item) i).getMaxDamage());
							map.forEach( new BiConsumer< EntityAttribute, EntityAttributeModifier >() {
								@Override
								public void accept( EntityAttribute entityAttribute, EntityAttributeModifier entityAttributeModifier ) {
									Elysium.LOGGER.info(","+entityAttribute.getTranslationKey()+":"+entityAttributeModifier.getValue()+","+entityAttributeModifier.getOperation());
								}
							} );
						}catch(Exception ignored){

						}
					}
				} );*/
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		return 1;
	}

	private static int configReload( CommandContext< ServerCommandSource > ctx ) {
		Config.loadFromDisk( true );

		var players = ctx.getSource().getServer().getPlayerManager().getPlayerList();
		Elysium.LOGGER.info( "[Elysium] Sending config update to {} players", players.size() );
		for ( var player : players ) {
			ServerPlayNetworking.send( player, CONFIG_SYNC_ID, Config.getPacketBufData() );
		}

		ctx.getSource().sendFeedback( Text.literal( "Config reloaded!" ), false );
		return 1;
	}

	private static int configSpawnSet( CommandContext< ServerCommandSource > ctx ) throws CommandSyntaxException {
		var pos = BlockPosArgumentType.getBlockPos( ctx, "pos" );
		var old = Config.getData().claims.spawnPosition;
		var message = Text.literal( "Spawn has been set to " ).append( position( pos ) );
		if ( old != null ) message.append( Text.literal( ", was " ) ).append( position( old ) );

		Config.getData().claims.spawnPosition = pos;
		Config.saveData();

		ctx.getSource().sendFeedback( message, false );

		return 1;
	}

	private static int configSpawn( CommandContext< ServerCommandSource > ctx ) {
		var pos = Config.getData().claims.spawnPosition;

		var message = pos == null ? Text.literal( "Spawn is not set" ) : Text.literal( "Spawn is at " ).append( position( pos ) );

		ctx.getSource().sendFeedback( message, false );

		return 1;
	}

	private static int toggleNoCommandFeedback( CommandContext< ServerCommandSource > ctx ) throws CommandSyntaxException {
		var comp = ctx.getSource().getPlayerOrThrow().getComponent( CompRegistry.ELYSIUM_PLAYER );

		comp.setNoCommandFeedback( !comp.getNoCommandFeedback() );

		return 1;
	}

	private static int claimGetTimePlayer( CommandContext< ServerCommandSource > ctx ) throws CommandSyntaxException {
		try {
			UUID player = EntityArgumentType.getPlayer( ctx, "owner" ).getUuid();
			assert Elysium.server != null : "y nu sevr";
			var stamp = CompRegistry.ELYSIUM_LEVEL.get( Elysium.server.getSaveProperties() ).getAnchorStamp( player );

			ctx.getSource().sendFeedback( Text.literal( ctx.getArgument( "owner", String.class ) + "'s last saved time is " + Instant.ofEpochMilli( stamp ).atZone( ZoneId.of( "ECT" ) ) ), false );
		} catch ( Exception e ) {
			ctx.getSource().sendFeedback( Text.literal( "Error while processing request" ), false );
			e.printStackTrace();
		}
		return 0;
	}

	private static int claimGetPosPlayer( CommandContext< ServerCommandSource > ctx ) {
		assert Elysium.server != null;
		PlayerManager playerManager = Elysium.server.getPlayerManager();
		ServerPlayerEntity serverPlayerEntity = playerManager.getPlayer( StringArgumentType.getString( ctx, "owner" ) );
		if ( serverPlayerEntity != null ) {
			Selection< Entry< ClaimBox, Claim > > claims = ClaimUtils.getClaimsOwnedBy( Elysium.server.getOverworld(), serverPlayerEntity.getUuid() );
			var timeRemaining = ( Math.abs( ( CompRegistry.ELYSIUM_LEVEL.get( Elysium.server.getSaveProperties() ).getAnchorStamp( serverPlayerEntity.getUuid() ) - Elysium.server.getWorld( ServerWorld.OVERWORLD ).getTime() ) ) / 20 ) * 1000;
			var time = System.currentTimeMillis() + timeRemaining;
			Date d = new Date( time );
			DateFormat formatter = new SimpleDateFormat( "dd/MM/yyyy HH:mm:ss" );
			formatter.setTimeZone( TimeZone.getTimeZone( "CET" ) );
			String dateFormatted = formatter.format( d );
			var stamp = CompRegistry.ELYSIUM_LEVEL.get( Elysium.server.getSaveProperties() ).getAnchorStamp( serverPlayerEntity.getUuid() );
			AtomicBoolean expired = new AtomicBoolean( false );
			claims.forEach( claimBoxClaimEntry -> {
				ClaimAnchorBlockEntity claimAnchorBlockEntity = ClaimUtils.getAnchor( Elysium.server.getOverworld(), claimBoxClaimEntry.getValue() );
				assert claimAnchorBlockEntity != null;
				if ( !claimAnchorBlockEntity.getPos().equals( new BlockPos( 0, -100, 0 ) ) ) {
					if ( claimBoxClaimEntry.getValue().getOwners().contains( serverPlayerEntity.getUuid() ) )
						if ( stamp == 0 ) {
							ctx.getSource().sendFeedback( Text.literal( StringArgumentType.getString( ctx, "owner" ) ).append( " claim at " ).append( position( claimAnchorBlockEntity.getPos() ) ).append( ". Claim not activated" ), false );
						} else {
							ctx.getSource().sendFeedback( Text.literal( StringArgumentType.getString( ctx, "owner" ) ).append( " claim at " ).append( position( claimAnchorBlockEntity.getPos() ) ).append( ". Time end: " + dateFormatted ), false );
						}
				} else {
					expired.set( true );
				}
			} );
			if ( expired.get() ) {
				GetOffMyLawn.CLAIM.get( Elysium.server.getOverworld() ).getClaims().entries().forEach( claimEntry -> {
					ClaimAnchorBlockEntity claimAnchorBlockEntity = ClaimUtils.getAnchor( Elysium.server.getOverworld(), claimEntry.getValue() );
					assert claimAnchorBlockEntity != null;
					if ( ( (ElysiumClaimBlockEntity) claimAnchorBlockEntity ).elysium$getOld().equals( serverPlayerEntity.getUuid() ) ) {
						ctx.getSource().sendFeedback( Text.literal( StringArgumentType.getString( ctx, "owner" ) ).append( " claim at " ).append( position( claimAnchorBlockEntity.getPos() ) ).append( ". Claim expired, drops: " + dateFormatted ), false );
					}
				} );
			}
		} else {
			ctx.getSource().sendFeedback( Text.literal( "Player not found" ), false );
		}
		return 0;
	}

	private static int balanceImperiumDisplay( CommandContext< ServerCommandSource > ctx ) {
		var comp = getImperium( ctx );
		var bronze = comp.getBalance();

		var gold = bronze / 10000L;
		bronze = bronze % 10000L;

		var silver = bronze / 100L;
		bronze = bronze % 100L;

		var text = Text.translatable( "command.elysium.balance.display", "Imperium's", gold, silver, bronze, comp.getBalance() );
		ctx.getSource().sendFeedback( text, false );

		return 1;
	}

	private static int balanceImperiumAdd( CommandContext< ServerCommandSource > ctx ) {
		var amount = IntegerArgumentType.getInteger( ctx, "amount" );

		var comp = getImperium( ctx );
		var balance = comp.addBalance( amount );

		var text = Text.translatable( "command.elysium.balance.add", amount, balance );
		ctx.getSource().sendFeedback( text, false );

		return 1;
	}

	private static int balanceImperiumRemove( CommandContext< ServerCommandSource > ctx ) {
		var amount = IntegerArgumentType.getInteger( ctx, "amount" );

		var comp = getImperium( ctx );
		var balance = comp.addBalance( -amount );

		var text = Text.translatable( "command.elysium.balance.remove", amount, balance );
		ctx.getSource().sendFeedback( text, false );

		return 1;
	}

	private static @NotNull ImperiumLevelComponent getImperium( CommandContext< ServerCommandSource > ctx ) {
		return CompRegistry.IMPERIUM_LEVEL.get( ctx.getSource().getServer().getSaveProperties() );
	}

	private static int untrustall( CommandContext< ServerCommandSource > ctx ) {
		GetOffMyLawn.CLAIM.get( Objects.requireNonNull( ctx.getSource().getServer().getWorld( ServerWorld.OVERWORLD ) ) ).getClaims().entries().forEach( claimBoxClaimEntry -> {
			Elysium.LOGGER.info( claimBoxClaimEntry.getValue().getTrusted().toString() );
			claimBoxClaimEntry.getValue().getTrusted().clear();
		} );
		return 1;
	}

	public static LiteralArgumentBuilder< ServerCommandSource> status() {
		return literal( "status" ).then( argument( "new_status",StringArgumentType.greedyString() ).executes( ElysiumCommand::statusCommand ) );
	}
	public static LiteralArgumentBuilder< ServerCommandSource> removeStatus() {
		return literal( "statusdel" ).executes( ElysiumCommand::delStatus ) ;
	}

	private static int delStatus( CommandContext< ServerCommandSource> serverCommandSourceCommandContext ) {
		try {
			CompRegistry.ELYSIUM_LEVEL.get(Elysium.server.getSaveProperties()).setPlayerStatus( serverCommandSourceCommandContext.getSource().getPlayer().getUuid(),"" );
			serverCommandSourceCommandContext.getSource().getPlayer().sendMessage( Text.literal("Status deleted") );
		}catch(Exception ignored){
			return 2;
		}

		return 1;
	}

	private static int statusCommand( CommandContext< ServerCommandSource> serverCommandSourceCommandContext ) {
		try {
			CompRegistry.ELYSIUM_LEVEL.get(Elysium.server.getSaveProperties()).setPlayerStatus( serverCommandSourceCommandContext.getSource().getPlayer().getUuid(),StringArgumentType.getString(serverCommandSourceCommandContext,"new_status") );
			serverCommandSourceCommandContext.getSource().getPlayer().sendMessage( Text.literal("Status saved") );
		}catch(Exception ignored){
			return 2;
		}

		return 1;
	}

	public static LiteralArgumentBuilder< ServerCommandSource> delNickname() {
		return literal( "delNickname" ).requires( it -> Permissions.check( it, "elysium.elysium", 2 ) ).executes( ElysiumCommand::delNick )
			.then( argument( "player", StringArgumentType.string() ).executes( ElysiumCommand::delCustomNick ));
	}

	private static int delCustomNick( CommandContext< ServerCommandSource> serverCommandSourceCommandContext ) {
		try {
			if(StateSaverAndLoader.playerNames.containsValue(StringArgumentType.getString( serverCommandSourceCommandContext,"player" ) )) {
				StateSaverAndLoader.playerNames.entrySet().forEach( new Consumer< Map.Entry< UUID, String > >() {
					@Override
					public void accept( Map.Entry< UUID, String > uuidStringEntry ) {
						if(uuidStringEntry.getValue().equalsIgnoreCase( StringArgumentType.getString( serverCommandSourceCommandContext,"player" )  )){
							StateSaverAndLoader.playerNames.remove( uuidStringEntry.getKey() );
						}
					}
				} );
				StateSaverAndLoader.playerNames.remove( serverCommandSourceCommandContext.getSource().getPlayer().getUuid() );
				NoBadPlayers.stateSaverAndLoader.markDirty();
			}
		}catch(Exception ignored ){
		}
		return 1;
	}

	private static int delNick( CommandContext< ServerCommandSource> serverCommandSourceCommandContext ) {
		try {
			StateSaverAndLoader.playerNames.remove( serverCommandSourceCommandContext.getSource().getPlayer().getUuid() );
			NoBadPlayers.stateSaverAndLoader.markDirty();
		}catch(Exception ignored ){
		}
		return 1;
	}
}

package com.enderzombi102.elysium.command;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import me.lucko.fabric.api.permissions.v0.Permissions;
import net.minecraft.command.argument.DimensionArgumentType;
import net.minecraft.command.argument.Vec3ArgumentType;
import net.minecraft.server.command.ServerCommandSource;

import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.server.command.CommandManager.literal;

public class TpxCommand {
	public static LiteralArgumentBuilder< ServerCommandSource > command() {
		return literal( "tpx" )
			.requires( it -> Permissions.check( it, "elysium.tpx", 2 ) )
			.then( argument( "dimension", DimensionArgumentType.dimension() )
				.then( argument( "pos", Vec3ArgumentType.vec3() )
					.executes( TpxCommand::tpx )
				)
				.executes( TpxCommand::tpx )
			);
	}

	private static int tpx( CommandContext< ServerCommandSource > ctx ) throws CommandSyntaxException {
		var destination = DimensionArgumentType.getDimensionArgument( ctx, "dimension" );

		var pos = destination.getSpawnPos();
		try {
			pos = Vec3ArgumentType.getPosArgument( ctx, "pos" ).toAbsoluteBlockPos( ctx.getSource() );
		} catch ( IllegalArgumentException e ) {
			if ( !e.getMessage().startsWith( "No" ) )
				throw e;
		}

		var player = ctx.getSource().getPlayerOrThrow();
		player.teleport( destination, pos.getX(), pos.getY(), pos.getZ(), player.headYaw, player.getPitch() );

		return 1;
	}
}

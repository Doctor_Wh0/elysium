package com.enderzombi102.elysium.component;

import com.enderzombi102.elysium.component.interfaces.ElysiumLevelComponent;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.world.WorldProperties;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ElysiumLevelComponentImpl implements ElysiumLevelComponent {
	private final Map< UUID, Long > playerTimes = new HashMap<>();
	private final Map< UUID, Long > anchorStamps = new HashMap<>();
	private final Map< UUID, String > timeMethods = new HashMap<>();
	private final Map< UUID, String > status = new HashMap<>();

	public ElysiumLevelComponentImpl( WorldProperties props ) {
	}

	@Override
	public long getLastPlayerTime( @NotNull UUID player ) {
		return this.playerTimes.getOrDefault( player, 0L );
	}

	@Override
	public void setLastPlayerTime( @NotNull UUID player, long time ) {
		this.playerTimes.put( player, time );
	}

	@Override
	public String getPlayerStatus( @NotNull UUID player ) {
		return this.status.getOrDefault( player, "");
	}

	@Override
	public void setPlayerStatus( @NotNull UUID player, String time ) {
		this.status.put( player, time );
	}


	@Override
	public long getAnchorStamp( @NotNull UUID owner ) {
		return this.anchorStamps.getOrDefault( owner, 0L );
	}

	@Override
	public void setAnchorStamp( @NotNull UUID owner, long stamp ) {
		this.anchorStamps.put( owner, stamp );
	}

	@Override
	public String getTimeMethod( @NotNull UUID owner ) {
		return this.timeMethods.getOrDefault( owner, null );
	}

	@Override
	public void setTimeMethod( @NotNull UUID owner, String what ) {
		this.timeMethods.put( owner, what );
	}


	@Override
	public void readFromNbt( NbtCompound nbt ) {
		this.playerTimes.clear();
		this.anchorStamps.clear();
		this.timeMethods.clear();
		this.status.clear();
		var map = nbt.getCompound( "playerTimes" );
		for ( var key : map.getKeys() )
			this.playerTimes.put( UUID.fromString( key ), map.getLong( key ) );

		map = nbt.getCompound( "anchorStamps" );
		for ( var key : map.getKeys() )
			this.anchorStamps.put( UUID.fromString( key ), map.getLong( key ) );

		map = nbt.getCompound( "status" );
		for ( var key : map.getKeys() )
			this.status.put( UUID.fromString( key ), map.getString( key ) );

		map = nbt.getCompound( "timeMethods" );
		for ( var key : map.getKeys() )
			this.timeMethods.put( UUID.fromString( key ), map.getString( key ) );
	}

	@Override
	public void writeToNbt( NbtCompound nbt ) {
		var map = new NbtCompound();
		for ( var entry : this.playerTimes.entrySet() )
			map.putLong( entry.getKey().toString(), entry.getValue() );

		nbt.put( "playerTimes", map );

		map = new NbtCompound();
		for ( var entry : this.anchorStamps.entrySet() )
			map.putLong( entry.getKey().toString(), entry.getValue() );

		nbt.put( "anchorStamps", map );

		map = new NbtCompound();
		for ( var entry : this.status.entrySet() )
			map.putString( entry.getKey().toString(), entry.getValue() );

		nbt.put( "status", map );

		map = new NbtCompound();
		for ( var entry : this.timeMethods.entrySet() )
			map.putString( entry.getKey().toString(), entry.getValue() );

		nbt.put( "timeMethods", map );
	}
}

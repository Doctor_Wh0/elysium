package com.enderzombi102.elysium.component;

import com.enderzombi102.elysium.component.interfaces.ElysiumPlayerComponent;
import com.enderzombi102.elysium.registry.CompRegistry;
import dev.onyxstudios.cca.api.v3.component.sync.AutoSyncedComponent;
import draylar.goml.api.ClaimUtils;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.Box;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.stream.Collectors;

public class ElysiumPlayerComponentImpl implements ElysiumPlayerComponent, AutoSyncedComponent {
	private final PlayerEntity player;
	private boolean noCommandFeedback = false;
	private @Nullable Box currentClaim;

	public ElysiumPlayerComponentImpl( PlayerEntity player ) {
		this.player = player;
	}

	@Override
	public boolean getNoCommandFeedback() {
		return this.noCommandFeedback;
	}

	@Override
	public void setNoCommandFeedback( boolean value ) {
		this.noCommandFeedback = value;
	}

	@Override
	public @Nullable Box getClosestClaimBox() {
		return this.currentClaim;
	}

	@Override
	public void serverTick() {
		var pos = this.player.getBlockPos();
		if ( this.currentClaim == null || !this.currentClaim.contains( pos.getX(), pos.getY(), pos.getZ() ) ) {
			if ( this.player.age % 40 == 1 ) // only run every 40 ticks ( 2s )
				return;

			var newer = ClaimUtils.getClaimsInBox(
					this.player.getWorld(),
					com.jamieswhiteshirt.rtree3i.Box.create(
						pos.getX() - 10,
						pos.getY() - 10,
						pos.getZ() - 10,
						pos.getX() + 10,
						pos.getY() + 10,
						pos.getZ() + 10
					)
				)
				.collect( Collectors.toList() )
				.stream()
				.findFirst()
				.map( it -> it.getKey().minecraftBox() )
				.orElse( null );

			if ( !Objects.equals( this.currentClaim, newer ) ) {
				this.currentClaim = newer;
				CompRegistry.ELYSIUM_PLAYER.sync( this.player );
			}
		}
	}

	@Override
	public void readFromNbt( NbtCompound nbt ) {
		this.noCommandFeedback = nbt.getBoolean( "noCommandFeedback" );
	}

	@Override
	public void writeToNbt( NbtCompound nbt ) {
		nbt.putBoolean( "noCommandFeedback", this.noCommandFeedback );
	}

	@Override
	public void writeSyncPacket( PacketByteBuf buf, ServerPlayerEntity recipient ) {
		if ( this.currentClaim == null )
			buf.writeBoolean( false );
		else {
			buf.writeBoolean( true );
			buf.writeIntArray(
				new int[] {
					(int) this.currentClaim.minX,
					(int) this.currentClaim.minY,
					(int) this.currentClaim.minZ,
					(int) this.currentClaim.maxX,
					(int) this.currentClaim.maxY,
					(int) this.currentClaim.maxZ
				}
			);
		}
	}

	@Override
	public void applySyncPacket( PacketByteBuf buf ) {
		if ( buf.readBoolean() ) {
			var arr = buf.readIntArray();
			this.currentClaim = new Box(
				arr[ 0 ],
				arr[ 1 ],
				arr[ 2 ],
				arr[ 3 ],
				arr[ 4 ],
				arr[ 5 ]
			);
		} else
			this.currentClaim = null;
	}

	@Override
	public boolean shouldSyncWith( ServerPlayerEntity player ) {
		return player == this.player;
	}
}

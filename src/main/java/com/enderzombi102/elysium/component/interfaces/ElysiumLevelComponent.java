package com.enderzombi102.elysium.component.interfaces;

import dev.onyxstudios.cca.api.v3.component.ComponentV3;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public interface ElysiumLevelComponent extends ComponentV3 {
	// Player time
	long getLastPlayerTime( @NotNull UUID player );

	void setLastPlayerTime( @NotNull UUID player, long amount );

	// Claim anchor timestamp backups
	long getAnchorStamp( @NotNull UUID owner );

	void setAnchorStamp( @NotNull UUID owner, long stamp );

	// TimeMethod
	String getTimeMethod( @NotNull UUID owner );

	void setTimeMethod( @NotNull UUID owner, String what );

	// Status
	String getPlayerStatus( @NotNull UUID player );

	void setPlayerStatus( @NotNull UUID player, String time );

}

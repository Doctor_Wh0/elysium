package com.enderzombi102.elysium.component.interfaces;

import dev.onyxstudios.cca.api.v3.component.tick.ServerTickingComponent;
import dev.onyxstudios.cca.api.v3.entity.PlayerComponent;
import net.minecraft.util.math.Box;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings( "UnstableApiUsage" )
public interface ElysiumPlayerComponent extends PlayerComponent< ElysiumPlayerComponent >, ServerTickingComponent {
	boolean getNoCommandFeedback();

	void setNoCommandFeedback( boolean value );

	@Nullable Box getClosestClaimBox();
}

package com.enderzombi102.elysium.config;

import blue.endless.jankson.Comment;
import com.enderzombi102.elysium.data.ItemInput;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

import static com.enderzombi102.elysium.Elysium.LOGGER;

public class ArmorData {
	@Comment( "Enable the registration of the items with the values modified" )
	public @NotNull Boolean enable = true;

	@Comment( """
		An object of `<id_armor> - <>` pairs, used to change the modifiers of armor.
		Example:
		"minecraft:iron_chestplate": {   // id of item
					modifiers: {
						"generic.armor": [  // id of attribute 
							"6",			// value
							"0",			// OPERATION  0 -> Addition, 1 -> Multiply base, 2 -> Multiply total
						],
					},
				},
		""" )
	public @NotNull HashMap< String, ItemInput > armor = new HashMap<>();

	public void fill() {
		ArrayList< String > modifiers = new ArrayList<>(Arrays.asList(
			"generic.armor",
			"generic.durability"
		));
		try ( BufferedReader reader = new BufferedReader( new InputStreamReader( Objects.requireNonNull( getClass().getClassLoader().getResourceAsStream( "new_armor.csv" ) ) ) ) ) {
			String line;
			while ( ( line = reader.readLine() ) != null ) {
				int i = 0;
				String[] values = line.split( "," );
				String id = null;
				HashMap< String, ArrayList< Object > > hashMap = new HashMap<>();
				for ( String value : values ) {
					value = value.replace( "\"", "" );
					if ( id == null ) {
						id = value;
					} else {
						if ( value.length() > 0 && modifiers.get( i ).length()>0) {
							if ( value.contains( ";" ) ) {
								String[] v2=value.split(";");
								hashMap.put( modifiers.get( i ), new ArrayList<>( Arrays.asList( v2[0], v2[1] ) ) );
							}
						}
						i = i + 1;
					}

				}
				this.armor.put( id, new ItemInput( hashMap ) );
			}
		} catch ( IOException e ) {
			LOGGER.error("[Elysium] Error loading armor data file");
			e.printStackTrace();
		}
	}

}

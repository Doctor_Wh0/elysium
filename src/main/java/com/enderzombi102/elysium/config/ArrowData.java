package com.enderzombi102.elysium.config;

import blue.endless.jankson.Comment;
import com.enderzombi102.elysium.Elysium;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.function.BiConsumer;

import static com.enderzombi102.elysium.Elysium.LOGGER;

public class ArrowData {
	@Comment( "Enable the registration of the items with the values modified" )
	public @NotNull Boolean enable = true;

	@Comment( """
		An object of `<id_ranged> - <>` pairs, used to change the modifiers of ranged bow or arrow.
		Example:
		"minecraft:bow": {   // id of item
					modifiers: {
						"generic.durability": [  // id of attribute 
							"6",			// value
						],
					},
				},
		""" )
	public @NotNull HashMap< String, Float > arrow = new HashMap<>();

	public void fill() {
		try ( BufferedReader reader = new BufferedReader( new InputStreamReader( Objects.requireNonNull( getClass().getClassLoader().getResourceAsStream( "arrow.csv" ) ) ) ) ) {
			String line;
			while ( ( line = reader.readLine() ) != null ) {
				String[] values = line.split( "," );
				this.arrow.put( values[0], Float.parseFloat( values[1] ) );
			}
		} catch ( IOException e ) {
			LOGGER.error("[Elysium] Error loading arrow data file");
			e.printStackTrace();
		}
	}
}

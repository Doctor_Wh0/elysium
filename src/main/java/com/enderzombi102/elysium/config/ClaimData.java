package com.enderzombi102.elysium.config;

import blue.endless.jankson.Comment;
import net.minecraft.util.math.BlockPos;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ClaimData {
	@Comment( "The center block position of the spawn" )
	public @Nullable BlockPos spawnPosition = null;

	@Comment( "The distance between claims" )
	public int distanceRadiusBetween = 124;

	@Comment( "The y distance between claims borders, this option depends on dreamingGomlFixes" )
	public int yDistanceBetweenBorders = 100;

	@Comment( "The xz distance between claims borders, this option depends on dreamingGomlFixes" )
	public int xzDistanceBetweenBorders = 9;

	@Comment( "Tax-related configurations" )
	public @NotNull TaxesData taxes = new TaxesData();

	@Comment("After how many days the admin claim drops")
	public int daysAfterAdminClaim = 15;

	@Comment("Duration of carpenter contract")
	public int carpenterContractDays = 5;

}

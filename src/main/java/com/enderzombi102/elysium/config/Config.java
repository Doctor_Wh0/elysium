package com.enderzombi102.elysium.config;

import blue.endless.jankson.*;
import blue.endless.jankson.api.DeserializationException;
import blue.endless.jankson.api.SyntaxError;
import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.data.ItemInput;
import com.enderzombi102.elysium.util.Const;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.jamieswhiteshirt.rtree3i.Entry;
import me.shedaniel.rei.api.client.gui.widgets.Arrow;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.math.BlockPos;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.function.BiConsumer;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.elysium.util.Const.*;

// TODO: Heavy refactor -> it can be optimized (now is very redundant)
public class Config {
	private static final Jankson CONFIG_BUILDER = Jankson.builder()
		.registerDeserializer(
			String.class, BlockPos.class,
			( it, marshaller ) -> {
				var arr = it.split( ", " );
				assert arr.length == 3 : "Invalid BlockPos construct";
				return new BlockPos( Double.parseDouble( arr[ 0 ] ), Double.parseDouble( arr[ 1 ] ), Double.parseDouble( arr[ 2 ] ) );
			}
		)
		.registerSerializer( BlockPos.class, ( it, marshaller ) -> JsonPrimitive.of( it.toShortString() ) )
		.build();
	private static final Jankson ARMOR_BUILDER = Jankson.builder()
		.registerTypeAdapter( ArmorData.class, ( json ) -> {
			ArmorData armorData = new ArmorData();
			if ( json != null ) {
				JsonObject obj = json;
				if ( obj.containsKey( "enable" ) ) {
					armorData.enable = obj.getBoolean( "enable", true );
				}
				if ( obj.containsKey( "armor" ) ) {
					JsonObject armorObj = obj.getObject( "armor" );
					for ( String key : armorObj.keySet() ) {
						if ( armorObj.getObject( key ).containsKey( "modifiers" ) ) {
							JsonObject modifiers = armorObj.getObject( key ).getObject( "modifiers" );
							HashMap< String, ArrayList< Object > > m = new HashMap<>();
							for ( String modifier : modifiers.keySet() ) {
								ArrayList< Object > arrayList = new ArrayList<>();
								arrayList.addAll( List.of( modifiers.get( modifier ).toString().replaceAll( "\\[|\\]|\\s", "" ).replaceAll( "\"", "" ).split( "," ) ) );
								m.put( modifier, arrayList );
							}
							armorData.armor.put( key, new ItemInput( m ) );
						}
					}
				}
			}
			return armorData;
		} ).build();
	private static final Jankson WEAPON_BUILDER = Jankson.builder()
		.registerTypeAdapter( WeaponData.class, ( json ) -> {
			WeaponData weaponData = new WeaponData();
			if ( json != null ) {
				JsonObject obj = json;
				if ( obj.containsKey( "enable" ) ) {
					weaponData.enable = obj.getBoolean( "enable", true );
				}
				if ( obj.containsKey( "weapon" ) ) {
					JsonObject armorObj = obj.getObject( "weapon" );
					for ( String key : armorObj.keySet() ) {
						if ( armorObj.getObject( key ).containsKey( "modifiers" ) ) {
							JsonObject modifiers = armorObj.getObject( key ).getObject( "modifiers" );
							HashMap< String, ArrayList< Object > > m = new HashMap<>();
							for ( String modifier : modifiers.keySet() ) {
								ArrayList< Object > arrayList = new ArrayList<>();
								arrayList.addAll( List.of( modifiers.get( modifier ).toString().replaceAll( "\\[|\\]|\\s", "" ).replaceAll( "\"", "" ).split( "," ) ) );
								m.put( modifier, arrayList );
							}
							weaponData.weapon.put( key, new ItemInput( m ) );
						}
					}
				}
			}
			return weaponData;
		} ).build();
	private static final Jankson RANGED_BUILDER = Jankson.builder()
		.registerTypeAdapter( RangedData.class, ( json ) -> {
			RangedData rangedData = new RangedData();
			if ( json != null ) {
				JsonObject obj = json;
				if ( obj.containsKey( "enable" ) ) {
					rangedData.enable = obj.getBoolean( "enable", true );
				}
				if ( obj.containsKey( "ranged" ) ) {
					JsonObject rangedObj = obj.getObject( "ranged" );
					for ( String key : rangedObj.keySet() ) {
						if ( rangedObj.getObject( key ).containsKey( "modifiers" ) ) {
							JsonObject modifiers = rangedObj.getObject( key ).getObject( "modifiers" );
							HashMap< String, ArrayList< Object > > m = new HashMap<>();
							for ( String modifier : modifiers.keySet() ) {
								ArrayList< Object > arrayList = new ArrayList<>();
								arrayList.addAll( List.of( modifiers.get( modifier ).toString().replaceAll( "\\[|\\]|\\s", "" ).replaceAll( "\"", "" ).split( "," ) ) );
								m.put( modifier, arrayList );
							}
							rangedData.ranged.put( key, new ItemInput( m ) );
						}
					}
				}
			}
			return rangedData;
		} ).build();
	private static final Jankson ARROW_BUILDER = Jankson.builder()
		.registerTypeAdapter( ArrowData.class, ( json ) -> {
			ArrowData arrowData = new ArrowData();
			if ( json != null ) {
				JsonObject obj = json;
				if ( obj.containsKey( "enable" ) ) {
					arrowData.enable = obj.getBoolean( "enable", true );
				}
				if ( obj.containsKey( "ranged" ) ) {
					JsonObject rangedObj = obj.getObject( "ranged" );
					for ( String key : rangedObj.keySet() ) {
						if ( rangedObj.getObject( key ).containsKey( "modifiers" ) ) {
							JsonObject modifiers = rangedObj.getObject( key ).getObject( "modifiers" );
							arrowData.arrow.put( key, modifiers.getFloat( "generic.attack_damage", 2 ) );
						}
					}
				}
			}
			return arrowData;
		} ).build();
	private static final Jankson SHIELD_BUILDER = Jankson.builder()
		.registerTypeAdapter( ShieldData.class, ( json ) -> {
			ShieldData shieldData = new ShieldData();
			if ( json != null ) {
				JsonObject obj = json;
				if ( obj.containsKey( "enable" ) ) {
					shieldData.enable = obj.getBoolean( "enable", true );
				}
				if ( obj.containsKey( "shield" ) ) {
					JsonObject armorObj = obj.getObject( "shield" );
					assert armorObj != null;
					for ( String key : armorObj.keySet() ) {
						if ( Objects.requireNonNull( armorObj.getObject( key ) ).containsKey( "modifiers" ) ) {
							JsonObject modifiers = Objects.requireNonNull( armorObj.getObject( key ) ).getObject( "modifiers" );
							HashMap< String, ArrayList< Object > > m = new HashMap<>();
							assert modifiers != null;
							for ( String modifier : modifiers.keySet() ) {
								ArrayList< Object > arrayList = new ArrayList<>( List.of( Objects.requireNonNull( modifiers.get( modifier ) ).toString().replaceAll( "\\[|]|\\s", "" ).replaceAll( "\"", "" ).split( "," ) ) );
								m.put( modifier, arrayList );
							}
							shieldData.shield.put( key, new ItemInput( m ) );
						}
					}
				}
			}
			return shieldData;
		} ).build();

	private static final Jankson EFFECT_BUILDER = Jankson.builder()
		.registerTypeAdapter( EffectData.class, ( json ) -> {
			EffectData effectData = new EffectData();
			try{
			if ( json != null ) {
				effectData.arcane_spell_power_bonus = json.getFloat( "arcane_spell_power_bonus", -0.1F );
				effectData.critical_chance_power_bonus = json.getFloat( "critical_chance_power_bonus", 0.1F );
				effectData.critical_damage_power_bonus = json.getFloat( "critical_damage_power_bonus", 0.1F );
				effectData.fire_endurance_bonus = json.getFloat( "fire_endurance_bonus", -0.1F );
				effectData.fire_spell_power_bonus = json.getFloat( "fire_spell_power_bonus", 0.1F );
				effectData.frost_spell_power_bonus = json.getFloat( "frost_spell_power_bonus", 0.1F );
				effectData.haste_spell_power_bonus = json.getFloat( "haste_spell_power_bonus", 0.1F );
				effectData.holy_spell_power_bonus = json.getFloat( "holy_spell_power_bonus", 0.1F );
				effectData.impact_power_bonus = json.getFloat( "impact_power_bonus", 0.1F );
				effectData.strength_bonus = json.getFloat( "strength_bonus", 0.1F );
				effectData.weakness_malus = json.getFloat( "weakness_malus", -0.1F );
			}
			}catch(Exception ignored){}

			return effectData;
		} ).build();
	private static final Jankson INGREDIENT_BUILDER = Jankson.builder()
		.build();

	private static final JsonGrammar GRAMMAR = JsonGrammar.builder()
		.withComments( true )
		.printTrailingCommas( true )
		.bareSpecialNumerics( true )
		.printUnquotedKeys( true )
		.build();
	private static final File CONFIG_FILE = FabricLoader.getInstance().getConfigDir().resolve( "elysium-tweaks.json5" ).toFile();

	private static final File ARMOR_FILE = FabricLoader.getInstance().getConfigDir().resolve( "elysium-armor.json5" ).toFile();
	private static final File WEAPON_FILE = FabricLoader.getInstance().getConfigDir().resolve( "elysium-weapon.json5" ).toFile();
	private static final File RANGED_FILE = FabricLoader.getInstance().getConfigDir().resolve( "elysium-ranged.json5" ).toFile();
	private static final File ARROW_FILE = FabricLoader.getInstance().getConfigDir().resolve( "elysium-arrow.json5" ).toFile();
	private static final File SHIELD_FILE = FabricLoader.getInstance().getConfigDir().resolve( "elysium-shield.json5" ).toFile();
	private static final File EFFECT_FILE = FabricLoader.getInstance().getConfigDir().resolve( "elysium-effect.json5" ).toFile();
	private static final File INGREDIENT_FILE = FabricLoader.getInstance().getConfigDir().resolve( "elysium-ingredient.json5" ).toFile();
	private static final File HIDE_FILE = FabricLoader.getInstance().getConfigDir().resolve( "elysium-hide.json5" ).toFile();

	private static @Nullable ConfigData DATA = null;
	private static @Nullable ArmorData ARMOR = null;
	private static @Nullable WeaponData WEAPON = null;
	private static @Nullable ArrowData ARROW = null;
	private static @Nullable RangedData RANGED = null;
	private static @Nullable ShieldData SHIELD = null;
	private static @Nullable EffectData EFFECT = null;
	private static @Nullable CustomIngredientData INGREDIENT = null;
	private static @Nullable HideData HIDE = null;
	private static @Nullable ConfigFrom SOURCE = null;


	private Config() {
	}

	public static @NotNull ConfigData getData() {
		if ( DATA == null )
			loadFromDisk( false );
		return DATA;
	}

	public static @NotNull ArmorData getArmor() {
		if ( ARMOR == null )
			loadFromDisk( false );
		return ARMOR;
	}
	public static @NotNull RangedData getRanged() {
		if ( RANGED == null )
			loadFromDisk( false );
		return RANGED;
	}
	public static @NotNull ArrowData getArrow() {
		if ( ARROW == null )
			loadFromDisk( false );
		return ARROW;
	}

	public static @NotNull WeaponData getWeapon() {
		if ( WEAPON == null )
			loadFromDisk( false );
		return WEAPON;
	}

	public static @NotNull ShieldData getShield() {
		if ( SHIELD == null )
			loadFromDisk( false );
		return SHIELD;
	}

	public static @NotNull EffectData getEffect() {
		if ( EFFECT == null )
			loadFromDisk( false );
		return EFFECT;
	}
	public static @NotNull CustomIngredientData getCustomIngredient() {
		if ( INGREDIENT == null )
			loadFromDisk( false );
		return INGREDIENT;
	}
	public static @NotNull HideData getHideData() {
		if ( HIDE == null )
			loadFromDisk( false );
		return HIDE;
	}
	public static void saveData() {
		if ( SOURCE != ConfigFrom.File && Elysium.server == null ) {
			LOGGER.info( "[Elysium] Tried to save config while running in detached mode, this is a no-op!" );
			return;
		}

		try {
			LOGGER.info( "[Elysium] Saving config to disk..." );
			Files.writeString( CONFIG_FILE.toPath(), CONFIG_BUILDER.toJson( DATA ).toJson( GRAMMAR ) );
			var server = Elysium.server;
			if ( server != null && server.isDedicated() ) {
				LOGGER.info( "[Elysium] Config saved, sending to clients!" );
				for ( var player : server.getPlayerManager().getPlayerList() ) {
					ServerPlayNetworking.send( player, CONFIG_SYNC_ID, Config.getPacketBufData() );
				}
			} else
				LOGGER.info( "[Elysium] Config saved" );
		} catch ( IOException e ) {
			LOGGER.error( "[Elysium] Failed to save config to disk: ", e );
		}
	}

	public static void saveArmor() {
		if ( SOURCE != ConfigFrom.File && Elysium.server == null ) {
			LOGGER.info( "[Elysium] Tried to save config while running in detached mode, this is a no-op!" );
			return;
		}

		try {
			LOGGER.info( "[Elysium] Saving armor config to disk..." );
			Files.writeString( ARMOR_FILE.toPath(), ARMOR_BUILDER.toJson( ARMOR ).toJson( GRAMMAR ) );
		} catch ( IOException e ) {
			LOGGER.error( "[Elysium] Failed to save armor config to disk: ", e );
		}
	}

	public static void saveWeapon() {
		if ( SOURCE != ConfigFrom.File && Elysium.server == null ) {
			LOGGER.info( "[Elysium] Tried to save config while running in detached mode, this is a no-op!" );
			return;
		}

		try {
			LOGGER.info( "[Elysium] Saving weapon config to disk..." );
			Files.writeString( WEAPON_FILE.toPath(), WEAPON_BUILDER.toJson( WEAPON ).toJson( GRAMMAR ) );
		} catch ( IOException e ) {
			LOGGER.error( "[Elysium] Failed to save weapon config to disk: ", e );
		}
	}
	public static void saveRanged() {
		if ( SOURCE != ConfigFrom.File && Elysium.server == null ) {
			LOGGER.info( "[Elysium] Tried to save config while running in detached mode, this is a no-op!" );
			return;
		}

		try {
			LOGGER.info( "[Elysium] Saving ranged config to disk..." );
			Files.writeString( RANGED_FILE.toPath(), RANGED_BUILDER.toJson( RANGED ).toJson( GRAMMAR ) );
		} catch ( IOException e ) {
			LOGGER.error( "[Elysium] Failed to save ranged config to disk: ", e );
		}
	}
	public static void saveArrow() {
		if ( SOURCE != ConfigFrom.File && Elysium.server == null ) {
			LOGGER.info( "[Elysium] Tried to save config while running in detached mode, this is a no-op!" );
			return;
		}

		try {
			LOGGER.info( "[Elysium] Saving arrow config to disk..." );
			Files.writeString( ARROW_FILE.toPath(), ARROW_BUILDER.toJson( ARROW ).toJson( GRAMMAR ) );
		} catch ( IOException e ) {
			LOGGER.error( "[Elysium] Failed to save arrow config to disk: ", e );
		}
	}

	public static void saveShield() {
		if ( SOURCE != ConfigFrom.File && Elysium.server == null ) {
			LOGGER.info( "[Elysium] Tried to save config while running in detached mode, this is a no-op!" );
			return;
		}

		try {
			LOGGER.info( "[Elysium] Saving shield config to disk..." );
			Files.writeString( SHIELD_FILE.toPath(), SHIELD_BUILDER.toJson( SHIELD ).toJson( GRAMMAR ) );
		} catch ( IOException e ) {
			LOGGER.error( "[Elysium] Failed to save shield config to disk: ", e );
		}
	}

	public static void saveEffect() {
		if ( SOURCE != ConfigFrom.File && Elysium.server == null ) {
			LOGGER.info( "[Elysium] Tried to save config while running in detached mode, this is a no-op!" );
			return;
		}

		try {
			LOGGER.info( "[Elysium] Saving effect config to disk..." );
			Files.writeString( EFFECT_FILE.toPath(), EFFECT_BUILDER.toJson( EFFECT ).toJson( GRAMMAR ) );
		} catch ( IOException e ) {
			LOGGER.error( "[Elysium] Failed to save effect config to disk: ", e );
		}
	}
	public static void saveIngredient() {
		if ( SOURCE != ConfigFrom.File && Elysium.server == null ) {
			LOGGER.info( "[Elysium] Tried to save config while running in detached mode, this is a no-op!" );
			return;
		}

		try {
			LOGGER.info( "[Elysium] Saving ingredient config to disk..." );
			Files.writeString( INGREDIENT_FILE.toPath(), INGREDIENT_BUILDER.toJson( INGREDIENT ).toJson( GRAMMAR ) );
		} catch ( IOException e ) {
			LOGGER.error( "[Elysium] Failed to save ingredient config to disk: ", e );
		}
	}
	public static void saveHide() {
		if ( SOURCE != ConfigFrom.File && Elysium.server == null ) {
			LOGGER.info( "[Elysium] Tried to save config while running in detached mode, this is a no-op!" );
			return;
		}

		try {
			LOGGER.info( "[Elysium] Saving hide items config to disk..." );
			Files.writeString( HIDE_FILE.toPath(), INGREDIENT_BUILDER.toJson( HIDE ).toJson( GRAMMAR ) );
		} catch ( IOException e ) {
			LOGGER.error( "[Elysium] Failed to save hide items config to disk: ", e );
		}
	}

	public static void loadFromDisk( boolean reloading ) {
		if ( reloading )
			LOGGER.info( "[Elysium] Reloading config..." );
		else
			LOGGER.info( "[Elysium] Loading config..." );

		if ( !CONFIG_FILE.exists() ) {
			LOGGER.info( "[Elysium] Config does not exist, creating..." );
			DATA = new ConfigData();
			SOURCE = ConfigFrom.File;
			saveData();
		}
		if ( !ARMOR_FILE.exists() ) {
			LOGGER.info( "[Elysium] Armor config does not exist, creating..." );
			ARMOR = new ArmorData();
			ARMOR.fill();
			SOURCE = ConfigFrom.File;
			saveArmor();
		}
		if ( !WEAPON_FILE.exists() ) {
			LOGGER.info( "[Elysium] Weapon config does not exist, creating..." );
			WEAPON = new WeaponData();
			WEAPON.fill();
			SOURCE = ConfigFrom.File;
			saveWeapon();
		}
		if ( !RANGED_FILE.exists() ) {
			LOGGER.info( "[Elysium] Weapon config does not exist, creating..." );
			RANGED = new RangedData();
			RANGED.fill();
			SOURCE = ConfigFrom.File;
			saveRanged();
		}
		if ( !ARROW_FILE.exists() ) {
			LOGGER.info( "[Elysium] Weapon config does not exist, creating..." );
			ARROW = new ArrowData();
			ARROW.fill();
			SOURCE = ConfigFrom.File;
			saveArrow();
		}
		if ( !SHIELD_FILE.exists() ) {
			LOGGER.info( "[Elysium] Shield config does not exist, creating..." );
			SHIELD = new ShieldData();
			SHIELD.fill();
			SOURCE = ConfigFrom.File;
			saveShield();
		}
		if ( !EFFECT_FILE.exists() ) {
			LOGGER.info( "[Elysium] Shield config does not exist, creating..." );
			EFFECT = new EffectData();
			EFFECT.fill();
			SOURCE = ConfigFrom.File;
			saveEffect();
		}
		if ( !INGREDIENT_FILE.exists() ) {
			LOGGER.info( "[Elysium] Shield config does not exist, creating..." );
			INGREDIENT = new CustomIngredientData();
			SOURCE = ConfigFrom.File;
			saveIngredient();
		}
		if ( !HIDE_FILE.exists() ) {
			LOGGER.info( "[Elysium] Shield config does not exist, creating..." );
			HIDE = new HideData();
			SOURCE = ConfigFrom.File;
			saveHide();
		}
		try {
			DATA = CONFIG_BUILDER.fromJsonCarefully( CONFIG_BUILDER.load( CONFIG_FILE ), ConfigData.class );
			ARMOR = ARMOR_BUILDER.fromJsonCarefully( ARMOR_BUILDER.load( ARMOR_FILE ), ArmorData.class );
			WEAPON = WEAPON_BUILDER.fromJsonCarefully( WEAPON_BUILDER.load( WEAPON_FILE ), WeaponData.class );
			RANGED = RANGED_BUILDER.fromJsonCarefully( RANGED_BUILDER.load( RANGED_FILE ), RangedData.class );
			ARROW = ARROW_BUILDER.fromJsonCarefully( ARROW_BUILDER.load( ARROW_FILE ), ArrowData.class );
			SHIELD = SHIELD_BUILDER.fromJsonCarefully( SHIELD_BUILDER.load( SHIELD_FILE ), ShieldData.class );
			EFFECT = EFFECT_BUILDER.fromJsonCarefully( EFFECT_BUILDER.load( EFFECT_FILE ), EffectData.class );
			INGREDIENT = INGREDIENT_BUILDER.fromJsonCarefully( INGREDIENT_BUILDER.load( INGREDIENT_FILE ), CustomIngredientData.class );
			HIDE = INGREDIENT_BUILDER.fromJsonCarefully( INGREDIENT_BUILDER.load( HIDE_FILE ), HideData.class );
			SOURCE = ConfigFrom.File;
			if ( reloading )
				LOGGER.info( "[Elysium] Config reloaded" );
			else
				LOGGER.info( "[Elysium] Config loaded" );
		} catch ( SyntaxError | IOException | DeserializationException e ) {
			LOGGER.error( "[Elysium] Failed to load config: ", e );
		}
	}

	public static void loadFromPacketData( @NotNull String version, @NotNull String json ) {
		LOGGER.info( "[Elysium] Loading config received from server" );

		if ( !version.equals( Const.VERSION ) )
			LOGGER.warn( "[Elysium] Config received from server has a different mod version attached: %s (ours) vs %s (theirs)".formatted( Const.VERSION, version ) );

		try {
			DATA = CONFIG_BUILDER.fromJson( CONFIG_BUILDER.load( json ), ConfigData.class );
			SOURCE = ConfigFrom.Server;
			LOGGER.info( "[Elysium] Loaded config from server" );
		} catch ( SyntaxError e ) {
			throw new RuntimeException( e );
		}
	}
	public static @NotNull PacketByteBuf getPacketBufData() {
		return PacketByteBufs.create()
			.writeString( Const.VERSION )
			.writeString( CONFIG_BUILDER.toJson( DATA ).toJson( JsonGrammar.COMPACT ) );
	}


	private enum ConfigFrom {
		File,
		Server
	}
}

package com.enderzombi102.elysium.config;

import blue.endless.jankson.Comment;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class ConfigData {
	@Comment( "Additional (debug) logging switches" )
	public @NotNull LoggingData logging = new LoggingData();

	@Comment( "Feature switches, used to (dis|en)able entire features" )
	public @NotNull FeatureSwitchData features = new FeatureSwitchData();

	@Comment( "Claim settings" )
	public @NotNull ClaimData claims = new ClaimData();

	@Comment( """
		Settings for the experience blocker, each dimension may have its settings, or use the default one ( via the `elysium:default` key ).
		Example:
		```json5
		// the dimension's registry key, or `elysium:default`
		"elysium:default": {
		    // Blocks the dropping of xp in this dimension
		    enable: true,
		    // Allow furnaces to drop xp
		    allowFurnaceExp: true,
		    // Allow xp bottles to drop xp
		    allowBottledExp: true,
		    // Array of mob ids allowed to drop xp anyway,
		    // you can use `elysium:hostile` and `elysium:animal` to have a sort of "wildcard allow"
		    allowedMobSources: [
		        "elysium:hostile",
		        "elysium:animal",
		    ],
		},
		```
		""" )
	public @NotNull Map< String, ExperienceData > experienceSettings = new HashMap<>();

	@Comment( """
		An object of `<dimension> - <gamemode>` pairs, used to force a gamemode upon world change.
		The default gamemode is given from the `elysium:default` key-value.
		""" )
	public @NotNull Map< String, String > dimensionGamemodes = new HashMap<>();
	@Comment( "Configuration for the \"\"simple\"\" anti-cheat" )
	public @NotNull AntiCheatData antiCheat = new AntiCheatData();

	{
		this.dimensionGamemodes.putIfAbsent( "elysium:default", "survival" );
	}
}

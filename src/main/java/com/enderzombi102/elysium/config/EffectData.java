package com.enderzombi102.elysium.config;

import blue.endless.jankson.Comment;

public class EffectData {
	@Comment( "Fire Endurance")
	public float fire_endurance_bonus = -0.10f;
	@Comment( "Strength" )
	public float strength_bonus = 0.10f;
	@Comment( "Weakness" )
	public float weakness_malus = -0.10f;
	@Comment( "Fire Spell Power " )
	public float fire_spell_power_bonus = 0.10f;
	@Comment( "Frost Spell Power " )
	public float frost_spell_power_bonus = 0.10f;
	@Comment( "Arcane Spell Power" )
	public float arcane_spell_power_bonus = 0.10f;
	@Comment( "Holy Spell Power" )
	public float holy_spell_power_bonus = 0.10f;
	@Comment( "Impact" )
	public float impact_power_bonus = 0.10f;
	@Comment( "Critical Damage" )
	public float critical_damage_power_bonus = 0.10f;
	@Comment( "Critical Chance" )
	public float critical_chance_power_bonus = 0.10f;
	@Comment( "Haste Spell" )
	public float haste_spell_power_bonus = 0.10f;

	public void fill(){
		this.arcane_spell_power_bonus =  -0.1F;
		this.critical_chance_power_bonus = 0.1F;
		this.critical_damage_power_bonus = 0.1F;
		this.fire_endurance_bonus = -0.1F;
		this.fire_spell_power_bonus = 0.1F;
		this.frost_spell_power_bonus = 0.1F;
		this.haste_spell_power_bonus = 0.1F;
		this.holy_spell_power_bonus = 0.1F;
		this.impact_power_bonus = 0.1F;
		this.strength_bonus = 0.1F;
		this.weakness_malus = -0.1F;
	}

}

package com.enderzombi102.elysium.config;

import blue.endless.jankson.Comment;

import java.util.ArrayList;

public class HideData {
	public boolean enable = true;
	@Comment( "Specify as \"modid:id_item\":\"{Damage: 0}\"" )
	public ArrayList< String > items = new ArrayList<>();

	//public HashMap< String, String > items = new HashMap<>();
	{
		items.add( "soulsweapons:boss_compass{}" );
		items.add( "elysium:bronze_axe{}" );
		items.add( "mythicmobs:bronze_block{}" );
		items.add( "mythicmobs:bronze_block_cut{}" );
		items.add( "elysium:bronze_hoe{}" );
		items.add( "magistuarmory:bronze_ingot{}" );
		items.add( "mythicmobs:bronze_ingot{}" );
		items.add( "elysium:bronze_pickaxe{}" );
		items.add( "elysium:bronze_shovel{}" );
		items.add( "elysium:bronze_sword{}" );
		items.add( "minecraft:chest_minecart{}" );
		items.add( "minecraft:coal_ore{}" );
		items.add( "minecraft:deepslate_lapis_ore{}" );
		items.add( "minecraft:deepslate_iron_ore{}" );
		items.add( "minecraft:deepslate_gold_ore{}" );
		items.add( "minecraft:deepslate_emerald_ore{}" );
		items.add( "minecraft:deepslate_diamond_ore{}" );
		items.add( "minecraft:deepslate_copper_ore{}" );
		items.add( "minecraft:deepslate_coal_ore{}" );
		items.add( "graveyard:dark_iron_ingot{}" );
		items.add( "soulsweapons:crimson_ingot{}" );
		items.add( "minecraft:copper_ore{}" );
		items.add( "minecraft:comparator{}" );
		items.add( "minecraft:command_block_minecart{}" );
		items.add( "minecraft:deepslate_redstone_ore{}" );
		items.add( "minecraft:diamond_ore{}" );
		items.add( "minecraft:dispenser{}" );
		items.add( "minecraft:emerald_ore{}" );
		items.add( "supplementaries:dispenser_minecart{}" );
		items.add( "extraalchemy:empty_vial{}" );
		items.add( "supplementaries:flax_seeds{}" );
		items.add( "minecraft:furnace_minecart{}" );
		items.add( "vinery:gloves{}" );
		items.add( "minecraft:gold_ore{}" );
		items.add( "minecraft:hopper{}" );
		items.add( "soulsweapons:moonstone_ring{}" );
		items.add( "soulsweapons:moonstone_pickaxe{}" );
		items.add( "soulsweapons:moonstone_ore_deepslate{}" );
		items.add( "soulsweapons:moonstone_ore{}" );
		items.add( "soulsweapons:moonstone_hoe{}" );
		items.add( "soulsweapons:moonstone_block{}" );
		items.add( "soulsweapons:moonstone_axe{}" );
		items.add( "soulsweapons:moonstone{}" );
		items.add( "minecraft:minecart{}" );
		items.add( "minecraft:lapis_ore{}" );
		items.add( "minecraft:iron_ore{}" );
		items.add( "minecraft:hopper_minecart{}" );
		items.add( "soulsweapons:moonstone_shovel{}" );
		items.add( "minecraft:nether_gold_ore{}" );
		items.add( "minecraft:nether_quartz_ore{}" );
		items.add( "minecraft:observer{}" );
		items.add( "byg:pendorite_ingot{}" );
		items.add( "minecraft:piston{}" );
		items.add( "davespotioneering:potioneer_gauntlet{}" );
		items.add( "minecraft:redstone_ore{}" );
		items.add( "supplementaries:relayer{}" );
		items.add( "minecraft:repeater{}" );
		items.add( "spellbladenext:runeblazing_ingot{}" );
		items.add( "minecraft:tnt_minecart{}" );
		items.add( "minecraft:sticky_piston{}" );
		items.add( "soulsweapons:soul_ingot_leggings{}" );
		items.add( "soulsweapons:soul_ingot_helmet{}" );
		items.add( "soulsweapons:soul_ingot_chestplate{}" );
		items.add( "soulsweapons:soul_ingot_boots{}" );
		items.add( "soulsweapons:soul_ingot{}" );
		items.add( "minecraft:shield{}" );
		items.add( "chipped:seeded_nether_sprouts{}" );
		items.add( "spellbladenext:runegleaming_ingot{}" );
		items.add( "spellbladenext:runefrosted_ingot{}" );
		items.add( "goml:reinforced_upgrade_kit{}" );
		items.add( "goml:glistening_upgrade_kit{}" );
		items.add( "goml:crystal_upgrade_kit{}" );
		items.add( "goml:emeradic_upgrade_kit{}" );
		items.add( "goml:withered_upgrade_kit{}" );
		items.add( "minecraft:potion{Potion:\"minecraft:water\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:night_vision\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:long_night_vision\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:invisibility\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:long_invisibility\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:leaping\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:long_leaping\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:strong_leaping\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:fire_resistance\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:long_fire_resistance\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:swiftness\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:long_swiftness\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:strong_swiftness\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:strong_harming\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:harming\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:strong_healing\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:healing\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:long_water_breathing\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:water_breathing\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:strong_turtle_master\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:long_turtle_master\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:turtle_master\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:strong_slowness\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:long_slowness\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:slowness\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:poison\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:long_poison\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:strong_poison\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:regeneration\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:long_regeneration\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:strong_regeneration\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:strength\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:long_strength\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:strong_strength\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:weakness\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:long_weakness\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:luck\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:powerful_strength\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:extended_slow_falling\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:powerful_regeneration\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:powerful_instant_health\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:extended_water_breathing\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:powerful_speed\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:extended_fire_resistance\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:powerful_jump_boost\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:extended_invisibility\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:extended_night_vision\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:long_slow_falling\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:slow_falling\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:haste\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:absorption\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:levitation\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:dolphins_grace\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"davespotioneering:milk\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"davespotioneering:strong_invisibility\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:antidote\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:antidote2\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:spawn_recall\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:home_return\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:improved_strength\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:fire_endurance_potion_lv1\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:frost_spell_power_potion_lv1\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:fire_spell_power_potion_lv3\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:fire_spell_power_potion_lv2\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:fire_spell_power_potion_lv1\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:improved_weakness_potion_lv3\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:improved_weakness_potion_lv1\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:improved_strength_potion_lv3\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:improved_strength_potion_lv2\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:improved_strength_potion_lv1\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:fire_endurance_potion_lv3\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:fire_endurance_potion_lv2\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:frost_spell_power_potion_lv2\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:frost_spell_power_potion_lv3\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:arcane_spell_power_potion_lv1\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:arcane_spell_power_potion_lv2\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:arcane_spell_power_potion_lv3\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:holy_spell_power_potion_lv1\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:holy_spell_power_potion_lv2\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:holy_spell_power_potion_lv3\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:impact_power_potion_lv1\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:impact_power_potion_lv2\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:impact_power_potion_lv3\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:critical_damage_power_potion_lv1\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:jump_boost_lv2\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:levitation_lv3\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:levitation_lv2\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:levitation_lv1\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:haste_spell_power_potion_lv3\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:haste_spell_power_potion_lv2\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:haste_spell_power_potion_lv1\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:critical_chance_power_potion_lv3\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:critical_chance_power_potion_lv2\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:critical_chance_power_potion_lv1\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:critical_damage_power_potion_lv3\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:critical_damage_power_potion_lv2\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:jump_boost\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"elysium:swiftness_lv2\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"enderzoology:displacement\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"enderzoology:strong_displacement\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"enderzoology:decay\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"enderzoology:long_decay\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"enderzoology:strong_decay\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"enderzoology:confusion\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"enderzoology:long_confusion\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"enderzoology:strong_confusion\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"enderzoology:rising\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"enderzoology:long_rising\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:photosynthesis_strong\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:photosynthesis_long\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:photosynthesis\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:magnetism_strong\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:magnetism_long\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:magnetism\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:fuse_strong\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:fuse_long\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:fuse\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:crumbling_strong\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:crumbling_long\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:crumbling\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:recall\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:recall_long\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:recall_strong\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:sails\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:sails_long\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:sails_strong\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:learning\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:learning_long\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:learning_strong\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:gravity\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:gravity_long\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:gravity_strong\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:returning\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:detection_strong\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:detection_long\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:detection\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:piper_long\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:piper\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:pacifism_strong\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:pacifism_long\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:pacifism\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:combustion_strong\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:combustion_long\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:combustion\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:concentration\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:shrinking\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:shrinking_long\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:shrinking_strong\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:growing\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:growing_long\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"extraalchemy:growing_strong\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"miskatonicmysteries:resonance\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"miskatonicmysteries:resonance_long\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"miskatonicmysteries:resonance_strong\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"mutantmonsters:chemical_x\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"naturalist:forest_dasher\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:long_warding\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:strong_warding\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:warding\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"rottencreatures:strong_freeze\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"rottencreatures:long_freeze\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"rottencreatures:freeze\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:supportive_revivify_potion\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:revivify_potion\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"naturalist:long_glowing\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"naturalist:glowing\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"naturalist:strong_forest_dasher\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"naturalist:long_forest_dasher\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"minecraft:tainted_ambrosia\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"uselessreptile:acid\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"uselessreptile:long_acid\"}" );
		items.add( "minecraft:tipped_arrow{Potion:\"uselessreptile:strong_acid\"}" );

	}
}

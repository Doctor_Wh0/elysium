package com.enderzombi102.elysium.config;

import blue.endless.jankson.Comment;
import com.enderzombi102.elysium.data.ItemInput;
import net.minecraft.item.RangedWeaponItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

import static com.enderzombi102.elysium.Elysium.LOGGER;

public class RangedData {
	@Comment( "Enable the registration of the items with the values modified" )
	public @NotNull Boolean enable = true;

	@Comment( """
		An object of `<id_ranged> - <>` pairs, used to change the modifiers of ranged bow or arrow.
		Example:
		"minecraft:bow": {   // id of item
					modifiers: {
						"generic.durability": [  // id of attribute 
							"6",			// value
						],
					},
				},
		""" )
	public @NotNull HashMap< String, ItemInput > ranged = new HashMap<>();

	public void fill() {
		try ( BufferedReader reader = new BufferedReader( new InputStreamReader( Objects.requireNonNull( getClass().getClassLoader().getResourceAsStream( "ranged.csv" ) ) ) ) ) {
			String line;
			while ( ( line = reader.readLine() ) != null ) {
				//ArrayList< String > modifiers = new ArrayList<>( Arrays.asList( "generic.durability","generic.attack_damage") );
				int i = 0;
				String[] values = line.split( "," );
				String id = null;
				HashMap< String, ArrayList< Object > > hashMap = new HashMap<>();
				for ( String value : values ) {
					value = value.replace( "\"", "" );
					if ( id == null ) {
						id = value;
					} else {
						if( Float.parseFloat(value)>15){
							hashMap.put( "generic.durability", new ArrayList<>( Arrays.asList( value, "0" ) ) );
						}else{
							hashMap.put( "generic.attack_damage", new ArrayList<>( Arrays.asList( value, "0" ) ) );
						}
						i = i + 1;
					}

				}
				this.ranged.put( id, new ItemInput( hashMap ) );

			}
		} catch ( IOException e ) {
			LOGGER.error("[Elysium] Error loading ranged data file");
			e.printStackTrace();
		}
	}
}

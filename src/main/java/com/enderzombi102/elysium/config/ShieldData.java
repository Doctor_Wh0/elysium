package com.enderzombi102.elysium.config;

import blue.endless.jankson.Comment;
import com.enderzombi102.elysium.data.ItemInput;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

import static com.enderzombi102.elysium.Elysium.LOGGER;

public class ShieldData {

	@Comment( "Enable the registration of the items with the values modified" )
	public @NotNull Boolean enable = false;

	@Comment( """
		An object of `<id_shield> - <>` pairs, used to change the modifiers of armor.
		Example:
		"magistuarmory:wood_target": {   // id of item
					modifiers: {
						"generic.durability": [  // id of attribute 
							"6",			// value
							"0",			// OPERATION  0 -> Addition, 1 -> Multiply base, 2 -> Multiply total
						],
					},
				},
		""" )
	public @NotNull HashMap< String, ItemInput > shield = new HashMap<>();

	public void fill() {
		try ( BufferedReader reader = new BufferedReader( new InputStreamReader( Objects.requireNonNull( getClass().getClassLoader().getResourceAsStream( "new_shield.csv" ) ) ) ) ) {
			String line;
			while ( ( line = reader.readLine() ) != null ) {
				String[] values = line.split( "," );
				String id = null;
				HashMap< String, ArrayList< Object > > hashMap = new HashMap<>();
				for ( String value : values ) {
					value = value.replace( "\"", "" );
					if ( id == null ) {
						id = value;
					} else {
						if ( !value.isEmpty() ) {
							if ( value.contains( "%" ) ) {
								value = value.replace( "%", "" );
								hashMap.put( "generic.durability", new ArrayList<>( Arrays.asList( value, "2" ) ) );
							} else {
								hashMap.put( "generic.durability", new ArrayList<>( Arrays.asList( value, "0" ) ) );
							}
						}
					}

				}
				this.shield.put( id, new ItemInput( hashMap ) );
			}
		} catch ( IOException e ) {
			LOGGER.error( "[Elysium] Error loading shield data file" );
			e.printStackTrace();
		}
	}
}

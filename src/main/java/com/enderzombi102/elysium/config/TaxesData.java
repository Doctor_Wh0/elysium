package com.enderzombi102.elysium.config;

import blue.endless.jankson.Comment;

public class TaxesData {
	@Comment( "The minimum tax fee" )
	public long minimumFee = 10;
	@Comment( "The maximum tax fee" )
	public long maximumFee = 1000000;
	@Comment( "The amount of days that need to pass for an anchor to drop, -1 to disable" )
	public int anchorDropDays = 2;
	@Comment( "Whether if an anchor drops its loot when has zero credit and $anchorDropDays have passed" )
	public boolean anchorDropDestroys = true;
	@Comment( "Time in seconds that the input slot takes to switch to the next item, -1 to never switch" )
	public int claimScreenInputCycleTime = 5;
}

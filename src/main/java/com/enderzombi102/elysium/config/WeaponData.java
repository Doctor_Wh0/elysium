package com.enderzombi102.elysium.config;

import blue.endless.jankson.Comment;
import com.enderzombi102.elysium.data.ItemInput;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

import static com.enderzombi102.elysium.Elysium.LOGGER;

public class WeaponData {
	@Comment( "Enable the registration of the items with the values modified" )
	public @NotNull Boolean enable = true;

	@Comment( """
		An object of `<id_weapon> - <>` pairs, used to change the modifiers of armor.
		Example:
		"minecraft:iron_sword": {   // id of item
					modifiers: {
						"generic.attack_damage": [  // id of attribute 
							"6",			// value
							"0",			// OPERATION  0 -> Addition, 1 -> Multiply base, 2 -> Multiply total
						],
					},
				},
		""" )
	public @NotNull HashMap< String, ItemInput > weapon = new HashMap<>();

	public void fill() {
		try ( BufferedReader reader = new BufferedReader( new InputStreamReader( Objects.requireNonNull( getClass().getClassLoader().getResourceAsStream( "weapons.csv" ) ) ) ) ) {
			String line;
			while ( ( line = reader.readLine() ) != null ) {
				ArrayList< String > modifiers = new ArrayList<>( Arrays.asList( "generic.attack_damage", "generic.attack_speed", "reach-entity-attributes:attack_range", "generic.durability", "generic.piercing", "generic.silver", "spell_power:healing", "spell_power:arcane", "spell_power:fire", "spell_power:frost", "spell_power:haste" ) );
				ArrayList< String > bow_modifiers = new ArrayList<>( Arrays.asList( "generic.attack_damage", "generic.durability" ) );
				int i = 0;
				String[] values = line.split( "," );
				String id = null;
				HashMap< String, ArrayList< Object > > hashMap = new HashMap<>();
				if ( values.length < 4 ) {
					modifiers = bow_modifiers;
				}
				for ( String value : values ) {
					value = value.replace( "\"", "" );
					if ( id == null ) {
						id = value;
					} else {
						if ( !value.isEmpty() && !modifiers.get( i ).isEmpty() ) {
							if ( value.contains( "%" ) ) {
								value = value.replace( "%", "" );
								hashMap.put( modifiers.get( i ), new ArrayList<>( Arrays.asList( value, "2" ) ) );
							} else {
								hashMap.put( modifiers.get( i ), new ArrayList<>( Arrays.asList( value, "0" ) ) );
							}
						}
						i = i + 1;
					}

				}
				this.weapon.put( id, new ItemInput( hashMap ) );

			}
		} catch ( IOException e ) {
			LOGGER.error("[Elysium] Error loading weapon data file");
			e.printStackTrace();
		}
	}
}

package com.enderzombi102.elysium.crops;

import com.enderzombi102.elysium.registry.ItemRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.CropBlock;
import net.minecraft.item.ItemConvertible;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.IntProperty;

public class BambooCropBlock extends CropBlock {
	public static final IntProperty AGE = IntProperty.of( "age",0,5 );
	public BambooCropBlock( Settings settings ) {
		super( settings );
	}

	@Override
	public int getMaxAge() {
		return 5;
	}

	@Override
	public IntProperty getAgeProperty() {
		return AGE;
	}

	@Override
	protected void appendProperties( StateManager.Builder< Block, BlockState > builder ) {
		builder.add(AGE);
	}


	@Override
	protected ItemConvertible getSeedsItem() {
		return ItemRegistry.BAMBOO_SEED;
	}
}

package com.enderzombi102.elysium.crops;

import com.nhoryzon.mc.farmersdelight.registry.BlocksRegistry;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.*;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.IntProperty;
import net.minecraft.tag.FluidTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.WorldView;
import net.minecraft.world.tick.OrderedTick;
import org.jetbrains.annotations.Nullable;

public class RiceCropBlock extends PlantBlock implements Fertilizable, FluidFillable {
	public static final IntProperty AGE = IntProperty.of( "age", 0, 7 );

	public RiceCropBlock() {
		super( FabricBlockSettings.copyOf( Blocks.WHEAT ) );
		this.setDefaultState( (BlockState) ( (BlockState) ( (BlockState) this.getStateManager().getDefaultState() ).with( AGE, 0 ) ));

	}
	@Override
	public BlockState getStateForNeighborUpdate( BlockState state, Direction direction, BlockState newState, WorldAccess world, BlockPos pos, BlockPos posFrom) {
		BlockState superState = super.getStateForNeighborUpdate(state, direction, newState, world, pos, posFrom);
		if (!superState.isAir()) {
			world.getFluidTickScheduler().scheduleTick( OrderedTick.create(Fluids.WATER, pos));
		}

		return superState;
	}

	@Override
	protected void appendProperties( StateManager.Builder< Block, BlockState > builder ) {
		super.appendProperties( builder );
		builder.add( AGE );
	}

	@Override
	public boolean canFillWithFluid( BlockView world, BlockPos pos, BlockState state, Fluid fluid ) {
		return false;
	}

	@Override
	public boolean tryFillWithFluid( WorldAccess world, BlockPos pos, BlockState state, FluidState fluidState ) {
		return false;
	}

	@Override
	public boolean canPlaceAt( BlockState state, WorldView world, BlockPos pos ) {
		FluidState fluid = world.getFluidState( pos );
		return super.canPlaceAt( state, world, pos ) && fluid.isIn( FluidTags.WATER ) && fluid.getLevel() == 8;
	}

	protected boolean canPlantOnTop( BlockState floor, BlockView world, BlockPos pos ) {
		return super.canPlantOnTop( floor, world, pos ) || floor.isOf( BlocksRegistry.RICH_SOIL.get() );
	}

	@Override
	public boolean isFertilizable( BlockView world, BlockPos pos, BlockState state, boolean isClient ) {
		return true;
	}

	@Override
	public boolean canGrow( World world, Random random, BlockPos pos, BlockState state ) {
		return true;
	}

	protected int getBonemealAgeIncrease( World world ) {
		return MathHelper.nextInt( world.getRandom(), 1, 4 );
	}

	@Override
	public void grow( ServerWorld world, Random random, BlockPos pos, BlockState state ) {
		int ageGrowth = Math.min( state.get( AGE ) + this.getBonemealAgeIncrease( world ), 7 );
		world.setBlockState( pos, (BlockState) state.with( AGE, ageGrowth ) );
	}

	@Override
	public FluidState getFluidState( BlockState state ) {
		return Fluids.WATER.getStill( false );
	}

	@Override
	public @Nullable BlockState getPlacementState( ItemPlacementContext context ) {
		FluidState fluid = context.getWorld().getFluidState( context.getBlockPos() );
		return fluid.isIn( FluidTags.WATER ) && fluid.getLevel() == 8 ? super.getPlacementState( context ) : null;
	}

	@Override
	public void randomTick( BlockState state, ServerWorld world, BlockPos pos, Random random ) {
		super.randomTick( state, world, pos, random );
		if ( world.isRegionLoaded( pos.add( -1, -1, -1 ), pos.add( 1, 1, 1 ) ) ) {
			if ( world.getLightLevel( pos.up(), 0 ) >= 6 && state.get( AGE ) <= 6 && random.nextInt( 3 ) == 0 ) {
				this.randomGrowTick( state, world, pos, random );
			}

		}
	}

	private void randomGrowTick( BlockState state, ServerWorld world, BlockPos pos, Random rand ) {
		int currentAge = state.get( AGE );
		if ( currentAge <= 6 && rand.nextInt( 6 ) == 0 ) {
			world.setBlockState( pos, this.withAge( currentAge + 1 ), 2 );

		}

	}

	public BlockState withAge( int age ) {
		return (BlockState) this.getDefaultState().with( AGE, age );
	}
}

package com.enderzombi102.elysium.data;

import com.google.common.collect.HashBiMap;
import io.github.apace100.calio.data.SerializableDataType;
import net.minecraft.block.Material;

import java.util.HashMap;

public class DataTypes {
	private static final HashMap< String, Material > MATERIAL_MAP = new HashMap<>() {{
		put( "air", Material.AIR );
		put( "plant", Material.PLANT );
		put( "organic_product", Material.ORGANIC_PRODUCT );
		put( "soil", Material.SOIL );
		put( "solid_organic", Material.SOLID_ORGANIC );
		put( "wood", Material.WOOD );
		put( "wool", Material.WOOL );
		put( "leaves", Material.LEAVES );
		put( "glass", Material.GLASS );
		put( "stone", Material.STONE );
		put( "metal", Material.METAL );
	}};

	public static final SerializableDataType< Material > MATERIAL = SerializableDataType.mapped( Material.class, HashBiMap.create( MATERIAL_MAP ) );
}

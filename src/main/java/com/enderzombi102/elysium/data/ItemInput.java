package com.enderzombi102.elysium.data;

import software.bernie.shadowed.fasterxml.jackson.annotation.JsonAnyGetter;
import software.bernie.shadowed.fasterxml.jackson.annotation.JsonAnySetter;

import java.util.ArrayList;
import java.util.HashMap;

public class ItemInput {
	public HashMap< String, ArrayList< Object > > modifiers;

	public ItemInput( HashMap< String, ArrayList< Object > > m ) {
		modifiers = m;
	}

	@JsonAnyGetter
	public HashMap< String, ArrayList< Object > > getModifiers() {
		return modifiers;
	}

	@JsonAnySetter
	public void setModifiers( HashMap< String, ArrayList< Object > > modifiers ) {
		this.modifiers = modifiers;
	}

}

package com.enderzombi102.elysium.effect;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.AttributeContainer;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;

public class AbsorptionEffect extends StatusEffect {
	public AbsorptionEffect( StatusEffectCategory statusEffectCategory, int i ) {
		super( statusEffectCategory, i );
	}

	@Override
	public void onRemoved( LivingEntity entity, AttributeContainer attributes, int amplifier ) {
		entity.setAbsorptionAmount( entity.getAbsorptionAmount() - (float) ( 4 * ( amplifier + 1 ) ) );
		super.onRemoved( entity, attributes, amplifier );
	}

	@Override
	public void onApplied( LivingEntity entity, AttributeContainer attributes, int amplifier ) {
		entity.setAbsorptionAmount( entity.getAbsorptionAmount() + (float) ( 4 * ( amplifier + 1 ) ) );
		super.onApplied( entity, attributes, amplifier );
	}
}

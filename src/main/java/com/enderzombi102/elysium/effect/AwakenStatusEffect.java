package com.enderzombi102.elysium.effect;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.registry.StatusEffectRegistry;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;


// TODO: Complete
public class AwakenStatusEffect extends StatusEffect {
    public AwakenStatusEffect(StatusEffectCategory statusEffectCategory, int color) {
        super(statusEffectCategory, color);
    }

    @Override
    public void applyUpdateEffect(LivingEntity entity, int amplifier) {
        try {
            if (entity.hasStatusEffect(StatusEffectRegistry.MESMERIZED))
                entity.removeStatusEffect(StatusEffectRegistry.MESMERIZED);
        } catch (Exception e) {
            Elysium.LOGGER.info("[Elysium] Error" + e.getMessage());
        }
    }

    @Override
    public boolean canApplyUpdateEffect(int duration, int amplifier) {
        return true;
    }


}

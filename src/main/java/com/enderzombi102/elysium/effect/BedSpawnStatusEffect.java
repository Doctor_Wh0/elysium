package com.enderzombi102.elysium.effect;

import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.tag.BlockTags;
import net.minecraft.tag.ItemTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;

import java.util.Optional;

public class BedSpawnStatusEffect extends StatusEffect {
	public BedSpawnStatusEffect( StatusEffectCategory statusEffectCategory, int color ) {
		super( statusEffectCategory, color );
	}

	public static BlockPos fromVec3d( Vec3d vec3d ) {
		return new BlockPos( (int) Math.round( vec3d.x ), (int) Math.round( vec3d.y ), (int) Math.round( vec3d.z ) );
	}

	@Override
	public void applyUpdateEffect( LivingEntity entity, int amplifier ) {
		if ( entity.getWorld().isClient || !entity.isPlayer() ) {
			return;
		}
		ServerPlayerEntity player = (ServerPlayerEntity) entity;
		teleportTargetToBedSpawn( entity, player );
	}

	private void teleportTargetToBedSpawn( LivingEntity entity, ServerPlayerEntity player ) {
		BlockPos spawn = player.getSpawnPointPosition();
		if ( spawn == null ) {
			spawn = player.getWorld().getSpawnPos();
		}
		RegistryKey< World > spawnDimension = player.getSpawnPointDimension();
		ServerWorld destination = player.getWorld().getServer().getWorld( spawnDimension );
        assert destination != null;
        Optional< Vec3d > a = PlayerEntity.findRespawnPosition( destination, spawn, 0, true, true );
		if ( a.isPresent() ) {
			BlockState blockState = destination.getBlockState( spawn );
			if ( blockState.isIn( BlockTags.BEDS ) || blockState.getBlock().asItem().getDefaultStack().isIn( ItemTags.BEDS ) ) {
				spawn = fromVec3d( a.get() );
				entity.stopRiding();
				player.fallDistance = 0;
				if ( destination != entity.getWorld() ) {
					player.teleport( destination, spawn.getX() + 0.5F, spawn.getY() + 0.6F, spawn.getZ() + 0.5F, 1f, 1f );
					player.getWorld().playSound( null, spawn.getX() + 0.5F, spawn.getY() + 0.6F, spawn.getZ() + 0.5F, SoundEvents.ITEM_CHORUS_FRUIT_TELEPORT, SoundCategory.PLAYERS, 1f, 1f );
				} else {
					entity.teleport( spawn.getX() + 0.5F, spawn.getY() + 0.6F, spawn.getZ() + 0.5F );
					entity.getWorld().playSound( null, spawn.getX() + 0.5F, spawn.getY() + 0.6F, spawn.getZ() + 0.5F, SoundEvents.ITEM_CHORUS_FRUIT_TELEPORT, SoundCategory.PLAYERS, 1f, 1f );
				}
			} else {
				Vec3d pos = entity.getPos();
				entity.getWorld().playSound( null, pos.getX(), pos.getY(), pos.getZ(), SoundEvents.BLOCK_REDSTONE_TORCH_BURNOUT, SoundCategory.PLAYERS, 1f, 1f );
			}
		}
	}

	@Override
	public boolean canApplyUpdateEffect( int duration, int amplifier ) {
		return duration == 1;
	}
}

package com.enderzombi102.elysium.effect;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;
// Dummy Effect
public class ClumsyEffect extends StatusEffect {
	public ClumsyEffect( StatusEffectCategory statusEffectCategory, int color ) {
		super( statusEffectCategory, color );
	}
	@Override
	public void applyUpdateEffect( LivingEntity entity, int amplifier ) {
		return;
	}
	@Override
	public boolean canApplyUpdateEffect( int duration, int amplifier ) {
		return duration == -1;
	}
}

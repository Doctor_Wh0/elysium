package com.enderzombi102.elysium.effect;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;

public class CustomStatusEffect extends StatusEffect {
	public CustomStatusEffect( StatusEffectCategory statusEffectCategory, int color ) {
		super(statusEffectCategory, color);
	}
	@Override
	public void applyUpdateEffect(LivingEntity livingEntity, int amplifier) {
		super.applyUpdateEffect(livingEntity, amplifier);
	}


	@Override
	public boolean canApplyUpdateEffect(int duration, int amplifier) {
		return true;
	}


}

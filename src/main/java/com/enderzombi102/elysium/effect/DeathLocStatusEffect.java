package com.enderzombi102.elysium.effect;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class DeathLocStatusEffect extends StatusEffect {
	public DeathLocStatusEffect( StatusEffectCategory statusEffectCategory, int color ) {
		super( statusEffectCategory, color );
	}

	@Override
	public void applyUpdateEffect( LivingEntity entity, int amplifier ) {
		if ( entity.getWorld().isClient || !entity.isPlayer() ) {
			return;
		}
		ServerPlayerEntity player = (ServerPlayerEntity) entity;
		teleportTargetToDeathLoc( entity, player );
	}

	private void teleportTargetToDeathLoc( LivingEntity entity, ServerPlayerEntity player ) {
		try {
			BlockPos spawn;
			ServerWorld destination = player.getWorld().getServer().getWorld( player.getLastDeathPos().get().getDimension() );
			spawn = player.getLastDeathPos().get().getPos();
			entity.stopRiding();
			player.fallDistance = 0;
			if ( destination != entity.getWorld() ) {
				Vec3d pos = entity.getPos();
				entity.getWorld().playSound( null, pos.getX(), pos.getY(), pos.getZ(), SoundEvents.BLOCK_REDSTONE_TORCH_BURNOUT, SoundCategory.PLAYERS, 1f, 1f );
			} else {
				entity.teleport( spawn.getX() + 0.5F, spawn.getY() + 0.6F, spawn.getZ() + 0.5F );
				entity.getWorld().playSound( null, spawn.getX() + 0.5F, spawn.getY() + 0.6F, spawn.getZ() + 0.5F, SoundEvents.ITEM_CHORUS_FRUIT_TELEPORT, SoundCategory.PLAYERS, 1f, 1f );
			}
		} catch ( Exception e ) {
			Vec3d pos = entity.getPos();
			entity.getWorld().playSound( null, pos.getX(), pos.getY(), pos.getZ(), SoundEvents.BLOCK_REDSTONE_TORCH_BURNOUT, SoundCategory.PLAYERS, 1f, 1f );
		}
	}

	@Override
	public boolean canApplyUpdateEffect( int duration, int amplifier ) {
		return duration == 1;
	}
}

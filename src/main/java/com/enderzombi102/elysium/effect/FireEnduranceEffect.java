package com.enderzombi102.elysium.effect;

import net.minecraft.entity.effect.StatusEffectCategory;
import net.spell_power.api.statuseffects.SpellVulnerabilityStatusEffect;

public class FireEnduranceEffect extends SpellVulnerabilityStatusEffect {
    public FireEnduranceEffect( StatusEffectCategory statusEffectCategory, int color ) {
        super(statusEffectCategory, color);
    }
}

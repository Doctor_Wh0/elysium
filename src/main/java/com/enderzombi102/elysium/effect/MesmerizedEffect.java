package com.enderzombi102.elysium.effect;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.registry.StatusEffectRegistry;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.AttributeContainer;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;
import net.minecraft.entity.effect.StatusEffectInstance;

// TODO: Complete
public class MesmerizedEffect extends StatusEffect {
    public MesmerizedEffect(StatusEffectCategory statusEffectCategory, int color) {
        super(statusEffectCategory, color);
    }


    @Override
    public void onRemoved(LivingEntity entity, AttributeContainer attributes, int amplifier) {
        super.onRemoved(entity, attributes, amplifier);
        try {
            if (!entity.hasStatusEffect(StatusEffectRegistry.AWAKEN)) {
                if (amplifier == 1) {
                    entity.addStatusEffect(new StatusEffectInstance(StatusEffectRegistry.AWAKEN, 240));
                } else {
                    entity.addStatusEffect(new StatusEffectInstance(StatusEffectRegistry.AWAKEN, 120));
                }
            }
        } catch (Exception e) {
            Elysium.LOGGER.info("[Elysium] Error" + e.getMessage());
        }
    }
}

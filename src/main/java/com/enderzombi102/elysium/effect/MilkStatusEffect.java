package com.enderzombi102.elysium.effect;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.InstantStatusEffect;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;
import net.minecraft.entity.effect.StatusEffectInstance;
import org.jetbrains.annotations.Nullable;

import java.util.Set;
import java.util.stream.Collectors;

public class MilkStatusEffect extends InstantStatusEffect {
    public MilkStatusEffect(StatusEffectCategory beneficial, int i) {
        super(beneficial, i);
    }

    @Override
    public void applyInstantEffect(@Nullable Entity source, @Nullable Entity attacker, LivingEntity target, int amplifier, double proximity) {
        Set<StatusEffect> compatibleEffectList = target.getStatusEffects()
                .stream()
                .map(StatusEffectInstance::getEffectType)
                .filter(effectType -> effectType.getCategory() == StatusEffectCategory.HARMFUL)
                .collect(Collectors.toUnmodifiableSet());

        if (!compatibleEffectList.isEmpty())
            if (amplifier == 0) {
                compatibleEffectList.stream()
                        .skip(target.getWorld().getRandom().nextInt(compatibleEffectList.size()))
                        .findFirst()
                        .ifPresent(target::removeStatusEffect);
            } else {
                compatibleEffectList.forEach(target::removeStatusEffect);
            }
    }
}

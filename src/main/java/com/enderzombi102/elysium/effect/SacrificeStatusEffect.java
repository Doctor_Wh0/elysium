package com.enderzombi102.elysium.effect;

import com.enderzombi102.elysium.Elysium;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.AttributeContainer;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;

public class SacrificeStatusEffect extends StatusEffect {
	public boolean done = false;

	public SacrificeStatusEffect( StatusEffectCategory statusEffectCategory, int color ) {
		super( statusEffectCategory, color );
	}

	@Override
	public boolean canApplyUpdateEffect( int duration, int amplifier ) {
		return true;
	}

	@Override
	public void onApplied( LivingEntity entity, AttributeContainer attributes, int amplifier ) {
		super.onApplied( entity, attributes, amplifier );
	}

	@Override
	public void applyUpdateEffect( LivingEntity entity, int amplifier ) {
		try {
			if ( !done ) {
				entity.addStatusEffect( new StatusEffectInstance( StatusEffects.DARKNESS, 20 * 7, 0, false, false, true ) );
				entity.addStatusEffect( new StatusEffectInstance( StatusEffects.INSTANT_DAMAGE, 2, amplifier, false, false, true ) );
				done = true;
			}
		} catch ( Exception e ) {
			Elysium.LOGGER.info( "[Elysium] Error" + e.getMessage() );
		}
	}
}

package com.enderzombi102.elysium.effect;

import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.LivingEntity;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.util.math.Vec3d;
import net.spell_engine.api.effect.CustomParticleStatusEffect;

public class StunSpawner implements CustomParticleStatusEffect.Spawner {
    @Override
    public void spawnParticles(LivingEntity livingEntity, int i) {
        var world = livingEntity.getWorld();
        if (world.isClient && world instanceof ClientWorld clientWorld) {
            var time = livingEntity.age + livingEntity.world.getTime();
            var angle = Math.toRadians((time % 360) * 18F);
            var rotated = new Vec3d(0, 0, livingEntity.getWidth() * 0.5F).rotateY((float) angle);
            var spawnPosition = livingEntity
                    .getPos()
                    .add(0, livingEntity.getHeight() * 1.2F, 0)
                    .add(rotated);
            clientWorld.addParticle(ParticleTypes.CRIT, true, spawnPosition.x, spawnPosition.y, spawnPosition.z, 0, 0, 0);
        }
    }
}

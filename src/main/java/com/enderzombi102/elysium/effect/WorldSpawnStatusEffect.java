package com.enderzombi102.elysium.effect;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;

public class WorldSpawnStatusEffect extends StatusEffect {
    public WorldSpawnStatusEffect(StatusEffectCategory statusEffectCategory, int color) {
        super(statusEffectCategory, color);
    }

    @Override
    public void applyUpdateEffect(LivingEntity entity, int amplifier) {
        if (entity.getWorld().isClient || !entity.isPlayer()) {
            return;
        }
        ServerPlayerEntity player = (ServerPlayerEntity) entity;
        teleportTargetToWorldSpawn(entity, player);
    }

    private void teleportTargetToWorldSpawn(LivingEntity entity, ServerPlayerEntity player) {
        BlockPos spawn;
        ServerWorld serverWorld = player.getWorld();
        RegistryKey<World> spawnDimension = player.getSpawnPointDimension();
        ServerWorld destination = player.getWorld().getServer().getWorld(spawnDimension);
        assert destination != null;
        spawn = destination.getSpawnPos();
        entity.stopRiding();
        player.fallDistance = 0;
        if (destination != entity.getWorld()) {
            player.teleport(destination, spawn.getX() + 0.5F, spawn.getY() + 0.6F, spawn.getZ() + 0.5F, 1f, 1f);
            player.getWorld().playSound(null, spawn.getX() + 0.5F, spawn.getY() + 0.6F, spawn.getZ() + 0.5F, SoundEvents.ITEM_CHORUS_FRUIT_TELEPORT, SoundCategory.PLAYERS, 1f, 1f);
        } else {
            entity.teleport(spawn.getX() + 0.5F, spawn.getY() + 0.6F, spawn.getZ() + 0.5F);
            entity.getWorld().playSound(null, spawn.getX() + 0.5F, spawn.getY() + 0.6F, spawn.getZ() + 0.5F, SoundEvents.ITEM_CHORUS_FRUIT_TELEPORT, SoundCategory.PLAYERS, 1f, 1f);
        }
    }

    @Override
    public boolean canApplyUpdateEffect(int duration, int amplifier) {
        return duration == 1;
    }
}

package com.enderzombi102.elysium.event;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.registry.ItemRegistry;
import net.fabricmc.loader.impl.util.StringUtil;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.mk.archers_arsenal.items.ModItems;

import java.util.List;

public class ItemTooltipCallbackEvent {
	public static void register( ItemStack itemStack, TooltipContext tooltipContext, List< Text > texts ) {
		if ( itemStack.getItem().equals( ItemRegistry.ITEMS.get( "scroll_arcane" ) ) || itemStack.getItem().equals( ItemRegistry.ITEMS.get( "scroll_fire" ) ) || itemStack.getItem().equals( ItemRegistry.ITEMS.get( "scroll_frost" ) ) || itemStack.getItem().equals( ItemRegistry.ITEMS.get( "scroll_heal" ) ) ) {
			texts.add( Text.of( "§3§lBreakable" ) );
			texts.add( Text.of( "§4One Use" ) );
		}
		if ( itemStack.getItem().equals( ItemRegistry.ITEMS.get( "scroll_blank" ) ) ) {
			texts.add( Text.of( "§3§lUseful in crafting" ) );
			texts.add( Text.of( "§4can be used as a vessel for a spell" ) );
		}
		if( itemStack.isIn( Elysium.ITEM_TOOLTIP )){
			Text name=itemStack.getName();
			texts.clear();
			texts.add(name);
		}
		if ( itemStack.getSubNbt( "imbued" ) != null ) {
			texts.add( 1, Text.translatable( "points.label" ).append( Text.literal( " ".concat( itemStack.getNbt().getInt( "points" ) + " punti" ) ) ).formatted( Formatting.GOLD ) );
		}
		itemStack.kjs$getTags().forEach( identifier -> {
			try {
				Text t;
				if ( identifier.getNamespace().equalsIgnoreCase( "elysium" ) ) {
					if ( !identifier.getPath().contains( "/all" ) && !identifier.getPath().contains( "/sets/" ) ) {
						String[] total = identifier.getPath().split( "/" );
						switch ( total.length ) {
							case 1:
								if ( total[ 0 ].contains( "arrow_t" ) ) {
									t = Text.literal( "Rank: " ).formatted( Formatting.YELLOW )
										.append( Text.literal( total[ 0 ].replace("arrow_t","") ).formatted( Formatting.GRAY ) );
									if ( !texts.contains( t ) ) {
										texts.add( texts.size(), t );
									}
								}
								break;
							case 3:
								if ( total[ 0 ].equalsIgnoreCase( "gems" ) ) {
									t = Text.literal( "Type: " )
										.formatted( Formatting.YELLOW )
										.append( Text.literal( StringUtil.capitalize( total[ 1 ] ) )
											.formatted( Formatting.GRAY ) )
										.append( Text.literal( " Rank: " )
											.formatted( Formatting.YELLOW ) )
										.append( Text.literal( String.valueOf( ( Integer.parseInt( total[ 2 ] ) ) ) )
											.formatted( Formatting.GRAY ) );
								} else {
									t = Text.literal( "Class: " )
										.formatted( Formatting.YELLOW )
										.append( Text.literal( StringUtil.capitalize( total[ 1 ] ) )
											.formatted( Formatting.GRAY ) )
										.append( Text.literal( " Rank: " )
											.formatted( Formatting.YELLOW ) )
										.append( Text.literal( String.valueOf( ( Integer.parseInt( total[ 2 ] ) ) ) )
											.formatted( Formatting.GRAY ) );

								}
								if ( !texts.contains( t ) ) {
									texts.add( texts.size(), t );
								}
								break;
							case 4:
								t = Text.literal( "Class: " )
									.formatted( Formatting.YELLOW )
									.append( Text.literal( StringUtil.capitalize( total[ 2 ] ) )
										.formatted( Formatting.GRAY ) )
									.append( Text.literal( " Rank: " )
										.formatted( Formatting.YELLOW ) )
									.append( Text.literal( String.valueOf( ( Integer.parseInt( total[ 3 ] ) ) ) )
										.formatted( Formatting.GRAY ) );
								if ( !texts.contains( t ) ) {
									texts.add( texts.size(), t );
								}
								break;
							case 5:
								t = Text.literal( "Class: " )
									.formatted( Formatting.YELLOW )
									.append( Text.literal( StringUtil.capitalize( total[ 2 ] + " " + StringUtil.capitalize( total[ 3 ] ) ) )
										.formatted( Formatting.GRAY ) )
									.append( Text.literal( " Rank: " ).formatted( Formatting.YELLOW ) )
									.append( Text.literal( String.valueOf( ( Integer.parseInt( total[ 4 ] ) ) ) ).formatted( Formatting.GRAY ) );
								if ( !texts.contains( t ) ) {

									texts.add( texts.size(), t );
								}
								break;
						}
					}
				}
			} catch ( Exception e ) {
				Elysium.LOGGER.info( "ERROR " + itemStack.getItem() + " " + identifier.getPath() );
			}
		} );
	}
}

package com.enderzombi102.elysium.event;

import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.data.ItemInput;
import com.enderzombi102.elysium.imixin.MutableMultiMap;
import com.enderzombi102.elysium.item.CustomArmorMaterial;
import com.enderzombi102.elysium.item.CustomToolMaterial;
import com.enderzombi102.elysium.item.ItemAccessor;
import com.enderzombi102.elysium.util.Util;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import dev.latvian.mods.kubejs.item.FoodBuilder;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.item.*;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.recipe.Ingredient;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.projectile_damage.api.IProjectileWeapon;
import net.spell_engine.api.item.ConfigurableAttributes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.function.BiConsumer;

// TODO: Check- probably to fix
public class ModifyItemAttributeModifierEvent {
	public static final HashMap< String, String > repair = new HashMap<>() {{
		put( "magistuarmory:copper_buckler", "elysium:patch_copper" );
		put( "magistuarmory:copper_ellipticalshield", "elysium:patch_copper" );
		put( "magistuarmory:copper_heatershield", "elysium:patch_copper" );
		put( "magistuarmory:copper_kiteshield", "elysium:patch_copper" );
		put( "magistuarmory:copper_pavese", "elysium:patch_copper" );
		put( "magistuarmory:copper_rondache", "elysium:patch_copper" );
		put( "magistuarmory:copper_roundshield", "elysium:patch_copper" );
		put( "magistuarmory:copper_target", "elysium:patch_copper" );
		put( "magistuarmory:copper_tartsche", "elysium:patch_copper" );
		put( "magistuarmory:corruptedroundshield", "elysium:patch_diamond" );
		put( "magistuarmory:diamond_buckler", "elysium:patch_diamond" );
		put( "magistuarmory:diamond_ellipticalshield", "elysium:patch_diamond" );
		put( "magistuarmory:diamond_heatershield", "elysium:patch_diamond" );
		put( "magistuarmory:diamond_kiteshield", "elysium:patch_diamond" );
		put( "magistuarmory:diamond_pavese", "elysium:patch_diamond" );
		put( "magistuarmory:diamond_rondache", "elysium:patch_diamond" );
		put( "magistuarmory:diamond_roundshield", "elysium:patch_diamond" );
		put( "magistuarmory:diamond_target", "elysium:patch_diamond" );
		put( "magistuarmory:diamond_tartsche", "elysium:patch_diamond" );
		put( "magistuarmory:gold_buckler", "elysium:patch_gold" );
		put( "magistuarmory:gold_ellipticalshield", "elysium:patch_gold" );
		put( "magistuarmory:gold_heatershield", "elysium:patch_gold" );
		put( "magistuarmory:gold_kiteshield", "elysium:patch_gold" );
		put( "magistuarmory:gold_pavese", "elysium:patch_gold" );
		put( "magistuarmory:gold_rondache", "elysium:patch_gold" );
		put( "magistuarmory:gold_roundshield", "elysium:patch_gold" );
		put( "magistuarmory:gold_target", "elysium:patch_gold" );
		put( "magistuarmory:gold_tartsche", "elysium:patch_gold" );
		put( "magistuarmory:iron_buckler", "elysium:patch_iron" );
		put( "magistuarmory:iron_ellipticalshield", "elysium:patch_iron" );
		put( "magistuarmory:iron_heatershield", "elysium:patch_iron" );
		put( "magistuarmory:iron_kiteshield", "elysium:patch_iron" );
		put( "magistuarmory:iron_pavese", "elysium:patch_iron" );
		put( "magistuarmory:iron_rondache", "elysium:patch_iron" );
		put( "magistuarmory:iron_roundshield", "elysium:patch_iron" );
		put( "magistuarmory:iron_target", "elysium:patch_iron" );
		put( "magistuarmory:iron_tartsche", "elysium:patch_iron" );
		put( "magistuarmory:netherite_buckler", "elysium:patch_netherite" );
		put( "magistuarmory:netherite_ellipticalshield", "elysium:patch_netherite" );
		put( "magistuarmory:netherite_heatershield", "elysium:patch_netherite" );
		put( "magistuarmory:netherite_kiteshield", "elysium:patch_netherite" );
		put( "magistuarmory:netherite_pavese", "elysium:patch_netherite" );
		put( "magistuarmory:netherite_rondache", "elysium:patch_netherite" );
		put( "magistuarmory:netherite_roundshield", "elysium:patch_netherite" );
		put( "magistuarmory:netherite_target", "elysium:patch_netherite" );
		put( "magistuarmory:netherite_tartsche", "elysium:patch_netherite" );
		put( "magistuarmory:silver_buckler", "elysium:patch_silver" );
		put( "magistuarmory:silver_ellipticalshield", "elysium:patch_silver" );
		put( "magistuarmory:silver_heatershield", "elysium:patch_silver" );
		put( "magistuarmory:silver_kiteshield", "elysium:patch_silver" );
		put( "magistuarmory:silver_pavese", "elysium:patch_silver" );
		put( "magistuarmory:silver_rondache", "elysium:patch_silver" );
		put( "magistuarmory:silver_roundshield", "elysium:patch_silver" );
		put( "magistuarmory:silver_target", "elysium:patch_silver" );
		put( "magistuarmory:silver_tartsche", "elysium:patch_silver" );
		put( "magistuarmory:steel_buckler", "elysium:patch_steel" );
		put( "magistuarmory:steel_ellipticalshield", "elysium:patch_steel" );
		put( "magistuarmory:steel_heatershield", "elysium:patch_steel" );
		put( "magistuarmory:steel_kiteshield", "elysium:patch_steel" );
		put( "magistuarmory:steel_pavese", "elysium:patch_steel" );
		put( "magistuarmory:steel_rondache", "elysium:patch_steel" );
		put( "magistuarmory:steel_roundshield", "elysium:patch_steel" );
		put( "magistuarmory:steel_target", "elysium:patch_steel" );
		put( "magistuarmory:steel_tartsche", "elysium:patch_steel" );
		put( "magistuarmory:tin_buckler", "elysium:patch_tin" );
		put( "magistuarmory:tin_ellipticalshield", "elysium:patch_tin" );
		put( "magistuarmory:tin_heatershield", "elysium:patch_tin" );
		put( "magistuarmory:tin_kiteshield", "elysium:patch_tin" );
		put( "magistuarmory:tin_pavese", "elysium:patch_tin" );
		put( "magistuarmory:tin_rondache", "elysium:patch_tin" );
		put( "magistuarmory:tin_roundshield", "elysium:patch_tin" );
		put( "magistuarmory:tin_target", "elysium:patch_tin" );
		put( "magistuarmory:tin_tartsche", "elysium:patch_tin" );
		put( "magistuarmory:wood_buckler", "magistuarmory:leather_strip" );
		put( "magistuarmory:wood_ellipticalshield", "magistuarmory:leather_strip" );
		put( "magistuarmory:wood_heatershield", "magistuarmory:leather_strip" );
		put( "magistuarmory:wood_kiteshield", "magistuarmory:leather_strip" );
		put( "magistuarmory:wood_pavese", "magistuarmory:leather_strip" );
		put( "magistuarmory:wood_rondache", "magistuarmory:leather_strip" );
		put( "magistuarmory:wood_roundshield", "magistuarmory:leather_strip" );
		put( "magistuarmory:wood_target", "magistuarmory:leather_strip" );
		put( "magistuarmory:wood_tartsche", "magistuarmory:leather_strip" );
		put( "archers_arsenal:bow_diamond", "elysium:cote_diamond" );
		put( "archers_arsenal:bow_gold", "elysium:cote_gold" );
		put( "archers_arsenal:bow_iron", "elysium:cote_iron" );
		put( "archers_arsenal:bow_leather", "elysium:cote_tin" );
		put( "archers_arsenal:bow_netherite", "elysium:cote_netherite" );
		put( "magistuarmory:longbow", "elysium:cote_copper" );

	}};
	public static boolean done = false;

	public static void register( ItemStack itemStack, EquipmentSlot slot, Multimap< EntityAttribute, EntityAttributeModifier > attributeModifiers ) {
		try {
			if ( itemStack.getItem() instanceof ArmorItem armorItem ) {
				armorItem.kjs$setArmorToughness( 0 );
				armorItem.kjs$setArmorKnockbackResistance( 0 );
			}
		} catch ( Exception ignored ) {
		}
		if ( itemStack.getItem() instanceof BlockItem blockItem ) {
			blockItem.getBlock().kjs$setFriction( 0.6F );
		}
		try {
			if ( itemStack.getItem().isFood() ) {
				itemStack.getItem().kjs$setFoodProperties( (FoodComponent) null );
			}
			if ( itemStack.isIn( Util.FOOD_T0 ) || itemStack.isIn( Util.FOOD_T1 ) || itemStack.isIn( Util.FOOD_T2 ) || itemStack.isIn( Util.FOOD_T3 ) || itemStack.isIn( Util.FOOD_T4 ) || itemStack.isIn( Util.FOOD_T5 ) || itemStack.isIn( Util.BEER ) ) {
				FoodBuilder foodBuilder = new FoodBuilder();
				foodBuilder.hunger( 2 );
				foodBuilder.saturation( 0 );
				itemStack.getItem().kjs$setFoodProperties( foodBuilder.build() );
			}
		} catch ( Exception ignored ) {
		}
		if ( !done ) {
			armor();
			weapon();
			ingredient();
			ranged();
			shield();
			done = true;
		}
		NbtCompound nbt;
		if ( ( nbt = itemStack.getSubNbt( "imbued" ) ) != null ) {
			for ( String key : nbt.getKeys() ) {
				try {
					EquipmentSlot s;
					if ( itemStack.getItem() instanceof ArmorItem ) {
						s = ( (ArmorItem) itemStack.getItem() ).getSlotType();
					} else {
						s = EquipmentSlot.MAINHAND;
					}
					// TODO: WIP
					Collection< EntityAttributeModifier > collection = itemStack.getItem().getAttributeModifiers( s ).get( Registry.ATTRIBUTE.get( new Identifier( key ) ) );
					Collection< EntityAttributeModifier > startCollection = itemStack.getItem().getDefaultStack().getAttributeModifiers( s ).get( Registry.ATTRIBUTE.get( new Identifier( key ) ) );
					if ( !collection.isEmpty() ) {
						EntityAttributeModifier modifier = collection.stream().findFirst().get();
						/*if(startCollection.isEmpty()){

						}else{

						}*/
						if ( !key.equalsIgnoreCase( "generic.durability" ) ) {
							if ( !modifier.getName().equalsIgnoreCase( "Custom" ) ) {
								itemStack.addAttributeModifier( Registry.ATTRIBUTE.get( new Identifier( key ) ), new EntityAttributeModifier( key, nbt.getDouble( key ) + modifier.getValue(), EntityAttributeModifier.Operation.ADDITION ), s );

							} else {
								itemStack.addAttributeModifier( Registry.ATTRIBUTE.get( new Identifier( key ) ), new EntityAttributeModifier( key, nbt.getDouble( key ), EntityAttributeModifier.Operation.ADDITION ), s );
							}
						}
					} else {
						itemStack.addAttributeModifier( Registry.ATTRIBUTE.get( new Identifier( key ) ), new EntityAttributeModifier( key, nbt.getDouble( key ), EntityAttributeModifier.Operation.ADDITION ), s );
					}
				} catch ( Exception ignored ) {
					ignored.printStackTrace();
				}
			}
		}
	}

	private static void ingredient() {
		if ( Config.getCustomIngredient().enable ) {
			Config.getCustomIngredient().items.forEach( new BiConsumer< String, String >() {
				@Override
				public void accept( String s, String s2 ) {
					try {
						if ( Registry.ITEM.get( new Identifier( s ) ) instanceof MutableMultiMap mutableMultiMap ) {
							try {
								mutableMultiMap.setType( new CustomArmorMaterial( ( (ArmorItem) mutableMultiMap ).getMaterial(), () -> Ingredient.ofItems( Registry.ITEM.get( new Identifier( s2 ) ) ) ) );
							} catch ( Exception e ) {
								mutableMultiMap.setType( new CustomToolMaterial( ( (ToolItem) mutableMultiMap ).getMaterial(), () -> Ingredient.ofItems( Registry.ITEM.get( new Identifier( s2 ) ) ) ) );
							}
						}
					} catch ( Exception ignored ) {
					}
				}
			} );
		}
	}

	private static void armor() {
		if ( Config.getArmor().enable ) {
			Config.getArmor().armor.forEach( new BiConsumer< String, ItemInput >() {
				@Override
				public void accept( String s, ItemInput itemInput ) {
					if ( ( Registry.ITEM.get( new Identifier( s ) ) instanceof MutableMultiMap ) || ( Registry.ITEM.get( new Identifier( s ) ) instanceof ConfigurableAttributes ) ) {
						Item i = Registry.ITEM.get( new Identifier( s ) );
						if ( i instanceof MutableMultiMap ) {
							Multimap< EntityAttribute, EntityAttributeModifier > attribute = ( (MutableMultiMap) i ).getMutableAttributeMap();
							attribute.clear();
						}
						ImmutableMultimap.Builder< EntityAttribute, EntityAttributeModifier > builder = ImmutableMultimap.builder();
						itemInput.modifiers.forEach( new BiConsumer< String, ArrayList< Object > >() {
							@Override
							public void accept( String s2, ArrayList< Object > objects ) {
								Float value = Float.parseFloat( (String) objects.get( 0 ) );
								if ( s2.equalsIgnoreCase( "generic.durability" ) ) {
									i.kjs$setMaxDamage( value.intValue() );
								} else {
									EntityAttributeModifier entityAttributeModifier = new EntityAttributeModifier( s2, value,
										EntityAttributeModifier.Operation.fromId( Float.valueOf( (String) objects.get( 1 ) ).intValue() ) );
									builder.put( Registry.ATTRIBUTE.get( new Identifier( s2 ) ), entityAttributeModifier );
								}
							}
						} );
						if ( i.getName().contains( Text.literal( "magistu" ) ) ) {
							( (MutableMultiMap) i ).setDefaultModifiers( builder.build() );
						} else {
							( (MutableMultiMap) i ).setAttributeModifiers( builder.build() );
						}
					}
				}
			} );

		}
	}

	private static void weapon() {
		if ( Config.getWeapon().enable ) {
			Config.getWeapon().weapon.forEach( new BiConsumer< String, ItemInput >() {
				@Override
				public void accept( String s, ItemInput itemInput ) {
					if ( ( Registry.ITEM.get( new Identifier( s ) ) instanceof MutableMultiMap ) || ( Registry.ITEM.get( new Identifier( s ) ) instanceof ConfigurableAttributes || ( Registry.ITEM.get( new Identifier( s ) ) instanceof MiningToolItem ) ) ) {
						Item i = Registry.ITEM.get( new Identifier( s ) );
						if ( i instanceof MutableMultiMap ) {
							Multimap< EntityAttribute, EntityAttributeModifier > attribute = ( (MutableMultiMap) i ).getMutableAttributeMap();
							attribute.clear();
						}
						ImmutableMultimap.Builder< EntityAttribute, EntityAttributeModifier > builder = ImmutableMultimap.builder();
						itemInput.modifiers.forEach( new BiConsumer< String, ArrayList< Object > >() {
							@Override
							public void accept( String s2, ArrayList< Object > objects ) {
								Float value = Float.valueOf( (String) objects.get( 0 ) );
								if ( s2.equalsIgnoreCase( "generic.durability" ) ) {
									i.kjs$setMaxDamage( value.intValue() );
								} else {
									EntityAttributeModifier entityAttributeModifier;
									if ( s2.equalsIgnoreCase( "generic.silver" ) && ( (String) objects.get( 0 ) ).length() > 0 ) {
										assert i instanceof MutableMultiMap;
										( (MutableMultiMap) i ).setIsSilver( true );
										( (MutableMultiMap) i ).setSilverAttackDamage( value );
									} else if ( s2.equalsIgnoreCase( "generic.piercing" ) && ( (String) objects.get( 0 ) ).length() > 0 ) {
										( (MutableMultiMap) i ).setPiercing( value.intValue() );
									} else if ( s2.equalsIgnoreCase( "generic.attack_damage" ) ) {
										entityAttributeModifier = new EntityAttributeModifier( ItemAccessor.ATTACK_DAMAGE_MODIFIER_ID(), s2, value,
											EntityAttributeModifier.Operation.fromId( Float.valueOf( (String) objects.get( 1 ) ).intValue() ) );
										builder.put( Registry.ATTRIBUTE.get( new Identifier( s2 ) ), entityAttributeModifier );
									} else if ( s2.equalsIgnoreCase( "generic.attack_speed" ) ) {
										entityAttributeModifier = new EntityAttributeModifier( ItemAccessor.ATTACK_SPEED_MODIFIER_ID(), s2, value,
											EntityAttributeModifier.Operation.fromId( Float.valueOf( (String) objects.get( 1 ) ).intValue() ) );
										builder.put( Registry.ATTRIBUTE.get( new Identifier( s2 ) ), entityAttributeModifier );
									} else {
										entityAttributeModifier = new EntityAttributeModifier( s2, value,
											EntityAttributeModifier.Operation.fromId( Float.valueOf( (String) objects.get( 1 ) ).intValue() ) );
										builder.put( Registry.ATTRIBUTE.get( new Identifier( s2 ) ), entityAttributeModifier );
									}

								}
							}
						} );
						if ( i instanceof ConfigurableAttributes ) {
							( (ConfigurableAttributes) i ).setAttributes( builder.build() );
						} else {
							try {
								( (MutableMultiMap) i ).setDefaultModifiers( builder.build() );
							} catch ( Exception e ) {
							} finally {
								try {
									( (MutableMultiMap) i ).setAttributeModifiers( builder.build() );
								} catch ( Exception e ) {
									try {
										( (MutableMultiMap) i ).setToolModifiers( builder.build() );
									} catch ( Exception e1 ) {
									}
								}
							}
						}

					}
				}
			} );

		}
	}

	private static void ranged() {
		if ( Config.getRanged().enable ) {
			Config.getRanged().ranged.forEach( new BiConsumer< String, ItemInput >() {
				@Override
				public void accept( String s, ItemInput itemInput ) {
					if ( ( Registry.ITEM.get( new Identifier( s ) ) instanceof IProjectileWeapon ) ) {
						Item i = Registry.ITEM.get( new Identifier( s ) );
						itemInput.modifiers.forEach( new BiConsumer< String, ArrayList< Object > >() {
							@Override
							public void accept( String s, ArrayList< Object > objects ) {
								if ( s.equals( "generic.durability" ) ) {
									i.kjs$setMaxDamage( Integer.parseInt( (String) objects.get( 0 ) ) );
								}
							}
						} );
					}
				}
			} );

		}
	}

	private static void shield() {
		if ( Config.getShield().enable ) {
			Config.getShield().shield.forEach( ( s, itemInput ) -> {
				Item i = Registry.ITEM.get( new Identifier( s ) );
				itemInput.modifiers.forEach( new BiConsumer< String, ArrayList< Object > >() {
					@Override
					public void accept( String s2, ArrayList< Object > objects ) {
						float value = Float.parseFloat( (String) objects.get( 0 ) );
						if ( s2.equalsIgnoreCase( "generic.durability" ) ) {
							i.kjs$setMaxDamage( (int) value );
						}
					}
				} );
			} );

		}
	}

}

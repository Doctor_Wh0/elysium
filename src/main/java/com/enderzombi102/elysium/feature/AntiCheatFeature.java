package com.enderzombi102.elysium.feature;

import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.mixin.other.ServerLoginNetworkHandlerAccessor;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import me.lucko.fabric.api.permissions.v0.Permissions;
import net.fabricmc.fabric.api.client.networking.v1.ClientLoginNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.fabricmc.fabric.api.networking.v1.ServerLoginConnectionEvents;
import net.fabricmc.fabric.api.networking.v1.ServerLoginNetworking;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.metadata.ModMetadata;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientLoginNetworkHandler;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerLoginNetworkHandler;
import net.minecraft.text.Text;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.elysium.util.Const.getId;

public class AntiCheatFeature {
	// server -> request mod list + provide banned classes -> client
	// client -> check banned classes ^ provide mod list -> server
	// server -> disconnect if ( found banned || check mod list )
	public static void register() {
		// receive mod list
		ServerLoginNetworking.registerGlobalReceiver( getId( "mod_list" ), AntiCheatFeature::onModListResponse );
		// request mod list + provide banned classes
		ServerLoginConnectionEvents.QUERY_START.register( ( handler, server, sender, synchronizer ) -> {
			if ( !Config.getData().features.modListCheck )
				return;

			var buf = PacketByteBufs.create();
			buf.writeCollection( Config.getData().antiCheat.classes, PacketByteBuf::writeString );
			sender.sendPacket( getId( "mod_list" ), buf );
		} );

		LOGGER.info( "[Elysium] Registered networking server stuff" );
	}

	public static void registerClient() {
		ClientLoginNetworking.registerGlobalReceiver( getId( "mod_list" ), AntiCheatFeature::onModListRequest );
		LOGGER.info( "[Elysium] Registered networking client stuff" );
	}

	/**
	 * @apiNote This is run on the netty's network thread!
	 */
	private static void onModListResponse( MinecraftServer server, ServerLoginNetworkHandler handler, boolean understood, PacketByteBuf buf, ServerLoginNetworking.LoginSynchronizer synchronizer, PacketSender responseSender ) {
		// disconnect if ( found banned || check mod list )
		if ( !understood )
			return;

		if ( !Config.getData().features.modListCheck )
			return;

		var profile = ( (ServerLoginNetworkHandlerAccessor) handler ).getProfile();

		if ( profile.getId() != null && Permissions.check( profile, "elysium.bypass_mod_check" ).join() )
			return;

		// if the client failed the class check, fail faster
		var failedClassCheck = buf.readBoolean();
		if ( failedClassCheck ) {
			var culpritClasses = buf.readCollection( ArrayList::new, PacketByteBuf::readString );

			LOGGER.info(
				"[Elysium] Player connecting from `{}` failed the class check, found classes: {}",
				handler.getConnection().getAddress(),
				culpritClasses
			);
			handler.disconnect( Text.translatable( "text.elysium.class_check_failed" ) );
		}

		var modIds = Config.getData().antiCheat.mods;
		var missing = new ArrayList< String >();
		var invalid = new ArrayList< String >();

		var clientMods = buf.readCollection( ArrayList::new, PacketByteBuf::readString );
		// check client's mod list
		for ( var modId : modIds ) {
			if ( modId.startsWith( "!" ) ) {
				modId = modId.substring( 1 );
				if ( !clientMods.contains( modId ) )
					missing.add( modId );
			} else if ( clientMods.contains( modId ) )
				invalid.add( modId );
		}

		// true if all is good
		if ( invalid.isEmpty() && missing.isEmpty() )
			return;

		// found some mods, fail
		LOGGER.info(
			"[Elysium] Player connecting from `{}` failed the mod list check, found mods: {}, missing mods: {}",
			handler.getConnection().getAddress(),
			invalid,
			missing
		);
		handler.disconnect( Text.translatable( "text.elysium.mod_check_failed", String.join( "\n- ", invalid ) ) );
	}

	private static CompletableFuture< @Nullable PacketByteBuf > onModListRequest( MinecraftClient client, ClientLoginNetworkHandler handler, PacketByteBuf buf, Consumer< GenericFutureListener< ? extends Future< ? super Void > > > listenerAdder ) {
		// check banned classes ^ provide mod list
		var classes = buf.readCollection( ArrayList::new, PacketByteBuf::readString );

		LOGGER.info( "[Elysium] Requested to check for the following classes: {}", classes );

		var send = PacketByteBufs.create();
		var invalid = classes.stream()
			.filter( it -> {
				// invert result
				var flag = it.startsWith( "!" );
				if ( flag )
					it = it.substring( 1 );

				try {
					Class.forName( it, false, FabricLoader.class.getClassLoader() );
					return !flag;
				} catch ( ClassNotFoundException e ) {
					return !flag;
				}
			} )
			.toList();

		send.writeBoolean( !invalid.isEmpty() ); // class check
		if ( !invalid.isEmpty() )
			send.writeCollection( invalid, PacketByteBuf::writeString );
		else
			send.writeCollection(
				FabricLoader.getInstance()
					.getAllMods()
					.stream()
					.map( ModContainer::getMetadata )
					.map( ModMetadata::getId )
					.toList(),
				PacketByteBuf::writeString
			);

		return CompletableFuture.completedFuture( send );
	}
}

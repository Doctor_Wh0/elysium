package com.enderzombi102.elysium.feature;

import com.enderzombi102.elysium.util.TaxesUtil;
import com.enderzombi102.elysium.util.WorldParticleUtils;
import draylar.goml.api.ClaimBox;
import draylar.goml.api.ClaimUtils;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayConnectionEvents;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerTickEvents;
import net.fabricmc.fabric.api.event.player.UseBlockCallback;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.particle.BlockStateParticleEffect;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.tag.BlockTags;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import org.apache.commons.lang3.tuple.Triple;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;

import static com.enderzombi102.elysium.util.Const.*;

public class ClaimFeature {
	// --- SERVER ONLY ---
	private static final Map< UUID, ClaimBox > PLAYER_CLOSEST = new HashMap<>();
	// --- CLIENT ONLY ---
	private static final BlockState[] STATES = Registry.BLOCK.stream()
		.filter( b -> {
			var id = Registry.BLOCK.getId( b );
			return id.getNamespace().equals( "minecraft" ) && id.getPath().endsWith( "_concrete" );
		} )
		.map( Block::getDefaultState )
		.toList()
		.toArray( new BlockState[ 0 ] );
	private static final @NotNull List< Triple< Integer, BlockPos, BlockPos > > CLAIMS = Collections.synchronizedList( new ArrayList<>() );
	private static @Nullable Claim closestClaim = null;
	private static boolean adminModer = false;
	private static long lastReceivedMillis;

	public static void register() {
		ServerTickEvents.END_WORLD_TICK.register( ClaimFeature::sendClosestClaimUpdate );
	}

	private static void sendClosestClaimUpdate( @NotNull ServerWorld world ) {
		for ( var player : world.getPlayers() ) {
			var maybeEntry = ClaimUtils.getClaimsInBox(
					world,
					player.getBlockPos().add( -16, -16, -16 ),
					player.getBlockPos().add( 16, 16, 16 )
				)
				.filter( it -> !it.getValue().isOwner( player ) )
				.collect( Collectors.toList() )
				.stream()
				.findFirst();

			var buf = PacketByteBufs.create();

			if ( maybeEntry.isEmpty() ) {
				if ( PLAYER_CLOSEST.get( player.getUuid() ) == null )
					continue;

				PLAYER_CLOSEST.put( player.getUuid(), null );

				buf.writeBoolean( false );
			} else {
				var entry = maybeEntry.get();
				var claimBox = entry.getKey();

				if ( PLAYER_CLOSEST.get( player.getUuid() ) == claimBox )
					continue;

				PLAYER_CLOSEST.put( player.getUuid(), claimBox );

				if ( claimBox == null )
					buf.writeBoolean( false );
				else {
					var claim = entry.getValue();
					buf.writeBoolean( true );
					buf.writeOptional( claim.getOwners().stream().findFirst(), PacketByteBuf::writeUuid );
					buf.writeBoolean( !TaxesUtil.isClaimDisabled( claim, world ) );

					var box = claimBox.toBox();
					buf.writeLong( new BlockPos( box.x1(), box.y1(), box.z1() ).asLong() );  // minPos
					buf.writeLong( new BlockPos( box.x2(), box.y2(), box.z2() ).asLong() );  // maxPos
				}
			}

			ServerPlayNetworking.send( player, CLAIM_CLOSEST_ID, buf );
		}
	}

	public static void registerClient() {
		ClientPlayNetworking.registerGlobalReceiver( CLAIM_ADMIN_MODER_ID, ClaimFeature::receiveAdminModeUpdate );
		ClientPlayNetworking.registerGlobalReceiver( CLAIM_OUTLINES_ID, ClaimFeature::receiveClaimOutlines );
		ClientPlayNetworking.registerGlobalReceiver( CLAIM_CLOSEST_ID, ClaimFeature::receiveClosestClaim );
		ClientPlayConnectionEvents.DISCONNECT.register( ClaimFeature::onPlayDisconnect );
		ClientTickEvents.END_WORLD_TICK.register( ClaimFeature::onEndTick );
		UseBlockCallback.EVENT.register( ClaimFeature::blockUnwantedInteractions );
	}

	private static void receiveAdminModeUpdate( MinecraftClient client, ClientPlayNetworkHandler handler, PacketByteBuf buf, PacketSender sender ) {
		adminModer = buf.readBoolean();
	}

	/**
	 * Receives the claim the player is in a 16b radius from.
	 *
	 * @param client  the minecraft client instance.
	 * @param handler the client connection handler.
	 * @param data    the packed data the server sent.
	 * @param sender  the packet sender.
	 */
	private static void receiveClosestClaim( MinecraftClient client, ClientPlayNetworkHandler handler, PacketByteBuf data, PacketSender sender ) {
		Claim claim;

		var present = data.readBoolean();
		if ( present ) {
			var owner = data.readOptional( PacketByteBuf::readUuid ).orElse( null );
			var status = data.readBoolean();
			var minPos = data.readLong();
			var maxPos = data.readLong();
			var bounds = new Box( BlockPos.fromLong( minPos ), BlockPos.fromLong( maxPos ) );
			claim = new Claim( owner, status, bounds );
		} else
			claim = null;

		client.execute( () -> closestClaim = claim );
	}

	/**
	 * Receives the claim outlines from the server.
	 *
	 * @param client  the minecraft client instance.
	 * @param handler the client connection handler.
	 * @param data    the packed data the server sent.
	 * @param sender  the packet sender.
	 */
	private static void receiveClaimOutlines( MinecraftClient client, ClientPlayNetworkHandler handler, PacketByteBuf data, PacketSender sender ) {
		var count = data.readInt();
		var claims = new ArrayList< Triple< Integer, BlockPos, BlockPos > >( count );
		var arr = new long[ 3 ];

		for ( var i = 0; i < count; i += 1 ) {
			data.readLongArray( arr );
			claims.add( Triple.of( (int) arr[ 0 ], BlockPos.fromLong( arr[ 1 ] ), BlockPos.fromLong( arr[ 2 ] ) ) );
		}

		client.execute( () -> {
			CLAIMS.clear();
			CLAIMS.addAll( claims );
			lastReceivedMillis = System.currentTimeMillis();
		} );
	}

	/**
	 * Spawn the claim particles, timings have been tuned at runtime.
	 *
	 * @param world the client-side version of the world the player is in.
	 */
	private static void onEndTick( ClientWorld world ) {
		synchronized ( CLAIMS ) {
			if ( System.currentTimeMillis() - lastReceivedMillis > 4466 )
				return;

			for ( var triple : CLAIMS )
				WorldParticleUtils.spawnCube(
					MinecraftClient.getInstance(),
					world,
					triple.getMiddle(), triple.getRight(),
					new BlockStateParticleEffect( ParticleTypes.BLOCK_MARKER, STATES[ ( triple.getLeft() & 65535 ) % STATES.length ] )
				);
		}
	}

	/**
	 * Reset claim data, lets not leak anything!
	 *
	 * @param handler the client connection handler.
	 * @param client  the minecraft client instance.
	 */
	private static void onPlayDisconnect( ClientPlayNetworkHandler handler, MinecraftClient client ) {
		lastReceivedMillis = 0;
		CLAIMS.clear();
	}

	private static ActionResult blockUnwantedInteractions( PlayerEntity player, World world, Hand hand, BlockHitResult hit ) {
		var state = world.getBlockState( hit.getBlockPos() );

		// do nothing if its air or if we don't have a claim
		if ( state.isAir() || ClaimFeature.getClosestClaim() == null )
			return ActionResult.PASS;

		// if in admin mode, avoid blocking stuff
		if ( isAdminModing() )
			return ActionResult.PASS;

		// check if the claim is active and that the player isn't the owner
		if ( !ClaimFeature.getClosestClaim().active() || player.getUuid().equals( ClaimFeature.getClosestClaim().owner() ) )
			return ActionResult.PASS;

		// check that we're interacting with a blocked object
		if ( state.isIn( BlockTags.DOORS ) || state.isIn( BlockTags.FENCE_GATES ) )
			return ActionResult.FAIL;

		return ActionResult.PASS;
	}

	public static @Nullable Claim getClosestClaim() {
		return closestClaim;
	}

	public static boolean isAdminModing() {
		return adminModer;
	}

	public record Claim( @Nullable UUID owner, boolean active, @NotNull Box bounds ) {
	}
}

package com.enderzombi102.elysium.imixin;

import java.util.HashMap;
import java.util.UUID;

public interface ElysiumClaimBlockEntity {
	void elysium$addTime( long amount );

	long elysium$getTime();
	void elysium$setTime(long time);

	UUID elysium$getOld();
	void elysium$addCarpenter(UUID carpenter, Long value);
	void elysium$removeCarpenter(UUID carpenter);
	HashMap<UUID,Long> elysium$getCarpenters();
	void elysium$setTowered(boolean towered);
	boolean elysium$getTowered();
}

package com.enderzombi102.elysium.imixin;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import com.magistuarmory.item.WeaponType;
import dev.latvian.mods.kubejs.core.NoMixinException;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.item.ArmorMaterial;
import net.minecraft.item.ToolMaterial;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;

public interface MutableMultiMap {
	default void setType( ArmorMaterial type ){throw new NoMixinException();}
	default void setType( ToolMaterial type ){throw new NoMixinException();}
	default ArmorMaterial getArmorMaterial(){throw new NoMixinException();}
	default ToolMaterial getToolMaterial(){throw new NoMixinException();}
    default Multimap<EntityAttribute, EntityAttributeModifier> getAttributeModifiers() {
        throw new NoMixinException();
    }

    default void setAttributeModifiers(Multimap<EntityAttribute, EntityAttributeModifier> attributes) {
        throw new NoMixinException();
    }

    default Multimap<EntityAttribute, EntityAttributeModifier> getToolModifiers() {
        throw new NoMixinException();
    }

    default void setToolModifiers(Multimap<EntityAttribute, EntityAttributeModifier> attributes) {
        throw new NoMixinException();
    }

    default Multimap<EntityAttribute, EntityAttributeModifier> getDefaultModifiers() {
        throw new NoMixinException();
    }

    default void setDefaultModifiers(Multimap<EntityAttribute, EntityAttributeModifier> attributes) {
        throw new NoMixinException();
    }

    default WeaponType getType() {
        throw new NoMixinException();
    }

    default void setType(WeaponType type) {
        throw new NoMixinException();
    }

    default Multimap<EntityAttribute, EntityAttributeModifier> getMutableAttributeMap() {
        Multimap<EntityAttribute, EntityAttributeModifier> attributes;
        try {
            attributes = getDefaultModifiers();
            if (attributes instanceof ImmutableMultimap) {
                attributes = ArrayListMultimap.create(attributes);
                setDefaultModifiers(attributes);
            }
        } catch (Exception e) {
            try {
                attributes = getToolModifiers();
                if (attributes instanceof ImmutableMultimap) {
                    attributes = ArrayListMultimap.create(attributes);
                    setToolModifiers(attributes);
                }
            } catch (Exception e1) {
                attributes = getAttributeModifiers();
                if (attributes instanceof ImmutableMultimap) {
                    attributes = ArrayListMultimap.create(attributes);
                    setAttributeModifiers(attributes);
                }
            }
        }

        return attributes;
    }

    default void setIsSilver(boolean isSilver) {
        throw new NoMixinException();
    }

    default void setSilverAttackDamage(float silverAttackDamage) {
        throw new NoMixinException();
    }

    default void setArmorPiercing(int armorPiercing) {
        throw new NoMixinException();
    }

    default void setPiercing(int piercing) {
        WeaponType type = getType();
        ((MutableMultiMap) type).setArmorPiercing(piercing);
        setType(type);
    }

}

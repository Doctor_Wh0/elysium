package com.enderzombi102.elysium.item;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.ArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.recipe.Ingredient;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;

public class ArmorClothItem extends ArmorItem {
	public ArmorClothItem( String name, EquipmentSlot equipmentSlot ) {
		super( new ArmorMaterial() {
			@Override
			public int getDurability( EquipmentSlot slot ) {
				switch ( slot ) {
					case HEAD -> {
						return 33;
					}
					case CHEST -> {
						return 48;
					}
					case LEGS -> {
						return 45;
					}
					case FEET -> {
						return 39;
					}
					default -> {
						return 0;
					}
				}
			}

			@Override
			public int getProtectionAmount( EquipmentSlot slot ) {
				switch ( slot ) {
					case CHEST -> {
						return 2;
					}
					case LEGS -> {
						return 1;
					}
					default -> {
						return 0;
					}
				}
			}

			@Override
			public int getEnchantability() {
				return 0;
			}

			@Override
			public SoundEvent getEquipSound() {
				return SoundEvents.BLOCK_WOOL_PLACE;
			}

			@Override
			public Ingredient getRepairIngredient() {
				return Ingredient.ofStacks( Items.STRING.getDefaultStack() );
			}

			@Override
			public String getName() {
				return name;
			}

			@Override
			public float getToughness() {
				return 0;
			}

			@Override
			public float getKnockbackResistance() {
				return 0;
			}
		}, equipmentSlot, new Item.Settings() );
	}
}

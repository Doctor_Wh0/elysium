package com.enderzombi102.elysium.item;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.imixin.ElysiumClaimBlockEntity;
import com.enderzombi102.elysium.registry.ItemRegistry;
import com.jamieswhiteshirt.rtree3i.Entry;
import draylar.goml.api.Claim;
import draylar.goml.api.ClaimBox;
import draylar.goml.api.ClaimUtils;
import draylar.goml.block.entity.ClaimAnchorBlockEntity;
import io.github.apace100.origins.origin.OriginLayers;
import io.github.apace100.origins.registry.ModComponents;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.text.Text;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class CarpenterContract extends Item {
	public CarpenterContract( Settings settings ) {
		super( settings );
	}


	@Override
	public TypedActionResult< ItemStack > use( World world, PlayerEntity user, Hand hand ) {
		if ( !world.isClient() ) {
			assert Elysium.server != null;
			if ( hand == Hand.MAIN_HAND ) {
				NbtCompound nbts = user.getMainHandStack().getNbt();
				if ( nbts == null )
					nbts = new NbtCompound();
				// Se contiene l'UUID del proprietario allora il proprietario ha già cliccato sul claim
				if ( nbts.contains( "claimOwner" ) ) {
					// se contiene l'UUID del carpentiere allora il proprietario ha già cliccato sul carpentiere e sul claim
					if ( nbts.contains( "carpenter" ) ) {
						// Trust player and start counting
						if ( user.getUuid().equals( nbts.getUuid( "claimOwner" ) ) ) {
							if ( nbts.contains( "active" ) ) {
								if ( nbts.contains( "sure" ) ) {
									Long created = Long.valueOf( ( nbts.get( "sure" ).asString() ) );
									Timestamp timestamp = new Timestamp( System.currentTimeMillis() );
									Long time = timestamp.getTime();
									Long diff = time - created;
									if ( ( diff / 1000 ) < 30 ) {
										Set< Entry< ClaimBox, Claim > > s = ClaimUtils.getClaimsOwnedBy( world, nbts.getUuid( "claimOwner" ) ).filter( claimEntry -> !Objects.equals( claimEntry.getValue().getOrigin(), new BlockPos( 0, 0, -100 ) ) ).collect( Collectors.toSet() );
										NbtCompound finalNbts = nbts;
										s.forEach( claimEntry -> {
											ClaimAnchorBlockEntity claimAnchorBlockEntity = ClaimUtils.getAnchor( Elysium.server.getOverworld(), claimEntry.getValue() );
											assert claimAnchorBlockEntity != null;
											( (ElysiumClaimBlockEntity) claimAnchorBlockEntity ).elysium$removeCarpenter( finalNbts.getUuid( "carpenter" ) );
											claimEntry.getValue().untrust( finalNbts.getUuid( "carpenter" ) );
										} );
										ItemStack itemStack = user.getMainHandStack();
										itemStack.decrement( 1 );
										user.sendMessage( Text.literal( "[Carpenter Contract] Contract resolved" ) );
										user.kjs$setMainHandItem( ItemStack.EMPTY );
										return new TypedActionResult<>( ActionResult.PASS, ItemStack.EMPTY );
									} else {
										if(world.getTime() < nbts.getLong( "active" )){
											nbts.putString( "sure", String.valueOf( time ) );
											user.sendMessage( Text.literal( "[Carpenter Contract] Do you want to resolve this contract? Use again to CONFIRM" ) );
											return new TypedActionResult<>( ActionResult.SUCCESS, user.getMainHandStack().kjs$withNBT( nbts ) );
										}else {
											user.sendMessage( Text.literal( "[Carpenter Contract] Contract already resolved" ) );
											user.kjs$setMainHandItem( ItemStack.EMPTY );
											return new TypedActionResult<>( ActionResult.PASS, ItemStack.EMPTY );
										}
									}
								} else {
									if(world.getTime() < nbts.getLong( "active" )){
										Timestamp timestamp = new Timestamp( System.currentTimeMillis() );
										Long time = timestamp.getTime();
										nbts.putString( "sure", String.valueOf( time ) );
										user.sendMessage( Text.literal( "[Carpenter Contract] Do you want to resolve this contract? Use again to CONFIRM" ) );
										return new TypedActionResult<>( ActionResult.SUCCESS, user.getMainHandStack().kjs$withNBT( nbts ) );
									}else {
										user.sendMessage( Text.literal( "[Carpenter Contract] Contract already resolved" ) );
										user.kjs$setMainHandItem( ItemStack.EMPTY );
										return new TypedActionResult<>( ActionResult.PASS, ItemStack.EMPTY );
									}
								}
							} else {
								Set< Entry< ClaimBox, Claim > > s = ClaimUtils.getClaimsOwnedBy( world, nbts.getUuid( "claimOwner" ) ).collect( Collectors.toSet() );
								NbtCompound finalNbts1 = nbts;
								s.forEach( claimEntry -> {
									ClaimAnchorBlockEntity claimAnchorBlockEntity = ClaimUtils.getAnchor( Elysium.server.getOverworld(), claimEntry.getValue() );
									assert claimAnchorBlockEntity != null;
									//((ElysiumClaimBlockEntity)claimAnchorBlockEntity).elysium$addCarpenter( user.getUuid(), world.getTime()+ Config.getData().claims.carpenterContractDays*24*60*60*20 );
									// 5 giorni
									( (ElysiumClaimBlockEntity) claimAnchorBlockEntity ).elysium$addCarpenter( finalNbts1.getUuid( "carpenter" ), world.getTime() + 120 * 20 );
								} );
								user.sendMessage( Text.literal( "[Carpenter Contract] Contract started" ) );
								nbts.putLong( "active", world.getTime() + 120 * 20 );
								return new TypedActionResult<>( ActionResult.SUCCESS, user.getMainHandStack().kjs$withNBT( nbts ) );
							}
						}
					}
				} else {
					// proprietario che clicca sul claim
					Set< Entry< ClaimBox, Claim > > s = ClaimUtils.getClaimsAt( world, user.getBlockPos() ).filter( claimBoxClaimEntry -> claimBoxClaimEntry.getValue().getOwners().contains( user.getUuid() ) ).collect( Collectors.toSet() );
					if ( s.isEmpty() ) {
						user.sendMessage( Text.literal( "[Carpenter Contract] Not in a valid claim" ) );
						return new TypedActionResult<>( ActionResult.FAIL, new ItemStack( ItemRegistry.ITEMS.get( "carpenter_contract" ) ) );
					} else {
						nbts.putUuid( "claimOwner", user.getUuid() );
						user.sendMessage( Text.literal( "[Carpenter Contract] Claim succesfully binded to this contract" ) );
						return new TypedActionResult<>( ActionResult.SUCCESS, user.getMainHandStack().kjs$withNBT( nbts ) );
					}
				}
			}
		}
		return super.use( world, user, hand );
	}

	@Override
	public ActionResult useOnEntity( ItemStack stack, PlayerEntity user, LivingEntity entity, Hand hand ) {
		try {
			if ( entity.isPlayer() && hand == Hand.MAIN_HAND ) {
				NbtCompound nbts = stack.getNbt();
				if ( nbts != null ) {
					if ( nbts.contains( "claimOwner" ) ) {
						if ( nbts.getUuid( "claimOwner" ).equals( user.getUuid() ) ) {
							if ( ModComponents.ORIGIN.get( entity ).getOrigin( OriginLayers.getLayer( new Identifier( "elysium:class" ) ) ).getIdentifier().toString().contains( "carpenter" ) ) {
								nbts.putUuid( "carpenter", entity.getUuid() );
								user.sendMessage( Text.literal( "[Carpenter Contract] Carpenter " + entity.getEntityName() + " successfully binded to the contract" ) );
								stack.setNbt( nbts );
								user.kjs$setMainHandItem( stack );
								user.equipStack( EquipmentSlot.MAINHAND, stack );
								return ActionResult.SUCCESS;
							} else {
								user.sendMessage( Text.literal( "[Carpenter Contract] " + entity.getEntityName() + " is not a carpenter" ) );
							}
						} else {
							user.sendMessage( Text.literal( "[Carpenter Contract] You are not the owner of the claim" ) );
						}
					} else {
						user.sendMessage( Text.literal( "[Carpenter Contract] A carpenter is already binded to this contract" ) );
					}
				}
			}
		}catch(Exception e){
			user.sendMessage( Text.literal("[Carpenter contract] Error using the item") );
		}
		return super.useOnEntity( stack, user, entity, hand );
	}

	@Override
	public void appendTooltip( ItemStack stack, @Nullable World world, List< Text > tooltip, TooltipContext context ) {
		NbtCompound nbts = stack.getNbt();
		if ( nbts != null ) {
			if ( nbts.contains( "claimOwner" ) ) {
				tooltip.add( Text.literal( "Claim binded" ).formatted( Formatting.YELLOW ) );
				if ( nbts.contains( "carpenter" ) ) {
					tooltip.add( Text.literal( "Carpenter binded" ).formatted( Formatting.YELLOW ) );
					if ( nbts.contains( "active" ) ) {
						if ( world.kjs$isOverworld() ) {
							if ( world.getTime() < nbts.getLong( "active" )  ) {
								tooltip.add( Text.literal( "The contract has been activated" ).formatted( Formatting.GREEN ) );
								tooltip.add( Text.literal( "Use again the contract to resolve it" ).formatted( Formatting.RED ) );
							} else {
								tooltip.add( Text.literal( "The contract has expired" ).formatted( Formatting.DARK_RED ) );
							}
						}
					} else {
						tooltip.add( Text.literal( "Use the item to start the contract" ).formatted( Formatting.DARK_GREEN ) );
					}
				} else {
					tooltip.add( Text.literal( "Use the item on the carpenter you want to bind" ).formatted( Formatting.AQUA ) );
				}

			}
		}else{
			tooltip.add( Text.literal( "Use the item in your claim to bind it" ).formatted( Formatting.AQUA ) );
		}
		super.appendTooltip( stack, world, tooltip, context );
	}

	@Override
	public boolean hasGlint( ItemStack stack ) {
		NbtCompound nbts = stack.getNbt();
		if ( nbts != null ) {
			return nbts.contains( "claimOwner" );
		} else {
			return false;
		}
	}
}

package com.enderzombi102.elysium.item;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.imixin.ElysiumClaimBlockEntity;
import com.enderzombi102.elysium.registry.CompRegistry;
import com.enderzombi102.elysium.registry.ItemRegistry;
import com.jamieswhiteshirt.rtree3i.Entry;
import com.mojang.datafixers.util.Pair;
import draylar.goml.GetOffMyLawn;
import draylar.goml.api.Claim;
import draylar.goml.api.ClaimBox;
import draylar.goml.api.ClaimUtils;
import draylar.goml.api.event.ClaimEvents;
import draylar.goml.block.ClaimAnchorBlock;
import draylar.goml.block.entity.ClaimAnchorBlockEntity;
import draylar.goml.registry.GOMLBlocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.enderzombi102.elysium.Elysium.LOGGER;

public class ClaimContract extends Item {
    public ClaimContract(Settings settings) {
        super(settings);
    }

    public static void changeClaim(Claim claim, ClaimBox claimBox, ServerWorld world, PlayerEntity player, Pair<ClaimAnchorBlock, Item> claimAnchor) {
        var newBox = ClaimUtils.createClaimBox(claim.getOrigin(), claimAnchor.getFirst().getRadius());
        GetOffMyLawn.CLAIM.get(world).remove(claim);
        BlockEntity oldBE = world.getBlockEntity(claim.getOrigin());
        world.setBlockState(claim.getOrigin(), claimAnchor.getFirst().getDefaultState());
        claim.internal_setIcon(claimAnchor.getSecond().getDefaultStack());
        claim.internal_setType(claimAnchor.getFirst());
        claim.internal_setClaimBox(newBox);
        claim.internal_updateChunkCount(world);
        claim.internal_setWorld(world.getRegistryKey().getValue());
        claim.addOwner(player.getUuid());
        try {
            GetOffMyLawn.CLAIM.get(world).add(claim);
            BlockEntity newBE = world.getBlockEntity(claim.getOrigin());
            if (oldBE instanceof ClaimAnchorBlockEntity && newBE instanceof ClaimAnchorBlockEntity) {
                ((ClaimAnchorBlockEntity) newBE).from(((ClaimAnchorBlockEntity) oldBE));
            }
            assert Elysium.server != null;
            var comp = CompRegistry.ELYSIUM_LEVEL.get(Elysium.server.getSaveProperties());
            comp.setAnchorStamp(player.getUuid(), (world.getTime()+10*60*20));
            comp.setTimeMethod(player.getUuid(), "server");
			ClaimAnchorBlockEntity claimAnchorBlockEntity=ClaimUtils.getAnchor( Elysium.server.getOverworld(),claim );
			assert claimAnchorBlockEntity != null;
			(( ElysiumClaimBlockEntity )claimAnchorBlockEntity).elysium$setTime((world.getTime()+10*60*20));
            ClaimEvents.CLAIM_RESIZED.invoker().onResizeEvent(claim, claimBox, newBox);
            LOGGER.warn("[Elysium-Ledger] {} claimed admin anchor claim at {} ", player.getName().getString(), claim.getOrigin().toShortString());
        } catch (Exception ignored) {
        }
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        if(!world.isClient()){
			assert Elysium.server!=null;
			if(hand==Hand.MAIN_HAND){
				var claim = ClaimUtils.getClaimsAt(world, user.getBlockPos());
				var owned = ClaimUtils.getClaimsOwnedBy(world, user.getUuid()).filter(new Predicate<Entry<ClaimBox, Claim>>() {
					@Override
					public boolean test(Entry<ClaimBox, Claim> claimBoxClaimEntry) {
						return !Objects.equals(claimBoxClaimEntry.getValue().getOrigin(), new BlockPos(0, 0, -100));
					}
				});
				if (!owned.isEmpty()) {
					user.sendMessage(Text.literal("[Claim Contract] You have an active claim"));
					LOGGER.info("[Elysium] Player " + user.getName().getString() + " tried to obtain a claim with another active claim");
					return new TypedActionResult<>(ActionResult.FAIL, new ItemStack(ItemRegistry.ITEMS.get("claim_contract")));
				}
				Set< Entry< ClaimBox, Claim > > set = claim.collect( Collectors.toSet() );
				if( !set.isEmpty() ) {
					ClaimAnchorBlockEntity actual = ClaimUtils.getAnchor( Elysium.server.getOverworld(),set.stream().findFirst().get().getValue() );
					assert actual != null;
					if(((ElysiumClaimBlockEntity)actual).elysium$getOld().equals( user.getUuid() )){
						AtomicBoolean c= new AtomicBoolean( false );
						GetOffMyLawn.CLAIM.get(Elysium.server.getOverworld()).getClaims().entries().forEach( claimEntry -> {
							if(!Objects.equals(claimEntry.getValue().getOrigin(), new BlockPos(0, 0, -100))) {
								ClaimAnchorBlockEntity claimAnchorBlockEntity = ClaimUtils.getAnchor( Elysium.server.getOverworld(), claimEntry.getValue() );
								assert claimAnchorBlockEntity != null;
								if ( ( (ElysiumClaimBlockEntity) claimAnchorBlockEntity ).elysium$getOld().equals( user.getUuid() ) ) {
									changeClaim( claimEntry.getValue(), claimEntry.getKey(), (ServerWorld) world, user, GOMLBlocks.MAKESHIFT_CLAIM_ANCHOR );
									if ( !c.get() ) {
										c.set( true );
									}
								}
							}
						} );
						if(c.get()) {
							ItemStack itemStack = user.getMainHandStack();
							itemStack.decrement( 1 );
							user.sendMessage(Text.literal("[Claim Contract] Claim successfully recovered"));
							return new TypedActionResult<>( ActionResult.SUCCESS, itemStack );
						}
					}else{
						user.sendMessage(Text.literal("[Claim Contract] The contract must be used within your claim"));
						return new TypedActionResult<>(ActionResult.FAIL, new ItemStack(ItemRegistry.ITEMS.get("claim_contract")));

					}

				}else{
					user.sendMessage(Text.literal("[Claim Contract] You are not within a valid claim"));
					return new TypedActionResult<>(ActionResult.FAIL, new ItemStack(ItemRegistry.ITEMS.get("claim_contract")));

				}
			}
		}
        return super.use(world, user, hand);
    }

    @Override
    public boolean hasGlint(ItemStack stack) {
        return true;
    }
}

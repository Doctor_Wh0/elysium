package com.enderzombi102.elysium.item;

import dev.emi.trinkets.api.SlotReference;
import dev.emi.trinkets.api.TrinketItem;
import draylar.goml.api.ClaimUtils;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import static com.enderzombi102.elysium.util.Const.CLAIM_OUTLINES_ID;

public class ClaimMonocleItem extends Item {
	public ClaimMonocleItem( Settings settings ) {
		super( settings );
	}

	@Override
	public void inventoryTick( ItemStack stack, World world, Entity entity, int slot, boolean selected ) {
		if ( entity instanceof ServerPlayerEntity player )
			if ( ( selected || player.getEquippedStack( EquipmentSlot.OFFHAND ) == stack ) )
				this.tick( player );
	}

	/*@Override
	public void tick( ItemStack stack, SlotReference slot, LivingEntity entity ) {
		super.tick( stack, slot, entity );

		if ( entity instanceof ServerPlayerEntity player )
			this.tick( player );
	}*/

	private void tick( @NotNull ServerPlayerEntity player ) {
		if ( player.age % 70 != 0 )
			return;

		var server = player.getServer();
		assert server != null : "A player had null set as their server, wtf?";

		var arrs = new ArrayList< long[] >();
		var world = player.getWorld();
		var distance = server.getPlayerManager().getViewDistance() * 16;

		ClaimUtils.getClaimsInBox(
			world,
			player.getBlockPos().add( -distance, -distance, -distance ),
			player.getBlockPos().add( distance, distance, distance )
		).forEach( claim -> {
			var box = claim.getKey().toBox();
			var minPos = new BlockPos( box.x1(), Math.max( box.y1(), world.getBottomY() ), box.z1() );
			var maxPos = new BlockPos( box.x2() - 1, Math.min( box.y2() - 1, world.getTopY() ), box.z2() - 1 );
			arrs.add( new long[] { claim.getValue().getOrigin().hashCode(), minPos.asLong(), maxPos.asLong() } );
		} );

		var pkt = PacketByteBufs.create();
		pkt.writeInt( arrs.size() );
		arrs.forEach( pkt::writeLongArray );

		ServerPlayNetworking.send( player, CLAIM_OUTLINES_ID, pkt );
	}
}

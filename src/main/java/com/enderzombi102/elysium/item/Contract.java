package com.enderzombi102.elysium.item;

import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.Entity;
import net.minecraft.entity.Tameable;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;
import java.util.UUID;

// TODO: to test
public class Contract extends Item {


	public Contract( Settings settings ) {
		super( settings );
	}

	public static void fillContract( ItemStack stack, PlayerEntity player, Entity entity ) {
		ItemStack filled = new ItemStack( stack.getItem(), 1 );
		stack.decrement( 1 );
		NbtCompound nbt = filled.getOrCreateNbt();
		nbt.putInt( "CustomModelData", 1 );
		nbt.putString( "contract_uuid", entity.getUuidAsString() );
		nbt.putString( "contract_name", entity.getDisplayName().getString() );
		nbt.putString( "contract_owner", Contract.getEntityOwner( entity ) );
		Timestamp timestamp = new Timestamp( System.currentTimeMillis() );
		Long time = timestamp.getTime();
		nbt.putString( "created", String.valueOf( time ) );
		PlayerInventory inventory = player.getInventory();
		inventory.offerOrDrop( filled );
	}

	public static boolean canSteal( Entity entity ) {
		return false;
	}

	public static boolean isContractFilled( ItemStack stack ) {
		return !stack.getOrCreateNbt().getString( "contract_uuid" ).isEmpty();
	}

	public static boolean isTargetOwned( PlayerEntity player, Entity entity ) {
		try {
			NbtCompound nbtCompound = new NbtCompound();
			entity.writeNbt( nbtCompound );
			NbtCompound user = new NbtCompound();
			player.writeNbt( user );

			if ( entity instanceof Tameable || nbtCompound.get( "Tame" ) != null || entity instanceof TameableEntity ) {
				if ( nbtCompound.get( "Owner" ) == null ) {
					return false;
				} else return nbtCompound.get( "Owner" ).equals( user.get( "UUID" ) );
			} else {
				Set< String > s = entity.getScoreboardTags();
				for ( String key : s ) {
					if ( key.contains( "Owner:" ) ) {
						String[] split = key.split( ":" );
						return split[ 1 ].equalsIgnoreCase( player.getEntityName() );
					}
				}
			}
			return false;
		} catch ( Exception e ) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean isContractValid( ItemStack stack, Entity entity ) {
		//Contracts are valid if the UUID of the entity matches the UUID of the contract.
		//The owner UUID in the contract must also match the current owner UUID of the entity (outdated contracts are invalid).
		if ( getEntityOwner( entity ) != null ) {
			return ( entity.getUuid().equals( getContractEntity( stack ) ) ) && ( getEntityOwner( entity ).equals( getContractOwner( stack ) ) );
		}
		return false;
	}

	public static UUID getContractEntity( ItemStack stack ) {
		NbtCompound nbt = stack.getOrCreateNbt();
		return UUID.fromString( nbt.getString( "contract_uuid" ) );
	}

	public static String getContractOwner( ItemStack stack ) {
		NbtCompound nbt = stack.getOrCreateNbt();
		return nbt.getString( "contract_owner" );
	}

	public static boolean hasOwner( Entity entity ) {
		NbtCompound nbtCompound = new NbtCompound();
		entity.writeNbt( nbtCompound );
		if ( entity instanceof Tameable || nbtCompound.get( "Tame" ) != null || entity instanceof TameableEntity ) {
			return nbtCompound.get( "Owner" ) != null;
		} else {
			Set< String > s = entity.getScoreboardTags();
			for ( String key : s ) {
				if ( key.contains( "Owner:" ) ) {
					return true;
				}
			}
		}
		return false;
	}

	public static String getEntityOwner( Entity entity ) {
		NbtCompound nbtCompound = new NbtCompound();
		entity.writeNbt( nbtCompound );
		if ( entity instanceof Tameable || nbtCompound.get( "Tame" ) != null || entity instanceof TameableEntity ) {
			if ( nbtCompound.get( "Owner" ) == null ) {
				return null;
			} else {
				return String.valueOf( nbtCompound.get( "Owner" ) );
			}
		} else {
			Set< String > s = entity.getScoreboardTags();
			for ( String key : s ) {
				if ( key.contains( "Owner:" ) ) {
					String[] split = key.split( ":" );
					return split[ 1 ];
				}
			}
		}
		return null;
	}

	public static void setEntityOwner( Entity entity, PlayerEntity player ) {
		NbtCompound nbtCompound = new NbtCompound();
		entity.writeNbt( nbtCompound );
		NbtCompound user = new NbtCompound();
		player.writeNbt( user );
		if ( entity instanceof Tameable || nbtCompound.get( "Tame" ) != null || entity instanceof TameableEntity ) {
			nbtCompound.putUuid( "Owner", player.getUuid() );
			entity.readNbt( nbtCompound );
		} else {
			Set< String > s = entity.getScoreboardTags();
			for ( String key : s ) {
				if ( key.contains( "Owner:" ) ) {
					entity.removeScoreboardTag( key );
				}
			}
			entity.addScoreboardTag( "Owner:" + player.getEntityName() );
		}
	}

	@Override
	public Text getName( ItemStack stack ) {
		if ( isContractFilled( stack ) ) {
			return Text.translatable( "item.elysium.filled_contract" );
		} else {
			return super.getName( stack );
		}
	}

	@Override
	public void appendTooltip( ItemStack stack, World world, List< Text > tooltip, TooltipContext tooltipContext ) {
		if ( isContractFilled( stack ) ) {
			Text entityText = Text.translatable( "text.elysium.tooltip_entity" ).formatted( Formatting.DARK_GRAY ).append( getContractEntityName( stack ) );
			tooltip.add( entityText );
			Text ownerText = Text.translatable( "text.elysium.tooltip_owner" ).formatted( Formatting.DARK_GRAY ).append( getContractOwner( stack ) );
			tooltip.add( ownerText );
		}
	}

	public void transferOwnership( PlayerEntity player, Entity entity ) {
		//String playerName = player.getDisplayName().getString();
		String entityName = entity.getDisplayName().getString();
		//String oldOwner = this.getEntityOwner(entity);
		if ( !player.world.isClient ) {
			player.sendMessage( Text.translatable( "text.elysium.message_new_owner", entityName ), false );
		}
		setEntityOwner( entity, player );
	}

	public String getContractEntityName( ItemStack stack ) {
		NbtCompound nbt = stack.getOrCreateNbt();
		return nbt.getString( "contract_name" );
	}
}

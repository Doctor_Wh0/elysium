package com.enderzombi102.elysium.item;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ArmorMaterial;
import net.minecraft.item.ArmorMaterials;
import net.minecraft.recipe.Ingredient;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.Lazy;

import java.util.function.Supplier;

public class CustomArmorMaterial implements ArmorMaterial{
	private final String name;
	private final int[] protectionAmounts;
	private final int[] durabilityAmounts;
	private final int enchantability;
	private final SoundEvent equipSound;
	private final float toughness;
	private final float knockbackResistance;
	private final Lazy<Ingredient> repairIngredientSupplier;
	public CustomArmorMaterial( ArmorMaterial armorMaterial, Supplier<Ingredient> repairIngredientSupplier) {
		this.name = armorMaterial.getName();
		this.protectionAmounts = new int[] {
			armorMaterial.getProtectionAmount( EquipmentSlot.FEET ),
			armorMaterial.getProtectionAmount( EquipmentSlot.LEGS ),
			armorMaterial.getProtectionAmount( EquipmentSlot.CHEST ),
			armorMaterial.getProtectionAmount( EquipmentSlot.HEAD )};
		this.durabilityAmounts= new int[] {
			armorMaterial.getDurability( EquipmentSlot.FEET ),
			armorMaterial.getDurability( EquipmentSlot.LEGS ),
			armorMaterial.getDurability( EquipmentSlot.CHEST ),
			armorMaterial.getDurability( EquipmentSlot.HEAD )};
		this.enchantability = armorMaterial.getEnchantability();
		this.equipSound = armorMaterial.getEquipSound();
		this.toughness = armorMaterial.getToughness();
		this.knockbackResistance = armorMaterial.getKnockbackResistance();
		this.repairIngredientSupplier = new Lazy<>(repairIngredientSupplier);
	}
	@Override
	public int getDurability( EquipmentSlot slot ) {
		return durabilityAmounts[slot.getEntitySlotId()];
	}

	@Override
	public int getProtectionAmount( EquipmentSlot slot ) {
		return protectionAmounts[slot.getEntitySlotId()];
	}

	@Override
	public int getEnchantability() {
		return enchantability;
	}

	@Override
	public SoundEvent getEquipSound() {
		return equipSound;
	}

	@Override
	public Ingredient getRepairIngredient() {
		return this.repairIngredientSupplier.get();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public float getToughness() {
		return toughness;
	}

	@Override
	public float getKnockbackResistance() {
		return knockbackResistance;
	}
}

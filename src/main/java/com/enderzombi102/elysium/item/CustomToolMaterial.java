package com.enderzombi102.elysium.item;

import net.minecraft.item.ToolMaterial;
import net.minecraft.recipe.Ingredient;
import net.minecraft.util.Lazy;

import java.util.function.Supplier;

public class CustomToolMaterial implements ToolMaterial {
	private final int miningLevel;
	private final int itemDurability;
	private final float miningSpeed;
	private final float attackDamage;
	private final int enchantability;
	private final Lazy<Ingredient> repairIngredient;

	public CustomToolMaterial( ToolMaterial toolMaterial, Supplier repairIngredient ) {
		this.miningLevel =toolMaterial.getMiningLevel() ;
		this.itemDurability = toolMaterial.getDurability();
		this.miningSpeed = toolMaterial.getMiningSpeedMultiplier();
		this.attackDamage = toolMaterial.getAttackDamage();
		this.enchantability = toolMaterial.getEnchantability();
		this.repairIngredient = new Lazy(repairIngredient);
	}
	@Override
	public int getDurability() {
		return itemDurability;
	}

	@Override
	public float getMiningSpeedMultiplier() {
		return miningSpeed;
	}

	@Override
	public float getAttackDamage() {
		return attackDamage;
	}

	@Override
	public int getMiningLevel() {
		return miningLevel;
	}

	@Override
	public int getEnchantability() {
		return enchantability;
	}

	@Override
	public Ingredient getRepairIngredient() {
		return repairIngredient.get();
	}
}

package com.enderzombi102.elysium.item;

import net.minecraft.advancement.criterion.Criteria;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.stat.Stats;
import net.minecraft.util.UseAction;
import net.minecraft.world.World;

public class DrinkItem extends Item {

	public DrinkItem( Item.Settings settings ) {
		super( settings );
	}
	/*@Override
	public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
		if(!world.isClient()) {
			try {
				if ( this.isFood() ) {
					ItemStack itemStack = user.getStackInHand( hand );
					Config.setHungerExpressionValues( ( (IHungerManager) user.getHungerManager() ).spiceOfFabric_getFoodHistory().getTimesEaten( itemStack ), Objects.requireNonNull( ( (Item) (Object) this ).getFoodComponent().getHunger() ), ( (Item) (Object) this ).getFoodComponent().getSaturationModifier(), itemStack.getMaxUseTime() );
					if ( user.canConsume( this.getFoodComponent().isAlwaysEdible() ) && this.getFoodComponent().getHunger() >= 1 && Config.getHungerValue() >= 1 ) {
						user.setCurrentHand( hand );
						return TypedActionResult.consume( itemStack );
					} else {
						return TypedActionResult.fail( itemStack );
					}
				} else {
					return TypedActionResult.pass( user.getStackInHand( hand ) );
				}
			} catch ( Exception e ) {
				e.printStackTrace();
				return TypedActionResult.pass( user.getStackInHand( hand ) );
			}
		}
		return super.use(world,user,hand);
	}*/

	public ItemStack finishUsing( ItemStack stack, World world, LivingEntity user ) {
		super.finishUsing( stack, world, user );
		if ( user instanceof ServerPlayerEntity serverPlayerEntity ) {
			Criteria.CONSUME_ITEM.trigger( serverPlayerEntity, stack );
			serverPlayerEntity.incrementStat( Stats.USED.getOrCreateStat( this ) );
		}
		stack.decrement( 1 );
		return stack.getCount() > 0 ? stack : ItemStack.EMPTY;
	}


	public int getMaxUseTime( ItemStack stack ) {
		return 40;
	}

	public UseAction getUseAction( ItemStack stack ) {
		return UseAction.DRINK;
	}

	public SoundEvent getDrinkSound() {
		return SoundEvents.ENTITY_GENERIC_DRINK;
	}

	public SoundEvent getEatSound() {
		return SoundEvents.ENTITY_GENERIC_DRINK;
	}

}

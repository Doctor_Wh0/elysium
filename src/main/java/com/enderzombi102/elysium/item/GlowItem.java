package com.enderzombi102.elysium.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class GlowItem extends Item {
	public GlowItem(Settings settings){
		super( settings );
	}
	@Override
	public boolean hasGlint( ItemStack stack ) {
		return true;
	}
}

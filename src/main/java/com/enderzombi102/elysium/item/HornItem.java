package com.enderzombi102.elysium.item;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.UseAction;
import net.minecraft.world.World;

import java.util.function.Consumer;

public class HornItem extends Item {

	public HornItem( Settings settings ) {
		super( settings );
	}

	@Override
	public TypedActionResult< ItemStack > use( World world, PlayerEntity user, Hand hand ) {
		ItemStack itemStack = user.getStackInHand( hand );
		if ( !world.isClient() ) {
			ItemStack melodic = ItemStack.EMPTY;
			for ( int i = 0; i < user.getInventory().size(); ++i ) {
				ItemStack itemStack2 = user.getInventory().getStack( i );
				if ( itemStack2.getItem().kjs$getId().equalsIgnoreCase( "elysium:melodic_rune" ) ) {
					melodic = itemStack2;
					i = user.getInventory().size();
				}
			}
			if ( melodic != ItemStack.EMPTY ) {
				user.setCurrentHand( hand );
				user.getItemCooldownManager().set( this, 1200 );
				itemStack.damage( 1, user, new Consumer< PlayerEntity >() {
					@Override
					public void accept( PlayerEntity playerEntity ) {
						itemStack.decrement( 1 );
					}

				} );
				melodic.decrement( 1 );
				return TypedActionResult.consume( itemStack );
			} else {
				user.sendMessage( Text.literal("Missing Melodic Rune").formatted( Formatting.RED ),true);
				return TypedActionResult.fail( itemStack );
			}
		}
		return TypedActionResult.pass( itemStack );
	}

	@Override
	public UseAction getUseAction( ItemStack stack ) {
		return UseAction.TOOT_HORN;
	}
}

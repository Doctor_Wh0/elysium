package com.enderzombi102.elysium.item;

import net.minecraft.item.Item;

import java.util.UUID;

public abstract class ItemAccessor extends Item {
	public ItemAccessor(Item.Settings settings) {
		super(settings);
	}

	public static UUID ATTACK_DAMAGE_MODIFIER_ID() {
		return ATTACK_DAMAGE_MODIFIER_ID;
	}

	public static UUID ATTACK_SPEED_MODIFIER_ID() {
		return ATTACK_SPEED_MODIFIER_ID;
	}
}


package com.enderzombi102.elysium.item;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.imixin.ElysiumClaimBlockEntity;
import com.enderzombi102.elysium.registry.ItemRegistry;
import com.jamieswhiteshirt.rtree3i.Entry;
import draylar.goml.GetOffMyLawn;
import draylar.goml.api.Claim;
import draylar.goml.api.ClaimBox;
import draylar.goml.api.ClaimUtils;
import draylar.goml.block.entity.ClaimAnchorBlockEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class TowerConcession extends Item {
	public TowerConcession( Settings settings ) {
		super( settings );
	}


	@Override
	public TypedActionResult< ItemStack > use( World world, PlayerEntity user, Hand hand ) {
		if ( !world.isClient() ) {
			assert Elysium.server != null;
			if ( hand == Hand.MAIN_HAND ) {
				Set< Entry< ClaimBox, Claim > > s = ClaimUtils.getClaimsOwnedBy( world, user.getUuid() ).filter( claimEntry -> !Objects.equals( claimEntry.getValue().getOrigin(), new BlockPos( 0, 0, -100 ) ) ).collect( Collectors.toSet() );
				if(s.isEmpty()){
					user.sendMessage( Text.literal( "[Tower Concession] No claims available" ) );
					return new TypedActionResult<>( ActionResult.FAIL, new ItemStack( ItemRegistry.ITEMS.get( "tower_concession" ) ) );
				}else {
					s.forEach( claimEntry -> {
						try {
							ClaimAnchorBlockEntity claimAnchorBlockEntity = ClaimUtils.getAnchor( Elysium.server.getOverworld(), claimEntry.getValue() );
							assert claimAnchorBlockEntity != null;
							( (ElysiumClaimBlockEntity) claimAnchorBlockEntity ).elysium$setTowered( true );
							ClaimBox claimBox = claimAnchorBlockEntity.getBox();
							GetOffMyLawn.CLAIM.get(world).remove(claimAnchorBlockEntity.getClaim());

							claimAnchorBlockEntity.getClaim().internal_setClaimBox(new ClaimBox( claimBox.getOrigin(), claimBox.getRadius(), (int) (claimBox.radiusY()*2.5),claimBox.noShift()));
							if (world instanceof ServerWorld world1) {
								claimAnchorBlockEntity.getClaim().internal_updateChunkCount(world1);
							}
							claimAnchorBlockEntity.getClaim().internal_setWorld( claimAnchorBlockEntity.getWorld().kjs$getDimension() );
							GetOffMyLawn.CLAIM.get(world).add(claimAnchorBlockEntity.getClaim());
						}catch(Exception e){
							user.sendMessage( Text.literal( "[Tower Concession] Error setting new height" ) );
							e.printStackTrace();
						}
					} );
					ItemStack itemStack = user.getMainHandStack();
					itemStack.decrement( 1 );
					user.sendMessage( Text.literal( "[Tower Concession] Your claims have doubled its heights" ) );
					user.kjs$setMainHandItem( ItemStack.EMPTY );
					return new TypedActionResult<>( ActionResult.PASS, ItemStack.EMPTY );
				}
			}

		}
		return super.use( world, user, hand );
	}

	@Override
	public void appendTooltip( ItemStack stack, @Nullable World world, List< Text > tooltip, TooltipContext context ) {
		tooltip.add( Text.literal( "Use in your claim to upgrade the height" ) );
		tooltip.add(Text.literal("Caution, if the claims expire you lose the concession").formatted( Formatting.DARK_RED ).formatted( Formatting.BOLD ));
		super.appendTooltip( stack, world, tooltip, context );
	}

	@Override
	public boolean hasGlint( ItemStack stack ) {
		return true;
	}
}

package com.enderzombi102.elysium.ledger;

import com.github.quiltservertools.ledger.actions.AbstractActionType;
import com.github.quiltservertools.ledger.utility.Sources;
import draylar.goml.api.Claim;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.NotNull;


// TODO: Remove (?)
public class EscapeActionType extends AbstractActionType {
	public EscapeActionType( ServerPlayerEntity player, Claim claim ) {
		this.setPos( player.getBlockPos() );
		this.setWorld( player.world.getRegistryKey().getValue() );
		this.setObjectIdentifier( new Identifier( "minecraft", "player" ) );
		this.setSourceName( Sources.PLAYER );
		this.setSourceProfile( player.getGameProfile() );
		this.setExtraData( "claim: `%s`".formatted( claim.getOrigin() ) );
	}

	@Override
	public @NotNull String getIdentifier() {
		return "goml-escape";
	}

	@Override
	public @NotNull String getTranslationType() {
		return "entity";
	}
}

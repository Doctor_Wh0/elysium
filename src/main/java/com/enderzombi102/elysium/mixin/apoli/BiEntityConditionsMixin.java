package com.enderzombi102.elysium.mixin.apoli;

import com.enderzombi102.elysium.Elysium;
import io.github.apace100.apoli.power.factory.condition.BiEntityConditions;
import io.github.apace100.apoli.power.factory.condition.ConditionFactory;
import io.github.apace100.calio.data.SerializableData;
import net.minecraft.entity.Entity;
import net.minecraft.entity.Tameable;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Set;

@Mixin(value = BiEntityConditions.class, remap = false)
public class BiEntityConditionsMixin {

	@Inject(at = @At("TAIL"), method = "register")
	private static void register( CallbackInfo callbackInfo) {
		register(new ConditionFactory<>(new Identifier("elysium", "owner"), new SerializableData(),
			(data, pair) -> {
				NbtCompound nbtCompound = new NbtCompound();
				pair.getRight().writeNbt(nbtCompound);
				NbtCompound user = new NbtCompound();
				pair.getLeft().writeNbt(user);
				if (pair.getRight().getType().isIn( Elysium.ELYSIUM_OWNER)) {
					if (pair.getRight() instanceof Tameable || nbtCompound.get("Tame") != null) {
						if (nbtCompound.get("Owner") == null) {
							return false;
						} else if (nbtCompound.get("Owner").equals(user.get("UUID"))) {
							return true;
						}
					} else {
						Set<String> s = pair.getRight().getScoreboardTags();
						for (String key : s) {
							if (key.contains("Owner:")) {
								String[] split = key.split(":");
								if (split[1].equalsIgnoreCase(pair.getLeft().getEntityName())) {
									return true;
								} else {
									return false;
								}
							}
						}
					}
				}
				return false;
			}
		));
	}

	@Shadow
	private static void register(ConditionFactory< Pair< Entity, Entity> > conditionFactory) {
	}
}

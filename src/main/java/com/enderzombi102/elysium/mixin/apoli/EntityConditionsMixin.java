package com.enderzombi102.elysium.mixin.apoli;

import com.enderzombi102.elysium.Elysium;
import io.github.apace100.apoli.power.factory.condition.ConditionFactory;
import io.github.apace100.apoli.power.factory.condition.EntityConditions;
import io.github.apace100.calio.data.SerializableData;
import net.minecraft.entity.Entity;
import net.minecraft.entity.Tameable;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.Identifier;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Set;

@Mixin( EntityConditions.class)
public class EntityConditionsMixin {

	@Inject(at = @At("TAIL"), method = "register",remap = false)
	private static void register( CallbackInfo callbackInfo) {
		register(new ConditionFactory<>(new Identifier("elysium", "owned"), new SerializableData(),
			(data, entity) -> {
				NbtCompound nbtCompound = new NbtCompound();
				entity.writeNbt(nbtCompound);
				if (entity.getType().isIn( Elysium.ELYSIUM_OWNER)) {
					if (entity instanceof Tameable || nbtCompound.get("Tame") != null) {
						if (nbtCompound.get("Owner") == null) {
							return false;
						} else {
							return true;
						}
					} else {
						Set<String> s = entity.getScoreboardTags();
						for (String key : s) {
							if (key.contains("Owner:")) {
								return true;
							}
						}
					}
				}
				return false;
			}
		));
	}

	@Shadow(remap = false)
	private static void register(ConditionFactory< Entity > conditionFactory) {
	}
}

package com.enderzombi102.elysium.mixin.arrow;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.config.Config;
import net.creep3rcrafter.projectiles.item.BoneArrowItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.function.BiConsumer;

@Mixin( BoneArrowItem.class)
public class BoneArrowItemMixin {
	@Inject( method = "getBaseDamage", at = @At( "HEAD" ), cancellable = true, remap = false )
	public void getBaseDamage( CallbackInfoReturnable< Double > cir ) {
		cir.setReturnValue( 1.5D );
		cir.cancel();
	}
}

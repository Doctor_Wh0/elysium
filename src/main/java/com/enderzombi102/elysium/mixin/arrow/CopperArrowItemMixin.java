package com.enderzombi102.elysium.mixin.arrow;

import net.creep3rcrafter.projectiles.item.CobwebArrowItem;
import net.creep3rcrafter.projectiles.item.CopperArrowItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin( CopperArrowItem.class)
public class CopperArrowItemMixin {
	@Inject( method = "getBaseDamage", at = @At( "HEAD" ), cancellable = true, remap = false )
	public void getBaseDamage( CallbackInfoReturnable< Double > cir ) {
		cir.setReturnValue( 3D );
		cir.cancel();
	}
}

package com.enderzombi102.elysium.mixin.arrow;

import net.creep3rcrafter.projectiles.item.CopperArrowItem;
import net.creep3rcrafter.projectiles.item.DiamondArrowItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin( DiamondArrowItem.class)
public class DiamondArrowItemMixin {
	@Inject( method = "getBaseDamage", at = @At( "HEAD" ), cancellable = true, remap = false )
	public void getBaseDamage( CallbackInfoReturnable< Double > cir ) {
		cir.setReturnValue( 6D );
		cir.cancel();
	}
}

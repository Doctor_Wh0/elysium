package com.enderzombi102.elysium.mixin.arrow;

import net.creep3rcrafter.projectiles.item.EchoArrowItem;
import net.creep3rcrafter.projectiles.item.EnderArrowItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin( EchoArrowItem.class)
public class EchoArrowItemMixin {
	@Inject( method = "getBaseDamage", at = @At( "HEAD" ), cancellable = true, remap = false )
	public void getBaseDamage( CallbackInfoReturnable< Double > cir ) {
		cir.setReturnValue( 3.5D );
		cir.cancel();
	}
}

package com.enderzombi102.elysium.mixin.arrow;

import net.creep3rcrafter.projectiles.item.GoldArrowItem;
import net.creep3rcrafter.projectiles.item.NetheriteArrowItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin( NetheriteArrowItem.class)
public class NetherQuartzArrowItemMixin {
	@Inject( method = "getBaseDamage", at = @At( "HEAD" ), cancellable = true, remap = false )
	public void getBaseDamage( CallbackInfoReturnable< Double > cir ) {
		cir.setReturnValue( 3.5D );
		cir.cancel();
	}
}

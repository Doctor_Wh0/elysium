package com.enderzombi102.elysium.mixin.arrow;

import net.creep3rcrafter.projectiles.item.NetheriteArrowItem;
import net.creep3rcrafter.projectiles.item.ObsidianArrowItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin( ObsidianArrowItem.class)
public class ObsidianArrowItemMixin {
	@Inject( method = "getBaseDamage", at = @At( "HEAD" ), cancellable = true, remap = false )
	public void getBaseDamage( CallbackInfoReturnable< Double > cir ) {
		cir.setReturnValue( 5D );
		cir.cancel();
	}
}

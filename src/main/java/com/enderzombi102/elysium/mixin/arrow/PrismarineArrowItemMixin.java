package com.enderzombi102.elysium.mixin.arrow;

import net.creep3rcrafter.projectiles.item.ObsidianArrowItem;
import net.creep3rcrafter.projectiles.item.PrismarineArrowItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin( PrismarineArrowItem.class)
public class PrismarineArrowItemMixin {
	@Inject( method = "getBaseDamage", at = @At( "HEAD" ), cancellable = true, remap = false )
	public void getBaseDamage( CallbackInfoReturnable< Double > cir ) {
		cir.setReturnValue( 4.5D );
		cir.cancel();
	}
}

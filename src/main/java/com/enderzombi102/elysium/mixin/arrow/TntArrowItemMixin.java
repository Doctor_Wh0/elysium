package com.enderzombi102.elysium.mixin.arrow;

import com.enderzombi102.elysium.config.Config;
import net.creep3rcrafter.projectiles.item.BambooArrowItem;
import net.creep3rcrafter.projectiles.item.TNTArrowItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin( TNTArrowItem.class)
public class TntArrowItemMixin {
	@Inject( method = "getBaseDamage", at = @At( "HEAD" ), cancellable = true , remap = false)
	public void getBaseDamage( CallbackInfoReturnable< Double > cir ) {
		cir.setReturnValue( 1.5D );
		cir.cancel();
	}
}

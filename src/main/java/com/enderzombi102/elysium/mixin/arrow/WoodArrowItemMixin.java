package com.enderzombi102.elysium.mixin.arrow;

import com.enderzombi102.elysium.config.Config;
import net.creep3rcrafter.projectiles.item.ChorusArrowItem;
import net.creep3rcrafter.projectiles.item.WoodArrowItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin( WoodArrowItem.class)
public class WoodArrowItemMixin {
	@Inject( method = "getBaseDamage", at = @At( "HEAD" ), cancellable = true, remap = false )
	public void getBaseDamage( CallbackInfoReturnable< Double > cir ) {
		cir.setReturnValue( 1D );
		cir.cancel();
	}
}

package com.enderzombi102.elysium.mixin.artifacts;

import artifacts.common.init.ModItems;
import artifacts.common.trinkets.TrinketsHelper;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.LivingEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin( EnchantmentHelper.class)
public class EnchantamentHelperMixin {

	@Inject(
		method = {"getKnockback"},
		at = {@At("RETURN")},
		cancellable = true
	)
	private static void increaseKnockback( LivingEntity entity, CallbackInfoReturnable<Integer> info) {
		if ( TrinketsHelper.isEquipped( ModItems.POCKET_PISTON, entity)) {
			info.setReturnValue(1);
		}

	}
}

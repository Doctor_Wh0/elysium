package com.enderzombi102.elysium.mixin.artifacts;

import artifacts.Artifacts;
import artifacts.common.item.curio.CurioItem;
import artifacts.common.item.curio.hands.FeralClawsItem;
import com.google.common.collect.Multimap;
import dev.emi.trinkets.api.SlotReference;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import java.util.UUID;

@Mixin( FeralClawsItem.class )
public class FeralClawsItemMixin extends CurioItem {
	/**
	 * @author Doc
	 * @reason Changed multiplayer
	 */
	@Overwrite
	protected Multimap< EntityAttribute, EntityAttributeModifier > applyModifiers( ItemStack stack, SlotReference slot, LivingEntity entity, UUID uuid) {
		Multimap<EntityAttribute, EntityAttributeModifier> result = super.applyModifiers(stack, slot, entity, uuid);
		EntityAttributeModifier modifier = new EntityAttributeModifier(uuid, Artifacts.id("feral_claws_attack_speed").toString(), 0.2, EntityAttributeModifier.Operation.MULTIPLY_TOTAL);
		result.put( EntityAttributes.GENERIC_ATTACK_SPEED, modifier);
		return result;
	}
}

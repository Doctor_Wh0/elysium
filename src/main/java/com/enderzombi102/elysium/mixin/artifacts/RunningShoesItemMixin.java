package com.enderzombi102.elysium.mixin.artifacts;

import artifacts.Artifacts;
import artifacts.common.item.curio.CurioItem;
import artifacts.common.item.curio.feet.RunningShoesItem;
import com.google.common.collect.Multimap;
import dev.emi.trinkets.api.SlotReference;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.UUID;
@Mixin( RunningShoesItem.class)
public class RunningShoesItemMixin extends CurioItem {
	@Final
	@Shadow
	@Mutable
	public static EntityAttributeModifier SPEED_BOOST_MODIFIER;
	@Final
	@Shadow
	@Mutable
	public static EntityAttributeModifier STEP_HEIGHT_MODIFIER;

	@Inject( at = @At( "HEAD" ), method = "<clinit>", cancellable = true )
	private static void init( CallbackInfo ci ){
		SPEED_BOOST_MODIFIER = new EntityAttributeModifier(UUID.fromString("ac7ab816-2b08-46b6-879d-e5dea34ff305"), "artifacts:running_shoes_movement_speed", 0.2, EntityAttributeModifier.Operation.MULTIPLY_TOTAL);
		STEP_HEIGHT_MODIFIER = new EntityAttributeModifier(UUID.fromString("7e97cede-a343-411f-b465-14cdf6df3666"), "artifacts:running_shoes_step_height", 0.5, EntityAttributeModifier.Operation.ADDITION);
		ci.cancel();
	}

}

package com.enderzombi102.elysium.mixin.chat;

import com.llamalad7.mixinextras.injector.ModifyReceiver;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.tree.CommandNode;
import me.lucko.fabric.api.permissions.v0.Permissions;
import net.minecraft.server.command.MessageCommand;
import net.minecraft.server.command.ServerCommandSource;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin( MessageCommand.class )
public class MessageCommandMixin {
	@ModifyReceiver(
		method = "register",
		at = @At(
			value = "INVOKE",
			target = "Lcom/mojang/brigadier/builder/LiteralArgumentBuilder;then(Lcom/mojang/brigadier/builder/ArgumentBuilder;)Lcom/mojang/brigadier/builder/ArgumentBuilder;",
			ordinal = 0,
			remap = false
		)
	)
	private static LiteralArgumentBuilder< ServerCommandSource > addPermissions( LiteralArgumentBuilder< ServerCommandSource > literal, ArgumentBuilder< ServerCommandSource, ? > argumentBuilder ) {
		return literal.requires( it -> Permissions.check( it, "elysium.msg" ) );
	}

	@ModifyReceiver(
		method = "register",
		at = @At(
			value = "INVOKE",
			target = "Lcom/mojang/brigadier/builder/LiteralArgumentBuilder;redirect(Lcom/mojang/brigadier/tree/CommandNode;)Lcom/mojang/brigadier/builder/ArgumentBuilder;",
			remap = false
		)
	)
	private static LiteralArgumentBuilder< ServerCommandSource > addPermissions1( LiteralArgumentBuilder< ServerCommandSource > literal, CommandNode< ServerCommandSource > node ) {
		return literal.requires( it -> Permissions.check( it, "elysium.msg" ) );
	}
}

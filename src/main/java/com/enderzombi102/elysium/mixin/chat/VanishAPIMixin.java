package com.enderzombi102.elysium.mixin.chat;

import com.enderzombi102.elysium.config.Config;
import me.drex.vanish.api.VanishAPI;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableTextContent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin( VanishAPI.class )
public interface VanishAPIMixin {
	@Inject(
		method = "broadcastHiddenMessage",
		at = @At( "HEAD" ),
		cancellable = true
	)
	private static void cancelSystemMessages( ServerPlayerEntity player, Text component, CallbackInfo ci ) {
		if ( !Config.getData().features.disableSystemMessages )
			return;

		if ( component.getContent() instanceof TranslatableTextContent trans && trans.getKey().startsWith( "multiplayer.player." ) )
			ci.cancel();
	}
}

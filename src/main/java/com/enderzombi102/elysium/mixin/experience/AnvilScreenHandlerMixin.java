package com.enderzombi102.elysium.mixin.experience;

import com.enderzombi102.elysium.config.Config;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.screen.Property;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerType;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin( AnvilScreenHandler.class )
public abstract class AnvilScreenHandlerMixin extends ScreenHandler {
	protected AnvilScreenHandlerMixin( @Nullable ScreenHandlerType< ? > type, int syncId ) {
		super( type, syncId );
	}

	@Redirect(
		method = "updateResult",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/screen/Property;set(I)V"
		)
	)
	private void makeLevelCostZero( Property property, int value ) {
		property.set( Config.getData().features.disableExperience ? 0 : value );
	}

	@Redirect(
		method = "canTakeOutput",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/screen/Property;get()I",
			ordinal = 1
		)
	)
	private int makeLevelCostZero( Property property ) {
		if ( Config.getData().features.disableExperience )
			return 1;

		return property.get();
	}
}

package com.enderzombi102.elysium.mixin.experience;

import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.config.ExperienceData;
import net.minecraft.block.entity.AbstractFurnaceBlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.projectile.thrown.ExperienceBottleEntity;
import net.minecraft.server.world.ServerEntityManager;
import net.minecraft.util.math.Box;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.entity.EntityLike;
import net.minecraft.world.entity.EntityLookup;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.enderlib.Functional.ifNull;

@Mixin( ServerEntityManager.class )
public abstract class ServerEntityManagerMixin< T extends EntityLike > {
	@Unique
	private static boolean isExpAllowedFrom( @NotNull ExperienceData config, @NotNull MobEntity mob ) {
		var type = Registry.ENTITY_TYPE.getId( mob.getType() );
		if ( config.allowedMobSources.contains( type.toString() ) ) {
			if ( Config.getData().logging.experienceBlocker )
				LOGGER.info( "[Elysium] Mob matched allowed type `{}`", type );
			return true;
		}

		if ( mob instanceof HostileEntity && config.allowedMobSources.contains( "elysium:hostile" ) ) {
			if ( Config.getData().logging.experienceBlocker )
				LOGGER.info( "[Elysium] Mob matched allowed type `elysium:hostile`" );
			return true;

		} else if ( mob instanceof AnimalEntity && config.allowedMobSources.contains( "elysium:animal" ) ) {
			if ( Config.getData().logging.experienceBlocker )
				LOGGER.info( "[Elysium] Mob matched allowed type `elysium:animal`" );
			return true;
		}

		return false;
	}

	@Shadow
	public abstract EntityLookup< T > getLookup();

	@Inject(
		method = "addEntity(Lnet/minecraft/world/entity/EntityLike;Z)Z",
		at = @At( "HEAD" ),
		cancellable = true
	)
	public void removeExperienceEntities( T entity, boolean existing, CallbackInfoReturnable< Boolean > cir ) {
		if ( !Config.getData().features.disableExperience )
			return;

		if ( existing || !( entity instanceof Entity ) )
			return;

		var world = ( (Entity) entity ).world;
		var id = world.getRegistryKey().getValue().toString();
		var config = ifNull(
			Config.getData().experienceSettings.get( id ), // then
			() -> Config.getData().experienceSettings.get( "elysium:default" )
		);

		if ( config == null ) {
			LOGGER.error( "[Elysium] Experience setting for dimension `{}` is missing, and there are no defaults, will do nothing!", id );
			return;
		}

		if ( entity instanceof ExperienceOrbEntity ) {
			if ( Config.getData().logging.experienceBlocker )
				LOGGER.info(
					"[Elysium] Experience config for `{}`{}: enabled={}, furnace={}, bottled={}, mobs={}",
					id,
					Config.getData().experienceSettings.containsKey( id ) ? "" : " ( using defaults )",
					config.enable,
					config.allowFurnaceExp,
					config.allowBottledExp,
					config.allowedMobSources
				);

			if ( !config.enable )
				return;

			var prevent = new boolean[] { true };
			this.getLookup().forEachIntersects(
				new Box( entity.getBlockPos() ),
				it -> {
					if ( !prevent[ 0 ] )
						return;

					if ( it instanceof MobEntity mob && isExpAllowedFrom( config, mob ) )
						prevent[ 0 ] = false;

					else if ( it instanceof ExperienceBottleEntity && config.allowBottledExp )
						prevent[ 0 ] = false;

					else if ( it instanceof AbstractFurnaceBlockEntity && config.allowFurnaceExp )
						prevent[ 0 ] = false;
				}
			);
			if ( prevent[ 0 ] )
				cir.setReturnValue( false );
		}
	}
}

package com.enderzombi102.elysium.mixin.falling;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.FallingBlock;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin( FallingBlock.class)
public abstract class FallingBlockMixin extends Block {

	public FallingBlockMixin( Settings settings ) {
		super( settings );
	}

	@Inject(method = "scheduledTick", at = @At("HEAD"), cancellable = true)
	private void cancelFalling( BlockState state, ServerWorld world, BlockPos pos, Random random, CallbackInfo ci ) {
		ci.cancel();
	}

	@Inject(method = "randomDisplayTick", at = @At("HEAD"), cancellable = true)
	private void cancelFallingParticle( BlockState state, World world, BlockPos pos, Random random, CallbackInfo ci ) {
		ci.cancel();
	}
}

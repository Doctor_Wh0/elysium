package com.enderzombi102.elysium.mixin.falling;

import net.minecraft.block.BlockState;
import net.minecraft.block.PointedDripstoneBlock;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.WorldView;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin( PointedDripstoneBlock.class)
public abstract class PointedDripstoneBlockMixin {
	@Shadow
	protected static boolean isPointingUp( BlockState state ) {
		return false;
	}

	@Shadow
	public abstract boolean canPlaceAt( BlockState state, WorldView world, BlockPos pos );

	@Inject( method="scheduledTick", at=@At(value="HEAD"),cancellable = true )
	public void scheduled( BlockState state, ServerWorld world, BlockPos pos, Random random, CallbackInfo ci ){
		if (this.isPointingUp(state) && !this.canPlaceAt(state, world, pos)) {
			world.breakBlock(pos, true);
		}
		ci.cancel();
	}

	@Inject(method="dripTick",at=@At(value="HEAD"),cancellable = true)
	private static void noFarm( BlockState state, ServerWorld world, BlockPos pos, float dripChance, CallbackInfo ci ){
		ci.cancel();
	}
}

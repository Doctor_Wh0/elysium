package com.enderzombi102.elysium.mixin.goml;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.imixin.ElysiumClaimBlockEntity;
import com.enderzombi102.elysium.registry.CompRegistry;
import com.enderzombi102.elysium.screen.ClaimAnchorGuiDescription;
import com.enderzombi102.elysium.util.TaxesUtil;
import com.jamieswhiteshirt.rtree3i.Box;
import com.jamieswhiteshirt.rtree3i.Entry;
import draylar.goml.GetOffMyLawn;
import draylar.goml.api.Claim;
import draylar.goml.api.ClaimBox;
import draylar.goml.api.ClaimUtils;
import draylar.goml.api.event.ClaimEvents;
import draylar.goml.block.entity.ClaimAnchorBlockEntity;
import draylar.goml.registry.GOMLBlocks;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import org.apfloat.Apfloat;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.enderzombi102.elysium.Elysium.ENTITY_REMOVE;
import static com.enderzombi102.elysium.Elysium.LOGGER;


@Mixin( ClaimAnchorBlockEntity.class )
public abstract class ClaimAnchorBlockEntityMixin extends BlockEntity implements ExtendedScreenHandlerFactory, ElysiumClaimBlockEntity {
	@Shadow
	private ClaimBox box;
	@Shadow
	private Claim claim;
	@Unique
	private long elysium$time = 0;
	@Unique
	private UUID elysium$owner = null;
	@Unique
	private String elysium$oldowner = null;
	@Unique
	private UUID elysium$old = null;
	@Unique
	private String elysium$timemethod = null;
	@Unique
	private HashMap< UUID, Long > elysium$carpenters = new HashMap<>();
	@Unique
	private boolean elysium$towered = false;

	public ClaimAnchorBlockEntityMixin( BlockEntityType< ? > type, BlockPos pos, BlockState state ) {
		super( type, pos, state );
	}

	@Inject(
		method = "tick",
		at = @At(
			value = "FIELD",
			target = "Ldraylar/goml/block/entity/ClaimAnchorBlockEntity;claim:Ldraylar/goml/api/Claim;",
			shift = At.Shift.BEFORE,
			ordinal = 2
		),
		locals = LocalCapture.CAPTURE_FAILHARD,
		cancellable = true
	)
	private static < T extends BlockEntity > void changeIfExpired( World eWorld, BlockPos pos, BlockState state, T entity, CallbackInfo ci, ServerWorld world, ClaimAnchorBlockEntity anchor ) {
		try {
			if ( !Config.getData().features.claimModifications )
				return;

			var config = Config.getData().claims.taxes;

			if ( config.anchorDropDays == -1 )
				return;

			var claim = anchor.getClaim();
			assert claim != null : "Wat";

			var self = (ClaimAnchorBlockEntityMixin) (Object) anchor;

			// try to cache the current owner's UUID
			if ( self.elysium$owner == null )
				self.elysium$owner = claim
					.getOwners()
					.stream()
					.findFirst()
					.orElse( null );

			if ( self.elysium$owner != null ) {
				assert Elysium.server != null : "Server is null when it shouldn't be";
				var comp = CompRegistry.ELYSIUM_LEVEL.get( Elysium.server.getSaveProperties() );
				// check if the timer was corrupted
				if ( self.elysium$time != 0 && comp.getAnchorStamp( self.elysium$owner ) == 0 ) {
					// TODO: FIX
					LOGGER.warn( "[Elysium] Backup timestamp not valid, setting backup for {}", pos.toShortString() );
					comp.setAnchorStamp( self.elysium$owner, self.elysium$time );
				}
				try {
					if ( !comp.getTimeMethod( self.elysium$owner ).equals( self.elysium$timemethod ) ) {
						self.elysium$timemethod = comp.getTimeMethod( self.elysium$owner );
					}
					if ( self.elysium$timemethod == null ) {
						var remainingTime = Math.abs( System.currentTimeMillis() - comp.getAnchorStamp( self.elysium$owner ) );
						var original = comp.getAnchorStamp( self.elysium$owner );
						comp.setAnchorStamp( self.elysium$owner, world.getTime() + ( remainingTime / 1000 ) * 20 );
						comp.setTimeMethod( self.elysium$owner, "server" );
						BlockEntity b = world.getBlockEntity( pos );
						NbtCompound n = b.createNbtWithIdentifyingData();
						n.putString( "TimeMethod", "server" );
						b.readNbt( n );
						LOGGER.info( "[Elysium] Time " + original + " converted to " + comp.getAnchorStamp( self.elysium$owner ) + " at " + pos.toShortString() );
					}
				}catch(Exception ignored){}
				if ( self.elysium$time != comp.getAnchorStamp( self.elysium$owner ) ) {
					self.elysium$time = comp.getAnchorStamp( self.elysium$owner );
				}
				if ( !self.elysium$carpenters.isEmpty() ) {
					self.elysium$carpenters.forEach( new BiConsumer< UUID, Long >() {
						@Override
						public void accept( UUID uuid, Long aLong ) {
							if ( aLong < Elysium.server.getWorld( ServerWorld.OVERWORLD ).getTime() ) {
								self.claim.untrust( uuid );
								self.elysium$carpenters.remove( uuid );
							}
						}
					} );
				}
				if ( ( self.elysium$time < Elysium.server.getWorld( ServerWorld.OVERWORLD ).getTime() && self.elysium$time != 0 && !self.getClaim().getType().equals( GOMLBlocks.ADMIN_CLAIM_ANCHOR.getFirst() ) ) ) {
					var anchorStamp = comp.getAnchorStamp( self.elysium$owner );
					if ( world.getTime() - anchorStamp > 0 ) {
						var claimInfo = anchor.getClaim();
						Claim c = new Claim( claimInfo.getOwners(), claimInfo.getTrusted(), new BlockPos( 0, 0, -100 ) );
						c.internal_setClaimBox( ClaimUtils.createClaimBox( new BlockPos( 0, 0, -100 ), 1 ) );
						claimInfo.getOwners().clear();
						claimInfo.getTrusted().clear();
						var newBox = claimInfo.getClaimBox();
						GetOffMyLawn.CLAIM.get( world ).remove( claim );
						GetOffMyLawn.CLAIM.get( world ).add( c );
						BlockEntity oldBE = world.getBlockEntity( pos );
						world.setBlockState( pos, GOMLBlocks.ADMIN_CLAIM_ANCHOR.getFirst().getDefaultState() );
						claimInfo.internal_setIcon( GOMLBlocks.ADMIN_CLAIM_ANCHOR.getSecond().getDefaultStack() );
						claimInfo.internal_setType( GOMLBlocks.ADMIN_CLAIM_ANCHOR.getFirst() );
						claimInfo.internal_setClaimBox( newBox );
						claimInfo.internal_updateChunkCount( world );
						claimInfo.internal_setWorld( anchor.getClaim().getWorld() );
						try {
							PlayerManager playerManager = Elysium.server.getPlayerManager();
							ServerPlayerEntity serverPlayerEntity = playerManager.getPlayer( self.elysium$owner );
							GetOffMyLawn.CLAIM.get( world ).add( claimInfo );
							BlockEntity newBE = world.getBlockEntity( pos );
							if ( oldBE instanceof ClaimAnchorBlockEntity && newBE instanceof ClaimAnchorBlockEntity ) {
								( (ClaimAnchorBlockEntity) newBE ).from( ( (ClaimAnchorBlockEntity) oldBE ) );
							}
							ClaimEvents.CLAIM_RESIZED.invoker().onResizeEvent( claimInfo, anchor.getBox(), newBox );
							LOGGER.warn( "[Elysium-Ledger] Replaced claim anchor at {} (owner: {})", pos.toShortString(), serverPlayerEntity.getEntityName() );
						} catch ( Exception ignored ) {
						}
						ci.cancel();
					}
					//} else if ( ( self.elysium$time + (long) Config.getData().claims.daysAfterAdminClaim * 24 * 60 * 60 * 20 ) < Elysium.server.getWorld( ServerWorld.OVERWORLD ).getTime() && self.elysium$time != 0 ) {
				} else if ( ( self.elysium$time + 15 * 20 ) < Elysium.server.getWorld( ServerWorld.OVERWORLD ).getTime() && self.elysium$time != 0 ) {
					deleteAllPreciousBlocks( world.getChunk( pos ), anchor );
					world.breakBlock( pos, false );
					PlayerManager playerManager = Elysium.server.getPlayerManager();
					ServerPlayerEntity serverPlayerEntity = playerManager.getPlayer( self.elysium$old );
					Set< Entry< ClaimBox, Claim > > response = ClaimUtils.getClaimsOwnedBy( world,
						serverPlayerEntity.getUuid() ).filter( new Predicate< Entry< ClaimBox, Claim > >() {
						@Override
						public boolean test( Entry< ClaimBox, Claim > claimBoxClaimEntry ) {
							return Objects.equals( claimBoxClaimEntry.getValue().getOrigin(), new BlockPos( 0, 0, -100 ) );
						}
					} ).collect( Collectors.toSet() );
					if ( response.stream().findFirst().get() != null )
						GetOffMyLawn.CLAIM.get( world ).remove( response.stream().findFirst().get().getValue() );
					LOGGER.warn( "[Elysium-Ledger] Broken admin claim anchor at {} (owner: {})", pos.toShortString(), self.elysium$oldowner );
					ci.cancel();
				}
			}
		} catch ( Exception e ) {
			LOGGER.warn( "[Elysium] Error with claim at pos " + pos.toShortString() );
			e.printStackTrace();
		}
	}

	@Unique
	private static void deleteAllPreciousBlocks( Chunk chunk, ClaimAnchorBlockEntity claimAnchorBlockEntity ) {
		Box b = ClaimUtils.getClaimsAt( claimAnchorBlockEntity.getWorld(), claimAnchorBlockEntity.getPos() ).collect( Collectors.toList() ).get( 0 ).getKey().toBox();
		for ( int i = b.x1(); i <= b.x2(); i++ ) {
			for ( int j = b.y1(); j < b.y2(); j++ ) {
				for ( int k = b.z1(); k <= b.z2(); k++ ) {
					BlockPos blockPos = new BlockPos( i, j, k );
					BlockState blockState = chunk.getBlockState( blockPos );
					try {
						if ( !blockState.isAir() && blockState.isIn( Elysium.BLOCK_REMOVE ) ) {
							if ( BlockEntityType.CHEST.supports( blockState ) ) {
								BlockEntityType.CHEST.get( claimAnchorBlockEntity.getWorld(), blockPos ).clear();
								continue;
							}
							claimAnchorBlockEntity.getWorld().breakBlock( blockPos, false );
						}
					} catch ( Exception ignored ) {
					}
				}
			}
		}
		ArrayList< Entity > entityArrayList = (ArrayList< Entity >) claimAnchorBlockEntity.getWorld().getOtherEntities( (Entity) null, new net.minecraft.util.math.Box( b.x1(), b.y1(), b.z1(), b.x2(), b.y2(), b.z2() ), EntityPredicates.EXCEPT_SPECTATOR );
		for ( Entity e : entityArrayList )
			if ( !e.isPlayer() ) {
				if ( e.getType().isIn( ENTITY_REMOVE ) || String.valueOf( e.getType() ).equalsIgnoreCase( "entity.minecraft.item" ) || e.kjs$isAnimal() ) {
					e.kill();
				}
			}
		ArrayList< Entity > e2 = (ArrayList< Entity >) claimAnchorBlockEntity.getWorld().getOtherEntities( (Entity) null, new net.minecraft.util.math.Box( b.x1(), b.y1(), b.z1(), b.x2(), b.y2(), b.z2() ), EntityPredicates.EXCEPT_SPECTATOR );
		for ( Entity e : e2 )
			if ( !e.isPlayer() ) {
				if ( e.getType().isIn( ENTITY_REMOVE ) || String.valueOf( e.getType() ).equalsIgnoreCase( "entity.minecraft.item" ) || e.kjs$isAnimal() ) {
					e.kill();
				}
			}
	}

	@Shadow
	public abstract @Nullable Claim getClaim();

	@Shadow
	public abstract @Nullable ClaimBox getBox();

	@Shadow
	public abstract void setClaim( Claim claim, ClaimBox box );

	@Inject(
		method = "writeNbt",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/block/entity/BlockEntity;writeNbt(Lnet/minecraft/nbt/NbtCompound;)V",
			shift = At.Shift.BEFORE
		)
	)
	private void saveAdditionalData( NbtCompound tag, CallbackInfo ci ) {
		tag.putLong( "Elysium$Time", this.elysium$time );
		if ( elysium$oldowner != null ) {
			tag.putString( "Old", elysium$oldowner );
		}
		try {
			tag.putBoolean( "Towered", elysium$towered );
		}catch(Exception ignored){}
		if ( elysium$timemethod != null ) {
			tag.putString( "TimeMethod", this.elysium$timemethod );
		}
	}

	@Inject(
		method = "readNbt",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/block/entity/BlockEntity;readNbt(Lnet/minecraft/nbt/NbtCompound;)V",
			shift = At.Shift.BEFORE
		)
	)
	private void loadAdditionalData( NbtCompound tag, CallbackInfo ci ) {
		if ( !tag.contains( "Elysium$Time" ) )
			LOGGER.warn( "[Elysium] Anchor at {} is missing time data!!!", this.pos );
		this.elysium$time = tag.getLong( "Elysium$Time" );
		try {
			this.elysium$oldowner = tag.getString( "Old" );
		} catch ( Exception e ) {
			elysium$oldowner = null;
		}
		try {
			this.elysium$towered=tag.getBoolean( "Towered" );
		}catch(Exception ignored){}
		/*try {
			this.elysium$old= tag.getUuid( "OldOwner_UUID" );
		} catch ( Exception e ) {
			elysium$old = null;
		}*/
		try {
			this.elysium$timemethod = tag.getString( "TimeMethod" );
		} catch ( Exception e ) {
			elysium$timemethod = null;
		}

	}

	@Inject( method = "from", at = @At( "TAIL" ), remap = false )
	public void rememberPreUpgradeValues( ClaimAnchorBlockEntity old, CallbackInfo ci ) {

		assert old != null : "Why are we transferring from an non-existent entity? wtf";
		var mixin = (ClaimAnchorBlockEntityMixin) (Object) old;
		this.elysium$time = mixin.elysium$time;
		this.elysium$owner = mixin.elysium$owner;
		this.elysium$towered=mixin.elysium$towered;
		try {
			assert world != null;
			if ( world.getBlockState( old.getPos() ) == GOMLBlocks.ADMIN_CLAIM_ANCHOR.getFirst().getDefaultState() && mixin.elysium$owner != null ) {
				PlayerManager playerManager = Elysium.server.getPlayerManager();
				ServerPlayerEntity serverPlayerEntity = playerManager.getPlayer( mixin.elysium$owner );
				this.elysium$oldowner = serverPlayerEntity.getEntityName();
				this.elysium$old = elysium$owner;
			}
		} catch ( Exception e ) {
			LOGGER.info( "Error Old Owner at pos " + old.getPos().toShortString() );
		}
	}

	// ExtendedScreenHandler
	@Override
	public Text getDisplayName() {
		// Using the block name as the screen title
		return Text.literal("");
	}

	@Override
	public ScreenHandler createMenu( int syncId, PlayerInventory inventory, PlayerEntity player ) {
		if ( Config.getData().logging.claimAnchorScreen )
			LOGGER.info( "[Elysium] Opening modified claim anchor screen for `@{}`", player.getEntityName() );

		var buf = PacketByteBufs.create();
		this.writeScreenOpeningData( (ServerPlayerEntity) player, buf );
		return new ClaimAnchorGuiDescription( syncId, inventory, buf, ScreenHandlerContext.create( this.world, this.pos ) );
	}

	@Override
	public void writeScreenOpeningData( ServerPlayerEntity player, PacketByteBuf buf ) {
		buf.writeLong( this.elysium$time );
		buf.writeInt( TaxesUtil.getDistance( this.pos ) );
		buf.writeString( TaxesUtil.getClaimPup( this.pos ).toString() );
	}

	// ElysiumClaimAnchorBlockEntity
	@Override
	public void elysium$addTime( long amount ) {
		assert Elysium.server != null : "Running server code on a client? wut";
		CompRegistry.IMPERIUM_LEVEL.get( Elysium.server.getSaveProperties() ).addBalance( amount );

		assert world != null;
		var currentTime = world.getTime();

		if ( this.elysium$time < currentTime )
			this.elysium$time = currentTime;

		var pup = TaxesUtil.getClaimPup( this.getPos() );

		var toAdd = new Apfloat( 86400 )
			.divide( pup )
			.multiply( new Apfloat( amount ) )
			.multiply( new Apfloat( 20 ) );
		this.elysium$time += toAdd.longValue();
		this.markDirty();

		if ( this.elysium$owner != null )
			CompRegistry.ELYSIUM_LEVEL
				.get( Elysium.server.getSaveProperties() )
				.setAnchorStamp( this.elysium$owner, this.elysium$time );
		else
			LOGGER.warn( "[Elysium] Owner was somehow null at {}", this.getPos() );
	}

	@Override
	public long elysium$getTime() {
		return this.elysium$time;
	}

	@Override
	public void elysium$setTime( long time ) {
		this.elysium$time = time;
	}

	@Override
	public UUID elysium$getOld() {
		return this.elysium$old;
	}

	@Override
	public void elysium$addCarpenter( UUID carpenter, Long value ) {
		this.claim.trust( carpenter );
		if ( !this.elysium$carpenters.containsKey( carpenter ) )
			this.elysium$carpenters.put( carpenter, value );
	}

	@Override
	public void elysium$removeCarpenter( UUID carpenter ) {
		this.elysium$carpenters.remove( carpenter );
	}

	@Override
	public HashMap< UUID, Long > elysium$getCarpenters() {
		return this.elysium$carpenters;
	}
	@Override
	public void elysium$setTowered(boolean towered) {
		this.elysium$towered = towered;
		}
	@Override
	public boolean elysium$getTowered() {
		return this.elysium$towered;
	}
}

package com.enderzombi102.elysium.mixin.goml;

import com.jamieswhiteshirt.rtree3i.Box;
import com.jamieswhiteshirt.rtree3i.Entry;
import com.jamieswhiteshirt.rtree3i.Selection;
import draylar.goml.GetOffMyLawn;
import draylar.goml.api.Claim;
import draylar.goml.api.ClaimBox;
import draylar.goml.api.ClaimUtils;
import draylar.goml.block.ClaimAnchorBlock;
import draylar.goml.item.ClaimAnchorBlockItem;
import me.lucko.fabric.api.permissions.v0.Options;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkSectionPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.*;

import java.util.Objects;
import java.util.Optional;

import static com.enderzombi102.elysium.util.Util.sonoAttaccato;

@Mixin( ClaimAnchorBlockItem.class )
public abstract class ClaimAnchorBlockItemMixin extends BlockItem {
	@Final
	@Shadow( remap = false )
	private ClaimAnchorBlock claimBlock;

	public ClaimAnchorBlockItemMixin( Block block, Settings settings ) {
		super( block, settings );
	}

	/**
	 * @author Doc
	 * @reason WIP
	 */
	@Overwrite
	protected boolean canPlace( ItemPlacementContext context, BlockState state ) {
		if ( context.getWorld().isClient ) {
			return true;
		} else {
			BlockPos pos = context.getBlockPos();
			int radius = this.claimBlock.getRadius();
			if ( radius <= 0 && !ClaimUtils.isInAdminMode( Objects.requireNonNull( context.getPlayer() ) ) ) {
				context.getPlayer().sendMessage( GetOffMyLawn.CONFIG.prefix( Text.translatable( "text.goml.cant_place_claim.admin_only" ).formatted( Formatting.RED ) ), false );
				return false;
			} else {
				radius = Math.max( radius, 1 );
				ClaimBox checkBox = ClaimUtils.createClaimBox( pos, radius );
				if ( !ClaimUtils.isInAdminMode( Objects.requireNonNull( context.getPlayer() ) ) ) {
					int count = ClaimUtils.getClaimsOwnedBy( context.getWorld(), context.getPlayer().getUuid() ).count();
					Optional< String > allowedCount = Options.get( context.getPlayer(), "goml.claim_limit" );
					Optional< String > allowedCount2 = Options.get( context.getPlayer(), "goml.claim_limit." + context.getWorld().getRegistryKey().getValue().toString() );
					int maxCount;
					if ( allowedCount2.isPresent() ) {
						try {
							maxCount = Integer.parseInt( allowedCount2.get() );
						} catch ( Throwable var12 ) {
							maxCount = GetOffMyLawn.CONFIG.maxClaimsPerPlayer;
						}
					} else if ( allowedCount.isPresent() ) {
						try {
							maxCount = Integer.parseInt( allowedCount.get() );
						} catch ( Throwable var11 ) {
							maxCount = GetOffMyLawn.CONFIG.maxClaimsPerPlayer;
						}
					} else {
						maxCount = GetOffMyLawn.CONFIG.maxClaimsPerPlayer;
					}

					if ( maxCount != -1 && count >= maxCount ) {
						context.getPlayer().sendMessage( GetOffMyLawn.CONFIG.prefix( Text.translatable( "text.goml.cant_place_claim.max_count_reached", count, GetOffMyLawn.CONFIG.maxClaimsPerPlayer ).formatted( Formatting.RED ) ), false );
						return false;
					}

					if ( GetOffMyLawn.CONFIG.isBlacklisted( context.getWorld(), checkBox.toBox() ) ) {
						context.getPlayer().sendMessage( GetOffMyLawn.CONFIG.prefix( Text.translatable( "text.goml.cant_place_claim.blacklisted_area", context.getWorld().getRegistryKey().getValue().toString(), context.getBlockPos().toShortString() ).formatted( Formatting.RED ) ), false );
						return false;
					}
				}
				Selection< Entry< ClaimBox, Claim > > selection = containsClaims( context.getWorld(), checkBox.getOrigin(), 2, context.getPlayer() );
				if ( containsClaims( context.getWorld(), checkBox.getOrigin(), 0, context.getPlayer() ).count() > 0 ) {
					context.getPlayer().sendMessage( Text.of( "Cannot place claim" ), false );
					return false;
				}
				if ( selection.isEmpty() && ClaimUtils.getClaimsOwnedBy( context.getWorld(), context.getPlayer().getUuid() ).isEmpty() ) {
					return super.canPlace( context, state );
				}
				if ( ClaimUtils.getClaimsOwnedBy( context.getWorld(), context.getPlayer().getUuid() ).isEmpty() || selection.isEmpty() ) {
					context.getPlayer().sendMessage( Text.of( "Cannot place claim" ), false );
					return false;
				}
				if ( selection.filter( claimBoxClaimEntry -> claimBoxClaimEntry.getValue().getOwners().contains( context.getPlayer().getUuid() ) ).isEmpty() ) {
					context.getPlayer().sendMessage( Text.of( "Cannot place claim" ), false );
					return false;
				}
				if ( !sonoAttaccato( context.getWorld(), checkBox.getOrigin(), 1, context.getPlayer().getUuid() ).isEmpty() && containsClaims( context.getWorld(), checkBox.getOrigin(), 1, context.getPlayer() ).filter( claimBoxClaimEntry -> !claimBoxClaimEntry.getValue().getOwners().contains( context.getPlayer().getUuid() ) ).isEmpty() ) {
					Selection< Entry< ClaimBox, Claim > > claims = ClaimUtils.getClaimsInBox( context.getWorld(), checkBox.rtree3iBox() );
					if ( GetOffMyLawn.CONFIG.allowClaimOverlappingIfSameOwner ) {
						claims = claims.filter( ( x ) -> !x.getValue().isOwner( context.getPlayer() ) || x.getKey().toBox().equals( checkBox.toBox() ) );
					}
					if ( claims.isNotEmpty() ) {
						MutableText list = Text.literal( "" );
						claims.forEach( ( c ) -> {
							Box box = c.getKey().toBox();
							MutableText var10001 = Text.literal( "[" ).formatted( Formatting.GRAY );
							int var10002 = box.x1();
							var10001 = var10001.append( Text.literal( var10002 + ", " + box.y1() + ", " + box.z1() ).formatted( Formatting.WHITE ) ).append( " | " );
							var10002 = box.x2();
							list.append( var10001.append( Text.literal( var10002 + ", " + box.y2() + ", " + box.z2() ).formatted( Formatting.WHITE ) ).append( "] " ) );
						} );
						context.getPlayer().sendMessage( GetOffMyLawn.CONFIG.prefix( Text.translatable( "text.goml.cant_place_claim.collides_with", list ).formatted( Formatting.RED ) ), false );
						return false;
					} else {
						return super.canPlace( context, state );
					}
				} else {
					context.getPlayer().sendMessage( Text.of( "Cannot place claim" ), false );
					return false;
				}
			}
		}
	}

	@Unique
	private Selection< Entry< ClaimBox, Claim > > containsClaims( World world, BlockPos origin, int val, PlayerEntity player ) {
		ChunkSectionPos center = ChunkSectionPos.from( origin );
		ChunkSectionPos start = ChunkSectionPos.from( new BlockPos( center.getMinX() - val * 16, -70, center.getMinZ() - val * 16 ) );
		ChunkSectionPos end = ChunkSectionPos.from( new BlockPos( center.getMaxX() + val * 16, 400, center.getMaxZ() + val * 16 ) );

		return ClaimUtils.getClaimsInBox( world, start.getCenterPos(), end.getCenterPos() );
	}
}

package com.enderzombi102.elysium.mixin.goml;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.imixin.ElysiumClaimBlockEntity;
import com.enderzombi102.elysium.registry.CompRegistry;
import com.jamieswhiteshirt.rtree3i.Entry;
import draylar.goml.GetOffMyLawn;
import draylar.goml.api.Claim;
import draylar.goml.api.ClaimBox;
import draylar.goml.api.ClaimUtils;
import draylar.goml.block.ClaimAnchorBlock;
import draylar.goml.block.entity.ClaimAnchorBlockEntity;
import draylar.goml.registry.GOMLBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.HashMap;
import java.util.Set;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.elysium.util.Util.isAdminClaim;

@Mixin( ClaimAnchorBlock.class )
public abstract class ClaimAnchorBlockMixin extends Block implements BlockEntityProvider {
	private ClaimAnchorBlockMixin( Settings settings ) {
		super( settings );
	}

	@Inject(
		method = "onPlaced",
		at = @At(
			value = "INVOKE",
			target = "Ldraylar/goml/api/event/ClaimEvents$GenericClaimEvent;onEvent(Ldraylar/goml/api/Claim;)V",
			shift = At.Shift.AFTER,
			remap = false
		)
	)
	public void afterPlaced( World world, BlockPos pos, BlockState state, LivingEntity placer, ItemStack itemStack, CallbackInfo ci ) {
		if ( !Config.getData().features.claimModifications )
			return;
		try {
			if ( placer instanceof ServerPlayerEntity player ) {
				if ( world.getBlockState( pos ) != GOMLBlocks.ADMIN_CLAIM_ANCHOR.getFirst().getDefaultState() ) {
					player.sendMessage( Text.translatable( "text.elysium.claim.welcome" ) );
				}
				assert Elysium.server != null : "Why it nul????";
				CompRegistry.ELYSIUM_LEVEL
					.get( Elysium.server.getSaveProperties() )
					.setAnchorStamp( player.getUuid(), 0 );
				CompRegistry.ELYSIUM_LEVEL
					.get( Elysium.server.getSaveProperties() )
					.setTimeMethod( player.getUuid(), "server" );
				BlockEntity b = world.getBlockEntity( pos );
				assert b != null;
				NbtCompound n = b.createNbtWithIdentifyingData();
				n.putString( "TimeMethod", "server" );
				b.readNbt( n );
				LOGGER.info( "[Elysium] Anchor placed, {}'s backup timestamp reset at [{}]", placer.getEntityName(), pos.toShortString() );
				ClaimAnchorBlockEntity claimAnchorBlockEntity = (ClaimAnchorBlockEntity) b;
				Set< Entry< ClaimBox, Claim > > s = ClaimUtils.getClaimsOwnedBy( world, player.getUuid() ).collect( Collectors.toSet() );
				s.forEach( claimEntry -> {
					ClaimAnchorBlockEntity temp = ClaimUtils.getAnchor( Elysium.server.getOverworld(), claimEntry.getValue() );
					assert temp != null;
					HashMap< UUID, Long > carpenters = ( (ElysiumClaimBlockEntity) temp ).elysium$getCarpenters();
					if ( ( (ElysiumClaimBlockEntity) temp ).elysium$getTowered() && !( (ElysiumClaimBlockEntity) claimAnchorBlockEntity ).elysium$getTowered() ) {
						( (ElysiumClaimBlockEntity) claimAnchorBlockEntity ).elysium$setTowered( true );
						ClaimBox claimBox = claimAnchorBlockEntity.getBox();
						GetOffMyLawn.CLAIM.get( world ).remove( claimAnchorBlockEntity.getClaim() );

						claimAnchorBlockEntity.getClaim().internal_setClaimBox( new ClaimBox( claimBox.getOrigin(), claimBox.getRadius(), (int) ( claimBox.radiusY() * 2.5 ), claimBox.noShift() ) );
						if ( world instanceof ServerWorld world1 ) {
							claimAnchorBlockEntity.getClaim().internal_updateChunkCount( world1 );
						}
						claimAnchorBlockEntity.getClaim().internal_setWorld( claimAnchorBlockEntity.getWorld().kjs$getDimension() );
						GetOffMyLawn.CLAIM.get( world ).add( claimAnchorBlockEntity.getClaim() );
						( (ElysiumClaimBlockEntity) claimAnchorBlockEntity ).elysium$setTowered( true );
					}
					carpenters.forEach( new BiConsumer< UUID, Long >() {
						@Override
						public void accept( UUID uuid, Long aLong ) {
							( (ElysiumClaimBlockEntity) claimAnchorBlockEntity ).elysium$addCarpenter( uuid, aLong );
						}
					} );
				} );
			}
		} catch ( Exception ignored ) {
		}
	}


	@Inject(
		method = "onUse",
		at = @At(
			value = "INVOKE",
			target = "Ldraylar/goml/api/Claim;openUi(Lnet/minecraft/server/network/ServerPlayerEntity;)V",
			shift = At.Shift.BEFORE
		),
		cancellable = true
	)
	public void openNewScreen( BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit, CallbackInfoReturnable< ActionResult > cir ) {
		if ( !Config.getData().features.claimModifications )
			return;

		// accounts for admin claim
		if ( isAdminClaim( state ) )
			return;

		// allow admins to open og screen when sneaking // NOTE: This won't work, as GOML ignores shift-clicks!
		if ( player.isSneaking() && player.getScoreboardTags().contains( "admin" ) )
			return;

		player.openHandledScreen( state.createScreenHandlerFactory( world, pos ) );
		cir.setReturnValue( ActionResult.SUCCESS );
	}

	@Nullable
	@Override
	public NamedScreenHandlerFactory createScreenHandlerFactory( BlockState state, World world, BlockPos pos ) {
		var entity = world.getBlockEntity( pos );
		assert entity instanceof ClaimAnchorBlockEntity : "Somehow, this block entity is not ours..?";
		return (NamedScreenHandlerFactory) entity;
	}

	@Override
	public MutableText getName() {
		if ( this.kjs$getId().equalsIgnoreCase( "goml:makeshift_claim_anchor" ) ) {
			return Text.translatable( "elysium.makeshift_claim_anchor" );
		} else if ( this.kjs$getId().equalsIgnoreCase( "goml:admin_claim_anchor" ) ) {
			return Text.translatable( "elysium.admin_claim_anchor" );
		}
		return super.getName();
	}
}

package com.enderzombi102.elysium.mixin.goml;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.util.TaxesUtil;
import com.llamalad7.mixinextras.injector.ModifyReturnValue;
import draylar.goml.api.Claim;
import draylar.goml.block.ClaimAnchorBlock;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;

import java.util.Objects;

@Mixin( Claim.class )
public abstract class ClaimMixin {
	@Shadow( remap = false )
	public abstract ClaimAnchorBlock getType();

	@Shadow
	public abstract Identifier getWorld();

	@Shadow
	public abstract @Nullable ServerWorld getWorldInstance( MinecraftServer server );

	@ModifyReturnValue(
		method = "hasPermission(Ljava/util/UUID;)Z",
		at = @At( "RETURN" ),
		remap = false
	)
	public boolean disableClaim( boolean original ) {
		if ( !Config.getData().features.claimModifications )
			return original;

		if ( Elysium.server == null || original )
			return original;

		// accounts for admin claim
		return TaxesUtil.isClaimDisabled(
			(Claim) (Object) this,
			Objects.requireNonNull( this.getWorldInstance( Elysium.server ) )
		);
	}
}

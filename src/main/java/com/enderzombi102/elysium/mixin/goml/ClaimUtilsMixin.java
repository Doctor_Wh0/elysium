package com.enderzombi102.elysium.mixin.goml;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.util.TaxesUtil;
import com.jamieswhiteshirt.rtree3i.Entry;
import com.jamieswhiteshirt.rtree3i.Selection;
import draylar.goml.GetOffMyLawn;
import draylar.goml.api.Claim;
import draylar.goml.api.ClaimBox;
import draylar.goml.api.ClaimUtils;
import net.minecraft.entity.Entity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkSectionPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldView;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.stream.Collectors;

@Mixin( ClaimUtils.class )
public abstract class ClaimUtilsMixin {
	@Shadow
	public static Selection< Entry< ClaimBox, Claim > > getClaimsAt( WorldView world, BlockPos pos ) {
		return null;
	}

	@Inject(
		method = "canDamageEntity",
		at = @At(
			value = "FIELD",
			target = "Ldraylar/goml/config/GOMLConfig;enablePvPinClaims:Z",
			remap = false
		),
		cancellable = true
	)
	private static void unblockPlayerVsPlayer( World world, Entity entity, DamageSource source, CallbackInfoReturnable< Boolean > cir ) {
		if ( !Config.getData().features.claimModifications )
			return;

		if ( world.isClient() )
			return;

		var claims = getClaimsAt( world, entity.getBlockPos() );
		assert claims != null : "Why is the claim selection null?";
		var claim = claims.collect( Collectors.toList() ).get( 0 ).getValue();

		// if the claim is disabled, enable the pvp
		// accounts for admin claim
		if ( TaxesUtil.isClaimDisabled( claim, (ServerWorld) world ) )
			cir.setReturnValue( true );
	}
	@Inject(
		method = "canDamageEntity",
		at = @At(
			value = "HEAD"
		),
		cancellable = true
	)
	private static void dummy( World world, Entity entity, DamageSource source, CallbackInfoReturnable< Boolean > cir ) {
		if(entity.getType().getUntranslatedName().equalsIgnoreCase( "target_dummy" )) {
			cir.setReturnValue( true );
		}
	}

	@Inject(
		method = "createClaimBox",
		at = @At(
			value = "RETURN"
		),
		cancellable = true
	)
	private static void createClaimBox( BlockPos pos, int radius, CallbackInfoReturnable< ClaimBox > cir ) {
		if ( GetOffMyLawn.CONFIG.makeClaimAreaChunkBound ) {
			ChunkSectionPos chunkPos = ChunkSectionPos.from( pos );
			radius = (int) ( ( Math.ceil( (double) radius / 16.0 ) - 1.0 ) * 16.0 ) + 8;
			int radiusY = (int) ( ( Math.ceil( (double) radius * GetOffMyLawn.CONFIG.claimAreaHeightMultiplier / 16.0 ) - 1.0 ) * 16.0 );
			cir.setReturnValue( new ClaimBox( chunkPos.getCenterPos(), radius, GetOffMyLawn.CONFIG.claimProtectsFullWorldHeight ? 32767 : radiusY, true ) );
		} else {
			cir.setReturnValue( new ClaimBox( pos, radius, GetOffMyLawn.CONFIG.claimProtectsFullWorldHeight ? 32767 : (int) ( (double) radius * GetOffMyLawn.CONFIG.claimAreaHeightMultiplier ) ) );
		}

	}

}

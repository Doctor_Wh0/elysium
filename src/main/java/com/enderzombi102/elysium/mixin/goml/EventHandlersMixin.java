package com.enderzombi102.elysium.mixin.goml;

import com.enderzombi102.elysium.util.Util;
import com.jamieswhiteshirt.rtree3i.Entry;
import com.jamieswhiteshirt.rtree3i.Selection;
import draylar.goml.EventHandlers;
import draylar.goml.api.Claim;
import draylar.goml.api.ClaimBox;
import draylar.goml.api.ClaimUtils;
import draylar.goml.block.entity.ClaimAnchorBlockEntity;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import java.util.List;
import java.util.stream.Collectors;

import static com.enderzombi102.elysium.util.Util.isAdminClaim;

@SuppressWarnings( "UnstableApiUsage" )
@Mixin( EventHandlers.class )
public class EventHandlersMixin {
	@Inject(
		method = "lambda$registerInteractBlockCallback$4",
		at = @At(
			value = "INVOKE",
			target = "Ldraylar/goml/EventHandlers;testPermission(Lcom/jamieswhiteshirt/rtree3i/Selection;Lnet/minecraft/entity/player/PlayerEntity;Lnet/minecraft/util/Hand;Lnet/minecraft/util/math/BlockPos;Ldraylar/goml/api/PermissionReason;)Lnet/minecraft/util/ActionResult;",
			shift = At.Shift.BEFORE
		),
		cancellable = true,
		locals = LocalCapture.CAPTURE_FAILHARD
	)
	private static void allowIntersectionalInteractions( PlayerEntity player, World world, Hand hand, BlockHitResult hit, CallbackInfoReturnable< ActionResult > cir, Selection< Entry< ClaimBox, Claim > > claimsFound ) {
		var claims = claimsFound.collect( Collectors.toList() );

		Claim closest = null;
		var closestDistance = 999999;
		for ( var entry : claims ) {
			var distance = entry.getValue().getOrigin().getManhattanDistance( hit.getBlockPos() );
			if ( distance < closestDistance ) {
				closest = entry.getValue();
				closestDistance = distance;
			}
		}

		if ( closest != null && isAdminClaim( closest ) )
			cir.setReturnValue( closest.hasPermission( player ) ? ActionResult.PASS : ActionResult.FAIL );
	}

	@Inject( method = "lambda$registerBreakBlockCallback$6", at = @At( "HEAD" ), cancellable = true )
	private static void registerBreakBlockCallback( World world, PlayerEntity player, BlockPos pos, BlockState state, BlockEntity blockEntity, CallbackInfoReturnable< Boolean > cir ) {
		if ( world != null && !world.isClient() ) {
			if ( blockEntity instanceof ClaimAnchorBlockEntity ) {
				cir.setReturnValue( true );
				Selection< Entry< ClaimBox, Claim > > claims = ClaimUtils.getClaimsOwnedBy( world, player.getUuid() );
				claims = claims.filter( claimBoxClaimEntry -> !claimBoxClaimEntry.getValue().getOrigin().equals( pos ) );
				switch ( claims.count() ) {
					case 2:
					case 3:
						claims.forEach( claimBoxClaimEntry -> {
							List< Entry< ClaimBox, Claim > > c = Util.sonoAttaccato( world, claimBoxClaimEntry.getValue().getOrigin(), 1, player.getUuid() ).stream().filter( claimBoxClaimEntry1 -> !claimBoxClaimEntry1.getValue().getOrigin().equals( pos ) && !claimBoxClaimEntry1.getValue().getOrigin().equals( claimBoxClaimEntry.getValue().getOrigin() ) ).toList();
							if ( c.isEmpty() ) {
								cir.setReturnValue( false );
							}
						} );
						break;
					case 4:
						final int[] n1 = { 0 };
						claims.forEach( claimBoxClaimEntry -> {
							List< Entry< ClaimBox, Claim > > c = Util.sonoAttaccato( world, claimBoxClaimEntry.getValue().getOrigin(), 1, player.getUuid() ).stream().filter( claimBoxClaimEntry1 -> !claimBoxClaimEntry1.getValue().getOrigin().equals( pos ) && !claimBoxClaimEntry1.getValue().getOrigin().equals( claimBoxClaimEntry.getValue().getOrigin() ) ).toList();
							if ( c.isEmpty() ) {
								cir.setReturnValue( false );
							} else if ( c.size() >= 2 ) {
								n1[ 0 ] = n1[ 0 ] + 1;
							}
						} );
						if ( n1[ 0 ] < 1 ) {
							cir.setReturnValue( false );
						}
						break;
					case 5:
						final int[] n2 = { 0 };
						claims.forEach( claimBoxClaimEntry -> {
							List< Entry< ClaimBox, Claim > > c = Util.sonoAttaccato( world, claimBoxClaimEntry.getValue().getOrigin(), 1, player.getUuid() ).stream().filter( claimBoxClaimEntry1 -> !claimBoxClaimEntry1.getValue().getOrigin().equals( pos ) && !claimBoxClaimEntry1.getValue().getOrigin().equals( claimBoxClaimEntry.getValue().getOrigin() ) ).toList();
							if ( c.isEmpty() ) {
								cir.setReturnValue( false );
							} else if ( c.size() >= 2 ) {
								n2[ 0 ] = n2[ 0 ] + 1;
							}
						} );
						if ( n2[ 0 ] < 2 ) {
							cir.setReturnValue( false );
						}
						break;
					default:
						if ( cir.getReturnValue() == null )
							cir.setReturnValue( true );
				}
				if ( !cir.getReturnValue() ) {
					// TODO: FIX
					player.sendMessage( Text.literal( "You cannot destroy this claim" ) );
				}
			} else {
				cir.setReturnValue( true );
			}
		}
	}
	/*@Inject( method="lambda$registerBreakBlockCallback$5", at = @At( "HEAD" ), cancellable = true )
	private static void registerBreakBlockCallback( PlayerEntity playerEntity, World world, Hand hand, BlockPos blockPos, Direction direction, CallbackInfoReturnable< ActionResult > cir ) {
		if (world != null && !world.isClient()) {
			Selection< Entry< ClaimBox, Claim > > claims = ClaimUtils.getClaimsOwnedBy( world, playerEntity.getUuid() );
			cir.setReturnValue( ActionResult.FAIL );
			switch ( claims.count() ) {
				case 3:
					claims.forEach( new Consumer< Entry< ClaimBox, Claim > >() {
						@Override
						public void accept( Entry< ClaimBox, Claim > claimBoxClaimEntry ) {
							if ( Util.sonoAttaccato( world, claimBoxClaimEntry.getValue().getOrigin(), 1, playerEntity.getUuid() ).stream().anyMatch( claimBoxClaimEntry1 -> !claimBoxClaimEntry1.equals( claimBoxClaimEntry ) ) ) {
								cir.setReturnValue( ActionResult.FAIL );
							}
						}
					} );
					cir.setReturnValue( ActionResult.PASS );
					break;
				case 4:
				case 5:
				case 6:
					claims.forEach( new Consumer< Entry< ClaimBox, Claim > >() {
						@Override
						public void accept( Entry< ClaimBox, Claim > claimBoxClaimEntry ) {
							if ( Util.sonoAttaccato( world, claimBoxClaimEntry.getValue().getOrigin(), 1, playerEntity.getUuid() ).stream().filter( claimBoxClaimEntry1 -> !claimBoxClaimEntry1.equals( claimBoxClaimEntry ) ).count() > 1 ) {
								cir.setReturnValue( ActionResult.FAIL );
							}
						}
					} );
					cir.setReturnValue( ActionResult.PASS );
					break;
				default:
					cir.setReturnValue( ActionResult.PASS );
					break;
			}
		}
	}
*/

}

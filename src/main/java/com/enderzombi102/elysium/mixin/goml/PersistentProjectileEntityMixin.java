package com.enderzombi102.elysium.mixin.goml;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.data.ItemInput;
import draylar.goml.api.ClaimUtils;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.hit.EntityHitResult;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.ArrayList;
import java.util.function.BiConsumer;

@Mixin( PersistentProjectileEntity.class )
public abstract class PersistentProjectileEntityMixin {
	@Shadow
	protected abstract ItemStack asItemStack();

	@Shadow
	private double damage;

	@Inject(
		method = "onEntityHit",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/util/math/Vec3d;length()D"
		)
	)
	private void disableProjectileDamage( EntityHitResult entityHitResult, CallbackInfo info ) {
		if ( !Config.getData().features.dreamingGomlFixes )
			return;

		var persistentProjectileEntity = (PersistentProjectileEntity) (Object) this;
		var owner = persistentProjectileEntity.getOwner();

		if ( owner != null && owner.isPlayer() && !entityHitResult.getEntity().getType().getUntranslatedName().equalsIgnoreCase( "target_dummy" ) && !entityHitResult.getEntity().kjs$isMonster() ) {

			var claims = ClaimUtils.getClaimsAt( persistentProjectileEntity.world, persistentProjectileEntity.getBlockPos() );
			if ( !claims.isEmpty() ) {
				persistentProjectileEntity.setDamage( 0 );
				persistentProjectileEntity.setCritical( false );
			}
		}
	}

}

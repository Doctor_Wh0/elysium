package com.enderzombi102.elysium.mixin.goml;

import com.enderzombi102.elysium.config.Config;
import draylar.goml.api.ClaimUtils;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.util.hit.EntityHitResult;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin( ProjectileEntity.class)
public class ProjectileEntityMixin {
	@Inject(
		method = "onEntityHit",
		at = @At(
			value = "HEAD"
		),
		cancellable = true
	)
	private void disableProjectileDamage( EntityHitResult entityHitResult, CallbackInfo info ) {
		if ( !Config.getData().features.dreamingGomlFixes )
			return;

		var projectileEntity = (ProjectileEntity) (Object) this;
		var owner = projectileEntity.getOwner();

		if ( owner != null && owner.isPlayer()  && !entityHitResult.getEntity().getType().getUntranslatedName().equalsIgnoreCase( "target_dummy" ) && !entityHitResult.getEntity().kjs$isMonster() ) {
			var claims = ClaimUtils.getClaimsAt( projectileEntity.world, projectileEntity.getBlockPos() );
			if ( !claims.isEmpty() ) {
				projectileEntity.discard();
				info.cancel();
				return;
			}
		}
	}
}

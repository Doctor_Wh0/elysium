package com.enderzombi102.elysium.mixin.goml;

import com.enderzombi102.elysium.config.Config;
import draylar.goml.api.ClaimUtils;
import net.minecraft.entity.Entity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.util.hit.EntityHitResult;
import net.spell_engine.api.spell.Spell;
import net.spell_engine.entity.SpellProjectile;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin( SpellProjectile.class)
public class SpellProjectileMixin {
	@Shadow
	private Spell spell;

	@Inject(
		method = "onEntityHit",
		at = @At(
			value = "HEAD"
		),
		cancellable = true
	)
	private void disableProjectileDamage( EntityHitResult entityHitResult, CallbackInfo info ) {
		if ( !Config.getData().features.dreamingGomlFixes )
			return;

		var spellProjectile = (SpellProjectile) (Object) this;
		var owner = spellProjectile.getOwner();

		if ( owner != null && owner.isPlayer()  && !entityHitResult.getEntity().getType().getUntranslatedName().equalsIgnoreCase( "target_dummy" ) && !entityHitResult.getEntity().kjs$isMonster() ) {
			var claims = ClaimUtils.getClaimsAt( spellProjectile.world, spellProjectile.getBlockPos() );
			if ( !claims.isEmpty() ) {
				spellProjectile.kill();
				info.cancel();
				return;
			}
		}
	}

}

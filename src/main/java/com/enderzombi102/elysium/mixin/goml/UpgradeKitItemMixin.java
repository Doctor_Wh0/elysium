package com.enderzombi102.elysium.mixin.goml;

import com.enderzombi102.elysium.config.Config;
import com.jamieswhiteshirt.rtree3i.Box;
import draylar.goml.item.UpgradeKitItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyArg;

@Mixin( UpgradeKitItem.class )
public class UpgradeKitItemMixin {
	@ModifyArg(
		method = "useOnBlock",
		at = @At(
			value = "INVOKE",
			target = "Ldraylar/goml/api/ClaimUtils;getClaimsInBox(Lnet/minecraft/world/WorldView;Lcom/jamieswhiteshirt/rtree3i/Box;Lcom/jamieswhiteshirt/rtree3i/Box;)Lcom/jamieswhiteshirt/rtree3i/Selection;"
		),
		index = 1
	)
	private Box disallowOverlapping( final Box claimBox ) {
		if ( !Config.getData().features.dreamingGomlFixes )
			return claimBox;

		var xz = Config.getData().claims.xzDistanceBetweenBorders;
		var y = Config.getData().claims.yDistanceBetweenBorders;
		return Box.create(
			claimBox.x1() - xz, claimBox.y1() - y, claimBox.z1() + xz,
			claimBox.x2() + xz, claimBox.y2() + y, claimBox.z2() + xz
		);
	}
}

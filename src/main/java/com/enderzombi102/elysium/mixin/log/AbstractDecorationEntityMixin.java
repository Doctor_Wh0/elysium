package com.enderzombi102.elysium.mixin.log;

import com.enderzombi102.elysium.Elysium;
import draylar.goml.api.ClaimUtils;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.decoration.AbstractDecorationEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.vehicle.BoatEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.Packet;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Objects;

@SuppressWarnings( "ALL" )
@Mixin( AbstractDecorationEntity.class )
public abstract class AbstractDecorationEntityMixin extends Entity {
	public AbstractDecorationEntityMixin( EntityType< ? > type, World world ) {
		super( type, world );
	}

	@Inject( at = @At( "HEAD" ), method = "damage" ,cancellable = true)
	public void damage( DamageSource source, float amount, CallbackInfoReturnable< Boolean > info ) {
		try {
			if(source.getAttacker()!=null){
				if ( (( source.getAttacker() != null && source.getAttacker().isPlayer() ))) {
					var claims = ClaimUtils.getClaimsAt( Objects.requireNonNull( source.getSource() ).getWorld(), new BlockPos(this.getX(),this.getY(),this.getZ()) );
					if ( !claims.isEmpty() && !claims.anyMatch( claimBoxClaimEntry ->  claimBoxClaimEntry.getValue().isOwner( source.getAttacker().getUuid() ) && !ClaimUtils.isInAdminMode( (PlayerEntity) source.getAttacker() ) ) ) {
						info.setReturnValue( false );
						info.cancel();
						return;
					}
				}else{
					info.setReturnValue( false );
					info.cancel();
				}
			}else{
				info.setReturnValue( false );
				info.cancel();
			}
		} catch ( Exception ignored ) {
		}
	}
}

package com.enderzombi102.elysium.mixin.log;

import com.enderzombi102.elysium.Elysium;
import draylar.goml.api.ClaimUtils;
import draylar.goml.registry.GOMLAugments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.decoration.ArmorStandEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.Packet;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.spell_power.api.SpellDamageSource;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.ArrayList;
import java.util.Objects;

import static com.enderzombi102.elysium.Elysium.LOGGER;


@SuppressWarnings( "ALL" )
@Mixin( ArmorStandEntity.class )
public abstract class ArmorStandEntityMixin extends LivingEntity {
	private static ArrayList< String > errors = new ArrayList<>();

	protected ArmorStandEntityMixin( EntityType< ? extends LivingEntity > entityType, World world ) {
		super( entityType, world );
	}


	@Inject( at = @At( "HEAD" ), method = "damage" ,cancellable = true)
	public void damage( DamageSource source, float amount, CallbackInfoReturnable< Boolean > info ) {
		try {
			if(source.getAttacker()!=null){
				if ( (( source.getAttacker() != null && source.getAttacker().isPlayer() ))) {
					var claims = ClaimUtils.getClaimsAt( Objects.requireNonNull( source.getSource() ).getWorld(), new BlockPos(this.getX(),this.getY(),this.getZ()) );
					if ( !claims.isEmpty() && !claims.anyMatch( claimBoxClaimEntry ->  claimBoxClaimEntry.getValue().isOwner( source.getAttacker().getUuid() ) && !ClaimUtils.isInAdminMode( (PlayerEntity) source.getAttacker() ) ) ) {
						info.setReturnValue( false );
						info.cancel();
						return;
					}
				}else{
					info.setReturnValue( false );
					info.cancel();
				}
			}else{
				info.setReturnValue( false );
				info.cancel();
			}
			/*if(source.getAttacker()!=null) {
				if (!source.getSource().isPlayer() && (source.isMagic() || source.isFire() || source.isExplosive() || source.isProjectile())) {
					info.setReturnValue( false );
					info.cancel();
					return;
				}
			}*/
		} catch ( Exception e ) {
			String output = "[Elysium ERROR] Strange armor stand at position " + ( (int) ( (ArmorStandEntity) (Object) this ).getX() ) + "," + ( (int) ( (ArmorStandEntity) (Object) this ).getY() ) + "," + ( (int) ( (ArmorStandEntity) (Object) this ).getZ() );
			boolean present = false;
			for ( int i = 0; i < errors.size(); i++ ) {
				if ( errors.get( i ).equalsIgnoreCase( output ) ) {
					present = true;
				}
			}
			if ( !present ) {
				LOGGER.info( "[Elysium ERROR] Strange armor stand at position " + ( (int) ( (ArmorStandEntity) (Object) this ).getX() ) + "," + ( (int) ( (ArmorStandEntity) (Object) this ).getY() ) + "," + ( (int) ( (ArmorStandEntity) (Object) this ).getZ() ) );
				errors.add( output );
			}
			info.setReturnValue( false );
			info.cancel();
		}
	}

	@Inject( at = @At( "HEAD" ), method = "travel" ,cancellable = true)
	public void travel( Vec3d movementInput, CallbackInfo info) {
		info.cancel();
	}

}

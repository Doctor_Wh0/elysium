package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.imixin.MutableMultiMap;
import com.google.common.collect.Multimap;
import dev.latvian.mods.kubejs.core.NoMixinException;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.ArmorMaterial;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin( ArmorItem.class )
public abstract class ArmorItemMixin implements MutableMultiMap {
	@Override
	@Accessor( "type" )
	@Mutable
	public abstract void setType( ArmorMaterial type );
	@Override
	@Accessor( "type" )
	@Mutable
	public abstract ArmorMaterial getArmorMaterial();

	@Override
	@Accessor( "attributeModifiers" )
	public abstract Multimap< EntityAttribute, EntityAttributeModifier > getAttributeModifiers();

	@Override
	@Accessor( "attributeModifiers" )
	@Mutable
	public abstract void setAttributeModifiers( Multimap< EntityAttribute, EntityAttributeModifier > attributes );
}

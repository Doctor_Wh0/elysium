package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.registry.CompRegistry;
import com.mojang.authlib.GameProfile;
import draylar.goml.GetOffMyLawn;
import draylar.goml.api.Claim;
import draylar.goml.api.ClaimUtils;
import draylar.goml.api.event.ClaimEvents;
import draylar.goml.block.entity.ClaimAnchorBlockEntity;
import draylar.goml.registry.GOMLBlocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.dedicated.command.BanCommand;
import net.minecraft.text.Text;
import net.minecraft.util.math.BlockPos;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Collection;
import java.util.UUID;

import static com.enderzombi102.elysium.Elysium.LOGGER;

@Mixin( BanCommand.class )
public class BanCommandMixin {
	@Inject( method = "ban", at = @At( "TAIL" ) )
	private static void ban( ServerCommandSource source, Collection< GameProfile > targets, @Nullable Text reason, CallbackInfoReturnable< Integer > cir ) {
		try {
			assert Elysium.server != null;
			var comp = CompRegistry.ELYSIUM_LEVEL.get( Elysium.server.getSaveProperties() );
			for ( GameProfile user : targets ) {
				source.getServer().getWorlds().forEach( serverWorld -> {
					var claims = ClaimUtils.getClaimsOwnedBy( serverWorld, user.getId() );
					claims.forEach( claimBoxClaimEntry -> {
						if ( claimBoxClaimEntry.getValue().getOwners().contains( user.getId() ) ) {
							if ( !claimBoxClaimEntry.getValue().getOrigin().equals( new BlockPos( 0, 0, -100 ) ) ) {
								var claimInfo = claimBoxClaimEntry.getValue();
								for ( UUID player : claimInfo.getOwners() )
									claimInfo.getOwners().remove( player );
								for ( UUID player : claimInfo.getTrusted() )
									claimInfo.untrust( player );
								var newBox = ClaimUtils.createClaimBox( claimBoxClaimEntry.getKey().getOrigin(), claimBoxClaimEntry.getKey().getRadius() );
								BlockEntity oldBE = serverWorld.getBlockEntity( claimBoxClaimEntry.getKey().getOrigin() );
								serverWorld.setBlockState( claimBoxClaimEntry.getKey().getOrigin(), GOMLBlocks.ADMIN_CLAIM_ANCHOR.getFirst().getDefaultState() );
								Claim c = new Claim( claimInfo.getOwners(), claimInfo.getTrusted(), new BlockPos( 0, 0, -100 ) );
								c.internal_setClaimBox( ClaimUtils.createClaimBox( new BlockPos( 0, 0, -100 ), 1 ) );
								GetOffMyLawn.CLAIM.get( serverWorld ).remove( claimBoxClaimEntry.getValue() );
								claimInfo.internal_setIcon( GOMLBlocks.ADMIN_CLAIM_ANCHOR.getSecond().getDefaultStack() );
								claimInfo.internal_setType( GOMLBlocks.ADMIN_CLAIM_ANCHOR.getFirst() );
								claimInfo.internal_setClaimBox( newBox );
								claimInfo.internal_updateChunkCount( serverWorld );
								claimInfo.internal_setWorld( claimBoxClaimEntry.getValue().getWorld() );

								GetOffMyLawn.CLAIM.get( serverWorld ).add( claimInfo );
								GetOffMyLawn.CLAIM.get( serverWorld ).add( c );
								BlockEntity newBE = serverWorld.getBlockEntity( claimBoxClaimEntry.getKey().getOrigin() );
								if ( oldBE instanceof ClaimAnchorBlockEntity && newBE instanceof ClaimAnchorBlockEntity ) {
									( (ClaimAnchorBlockEntity) newBE ).from( ( (ClaimAnchorBlockEntity) oldBE ) );
								}

								ClaimEvents.CLAIM_RESIZED.invoker().onResizeEvent( claimInfo, claimBoxClaimEntry.getKey(), newBox );
								LOGGER.warn( "[Elysium] Replaced claim anchor at {} (owner: {})", claimBoxClaimEntry.getValue().getOrigin().toShortString(), user.getName() );
							}
						}
					} );
				} );
			}
		} catch ( Exception e ) {
			e.printStackTrace();
		}
	}
}

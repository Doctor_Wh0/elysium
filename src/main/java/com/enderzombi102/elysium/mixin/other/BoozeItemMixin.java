package com.enderzombi102.elysium.mixin.other;

import dev.sterner.brewinandchewin.common.item.BoozeItem;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.List;

@Mixin( BoozeItem.class)
public class BoozeItemMixin {
	@Mutable
	@Shadow
	@Final
	protected int potency;

	@Mutable
	@Shadow
	@Final
	protected int duration;

	@Inject( at = @At( value = "FIELD", target = "Ldev/sterner/brewinandchewin/common/item/BoozeItem;duration:I" ), method = "<init>" )
	private void init( int potency, int duration, Item.Settings settings, CallbackInfo ci ) {
		this.potency=0;
		this.duration=duration;
	}

	@Inject( at = @At( value = "HEAD" ), method = "appendTooltip", cancellable = true )
	@Environment( EnvType.CLIENT )
	private void tooltip( ItemStack stack, @Nullable World world, List< Text > tooltip, TooltipContext context, CallbackInfo ci ) {
		ci.cancel();
		return;
	}

}

package com.enderzombi102.elysium.mixin.other;

import net.minecraft.entity.passive.ChickenEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin( ChickenEntity.class )
public class ChickenEntityMixin {
	@Inject(
		method = "tickMovement",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/entity/passive/ChickenEntity;playSound(Lnet/minecraft/sound/SoundEvent;FF)V",
			shift = At.Shift.BEFORE
		),
		cancellable = true )
	public void drop( CallbackInfo ci ){
		ci.cancel();
	}
}

package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.registry.ItemRegistry;
import net.minecraft.block.BlockState;
import net.minecraft.block.ComposterBlock;
import net.minecraft.entity.ItemEntity;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.state.property.IntProperty;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

@Mixin( ComposterBlock.class )
public class ComposterBlockMixin {
	@Shadow
	public static IntProperty LEVEL;

	@Inject( method = "emptyFullComposter", at = @At( "HEAD" ), cancellable = true )
	private static void emptyFullComposter( BlockState state, World world, BlockPos pos, CallbackInfoReturnable callbackInfoReturnable ) {
		if ( !world.isClient ) {

			float f = 0.7F;
			double d = (double) ( world.random.nextFloat() * 0.7F ) + 0.15000000596046448;
			double e = (double) ( world.random.nextFloat() * 0.7F ) + 0.06000000238418579 + 0.6;
			double g = (double) ( world.random.nextFloat() * 0.7F ) + 0.15000000596046448;
			ItemEntity itemEntity = new ItemEntity( world, (double) pos.getX() + d, (double) pos.getY() + e, (double) pos.getZ() + g, new ItemStack( ItemRegistry.ITEMS.getOrDefault( "dung_ball", Items.AIR ) ) );
			itemEntity.setToDefaultPickupDelay();
			world.spawnEntity( itemEntity );
		}

		BlockState blockState = emptyComposter( state, world, pos );
		world.playSound( null, pos, SoundEvents.BLOCK_COMPOSTER_EMPTY, SoundCategory.BLOCKS, 1.0F, 1.0F );
		callbackInfoReturnable.setReturnValue( blockState );
	}

	@Shadow
	static BlockState emptyComposter( BlockState state, WorldAccess world, BlockPos pos ) {
		Elysium.LOGGER.warn( "[Elysium] Error method emptyComposter pos: " + pos.toShortString() );
		return state;
	}

	@Inject( method = "getInventory", at = @At( "HEAD" ) )
	public SidedInventory getInventory( BlockState state, WorldAccess world, BlockPos pos, CallbackInfoReturnable callbackInfoReturnable ) {
		int i = state.get( LEVEL );
		if ( i == 8 ) {
			final Class< ? > nestedClass;
			try {
				nestedClass = Class.forName( "net.minecraft.block.ComposterBlock$FullComposterInventory" );
				final Constructor< ? > ctor = nestedClass.getDeclaredConstructors()[ 0 ];
				ctor.setAccessible( true );
				final Object instance = ctor.newInstance( state, world, pos, new ItemStack( ItemRegistry.ITEMS.getOrDefault( "dung_ball", Items.AIR ) ) );
				return (SidedInventory) instance;
			} catch ( ClassNotFoundException | InvocationTargetException | InstantiationException |
					  IllegalAccessException e ) {
				Elysium.LOGGER.warn( "[Elysium] Error constructor Composter inner class 1" );
			}
		} else {
			if ( i < 7 ) {
				final Class< ? > nestedClass;
				try {
					nestedClass = Class.forName( "net.minecraft.block.ComposterBlock$ComposterInventory" );
					final Constructor< ? > ctor = nestedClass.getDeclaredConstructors()[ 0 ];
					ctor.setAccessible( true );
					final Object instance = ctor.newInstance();
					return (SidedInventory) instance;
				} catch ( ClassNotFoundException | InvocationTargetException | InstantiationException |
						  IllegalAccessException e ) {
					Elysium.LOGGER.warn( "[Elysium] Error constructor Composter inner class 2" );
				}
			} else {
				final Class< ? > nestedClass;
				try {
					nestedClass = Class.forName( "net.minecraft.block.ComposterBlock$DummyInventory" );
					final Constructor< ? > ctor = nestedClass.getDeclaredConstructors()[ 0 ];
					ctor.setAccessible( true );
					final Object instance = ctor.newInstance();
					return (SidedInventory) instance;
				} catch ( ClassNotFoundException | InvocationTargetException | IllegalAccessException |
						  InstantiationException e ) {
					Elysium.LOGGER.warn( "[Elysium] Error constructor Composter inner class 3" );
				}
			}

		}
		Elysium.LOGGER.info( "[Elysium] Error return ComposterBlock" );
		return null;
	}
}

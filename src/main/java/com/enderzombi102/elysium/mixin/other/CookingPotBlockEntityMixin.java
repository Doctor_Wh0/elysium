package com.enderzombi102.elysium.mixin.other;

import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.Recipe;
import net.minecraft.util.collection.DefaultedList;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.gen.Invoker;

import java.util.*;

@Mixin( satisfy.bakery.entity.CookingPotEntity.class )
public abstract class CookingPotBlockEntityMixin {
	@Invoker( "canCraft" )
	public abstract boolean callcanCraft( Recipe< ? > recipe );

	@Invoker( "getStack" )
	public abstract ItemStack callgetStack( int slot );

	@Invoker( "setStack" )
	public abstract void callsetStack( int slot, ItemStack stack );

	@Overwrite( remap = false )
	private void craft( Recipe< ? > recipe ) {
		if ( callcanCraft( recipe ) ) {
			ItemStack recipeOutput = recipe.getOutput();
			ItemStack outputSlotStack = callgetStack( 0 );
			if ( outputSlotStack.isEmpty() ) {
				callsetStack( 0, recipeOutput.copy() );
			} else if ( outputSlotStack.isOf( recipeOutput.getItem() ) ) {
				outputSlotStack.increment( recipeOutput.getCount() );
			}
			DefaultedList< Ingredient > ingredients = recipe.getIngredients();
			ArrayList< Integer > l = new ArrayList<>();
			for ( int i = 1; i < 7; i++ ) {
				for ( int j = 0; j < ingredients.size(); j++ ) {
					if ( ingredients.get( j ).test( callgetStack( i ) ) && !l.contains( j ) ) {
						callgetStack( i ).decrement( 1 );
						l.add( j );
					}
				}
			}
			callgetStack( 7 ).decrement( 1 );
		}
	}
}

package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.imixin.MutableMultiMap;
import com.google.common.collect.Multimap;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.paladins.item.armor.CustomArmor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;
@Mixin( CustomArmor.class)
public abstract class CustomArmorMixin implements MutableMultiMap {
	@Override
	@Accessor( value = "attributes", remap = false )
	@Mutable
	public abstract void setAttributeModifiers( Multimap< EntityAttribute, EntityAttributeModifier > attributes );
}

package com.enderzombi102.elysium.mixin.other;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import satisfyu.vinery.item.DrinkBlockItem;

import java.util.List;

@Mixin( DrinkBlockItem.class )
public class DrinkBlockItemMixin {
	@Inject( at = @At( value = "HEAD" ), method = "appendTooltip", cancellable = true )
	@Environment( EnvType.CLIENT )
	private void tooltip( ItemStack stack, @Nullable World world, List< Text > tooltip, TooltipContext context, CallbackInfo ci ) {
		ci.cancel();
		return;
	}
}

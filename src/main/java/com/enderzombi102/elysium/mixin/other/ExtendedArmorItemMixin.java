package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.imixin.MutableMultiMap;
import com.google.common.collect.Multimap;
import immersive_armors.item.ExtendedArmorItem;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin( ExtendedArmorItem.class)
public abstract class ExtendedArmorItemMixin  implements MutableMultiMap {

	@Override
	@Accessor( value = "attributeModifiers", remap = false )
	@Mutable
	public abstract void setAttributeModifiers( Multimap<EntityAttribute, EntityAttributeModifier> attributes );
}

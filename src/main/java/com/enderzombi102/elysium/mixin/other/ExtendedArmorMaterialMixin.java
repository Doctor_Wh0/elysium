package com.enderzombi102.elysium.mixin.other;

import immersive_armors.armorEffects.ArmorEffect;
import immersive_armors.item.ExtendedArmorMaterial;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.ArrayList;
import java.util.List;

@Mixin( ExtendedArmorMaterial.class)
public class ExtendedArmorMaterialMixin {
	@Inject( at = @At( value = "HEAD" ), method = "getEffects", cancellable = true,  remap = false )
	private void getEffects( CallbackInfoReturnable< List< ArmorEffect > > cir ) {
		cir.setReturnValue( new ArrayList< ArmorEffect >() {
		} );
		cir.cancel();
		return;
	}
}

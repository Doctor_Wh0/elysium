package com.enderzombi102.elysium.mixin.other;

import com.mojang.datafixers.util.Pair;
import draylar.goml.GetOffMyLawn;
import draylar.goml.block.ClaimAnchorBlock;
import draylar.goml.item.ClaimAnchorBlockItem;
import draylar.goml.registry.GOMLBlocks;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Material;
import net.minecraft.item.Item;
import net.minecraft.util.registry.Registry;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.injection.ModifyArg;

import java.util.function.IntSupplier;

import static draylar.goml.registry.GOMLBlocks.ANCHORS;

@Mixin( GOMLBlocks.class )
public class GOMLBlocksMixin {
	/**
	 * @author Doc
	 * @reason Changed texture
	 */
	@Overwrite(remap = false)
	private static Pair< ClaimAnchorBlock, Item > register( String name, IntSupplier radius, float hardness, String texture) {
		String t=texture;
		if(name.equalsIgnoreCase( "makeshift_claim_anchor" )){
			t="eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvY2Q0MDQyNzBjNmViZWI4ZmM5NGEzOTQ2YWY1MzM0ZTRhNWI2NTdhOWJmMjVlNzMzNWU3NGFiMTg1In19fQ==";
		}else if(name.equalsIgnoreCase("admin_claim_anchor"  )){
			t="eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZDU3MGMxYmJkOGM0ZDA1ZDdiZWE2ZDk2MGUwYTZlYWI2MDBjOGViMjY2MzQ3OTkxNzZiMzExZGJmZjViMWI5OCJ9fX0=";
		}
		ClaimAnchorBlock claimAnchorBlock = (ClaimAnchorBlock) Registry.register(Registry.BLOCK, GetOffMyLawn.id(name), new ClaimAnchorBlock( FabricBlockSettings.of( Material.STONE).strength(hardness, 3600000.0F), radius, t));
		ClaimAnchorBlockItem registeredItem = (ClaimAnchorBlockItem)Registry.register(Registry.ITEM, GetOffMyLawn.id(name), new ClaimAnchorBlockItem(claimAnchorBlock, (new Item.Settings()).group(GetOffMyLawn.GROUP), 0));
		ANCHORS.add(claimAnchorBlock);
		return Pair.of(claimAnchorBlock, registeredItem);
	}

}

package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.registry.ItemRegistry;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.GlassBottleItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsage;
import net.minecraft.item.Items;
import net.minecraft.potion.PotionUtil;
import net.minecraft.potion.Potions;
import net.minecraft.stat.Stats;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin( GlassBottleItem.class)
public class GlassBottleItemMixin {
	@Inject( method = "use", at = @At( value = "INVOKE_ASSIGN", target = "Lnet/minecraft/util/TypedActionResult;success(Ljava/lang/Object;Z)Lnet/minecraft/util/TypedActionResult;", ordinal=1 ), cancellable = true )
	public void use( World world, PlayerEntity user, Hand hand, CallbackInfoReturnable< TypedActionResult< ItemStack > > cir ) {
		ItemStack itemStack = user.getStackInHand(hand);
		user.incrementStat( Stats.USED.getOrCreateStat( ((GlassBottleItem)(Object)this)));
		cir.setReturnValue( TypedActionResult.success(ItemUsage.exchangeStack(itemStack, user, ItemRegistry.WATER_BOTTLE.getDefaultStack() ), world.isClient()) );
		cir.cancel();
		return;
	}
}

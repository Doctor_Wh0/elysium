package com.enderzombi102.elysium.mixin.other;


import com.kwpugh.treeaxe.init.ItemInit;
import com.kwpugh.treeaxe.items.ItemTreeAxe;
import com.magistuarmory.item.ModItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ToolMaterials;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;


@Mixin( ItemInit.class )
public abstract class ItemInitMixin {
	@Accessor( "WOODEN_TREEAXE" )
	@Mutable
	public static void setWoodenTreeaxe( Item type ) {
	}

	@Accessor( "IRON_TREEAXE" )
	@Mutable
	public static void setIronTreeaxe( Item type ) {
	}

	@Accessor( "EMERALD_TREEAXE" )
	@Mutable
	public static void setEmeraldTreeaxe( Item type ) {
	}

	@Accessor( "DIAMOND_TREEAXE" )
	@Mutable
	public static void setDiamondTreeaxe( Item type ) {
	}

	@Accessor( "GOLD_TREEAXE" )
	@Mutable
	public static void setGoldTreeaxe( Item type ) {
	}

	@Accessor( "AMETHYST_TREEAXE" )
	@Mutable
	public static void setAmethystTreeaxe( Item type ) {
	}

	@Accessor( "NETHERITE_TREEAXE" )
	@Mutable
	public static void setNetheriteTreeaxe( Item type ) {
	}

	@Accessor( "COPPER_TREEAXE" )
	@Mutable
	public static void setCopperTreeaxe( Item type ) {
	}

	@Inject( at = @At( "HEAD" ), method = "<clinit>", cancellable = true )
	private static void register( CallbackInfo ci ) {
		setWoodenTreeaxe( new ItemTreeAxe( ToolMaterials.WOOD, ToolMaterials.WOOD.getAttackDamage(),-3, ( new Item.Settings() ).maxDamage( ToolMaterials.WOOD.getDurability() * 3 ).group( ItemGroup.TOOLS ) ) );
		setCopperTreeaxe( new ItemTreeAxe( ToolMaterials.IRON, ModItemTier.COPPER.getAttackDamage(), -3, ( new Item.Settings() ).maxDamage( ModItemTier.COPPER.getDurability() * 3 ).group( ItemGroup.TOOLS ) ) );
		setGoldTreeaxe( new ItemTreeAxe( ToolMaterials.GOLD, ToolMaterials.GOLD.getAttackDamage(), -3, ( new Item.Settings() ).maxDamage( ToolMaterials.GOLD.getDurability() * 3 ).group( ItemGroup.TOOLS ) ) );
		setIronTreeaxe( new ItemTreeAxe( ToolMaterials.IRON, ToolMaterials.IRON.getAttackDamage(), -3, ( new Item.Settings() ).maxDamage( ToolMaterials.IRON.getDurability() * 3 ).group( ItemGroup.TOOLS ) ) );
		setEmeraldTreeaxe( new ItemTreeAxe( ToolMaterials.NETHERITE, ToolMaterials.NETHERITE.getAttackDamage(), -3, ( new Item.Settings() ).maxDamage( ToolMaterials.NETHERITE.getDurability() * 3 ).group( ItemGroup.TOOLS ) ) );
		setAmethystTreeaxe( new ItemTreeAxe( ToolMaterials.DIAMOND, ToolMaterials.DIAMOND.getAttackDamage(), -3, ( new Item.Settings() ).maxDamage( ToolMaterials.DIAMOND.getDurability() * 3 ).group( ItemGroup.TOOLS ) ) );
		setDiamondTreeaxe( new ItemTreeAxe( ToolMaterials.DIAMOND, ToolMaterials.DIAMOND.getAttackDamage(), -3, ( new Item.Settings() ).maxDamage( ToolMaterials.DIAMOND.getDurability() * 3 ).group( ItemGroup.TOOLS ) ) );
		setNetheriteTreeaxe( new ItemTreeAxe( ToolMaterials.NETHERITE, ToolMaterials.NETHERITE.getAttackDamage(), -3, ( new Item.Settings() ).maxDamage( ToolMaterials.NETHERITE.getDurability() * 3 ).fireproof().group( ItemGroup.TOOLS ) ) );
		ci.cancel();
	}
}

package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.util.Util;
import io.github.apace100.origins.origin.OriginLayers;
import io.github.apace100.origins.registry.ModComponents;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import team.creative.itemphysic.server.ItemPhysicServer;

import java.util.Objects;

@Mixin( ItemPhysicServer.class )
public class ItemPhysicServerMixin {
	@Inject( method = "playerTouch(Lnet/minecraft/entity/ItemEntity;Lnet/minecraft/entity/player/PlayerEntity;Z)V", at = @At( "TAIL" ) )
	private static void playerTouch( ItemEntity entity, PlayerEntity player, boolean needsSneak, CallbackInfo callbackInfo ) {
		if ( entity.getStack().isIn( Util.ELYSIUM_DROPS ) ) {
			if ( player instanceof ServerPlayerEntity p ) {
				var r1 = ModComponents.ORIGIN.get( p );
				var role = r1.getOrigin( OriginLayers.getLayer( Identifier.tryParse( "elysium:role" ) ) ).getName().getString();
				if ( !p.kjs$getStages().getAll().contains( "admin" ) ) {
					p.addStatusEffect( new StatusEffectInstance( StatusEffects.GLOWING, 60 ) );
					Objects.requireNonNull( p.getServer() ).kjs$runCommandSilent( "broadcast minecraft:plain \"Un §l§6" + role + "§r ha raccolto §6§l" + Text.translatable( entity.getStack().getName().getString() ) + "§r§f.\"" );
					Elysium.LOGGER.info( "[Elysium] " + p.getName().getString() + " picked up [" + entity.getStack().getName().getString() + "] at " + ( (int) p.getX() ) + ", " + ( (int) p.getY() ) + ", " + ( (int) p.getZ() ) );
				}
			}

		}
	}

}

package com.enderzombi102.elysium.mixin.other;

import com.magistuarmory.item.armor.JoustingItem;
import com.magistuarmory.item.armor.MedievalArmorItem;
import me.shedaniel.cloth.clothconfig.shadowed.blue.endless.jankson.annotation.Nullable;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import java.util.List;

@Mixin( JoustingItem.class )
public class JoustingItemMixin extends MedievalArmorItem {
	public JoustingItemMixin( ArmorMaterial material, EquipmentSlot slot, Settings properties ) {
		super( material, slot, properties );
	}
	@Overwrite
	public void inventoryTick( ItemStack stack, World level, Entity entity, int i, boolean selected) {
		super.inventoryTick(stack, level, entity, i, selected);
		return;
	}
	@Overwrite
	public void appendTooltip( ItemStack stack, @Nullable World level, List< Text > tooltip, TooltipContext flag) {
		return;
	}
}

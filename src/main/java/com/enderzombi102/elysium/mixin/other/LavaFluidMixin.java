package com.enderzombi102.elysium.mixin.other;

import net.minecraft.block.BlockState;
import net.minecraft.fluid.FlowableFluid;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.LavaFluid;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.WorldAccess;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import static com.enderzombi102.elysium.registry.GameruleRegistry.DO_LAVA_FLOW;

@Mixin( LavaFluid.class )
public abstract class LavaFluidMixin extends FlowableFluid {
	@Inject(
		method = "flow",
		at = @At( "HEAD" ),
		cancellable = true
	)
	protected void flow( WorldAccess world, BlockPos pos, BlockState state, Direction direction, FluidState fluidState, CallbackInfo ci ) {
		var server = world.getServer();
		assert server != null : "Why is there a world with flowing lava with no server?";

		if ( !server.getGameRules().getBoolean( DO_LAVA_FLOW ) )
			ci.cancel();
	}
}

package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.registry.StatusEffectRegistry;
import com.google.common.collect.Multimap;
import draylar.goml.api.ClaimUtils;
import draylar.goml.registry.GOMLAugments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.spell_engine.api.item.weapon.SpellWeaponItem;
import net.spell_power.api.MagicSchool;
import net.spell_power.api.SpellDamageSource;
import net.spell_power.api.attributes.SpellAttributes;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Map;

@Mixin( value = LivingEntity.class, priority = 999999 )
public abstract class LivingEntityMixin extends Entity {
	@Shadow
	@Final
	private Map< StatusEffect, StatusEffectInstance > activeStatusEffects;

	public LivingEntityMixin( EntityType< ? > type, World world ) {
		super( type, world );
	}

	@Shadow
	public abstract ItemStack getStackInHand( Hand hand );

	@Inject( at = @At( "HEAD" ), method = "damage", cancellable = true )
	private void actual( DamageSource source, float amount, CallbackInfoReturnable< Boolean > cir ) {
		try {
			if ( Config.getData().features.dreamingGomlFixes ) {
				var claims = ClaimUtils.getClaimsAt( this.getWorld(), new BlockPos( this.getX(), this.getY(), this.getZ() ) );
				if ( !claims.isEmpty() ) {
					if (!(source.getAttacker() instanceof PlayerEntity) || !claims.anyMatch( claimBoxClaimEntry -> claimBoxClaimEntry.getValue().isOwner( source.getAttacker().getUuid() ) || claimBoxClaimEntry.getValue().hasAugment( GOMLAugments.get( Identifier.tryParse( "goml:pvp_arena" ) ) ) ) ) {
						if ( !this.getType().getUntranslatedName().equalsIgnoreCase( "target_dummy" ) && !this.kjs$isMonster() && !source.isOutOfWorld() ) {
							cir.setReturnValue( false );
							cir.cancel();
							return;
						}
					}
				}


				if ( source.getAttacker() instanceof PlayerEntity player && !( source instanceof SpellDamageSource ) ) {
					if ( player.getMainHandStack().getItem() instanceof SpellWeaponItem spellWeaponItem ) {
						Multimap< EntityAttribute, EntityAttributeModifier > m = spellWeaponItem.getAttributeModifiers( EquipmentSlot.MAINHAND );
						for ( EntityAttributeModifier attribute : m.values() ) {
							try {
								MagicSchool magicSchool = MagicSchool.fromAttributeId( new Identifier( attribute.getName() ) );
								if ( player.getAttributes().hasAttribute( SpellAttributes.POWER.get( magicSchool ).attribute ) ) {
									double value = player.getAttributeValue( SpellAttributes.POWER.get( magicSchool ).attribute );
									// TODO: equation to use amount damage original
									( (LivingEntity) (Object) this ).damage( SpellDamageSource.create( magicSchool, player ), (float) value );
								}
							} catch ( Exception ignored ) {
							}
						}
					}
				}
			}
		} catch ( Exception e ) {

		}
	}

	@Inject( at = @At( "HEAD" ), method = "hasStatusEffect", cancellable = true )
	public void hasStatusEffect( StatusEffect effect, CallbackInfoReturnable< Boolean > cir ) {
		if ( effect == StatusEffects.NIGHT_VISION ) {
			cir.setReturnValue( this.activeStatusEffects.containsKey( effect ) || this.activeStatusEffects.containsKey( StatusEffectRegistry.NIGHT_VISION_SONG ) );
		} else if ( effect== StatusEffects.RESISTANCE ) {
			cir.setReturnValue( this.activeStatusEffects.containsKey( effect ) || this.activeStatusEffects.containsKey( StatusEffectRegistry.RESISTANCE_SONG ) );
		} else {
			cir.setReturnValue( this.activeStatusEffects.containsKey( effect ) );
		}
	}

	@Inject( at = @At( "HEAD" ), method = "getStatusEffect", cancellable = true )
	public void getStatusEffect( StatusEffect effect, CallbackInfoReturnable< StatusEffectInstance > cir ) {
		if ( effect == StatusEffects.NIGHT_VISION || this.activeStatusEffects.containsKey( StatusEffectRegistry.NIGHT_VISION_SONG )) {
			StatusEffectInstance instance= this.activeStatusEffects.get( effect);
			if (instance==null){
				cir.setReturnValue( this.activeStatusEffects.get( StatusEffectRegistry.NIGHT_VISION_SONG ) );
			}else{
				cir.setReturnValue( instance );
			}
		} else if ( effect== StatusEffects.RESISTANCE || this.activeStatusEffects.containsKey( StatusEffectRegistry.RESISTANCE_SONG ) ) {
			StatusEffectInstance instance= this.activeStatusEffects.get( effect);
			if (instance==null){
				cir.setReturnValue( this.activeStatusEffects.get( StatusEffectRegistry.RESISTANCE_SONG ) );
			}else{
				cir.setReturnValue( instance );
			}
		} else {
			cir.setReturnValue( this.activeStatusEffects.get( effect ) );
		}
	}

	@Inject( at = @At( "HEAD" ), method = "canHaveStatusEffect", cancellable = true )
	public void canHave( StatusEffectInstance effect, CallbackInfoReturnable< Boolean > cir ) {
		if ( !this.getType().getUntranslatedName().equalsIgnoreCase( "target_dummy" ) && !this.kjs$isMonster() && !effect.getEffectType().isBeneficial() ) {
			var claims = ClaimUtils.getClaimsAt( this.getWorld(), new BlockPos( this.getX(), this.getY(), this.getZ() ) );
			if ( !claims.isEmpty() && !claims.anyMatch( claimBoxClaimEntry -> claimBoxClaimEntry.getValue().hasAugment( GOMLAugments.get( Identifier.tryParse( "goml:pvp_arena" ) ) ) ) ) {
				cir.setReturnValue( false );
				cir.cancel();
				return;
			}
		}
	}


}

package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.event.ModifyItemAttributeModifierEvent;
import com.magistuarmory.item.MedievalShieldItem;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

import java.util.function.Supplier;

@Mixin( MedievalShieldItem.class )
public class MedievalShieldItemMixin {
	@Shadow
	private Supplier< Ingredient > repairItem;

	/**
	 * @author Doc
	 * @reason Changed repairItem
	 */
	@Overwrite
	public boolean canRepair( ItemStack stack, ItemStack stack2 ) {
		if ( ModifyItemAttributeModifierEvent.repair.getOrDefault( stack.getItem().kjs$getId(), "null" ).equalsIgnoreCase( "null" ) ) {
			return ( (Ingredient) this.repairItem.get() ).test( stack2 );
		} else {
			return Ingredient.ofItems( Registry.ITEM.get( new Identifier( ModifyItemAttributeModifierEvent.repair.get( stack.getItem().kjs$getId() ) ) ) ).test( stack2 );
		}
	}
}

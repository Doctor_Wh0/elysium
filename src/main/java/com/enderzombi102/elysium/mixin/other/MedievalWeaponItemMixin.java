package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.imixin.MutableMultiMap;
import com.google.common.collect.Multimap;
import com.magistuarmory.item.MedievalWeaponItem;
import com.magistuarmory.item.WeaponType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.item.*;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin( MedievalWeaponItem.class )
public abstract class MedievalWeaponItemMixin extends SwordItem implements MutableMultiMap {
	@Shadow
	private boolean blockingPriority = false;
	@Shadow
	public final WeaponType type;

	public MedievalWeaponItemMixin(ToolMaterial toolMaterial, int attackDamage, float attackSpeed, Settings settings, WeaponType type) {
		super( toolMaterial, attackDamage, attackSpeed, settings );
        this.type = type;
    }

	@Override
	@Accessor( "defaultModifiers" )
	public abstract Multimap< EntityAttribute, EntityAttributeModifier > getDefaultModifiers();
	@Override
	@Accessor( "type" )
	public abstract WeaponType getType();

	@Override
	@Accessor( "type" )
	@Mutable
	public abstract void setType( WeaponType type );
	@Override
	@Accessor( "defaultModifiers" )
	@Mutable
	public abstract void setDefaultModifiers( Multimap< EntityAttribute, EntityAttributeModifier > attributes );
	@Override
	@Accessor( "isSilver" )
	@Mutable
	public abstract void setIsSilver( boolean isSilver);
	@Override
	@Accessor( "silverAttackDamage" )
	@Mutable
	public abstract void setSilverAttackDamage( float silverAttackDamage );
	@Overwrite
	public void inventoryTick( ItemStack stack, World level, Entity entity, int i, boolean selected) {
		if (entity instanceof LivingEntity livingentity) {
			if (type.canBlock()) {
				this.blockingPriority = !(livingentity.getMainHandStack().getItem() instanceof ShieldItem ) && !(livingentity.getOffHandStack().getItem() instanceof ShieldItem);
			}
		}
		super.inventoryTick(stack, level, entity, i, selected);
		return;
	}
}

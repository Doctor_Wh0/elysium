package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.imixin.MutableMultiMap;
import com.google.common.collect.Multimap;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.item.MiningToolItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin( MiningToolItem.class)
public abstract class MiningToolItemMixin implements MutableMultiMap {
	@Override
	@Accessor( value = "attributeModifiers" )
	@Mutable
	public abstract void setAttributeModifiers( Multimap<EntityAttribute, EntityAttributeModifier> attributes );
	@Override
	@Accessor( "attributeModifiers" )
	public abstract Multimap< EntityAttribute, EntityAttributeModifier > getAttributeModifiers();

}

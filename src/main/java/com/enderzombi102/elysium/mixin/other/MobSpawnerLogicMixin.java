package com.enderzombi102.elysium.mixin.other;

import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.MobSpawnerLogic;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin( MobSpawnerLogic.class )
public class MobSpawnerLogicMixin {
	private int spawns = 0;

	@Inject( at = @At( value = "INVOKE",
		target = "Lnet/minecraft/server/world/ServerWorld;syncWorldEvent(ILnet/minecraft/util/math/BlockPos;I)V" ),
		method = "serverTick(Lnet/minecraft/server/world/ServerWorld;Lnet/minecraft/util/math/BlockPos;)V",
		cancellable = true )
	private void entitySpawn( ServerWorld world, BlockPos pos, CallbackInfo ci ) {
		NbtCompound nbt = new NbtCompound();
		nbt = ( (MobSpawnerLogic) (Object) this ).writeNbt( nbt );
		String entity_string = nbt.get( "SpawnData" ).toString();
		entity_string = entity_string.substring( entity_string.indexOf( "\"" ) + 1 );
		entity_string = entity_string.substring( 0, entity_string.indexOf( "\"" ) );
		if ( entity_string.contains( "area_effect_cloud" ) )
			return;

		spawns++;
	}

	@Inject( at = @At( value = "INVOKE", target = "Lnet/minecraft/nbt/NbtCompound;getShort(Ljava/lang/String;)S" ), method = "readNbt" )
	private void readNbt( World world, BlockPos pos, NbtCompound nbt, CallbackInfo info ) {
		spawns = nbt.getInt( "spawns" );
	}

	@Inject( at = @At( value = "INVOKE", target = "Lnet/minecraft/nbt/NbtCompound;putShort(Ljava/lang/String;S)V" ), method = "writeNbt" )
	private void writeNbt( NbtCompound nbt, CallbackInfoReturnable< NbtCompound > info ) {
		nbt.putInt( "spawns", spawns );
	}
}

package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.event.ModifyItemAttributeModifierEvent;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.mk.archers_arsenal.items.ModBowItem;
import org.spongepowered.asm.mixin.Mixin;

@Mixin( ModBowItem.class)
public class ModBowItemMixin extends Item {

	public ModBowItemMixin( Settings settings ) {
		super( settings );
	}

	@Override
	public boolean canRepair( ItemStack stack, ItemStack ingredient ) {
		if ( ModifyItemAttributeModifierEvent.repair.getOrDefault( stack.getItem().kjs$getId(), "null" ).equalsIgnoreCase( "null" ) ) {
			return super.canRepair( stack, ingredient );
		} else {
			return Ingredient.ofItems( Registry.ITEM.get( new Identifier( ModifyItemAttributeModifierEvent.repair.get( stack.getItem().kjs$getId() ) ) ) ).test( ingredient );
		}
	}
}

package com.enderzombi102.elysium.mixin.other;

import com.magistuarmory.item.ModItemTier;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import static com.magistuarmory.item.ModItemTier.*;

@Mixin( ModItemTier.class )
public class ModItemTierMixin {

	@Shadow
	@Final
	private float attackDamageBonus;

	@Shadow
	@Final
	private int uses;

	@Shadow
	@Final
	private float speed;

	@Shadow
	@Final
	private int level;

	@Inject( method = "getAttackDamage", at = @At( "HEAD" ), cancellable = true )
	public void getAttackDamage( CallbackInfoReturnable< Float > ci ) {
		if ( ((ModItemTier)(Object) this).equals( COPPER ) ) {
			ci.setReturnValue( 1.5F );
		} else if ( ((Object) this).equals( SILVER ) ) {
			ci.setReturnValue( 2.5F );
		} else if ( ((Object) this).equals( TIN ) ) {
			ci.setReturnValue( 1F );
		} else if ( ((Object) this).equals( STEEL ) ) {
			ci.setReturnValue( 2.5F );
		} else {
			ci.setReturnValue( this.attackDamageBonus );
		}
	}

	@Inject( method = "getMiningLevel", at = @At( "HEAD" ), cancellable = true )
	public void getMiningLevel( CallbackInfoReturnable< Integer > ci ) {
		if ( ((ModItemTier)(Object) this).equals( COPPER ) ) {
			ci.setReturnValue( 2 );
		} else if ( ((Object) this).equals( SILVER ) ) {
			ci.setReturnValue( 5 );
		} else if ( ((Object) this).equals( TIN ) ) {
			ci.setReturnValue( 1 );
		} else if ( ((Object) this).equals( STEEL ) ) {
			ci.setReturnValue( 4 );
		} else {
			ci.setReturnValue( this.level );
		}
	}

	@Inject( method = "getDurability", at = @At( "HEAD" ), cancellable = true )
	public void getDurability( CallbackInfoReturnable< Integer > ci ) {
		if ( ((ModItemTier)(Object) this).equals( COPPER ) ) {
			ci.setReturnValue( 240 );
		} else if ( ((Object) this).equals( SILVER ) ) {
			ci.setReturnValue( 1440 );
		} else if ( ((Object) this).equals( TIN ) ) {
			ci.setReturnValue( 120 );
		} else if ( ((Object) this).equals( STEEL ) ) {
			ci.setReturnValue( 960 );
		} else {
			ci.setReturnValue( this.uses );
		}
	}
	@Inject( method = "getMiningSpeedMultiplier", at = @At( "HEAD" ), cancellable = true )
	public void getMiningSpeedMultiplier( CallbackInfoReturnable< Float > ci ) {
		if ( ((ModItemTier)(Object) this).equals( COPPER ) ) {
			ci.setReturnValue( 5F );
		} else if ( ((Object) this).equals( SILVER ) ) {
			ci.setReturnValue( 7F );
		} else if ( ((Object) this).equals( TIN ) ) {
			ci.setReturnValue( 4F );
		} else if ( ((Object) this).equals( STEEL ) ) {
			ci.setReturnValue( 7F );
		} else {
			ci.setReturnValue( this.speed );
		}
	}
}

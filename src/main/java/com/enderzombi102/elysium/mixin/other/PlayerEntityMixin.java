package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.item.Contract;
import com.enderzombi102.elysium.registry.CompRegistry;
import com.enderzombi102.elysium.registry.PowerTypeRegistry;
import com.enderzombi102.elysium.registry.StatusEffectRegistry;
import io.github.apace100.apoli.component.PowerHolderComponent;
import io.github.apace100.apoli.power.VariableIntPower;
import io.github.apace100.origins.registry.ModComponents;
import net.minecraft.block.BlockState;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.*;
import net.minecraft.entity.effect.StatusEffectUtil;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.HungerManager;
import net.minecraft.entity.player.PlayerAbilities;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.RangedWeaponItem;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.tag.FluidTags;
import net.minecraft.tag.TagKey;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.sql.Timestamp;
import java.util.function.Predicate;

import static com.enderzombi102.elysium.util.Const.ID;

@Mixin( value = PlayerEntity.class, priority = 999)
public abstract class PlayerEntityMixin extends LivingEntity {
	@Shadow
	@Final
	private PlayerInventory inventory;
	@Shadow
	@Final
	private PlayerAbilities abilities;

	protected PlayerEntityMixin( EntityType< ? extends LivingEntity > entityType, World world ) {
		super( entityType, world );
	}

	@Shadow
	public abstract ItemStack getEquippedStack( EquipmentSlot slot );

	@Shadow
	public abstract HungerManager getHungerManager();

	@Shadow
	public abstract boolean canConsume( boolean ignoreHunger );

	@Shadow
	public abstract void sendMessage( Text message, boolean overlay );

	@Shadow
	protected abstract void spawnParticles( ParticleEffect parameters );

	@ModifyVariable( method = "attack", at = @At( value = "JUMP", ordinal = 2 ), slice = @Slice( from = @At( value = "INVOKE", target = "Lnet/minecraft/entity/player/PlayerEntity;isSprinting()Z", ordinal = 1 ) ), index = 8 )
	private boolean attackMixin( boolean bl3 ) {
		return false;
	}

	@ModifyVariable( method = "attack", at = @At( "STORE" ), ordinal = 2 )
	private boolean attack( boolean bl3 ) {
		return false;
	}

	@Inject( method = "attack", at = @At( value = "INVOKE", target = "Lnet/minecraft/entity/LivingEntity;takeKnockback(DDD)V" ), cancellable = true )
	private void knockback( Entity target, CallbackInfo ci ) {
		if ( this.getEquippedStack( EquipmentSlot.MAINHAND ).getItem().equals( Items.AIR ) ) {
			ci.cancel();
		}
	}

	@Inject( method = "interact", at = @At( "HEAD" ), cancellable = true )
	private void interact( Entity entity, Hand hand, CallbackInfoReturnable< ActionResult > cir ) {
		if(!this.getWorld().isClient() && hand == Hand.MAIN_HAND) {
			ItemStack stack = ( (PlayerEntity) (Object) this ).getStackInHand( hand );
			if ( stack.getItem() instanceof Contract ) {
				if ( entity.getType().isIn( Elysium.ELYSIUM_OWNER ) ) {
					if ( Contract.isContractFilled( stack ) ) {
						if ( Contract.isContractValid( stack, entity ) ) {
							if ( Contract.isTargetOwned( ( (PlayerEntity) (Object) this ), entity ) ) {
								if ( !( (PlayerEntity) (Object) this ).getWorld().isClient ) {
									( (PlayerEntity) (Object) this ).sendMessage( Text.translatable( "text.elysium.same_owner" ), true );
								}
								cir.setReturnValue( ActionResult.SUCCESS );
							} else {
								try {
									NbtCompound nbtCompound = stack.getNbt();
									Long created = Long.valueOf( ( nbtCompound.get( "created" ).asString() ) );
									Timestamp timestamp = new Timestamp( System.currentTimeMillis() );
									Long time = timestamp.getTime();
									Long diff = time - created;
									if ( ( diff / 1000 ) < 30 ) {
										( (Contract) stack.getItem() ).transferOwnership( ( (PlayerEntity) (Object) this ), entity );
										stack.decrement( 1 );
										( (PlayerEntity) (Object) this ).world.playSound( null, ( (PlayerEntity) (Object) this ).getX(), ( (PlayerEntity) (Object) this ).getY(), ( (PlayerEntity) (Object) this ).getZ(), SoundEvents.ITEM_BOOK_PAGE_TURN, SoundCategory.NEUTRAL, 1.5F, 1.0F + ( ( (PlayerEntity) (Object) this ).world.random.nextFloat() - ( (PlayerEntity) (Object) this ).world.random.nextFloat() ) * 0.4F );
										cir.setReturnValue( ActionResult.SUCCESS );
									} else {
										( (PlayerEntity) (Object) this ).sendMessage( Text.translatable( "text.elysium.time_expired" ), true );
										cir.setReturnValue( ActionResult.SUCCESS );
									}
								} catch ( Exception e ) {
									Elysium.LOGGER.info( e.getMessage() );
									e.printStackTrace();
									cir.setReturnValue( ActionResult.SUCCESS );
								}
							}
						} else {
							( (PlayerEntity) (Object) this ).sendMessage( Text.translatable( "text.elysium.contract_fail" ), true );
							cir.setReturnValue( ActionResult.SUCCESS );
						}
					} else if ( Contract.isTargetOwned( ( (PlayerEntity) (Object) this ), entity ) || Contract.canSteal( entity ) ) {
						Contract.fillContract( stack, ( (PlayerEntity) (Object) this ), entity );
						( (PlayerEntity) (Object) this ).world.playSound( null, ( (PlayerEntity) (Object) this ).getX(), ( (PlayerEntity) (Object) this ).getY(), ( (PlayerEntity) (Object) this ).getZ(), SoundEvents.ITEM_BOOK_PAGE_TURN, SoundCategory.NEUTRAL, 1.5F, 1.0F + ( ( (PlayerEntity) (Object) this ).world.random.nextFloat() - ( (PlayerEntity) (Object) this ).world.random.nextFloat() ) * 0.4F );
						cir.setReturnValue( ActionResult.SUCCESS );
					} else {
						if ( !( (PlayerEntity) (Object) this ).getWorld().isClient ) {
							( (PlayerEntity) (Object) this ).sendMessage( Text.translatable( "text.elysium.contract_fail" ), true );
						}
						cir.setReturnValue( ActionResult.SUCCESS );
					}
				}
			} else if ( entity.getType().isIn( Elysium.ELYSIUM_OWNER ) ) {
				if ( !Contract.isTargetOwned( ( (PlayerEntity) (Object) this ), entity ) ) {
					NbtCompound nbtCompound = new NbtCompound();
					entity.writeNbt( nbtCompound );
					if ( !( entity instanceof Tameable ) && nbtCompound.get( "Tame" ) == null && !( entity instanceof TameableEntity ) && !Contract.hasOwner( entity ) ) {
						Contract.setEntityOwner( entity, ( (PlayerEntity) (Object) this ) );
						( (PlayerEntity) (Object) this ).sendMessage( Text.translatable( "text.elysium.first_owner" ), true );
						cir.setReturnValue( ActionResult.SUCCESS );
					}
				}
			} else if ( entity instanceof PlayerEntity ) {
				try {
					var value = 0;
					if ( PowerHolderComponent.KEY.get( entity ).hasPower( PowerTypeRegistry.FADE_SWITCH ) ) {
						var power = PowerHolderComponent.KEY.get( entity ).getPower( PowerTypeRegistry.FADE_SWITCH );
						if ( power instanceof VariableIntPower vip ) {
							value = vip.getValue();
						}
					}
					if ( stack.equals( ItemStack.EMPTY ) && value == 0 ) {
						String status = CompRegistry.ELYSIUM_LEVEL.get( Elysium.server.getSaveProperties() ).getPlayerStatus( entity.getUuid() );
						if ( status.length() > 1 ) {
							this.sendMessage( Text.literal( "Noti " + CompRegistry.ELYSIUM_LEVEL.get( Elysium.server.getSaveProperties() ).getPlayerStatus( entity.getUuid() ) ) );
						}
						cir.setReturnValue( ActionResult.PASS );
					}
				} catch ( Exception e ) {
				}
			}
		}
	}

	@Inject( method = "getArrowType", at = @At( "RETURN" ), cancellable = true )
	public void getArrowType( ItemStack stack, CallbackInfoReturnable< ItemStack > cir ) {
		if ( cir.getReturnValue() != ItemStack.EMPTY ) {
			int tier = -1;
			for ( int i = 0; i < 6; i++ ) {
				if ( stack.isIn( TagKey.of( Registry.ITEM_KEY, new Identifier( ID, "weapons/class/hunter/" + i ) ) ) ) {
					tier = i;
					i = 6;
				}
			}
			Predicate< ItemStack > predicate = ( (RangedWeaponItem) stack.getItem() ).getProjectiles();
			for ( int i = 0; i < this.inventory.size(); ++i ) {
				ItemStack itemStack2 = this.inventory.getStack( i );
				if ( tier != -1 ) {
					if ( predicate.test( itemStack2 ) && itemStack2.isIn( TagKey.of( Registry.ITEM_KEY, new Identifier( ID, "arrow_t" + tier ) ) ) ) {
						cir.setReturnValue( itemStack2 );
						return;
					}
				} else {
					if ( predicate.test( itemStack2 ) ) {
						cir.setReturnValue( itemStack2 );
						return;
					}
				}
				cir.setReturnValue( ItemStack.EMPTY );
			}
		}
	}

	/**
	 * @author Doc
	 * @reason Haste Song
	 */
	@Overwrite
	public float getBlockBreakingSpeed( BlockState block ) {
		float f = this.inventory.getBlockBreakingSpeed( block );
		if ( f > 1.0F ) {
			int i = EnchantmentHelper.getEfficiency( this );
			ItemStack itemStack = this.getMainHandStack();
			if ( i > 0 && !itemStack.isEmpty() ) {
				f += (float) ( i * i + 1 );
			}
		}

		if ( StatusEffectUtil.hasHaste( this )) {
			f *= 1.0F + (float) ( StatusEffectUtil.getHasteAmplifier( this ) + 1 ) * 0.2F;
		}else if( this.hasStatusEffect( StatusEffectRegistry.HASTE_SONG )){
			f *= 1.0F + (float) ( this.getStatusEffect(StatusEffectRegistry.HASTE_SONG).getAmplifier() + 1 ) * 0.025F;
		}

		if ( this.hasStatusEffect( StatusEffects.MINING_FATIGUE ) ) {
			float g;
			switch ( this.getStatusEffect( StatusEffects.MINING_FATIGUE ).getAmplifier() ) {
				case 0:
					g = 0.3F;
					break;
				case 1:
					g = 0.09F;
					break;
				case 2:
					g = 0.0027F;
					break;
				case 3:
				default:
					g = 8.1E-4F;
			}

			f *= g;
		}

		if ( this.isSubmergedIn( FluidTags.WATER ) && !EnchantmentHelper.hasAquaAffinity( this ) ) {
			f /= 5.0F;
		}

		if ( !this.onGround ) {
			f /= 5.0F;
		}

		return f;
	}
}


package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.registry.CompRegistry;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Objects;

@Mixin( PlayerManager.class )
public class PlayerManagerMixin {
	@Inject( method = "remove", at = @At( "HEAD" ) )
	public void rememberLeaveTime( ServerPlayerEntity player, CallbackInfo ci ) {
		assert Elysium.server != null : "Running server code on a client? wut";
		CompRegistry.ELYSIUM_LEVEL
			.get( Elysium.server.getSaveProperties() )
			.setLastPlayerTime( player.getUuid(), Objects.requireNonNull( player.server.getWorld( ServerWorld.OVERWORLD ) ).getTime() );
	}
}

package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.Elysium;
import com.llamalad7.mixinextras.injector.WrapWithCondition;
import io.github.apace100.apoli.component.PowerHolderComponentImpl;
import io.github.apace100.apoli.power.Power;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin( value = PowerHolderComponentImpl.class, remap = false )
public class PowerHolderComponentImplMixin {
	@WrapWithCondition(
		method = "removePower",
		at = {
			@At(
				value = "INVOKE",
				target = "Lio/github/apace100/apoli/power/Power;onRemoved()V"
			),
			@At(
				value = "INVOKE",
				target = "Lio/github/apace100/apoli/power/Power;onLost()V"
			)
		}
	)
	public boolean blockRemoveSideEffects( Power power ) {
		return !Elysium.reloading;
	}

	@WrapWithCondition(
		method = "addPower",
		at = {
			@At(
				value = "INVOKE",
				target = "Lio/github/apace100/apoli/power/Power;onAdded()V"
			),
			@At(
				value = "INVOKE",
				target = "Lio/github/apace100/apoli/power/Power;onGained()V"
			)
		}
	)
	public boolean blockGainSideEffects( Power power ) {
		return !Elysium.reloading;
	}
}

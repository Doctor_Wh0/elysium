package com.enderzombi102.elysium.mixin.other;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.passive.SheepEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin( SheepEntity.class )
public class SheepEntityMixin extends AnimalEntity {

	protected SheepEntityMixin( EntityType< ? extends AnimalEntity > entityType, World world ) {
		super( entityType, world );
	}

	@Inject( method = "interactMob", at = @At( "HEAD" ), cancellable = true )
	public void interactMob( PlayerEntity player, Hand hand, CallbackInfoReturnable< ActionResult > cir ) {
		ItemStack itemStack = player.getStackInHand( hand );
		if ( !itemStack.isOf( Items.SHEARS ) ) {
			cir.setReturnValue( super.interactMob( player, hand ) );
		} else {
			cir.setReturnValue( ActionResult.FAIL );
		}
	}

	@Nullable
	@Override
	public PassiveEntity createChild( ServerWorld world, PassiveEntity entity ) {
		return null;
	}
}

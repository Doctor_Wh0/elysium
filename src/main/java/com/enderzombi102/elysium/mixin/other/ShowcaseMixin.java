package com.enderzombi102.elysium.mixin.other;

import io.github.wouink.furnish.block.Showcase;
import io.github.wouink.furnish.block.tileentity.ShowcaseTileEntity;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin( Showcase.class )
public class ShowcaseMixin {
	@Inject( method = "onUse", at = @At( "HEAD" ), cancellable = true )
	public void onUse( BlockState state, World world, BlockPos pos, PlayerEntity playerEntity, Hand hand, BlockHitResult hitResult, CallbackInfoReturnable< ActionResult > callbackInfoReturnable ) {
		if ( !world.isClient() ) {
			BlockEntity tileEntity = world.getBlockEntity( pos );
			if ( tileEntity instanceof ShowcaseTileEntity ) {
				if ( ( playerEntity.getStackInHand( Hand.MAIN_HAND ).toString().equals( "1 air" ) ) || playerEntity.getStackInHand( Hand.MAIN_HAND ).getCount() > 0 ) {
					ItemStack itemStack = ( (ShowcaseTileEntity) tileEntity ).swap( playerEntity.getStackInHand( Hand.MAIN_HAND ) );
					playerEntity.setStackInHand( Hand.MAIN_HAND, itemStack );
				}
			}
			callbackInfoReturnable.setReturnValue( ActionResult.SUCCESS );
		}
	}
}

package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.imixin.MutableMultiMap;
import com.google.common.collect.Multimap;
import com.nhoryzon.mc.farmersdelight.item.SkilletItem;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin( SkilletItem.class )
public abstract class SkilletItemMixin implements MutableMultiMap {

	@Override
	@Accessor( value = "toolAttributes" )
	public abstract Multimap< EntityAttribute, EntityAttributeModifier > getToolModifiers();

	@Override
	@Accessor( value = "toolAttributes" )
	@Mutable
	public abstract void setToolModifiers( Multimap< EntityAttribute, EntityAttributeModifier > attributes );
}

package com.enderzombi102.elysium.mixin.other;

import dev.emi.trinkets.api.TrinketsApi;
import net.fabricmc.fabric.api.entity.event.v1.ServerLivingEntityEvents;
import net.fabricmc.fabric.api.util.TriState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.tag.TagKey;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.spell_engine.SpellEngineMod;
import net.spell_engine.api.effect.RemoveOnHit;
import net.spell_engine.api.item.trinket.SpellBookItem;
import net.spell_engine.fabric.FabricMod;
import net.spell_engine.utils.SoundHelper;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import java.util.Iterator;

import static com.enderzombi102.elysium.util.Const.ID;

@Mixin( FabricMod.class)
public class SpellEngineFabricModMixin {
	/**
	 * @author Doc
	 * @reason Check book - rank
	**/
	@Overwrite(remap = false)
	public void onInitialize() {
		SpellEngineMod.init();
		SpellEngineMod.registerEnchantments();
		SpellEngineMod.registerSpellBinding();
		SoundHelper.registerSounds();
		TrinketsApi.registerTrinketPredicate(new Identifier("spell_engine", "spell_book"), ( itemStack, slotReference, livingEntity) -> {
			if ( itemStack.getItem() instanceof SpellBookItem /*&& itemStack.isIn( TagKey.of( Registry.ITEM_KEY, new Identifier( ID, "book_t0" ) ) )*/ ) {
			
			}

			return itemStack.getItem() instanceof SpellBookItem ? TriState.TRUE : TriState.DEFAULT;
		});
		ServerLivingEntityEvents.ALLOW_DAMAGE.register(( entity, source, amount) -> {
			Entity attacker = source.getAttacker();
			if (amount > 0.0F && attacker != null) {
				Iterator var4 = entity.getStatusEffects().iterator();

				while(var4.hasNext()) {
					StatusEffectInstance instance = (StatusEffectInstance)var4.next();
					StatusEffect effect = instance.getEffectType();
					if ( RemoveOnHit.shouldRemoveOnDirectHit(effect)) {
						entity.removeStatusEffect(effect);
						break;
					}
				}
			}

			return true;
		});
	}
}

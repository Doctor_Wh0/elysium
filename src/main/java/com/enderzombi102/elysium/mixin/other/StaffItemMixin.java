package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.imixin.MutableMultiMap;
import com.google.common.collect.Multimap;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.spell_engine.api.item.weapon.StaffItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin( StaffItem.class)
public abstract class StaffItemMixin implements MutableMultiMap {
	@Override
	@Accessor( "attributes" )
	public abstract Multimap< EntityAttribute, EntityAttributeModifier > getDefaultModifiers();
	@Override
	@Accessor( "attributes" )
	@Mutable
	public abstract void setDefaultModifiers( Multimap< EntityAttribute, EntityAttributeModifier > attributes );
}

package com.enderzombi102.elysium.mixin.other;

import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.util.collection.DefaultedList;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.gen.Invoker;
import satisfy.bakery.recipe.StoveRecipe;

import java.util.ArrayList;

@Mixin( satisfy.bakery.entity.StoveBlockEntity.class )
public abstract class StoveBlockEntityMixin {
	@Invoker( "canCraft" )
	public abstract boolean callcanCraft( StoveRecipe recipe );

	@Invoker( "getStack" )
	public abstract ItemStack callgetStack( int slot );

	@Invoker( "setStack" )
	public abstract void callsetStack( int slot, ItemStack stack );

	//@Inject( method = "craft", at = @At( "HEAD" ), remap = false, cancellable = true )
	@Overwrite( remap = false )
	protected void craft( StoveRecipe recipe ) {
		if ( recipe != null && callcanCraft( recipe ) ) {
			ItemStack recipeOutput = recipe.getOutput();
			ItemStack outputSlotStack = callgetStack( 0 );
			if ( outputSlotStack.isEmpty() ) {
				callsetStack( 0, recipeOutput );
			} else if ( outputSlotStack.isOf( recipeOutput.getItem() ) ) {
				outputSlotStack.increment( recipeOutput.getCount() );
			}
			DefaultedList< Ingredient > ingredients = recipe.getIngredients();
			ArrayList< Integer > l = new ArrayList<>();
			for ( int i = 1; i < 4; i++ ) {
				for ( int j = 0; j < ingredients.size(); j++ ) {
					if ( ingredients.get( j ).test( callgetStack( i ) ) && !l.contains( j ) ) {
						callgetStack( i ).decrement( 1 );
						l.add( j );
					}
				}
			}
		}
	}
}

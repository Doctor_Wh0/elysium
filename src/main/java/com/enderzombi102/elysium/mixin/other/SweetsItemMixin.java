package com.enderzombi102.elysium.mixin.other;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import satisfy.bakery.item.SweetsItem;

import java.util.List;

@Mixin( SweetsItem.class)
public class SweetsItemMixin extends Item {
	public SweetsItemMixin( Settings settings ) {
		super( settings );
	}

	/**
	 * @author Doc
	 * @reason Remove effects
	 */
	@Overwrite
	public ItemStack finishUsing( ItemStack stack, World world, LivingEntity entity) {
		return super.finishUsing(stack, world, entity);
	}
	@Inject( at = @At( value = "HEAD" ), method = "appendTooltip", cancellable = true )
	@Environment( EnvType.CLIENT )
	private void tooltip( ItemStack stack, @Nullable World world, List< Text > tooltip, TooltipContext context, CallbackInfo ci ) {
		ci.cancel();
		return;
	}
}

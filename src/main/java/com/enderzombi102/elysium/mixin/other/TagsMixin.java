package com.enderzombi102.elysium.mixin.other;

import dev.latvian.mods.kubejs.util.Tags;
import net.minecraft.tag.TagKey;
import net.minecraft.util.registry.RegistryEntry;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import java.util.stream.Stream;

@Mixin( Tags.class )
public class TagsMixin {
	/**
	 * @author Doc
	 * @reason Removed useless warning
	 */
	@Overwrite
	private static < T > Stream< TagKey< T > > forHolder( RegistryEntry.Reference< T > registryHolder ) {
		return registryHolder.streamTags();
	}
}

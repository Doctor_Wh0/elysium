package com.enderzombi102.elysium.mixin.other;

import draylar.goml.api.ClaimUtils;
import net.mehvahdjukaar.dummmmmmy.common.TargetDummyEntity;
import net.mehvahdjukaar.dummmmmmy.configs.CommonConfigs;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.boss.WitherEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

import java.util.Map;
import java.util.Objects;

@Mixin( TargetDummyEntity.class)
public abstract class TargetDummyEntityMixin extends MobEntity {
	@Shadow
	@Final
	private Map< ServerPlayerEntity, Integer > currentlyAttacking;

	@Shadow
	private DamageSource currentDamageSource;

	@Shadow
	public abstract void dismantle( boolean drops );

	protected TargetDummyEntityMixin( EntityType< ? extends MobEntity > entityType, World world ) {
		super( entityType, world );
	}

	/**
	 * @author
	 * @reason
	 */
	@Overwrite
	public boolean damage( DamageSource source, float damage) {
		if (source == DamageSource.OUT_OF_WORLD) {
			this.remove( Entity.RemovalReason.KILLED);
			return true;
		} else if (!(source.getSource() instanceof WitherEntity ) && !(source.getAttacker() instanceof WitherEntity)) {
			Entity var4 = source.getAttacker();
			if (var4 instanceof PlayerEntity ) {
				PlayerEntity player = (PlayerEntity)var4;
				if (player instanceof ServerPlayerEntity ) {
					ServerPlayerEntity sp = (ServerPlayerEntity)player;
					this.currentlyAttacking.put(sp, (Integer) CommonConfigs.MAX_COMBAT_INTERVAL.get());
				}

				if (player.isSneaking() && player.getMainHandStack().isEmpty()) {
					var claims = ClaimUtils.getClaimsAt( Objects.requireNonNull( source.getSource() ).getWorld(), new BlockPos(this.getX(),this.getY(),this.getZ()) );
					if ( !claims.isEmpty() && !claims.anyMatch( claimBoxClaimEntry -> claimBoxClaimEntry.getValue().isOwner( source.getAttacker().getUuid() ) ) )  {
						return true;
					}else{
						this.dismantle(!player.isCreative());
						return false;
					}
				}
			}
			this.currentDamageSource = source;
			boolean result = super.damage(source, damage);
			this.currentDamageSource = null;
			this.hurtTime = 0;
			return result;
		} else {
			this.dismantle(true);
			return true;
		}
	}
}

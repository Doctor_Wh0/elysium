package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.imixin.MutableMultiMap;
import com.magistuarmory.item.ModItemTier;
import net.minecraft.block.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.gen.Accessor;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin( ToolItem.class)
public abstract class ToolItemMixin implements MutableMultiMap
{
	@Shadow
	@Final
	private ToolMaterial material;

	@Override
	@Accessor( "material" )
	@Mutable
	public abstract void setType( ToolMaterial type );

	@Override
	@Accessor( "material" )
	@Mutable
	public abstract ToolMaterial getToolMaterial();



	@Inject(method = "canRepair", at = @At ( value = "HEAD" ),cancellable = true)
	public void canRepair( ItemStack stack, ItemStack ingredient, CallbackInfoReturnable< Boolean > cir ) {
		if(stack.getItem() instanceof PickaxeItem ) {
			if(!this.material.getRepairIngredient().equals( ToolMaterials.WOOD.getRepairIngredient()) && !stack.getItem().kjs$getId().equalsIgnoreCase( "elysium:tin_pickaxe" )) {
				cir.setReturnValue( false );
				cir.cancel();
			}
		}
	}
}

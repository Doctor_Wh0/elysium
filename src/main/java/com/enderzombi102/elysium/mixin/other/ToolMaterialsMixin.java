package com.enderzombi102.elysium.mixin.other;

import net.minecraft.item.ToolMaterials;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin( ToolMaterials.class )
public class ToolMaterialsMixin {
	@Shadow
	@Final
	private int miningLevel;

	@Shadow
	@Final
	private int itemDurability;

	@Shadow
	@Final
	private float attackDamage;

	@Shadow
	@Final
	private float miningSpeed;

	@Inject( method = "getMiningLevel", at = @At( "HEAD" ), cancellable = true )
	public void getMiningLevel( CallbackInfoReturnable< Integer > ci ) {
		switch ( ( (ToolMaterials) (Object) this ) ) {
			case IRON:
				ci.setReturnValue( 3 );
				break;
			case GOLD:
				ci.setReturnValue( 4 );
				break;
			case DIAMOND:
				ci.setReturnValue( 5 );
				break;
			case NETHERITE:
				ci.setReturnValue( 6 );
				break;
			default:
				ci.setReturnValue( this.miningLevel );
		}
	}

	@Inject( method = "getMiningSpeedMultiplier", at = @At( "HEAD" ), cancellable = true )
	public void getMiningSpeedMultiplier( CallbackInfoReturnable< Float > ci ) {
		if ( ( (ToolMaterials) (Object) this ) == ToolMaterials.GOLD ) {
			ci.setReturnValue( 7F );
		} else {
			ci.setReturnValue( this.miningSpeed );
		}
	}

	@Inject( method = "getDurability", at = @At( "HEAD" ), cancellable = true )
	public void getDurability( CallbackInfoReturnable< Integer > ci ) {
		switch ( ( (ToolMaterials) (Object) this ) ) {
			case WOOD:
				ci.setReturnValue( 60 );
				break;
			case IRON:
				ci.setReturnValue( 480 );
				break;
			case GOLD:
				ci.setReturnValue( 720 );
				break;
			case DIAMOND:
				ci.setReturnValue( 1420 );
				break;
			case NETHERITE:
				ci.setReturnValue( 2160 );
				break;
			default:
				ci.setReturnValue( this.itemDurability );
		}
	}

	@Inject( method = "getAttackDamage", at = @At( "HEAD" ), cancellable = true )
	public void getAttackDamage( CallbackInfoReturnable< Float > ci ) {
		switch ( ( (ToolMaterials) (Object) this ) ) {
			case DIAMOND:
				ci.setReturnValue( 3.5F );
				break;
			case NETHERITE:
				ci.setReturnValue( 4.5F );
				break;
			case GOLD:
				ci.setReturnValue( 2.5F );
				break;
			default:
				ci.setReturnValue( this.attackDamage );
		}
	}
}

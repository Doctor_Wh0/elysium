package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.Elysium;
import com.github.NGoedix.videoplayer.client.gui.VideoScreen;
import com.mojang.blaze3d.platform.GlStateManager;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin( VideoScreen.class)
public class VideoScreenMixin extends Screen {

	@Shadow
	private boolean started;

	@Shadow
	private float volume;

	protected VideoScreenMixin( Text title ) {
		super( title );
	}

	@Inject(
		method = "close",
		at = @At( value = "INVOKE", target = "Lme/srrapero720/watermedia/api/player/SyncVideoPlayer;stop()V" ),
		cancellable = true
	)
	public void close( CallbackInfo ci ) {
		if(this.volume==101) {
			PacketByteBuf buf= PacketByteBufs.create();
			buf.writeBoolean( true );
			ClientPlayNetworking.send( Elysium.VIDEO_ID,buf);
		}
	}
}

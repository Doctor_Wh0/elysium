package com.enderzombi102.elysium.mixin.other;

import com.minecraftserverzone.weaponmaster.config.ModConfigs;
import com.minecraftserverzone.weaponmaster.gui.DetailedSettingsScreen;
import com.minecraftserverzone.weaponmaster.gui.WeaponMasterScreen;
import com.minecraftserverzone.weaponmaster.gui.toggleButton.ToggleButton;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

@Mixin( WeaponMasterScreen.class )
public abstract class WeaponMasterScreenMixin extends Screen {
	@Shadow
	protected abstract void changeData( int slot );

	protected WeaponMasterScreenMixin( Text title ) {
		super( title );
	}

	/**
	 * @author Doc
	 * @reason Removed "More" button
	 */
	@Overwrite
	protected void init() {
		boolean[] toggleSlots = new boolean[]{ ModConfigs.SLOT1, ModConfigs.SLOT2, ModConfigs.SLOT3, ModConfigs.SLOT4, ModConfigs.SLOT5, ModConfigs.SLOT6, ModConfigs.SLOT7, ModConfigs.SLOT8, ModConfigs.SLOT9, ModConfigs.SHIELD, ModConfigs.BANNER};

		for(int i = 0; i < 9; ++i) {
			int slot = i;
			this.addDrawableChild(new ToggleButton(this.width / 2 - 111 + slot * 16, this.height / 2 - 50, 16, 20, Text.translatable(String.valueOf(slot + 1)), ( p_96337_) -> {
				this.changeData(slot);
				p_96337_.toggle = !p_96337_.toggle;
			}, toggleSlots[slot], 0));
		}

		this.addDrawableChild(new ToggleButton(this.width / 2 - 111 + 0, this.height / 2 - 30, 40, 20, Text.translatable("weaponmaster.screen.shield"), (p_96337_) -> {
			this.changeData(9);
			p_96337_.toggle = !p_96337_.toggle;
		}, toggleSlots[9], 0));
		this.addDrawableChild(new ToggleButton(this.width / 2 - 111 + 40, this.height / 2 - 30, 45, 20, Text.translatable("weaponmaster.screen.banner"), (p_96337_) -> {
			this.changeData(10);
			p_96337_.toggle = !p_96337_.toggle;
		}, toggleSlots[10], 0));
		super.init();
	}
}

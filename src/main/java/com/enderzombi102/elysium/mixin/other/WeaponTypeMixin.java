package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.imixin.MutableMultiMap;
import com.google.common.collect.Multimap;
import com.magistuarmory.item.WeaponType;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin( WeaponType.class )
public abstract class WeaponTypeMixin implements MutableMultiMap {
	@Override
	@Accessor( value = "armorPiercing" )
	@Mutable
	public abstract void setArmorPiercing( int armorPiercing );
}

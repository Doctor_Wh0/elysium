package com.enderzombi102.elysium.mixin.other;

import com.enderzombi102.elysium.imixin.MutableMultiMap;
import com.google.common.collect.Multimap;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.wizards.item.WizardArmor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin( WizardArmor.class)
public abstract class WizardArmorMixin implements MutableMultiMap {

	@Override
	@Accessor( value = "attributes", remap = false )
	@Mutable
	public abstract void setAttributeModifiers( Multimap< EntityAttribute, EntityAttributeModifier > attributes );

}

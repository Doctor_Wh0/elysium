package com.enderzombi102.elysium.mixin.other;

import net.minecraft.entity.attribute.EntityAttributeInstance;
import net.minecraft.entity.passive.WolfEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;


@Mixin(WolfEntity.class)
public class WolfEntityMixin {
    @Redirect(method = "setTamed",
            at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/attribute/EntityAttributeInstance;setBaseValue(D)V"))
    private void setBaseValue(EntityAttributeInstance instance, double baseValue) {
    }

    @Redirect(method = "setTamed",
            at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/passive/WolfEntity;setHealth(F)V"))
    private void setHealth(WolfEntity instance, float v) {
    }
}

package com.enderzombi102.elysium.mixin.playerlist;

import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.registry.PowerTypeRegistry;
import com.llamalad7.mixinextras.injector.ModifyReceiver;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.server.command.ListCommand;
import net.minecraft.server.command.ServerCommandSource;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin( ListCommand.class )
public class ListCommandMixin {
	@ModifyReceiver(
		method = "register",
		at = @At(
			value = "INVOKE",
			target = "Lcom/mojang/brigadier/builder/LiteralArgumentBuilder;executes(Lcom/mojang/brigadier/Command;)Lcom/mojang/brigadier/builder/ArgumentBuilder;",
			ordinal = 0,
			remap = false
		)
	)
	private static LiteralArgumentBuilder< ServerCommandSource > addPermissions( LiteralArgumentBuilder< ServerCommandSource > literal, Command< ServerCommandSource > cmd ) {
		if ( !Config.getData().features.disablePlayerList )
			return literal;

		return literal.requires( it -> {
			return PowerTypeRegistry.NAMETAGS.isActive( it.getEntity() );
		} );
	}
}

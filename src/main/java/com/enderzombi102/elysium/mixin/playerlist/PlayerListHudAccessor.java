package com.enderzombi102.elysium.mixin.playerlist;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.hud.PlayerListHud;
import net.minecraft.text.Text;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin( PlayerListHud.class )
public interface PlayerListHudAccessor {
	@Accessor
	@NotNull MinecraftClient getClient();

	@Accessor
	@Nullable Text getHeader();

	@Accessor
	@Nullable Text getFooter();
}

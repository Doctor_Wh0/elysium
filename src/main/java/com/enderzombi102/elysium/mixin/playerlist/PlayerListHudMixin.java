package com.enderzombi102.elysium.mixin.playerlist;

import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.registry.PowerTypeRegistry;
import com.enderzombi102.elysium.util.PlayerListUtil;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.gui.hud.PlayerListHud;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.scoreboard.Scoreboard;
import net.minecraft.scoreboard.ScoreboardObjective;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin( PlayerListHud.class )
public abstract class PlayerListHudMixin extends DrawableHelper {
	@Shadow
	@Final
	private MinecraftClient client;

	@Inject(
		method = "render",
		at = @At( value = "HEAD" ),
		cancellable = true
	)
	@Environment( EnvType.CLIENT )
	public void removePlayersFromList( MatrixStack matrices, int scaledWindowWidth, Scoreboard scoreboard, ScoreboardObjective objective, CallbackInfo ci ) {
		if ( !Config.getData().features.disablePlayerList )
			return;
		assert this.client.player != null;
		if ( PowerTypeRegistry.NAMETAGS.isActive( this.client.player ) )
			return;
		ci.cancel();

		PlayerListUtil.render( (PlayerListHudAccessor) this, matrices, scaledWindowWidth );
	}
}

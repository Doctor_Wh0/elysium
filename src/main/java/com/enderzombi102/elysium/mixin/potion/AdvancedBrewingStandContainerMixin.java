package com.enderzombi102.elysium.mixin.potion;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import tfar.davespotioneering.menu.AdvancedBrewingStandContainer;

@Mixin( AdvancedBrewingStandContainer.class )
public class AdvancedBrewingStandContainerMixin {
	@Inject( method = "transferSlot", at = @At( "HEAD" ), cancellable = true )
	public void transferSlots( PlayerEntity playerIn, int index, CallbackInfoReturnable< ItemStack > cir ) {
		cir.setReturnValue( ItemStack.EMPTY );
	}
}

package com.enderzombi102.elysium.mixin.potion;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import tfar.davespotioneering.Events;

@Mixin( Events.class )
public class EventsMixin {
	@Inject( method = "milkCow", at = @At( "HEAD" ), cancellable = true )
	private static void milkCow( PlayerEntity player, World world, Hand hand, Entity clicked, @Nullable EntityHitResult hit, CallbackInfoReturnable< ActionResult > cir ) {
		cir.setReturnValue( ActionResult.PASS );
	}
}

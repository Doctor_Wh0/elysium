package com.enderzombi102.elysium.mixin.power;

import com.enderzombi102.elysium.registry.PowerTypeRegistry;
import net.minecraft.block.BlockState;
import net.minecraft.block.Fertilizable;
import net.minecraft.item.BoneMealItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

/**
 * Used for powers: elysium:better_bone_meal
 */
@Mixin( BoneMealItem.class )
public class BoneMealItemMixin {
	@Unique
	private static boolean normalBoneMeal = true;

	@Inject( method = "useOnFertilizable", at = @At( value = "INVOKE", target = "Lnet/minecraft/item/ItemStack;decrement(I)V" ) )
	private static void applyAdditionalFarmerBoneMeal( ItemStack stack, World world, BlockPos pos, CallbackInfoReturnable< Boolean > cir ) {
		if ( normalBoneMeal )
			return;

		BlockState blockState = world.getBlockState( pos );
		Fertilizable fertilizable = (Fertilizable) blockState.getBlock();
		if ( fertilizable.canGrow( world, world.random, pos, blockState ) )
			fertilizable.grow( (ServerWorld) world, world.random, pos, blockState );
	}
}

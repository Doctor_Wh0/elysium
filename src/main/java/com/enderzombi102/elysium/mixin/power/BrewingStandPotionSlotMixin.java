package com.enderzombi102.elysium.mixin.power;

import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

/**
 * Used for powers: elysium:longer_potions
 */
@Mixin( targets = "net/minecraft/screen/BrewingStandScreenHandler$PotionSlot" )
public class BrewingStandPotionSlotMixin {

	@Inject( method = "matches", at = @At( "HEAD" ), cancellable = true )
	private static void preventBrewingExtendedPotions( ItemStack stack, CallbackInfoReturnable< Boolean > cir ) {
		if ( stack.getOrCreateNbt().getBoolean( "Elysium$ExtendedPotion" ) )
			cir.setReturnValue( false );
	}
}

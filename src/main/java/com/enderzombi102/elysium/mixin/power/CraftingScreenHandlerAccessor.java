package com.enderzombi102.elysium.mixin.power;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.inventory.CraftingResultInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.packet.s2c.play.ScreenHandlerSlotUpdateS2CPacket;
import net.minecraft.recipe.CraftingRecipe;
import net.minecraft.recipe.RecipeType;
import net.minecraft.screen.CraftingScreenHandler;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.gen.Accessor;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Mixin( CraftingScreenHandler.class )
public abstract class CraftingScreenHandlerAccessor {
	@Accessor
	abstract PlayerEntity getPlayer();

	/**
	 * @author
	 * @reason
	 */
	@Inject( method = "updateResult", at=@At ( value = "TAIL"))
	private static void updateResult( ScreenHandler handler, World world, PlayerEntity player, CraftingInventory craftingInventory, CraftingResultInventory resultInventory, CallbackInfo ci ) {
		if (!world.isClient) {
			try {
				Optional< CraftingRecipe > optional = world.getServer().getRecipeManager().getFirstMatch( RecipeType.CRAFTING, craftingInventory, world );
				if ( optional.isPresent() ) {
					ServerPlayerEntity serverPlayerEntity = (ServerPlayerEntity) player;
					AtomicInteger damage = new AtomicInteger();
					CraftingRecipe recipe = optional.get();
					ItemStack itemStack = ItemStack.EMPTY;
					if ( recipe.getOutput().getItem().kjs$getId().contains( "magistuarmory" ) ) {
						craftingInventory.kjs$getAllItems().forEach( i -> {
							if ( i.getDamage() > 0 ) {
								damage.set( damage.addAndGet( i.getDamage() ) );
								;
							}
						} );
						CraftingRecipe craftingRecipe = (CraftingRecipe) optional.get();
						NbtCompound nbtCompound = new NbtCompound();
						nbtCompound.putInt( "Damage", damage.get() );
						if ( resultInventory.shouldCraftRecipe( world, serverPlayerEntity, craftingRecipe ) ) {
							itemStack = craftingRecipe.craft( craftingInventory );
						}
						itemStack.setNbt( nbtCompound );
						resultInventory.setStack( 0, itemStack );
						handler.setPreviousTrackedSlot( 0, itemStack );
						serverPlayerEntity.networkHandler.sendPacket( new ScreenHandlerSlotUpdateS2CPacket( handler.syncId, handler.nextRevision(), 0, itemStack ) );
					}
				}
			} catch ( Exception ignored ) {
			}
		}
	}
}

package com.enderzombi102.elysium.mixin.power;

import net.minecraft.entity.player.HungerManager;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

/**
 * Used for powers: elysium:better_crafted_food
 */
@Mixin( HungerManager.class )
public abstract class HungerManagerMixin {
	@Shadow
	public abstract void add( int food, float f );

	@Inject( method = "eat", at = @At( value = "INVOKE", target = "Lnet/minecraft/entity/player/HungerManager;add(IF)V", shift = At.Shift.AFTER ) )
	private void addFoodBonus( Item item, ItemStack stack, CallbackInfo ci ) {
		if ( !stack.hasNbt() )
			return;

		var tag = stack.getNbt();
		assert tag != null;
		if ( tag.contains( "Elysium$FoodBonus" ) ) {
			var foodBonus = tag.getInt( "Elysium$FoodBonus" );
			var saturationBonus = (float) foodBonus * 0.2F;
			this.add( foodBonus, saturationBonus );
		}
	}
}

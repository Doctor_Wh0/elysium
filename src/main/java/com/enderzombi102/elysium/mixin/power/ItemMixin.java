package com.enderzombi102.elysium.mixin.power;

import com.enderzombi102.elysium.Elysium;
import de.siphalor.spiceoffabric.config.Config;
import de.siphalor.spiceoffabric.util.IHungerManager;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.List;
import java.util.Objects;

@Mixin( Item.class )
public abstract class ItemMixin {
	@Accessor( "maxDamage" )
	@Mutable
	public abstract void setMaxDamage( int i );

	@Environment( EnvType.CLIENT )
	@Inject( method = "appendTooltip", at = @At( "HEAD" ) )
	private void appendFoodBonusInfo( ItemStack stack, World world, List< Text > tooltip, TooltipContext context, CallbackInfo ci ) {
		if ( stack != null ) {
			NbtCompound tag = stack.hasNbt() ? stack.getNbt() : null;
			if ( tag != null ) {
				if ( tag.contains( "Elysium$FoodBonus" ) ) {
					int bonus = tag.getInt( "Elysium$FoodBonus" );
					tooltip.add( Text.translatable( "elysium.food_bonus", bonus ).formatted( Formatting.GRAY ) );
				}
				if ( tag.contains( "Elysium$MiningSpeedMultiplier" ) ) {
					int bonusInt = Math.round( ( tag.getFloat( "Elysium$MiningSpeedMultiplier" ) - 1F ) * 100 );
					String bonus = bonusInt > 0 ? ( "+" + bonusInt + "%" ) : ( bonusInt + "%" );
					tooltip.add( Text.translatable( "elysium.mining_speed_bonus", bonus ).formatted( Formatting.BLUE ) );
				}
			}
		}
	}

}

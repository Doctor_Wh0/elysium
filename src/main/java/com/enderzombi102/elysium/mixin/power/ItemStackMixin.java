package com.enderzombi102.elysium.mixin.power;

import com.enderzombi102.elysium.Elysium;
import de.siphalor.spiceoffabric.config.Config;
import de.siphalor.spiceoffabric.util.IHungerManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Objects;

/**
 * Used for powers: elysium:longer_potions
 */
@Mixin( ItemStack.class )
public abstract class ItemStackMixin {
	@Shadow
	public abstract boolean hasNbt();

	@Shadow
	public @Nullable
	abstract NbtCompound getNbt();

	@Shadow
	@Nullable
	public abstract NbtCompound getSubNbt( String key );

	@Shadow
	public abstract Item getItem();

	@Inject( method = "getName", at = @At( value = "INVOKE", shift = At.Shift.BEFORE, target = "Lnet/minecraft/item/ItemStack;getItem()Lnet/minecraft/item/Item;" ), cancellable = true )
	private void getExtendedName( CallbackInfoReturnable< Text > cir ) {
		if ( !this.hasNbt() )
			return;
		var nbt = this.getNbt();
		assert nbt != null;
		if ( nbt.contains( "Elysium$OriginalName" ) )
			cir.setReturnValue( Text.Serializer.fromJson( nbt.getString( "Elysium$OriginalName" ) ) );
	}

	@Inject(method = "getMaxDamage", at = @At("TAIL"), cancellable = true)
	private void getMaxDamageMixin(CallbackInfoReturnable<Integer> info) {
		if (hasNbt() && getNbt().contains("imbued") && getSubNbt("imbued").contains(("generic.durability"))) {
			Double v = getSubNbt("imbued").getDouble( "generic.durability" );
			info.setReturnValue( (int) (info.getReturnValue() + (v > 0 ? v: 0 )));
		}
	}
	@Inject( method = "use", at = @At( "HEAD" ), cancellable = true )
	public void use( World world, PlayerEntity user, Hand hand, CallbackInfoReturnable< TypedActionResult< ItemStack > > cir ) {
		if ( this.getItem().isFood() ) {
			ItemStack itemStack = ((ItemStack)(Object)this);
			Config.setHungerExpressionValues( ( (IHungerManager) user.getHungerManager() ).spiceOfFabric_getFoodHistory().getTimesEaten( itemStack ), Objects.requireNonNull(  this.getItem().getFoodComponent().getHunger() ), this.getItem().getFoodComponent().getSaturationModifier(), itemStack.getMaxUseTime() );
			if ( user.canConsume(  this.getItem().getFoodComponent().isAlwaysEdible() ) && this.getItem().getFoodComponent().getHunger() >= 1 && Config.getHungerValue() >= 1 ) {
				user.setCurrentHand( hand );
				cir.setReturnValue( TypedActionResult.consume( itemStack ) );
			} else {
				cir.setReturnValue( TypedActionResult.fail( itemStack ) );
			}
		}
	}

	@Inject( method = "useOnBlock", at = @At( "HEAD" ), cancellable = true )
	public void use( ItemUsageContext context, CallbackInfoReturnable< ActionResult > cir ) {
		if ( this.getItem().isFood() ) {
			ItemStack itemStack = ((ItemStack)(Object)this);
			Config.setHungerExpressionValues( ( (IHungerManager) context.getPlayer().getHungerManager() ).spiceOfFabric_getFoodHistory().getTimesEaten( itemStack ), Objects.requireNonNull(  this.getItem().getFoodComponent().getHunger() ), this.getItem().getFoodComponent().getSaturationModifier(), itemStack.getMaxUseTime() );
			if ( context.getPlayer().canConsume(  this.getItem().getFoodComponent().isAlwaysEdible() ) && this.getItem().getFoodComponent().getHunger() >= 1 && Config.getHungerValue() >= 1 ) {
				ActionResult actionResult = itemStack.getItem().useOnBlock(context);
				cir.setReturnValue( actionResult );
			} else {
				cir.setReturnValue( ActionResult.FAIL );
			}
		}
	}
}

package com.enderzombi102.elysium.mixin.power;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.item.LingeringPotionItem;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.List;

/**
 * Used for powers: elysium:longer_potions
 */
@Mixin( LingeringPotionItem.class )
public class LingeringItemMixin {
	@Inject( method = "appendTooltip", at = @At( "HEAD" ) )
	@Environment( EnvType.CLIENT )
	private void appendExtendedTooltip( ItemStack stack, World world, List< Text > tooltip, TooltipContext context, CallbackInfo ci ) {
		if ( !stack.hasNbt() )
			return;

		var nbt = stack.getNbt();
		assert nbt != null;
		if ( nbt.contains( "Elysium$ExtendedPotion" ) )
			tooltip.add( Text.literal( "Extended" ).formatted( Formatting.GOLD ) );
	}
}

package com.enderzombi102.elysium.mixin.power;

import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.item.MiningToolItem;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import static org.objectweb.asm.Opcodes.GETFIELD;

/**
 * Used for powers: elysium:more_stone_break_speed
 */
@Mixin( MiningToolItem.class )
public class MiningToolItemMixin {
	@Shadow
	@Final
	protected float miningSpeed;

	@Redirect( method = "getMiningSpeedMultiplier", at = @At( value = "FIELD", target = "Lnet/minecraft/item/MiningToolItem;miningSpeed:F", opcode = GETFIELD, ordinal = 0 ) )
	private float applyMiningSpeedMultiplierMultiplier( MiningToolItem item, ItemStack stack, BlockState blockState ) {
		if ( stack != null )
			if ( stack.hasNbt() ) {
				var nbt = stack.getNbt();
				assert nbt != null;
				if ( nbt.contains( "Elysium$MiningSpeedMultiplier" ) )
					return miningSpeed * nbt.getFloat( "Elysium$MiningSpeedMultiplier" );
			}
		return miningSpeed;
	}
}

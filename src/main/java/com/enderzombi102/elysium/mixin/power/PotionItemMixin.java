package com.enderzombi102.elysium.mixin.power;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.advancement.criterion.Criteria;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.PotionItem;
import net.minecraft.potion.PotionUtil;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.stat.Stats;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;
import net.minecraft.world.event.GameEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Iterator;
import java.util.List;

/**
 * Used for powers: elysium:longer_potions
 */
@Mixin( PotionItem.class )
public class PotionItemMixin {
	@Inject( method = "appendTooltip", at = @At( "HEAD" ) )
	@Environment( EnvType.CLIENT )
	private void appendExtendedTooltip( ItemStack stack, World world, List< Text > tooltip, TooltipContext context, CallbackInfo ci ) {
		if ( !stack.hasNbt() )
			return;

		var nbt = stack.getNbt();
		assert nbt != null;
		if ( nbt.contains( "Elysium$ExtendedPotion" ) )
			tooltip.add( Text.literal( "Extended" ).formatted( Formatting.GOLD ) );
	}
	/**
	 * @author Doc
	 * @reason NO Glass bottle
	 */
	@Overwrite
	public ItemStack finishUsing(ItemStack stack, World world, LivingEntity user) {
		PlayerEntity playerEntity = user instanceof PlayerEntity ? (PlayerEntity)user : null;
		if (playerEntity instanceof ServerPlayerEntity ) {
			Criteria.CONSUME_ITEM.trigger((ServerPlayerEntity)playerEntity, stack);
		}

		if (!world.isClient) {
			List< StatusEffectInstance > list = PotionUtil.getPotionEffects(stack);

			for ( StatusEffectInstance statusEffectInstance : list ) {
				if ( statusEffectInstance.getEffectType().isInstant() ) {
					statusEffectInstance.getEffectType().applyInstantEffect( playerEntity, playerEntity, user, statusEffectInstance.getAmplifier(), 1.0 );
				} else {
					user.addStatusEffect( new StatusEffectInstance( statusEffectInstance ) );
				}
			}
		}

		if (playerEntity != null) {
			playerEntity.incrementStat( Stats.USED.getOrCreateStat((PotionItem)(Object)this));
			if (!playerEntity.getAbilities().creativeMode) {
				stack.decrement(1);
			}
		}

		user.emitGameEvent( GameEvent.DRINK);
		return stack;
	}
}

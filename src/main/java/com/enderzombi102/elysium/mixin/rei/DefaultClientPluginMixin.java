package com.enderzombi102.elysium.mixin.rei;

import com.enderzombi102.elysium.registry.ItemRegistry;
import me.shedaniel.rei.plugin.client.DefaultClientPlugin;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin( DefaultClientPlugin.class )
public class DefaultClientPluginMixin {
	@Redirect( method = "registerDisplays", at = @At( value = "FIELD", target = "Lnet/minecraft/item/Items;BONE_MEAL:Lnet/minecraft/item/Item;" ) )
	public Item registerDisplays() {
		return ItemRegistry.ITEMS.getOrDefault( "dung_ball", Items.AIR );
	}
}

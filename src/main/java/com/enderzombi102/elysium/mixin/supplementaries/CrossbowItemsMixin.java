package com.enderzombi102.elysium.mixin.supplementaries;

import com.enderzombi102.elysium.util.Util;
import com.magistuarmory.item.MedievalCrossbowItem;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.CrossbowItem;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin( MedievalCrossbowItem.class )
public abstract class CrossbowItemsMixin extends CrossbowItem {
	public CrossbowItemsMixin( Settings settings ) {
		super( settings );
	}

	@Inject(
		method = "loadProjectile",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/item/ItemStack;split(I)Lnet/minecraft/item/ItemStack;",
			remap = true
		),
		remap = false
	)
	private static void shrinkQuiverArrow( LivingEntity shooter, ItemStack crossbowStack, ItemStack ammoStack, boolean hasAmmo, boolean isCreative, CallbackInfoReturnable< Boolean > cir ) {
		var data = Util.getQuiverData( (PlayerEntity) shooter );
		if ( data != null )
			data.consumeArrow();
	}
}

package com.enderzombi102.elysium.mixin.supplementaries;

import com.enderzombi102.elysium.event.ModifyItemAttributeModifierEvent;
import com.enderzombi102.elysium.util.Util;
import com.magistuarmory.item.MedievalBowItem;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin( MedievalBowItem.class)
public class MedievalBowItemMixin extends BowItem {

	public MedievalBowItemMixin( Settings settings ) {
		super( settings );
	}

	@Inject(
		method = "onStoppedUsing",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/item/ItemStack;decrement(I)V"
		)
	)
	private void shrinkQuiverArrow( ItemStack stack, World level, LivingEntity livingEntity, int timeCharged, CallbackInfo ci ) {
		var data = Util.getQuiverData( (PlayerEntity) livingEntity );
		if ( data != null )
			data.consumeArrow();
	}

	@Override
	public boolean canRepair( ItemStack stack, ItemStack ingredient ) {
		if ( ModifyItemAttributeModifierEvent.repair.getOrDefault( stack.getItem().kjs$getId(), "null" ).equalsIgnoreCase( "null" ) ) {
			return super.canRepair( stack, ingredient );
		} else {
			return Ingredient.ofItems( Registry.ITEM.get( new Identifier( ModifyItemAttributeModifierEvent.repair.get( stack.getItem().kjs$getId() ) ) ) ).test( ingredient );
		}
	}
}

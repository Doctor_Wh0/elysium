package com.enderzombi102.elysium.mixin.supplementaries;

import com.enderzombi102.elysium.util.Util;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.world.World;
import net.mk.archers_arsenal.items.ModBowItem;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.List;

@Mixin( ModBowItem.class )
public abstract class ModBowItemMixin extends BowItem {
	@Shadow
	public boolean HAS_NATURAL_INFINITY;

	public ModBowItemMixin( Settings settings ) {
		super( settings );
	}

	@Inject(
		method = "onStoppedUsing",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/item/ItemStack;decrement(I)V"
		)
	)
	private void shrinkQuiverArrow( ItemStack stack, World level, LivingEntity livingEntity, int timeCharged, CallbackInfo ci ) {
		var data = Util.getQuiverData( (PlayerEntity) livingEntity );
		if ( data != null )
			data.consumeArrow();
	}

	@Inject( at = @At( value = "RETURN" ), method = "<init>" )
	private void init( Settings settings, int dmgMod, boolean inf, CallbackInfo ci ) {
		this.HAS_NATURAL_INFINITY = false;
	}
}

package com.enderzombi102.elysium.recipe;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.recipe.*;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.world.World;

import java.util.List;

public class AnvilRecipe  implements Recipe< SimpleInventory > {
	private final Identifier id;
	private final ItemStack output;
	private final List< ItemStack > recipeItems;

	public AnvilRecipe(Identifier id, List< ItemStack > ingredients, ItemStack itemStack ) {
		this.id = id;
		this.output = itemStack;
		this.recipeItems = ingredients;
	}

	@Override
	public boolean matches( SimpleInventory inventory, World world ) {
		if ( world.isClient() ) {
			return false;
		}
		return false;
	}

	@Override
	public DefaultedList< Ingredient > getIngredients() {
		DefaultedList< Ingredient > list = DefaultedList.ofSize( this.recipeItems.size() );
		for ( ItemStack itemStack : this.recipeItems ) {
			list.add( Ingredient.ofStacks( itemStack ) ) ;
		}
		return list;
	}

	@Override
	public ItemStack craft( SimpleInventory inventory ) {
		return this.getOutput().copy();
	}

	@Override
	public boolean fits( int width, int height ) {
		return true;
	}

	@Override
	public ItemStack getOutput() {
		return output.copy();
	}

	@Override
	public Identifier getId() {
		return id;
	}

	@Override
	public RecipeSerializer< ? > getSerializer() {
		return AnvilRecipe.Serializer.INSTANCE;
	}

	@Override
	public RecipeType< ? > getType() {
		return AnvilRecipe.Type.INSTANCE;
	}

	public static class Type implements RecipeType<AnvilRecipe> {
		public static final AnvilRecipe.Type INSTANCE = new AnvilRecipe.Type();
		public static final String ID = "anvil";
	}

	public static class Serializer implements RecipeSerializer<AnvilRecipe> {
		public static final AnvilRecipe.Serializer INSTANCE = new AnvilRecipe.Serializer();
		public static final String ID = "anvil";


		@Override
		public AnvilRecipe read(Identifier id, JsonObject json ) {
			ItemStack fakeOutput = ShapedRecipe.outputFromJson( JsonHelper.getObject( json, "output" ) );
			JsonArray ingredients = JsonHelper.getArray( json, "ingredients" );
			DefaultedList< ItemStack > inputs = DefaultedList.ofSize( 2, ItemStack.EMPTY );
			for ( int i = 0; i < ingredients.size(); i++ ) {
				inputs.set( i, ShapedRecipe.outputFromJson( ingredients.get( i ).getAsJsonObject() ) );
			}
			NbtCompound nbtCompound=new NbtCompound();
			nbtCompound.putInt( "Damage", inputs.get(0).getMaxDamage() /4 );
			ItemStack output=inputs.get(0).getItem().getDefaultStack();
			inputs.get( 0 ).setNbt( nbtCompound );
			inputs.set( 0, inputs.get( 0 ).kjs$withNBT( nbtCompound ));
			inputs.set(1, fakeOutput);
			return new AnvilRecipe( id, inputs, output );
		}

		@Override
		public AnvilRecipe read(Identifier id, PacketByteBuf buf ) {
			int size = buf.readVarInt();
			DefaultedList< ItemStack > inputs = DefaultedList.ofSize( size, ItemStack.EMPTY );
			inputs.replaceAll( ignored -> buf.readItemStack() );
			ItemStack output = buf.readItemStack();
			return new AnvilRecipe( id, inputs, output );
		}

		@Override
		public void write( PacketByteBuf buf, AnvilRecipe recipe ) {
			buf.writeVarInt( recipe.recipeItems.size() );
			for ( ItemStack itemStack : recipe.recipeItems ) {
				buf.writeItemStack( itemStack.copy() );
			}
			buf.writeItemStack( recipe.getOutput() );
		}
	}
}

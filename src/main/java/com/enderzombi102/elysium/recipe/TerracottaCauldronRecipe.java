package com.enderzombi102.elysium.recipe;

import com.enderzombi102.elysium.Elysium;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.recipe.*;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.world.World;

import java.util.List;
import java.util.function.Consumer;


public class TerracottaCauldronRecipe implements Recipe< SimpleInventory > {
	private final Identifier id;
	private final ItemStack output;
	private final List< ItemStack > recipeItems;

	public TerracottaCauldronRecipe( Identifier id, List< ItemStack > ingredients, ItemStack itemStack ) {
		this.id = id;
		this.output = itemStack;
		this.recipeItems = ingredients;
	}

	@Override
	public boolean matches( SimpleInventory inventory, World world ) {
		if ( world.isClient() ) {
			return false;
		}
		return false;
	}

	@Override
	public DefaultedList< Ingredient > getIngredients() {
		DefaultedList< Ingredient > list = DefaultedList.ofSize( this.recipeItems.size() );
		for ( ItemStack itemStack : this.recipeItems ) {
			list.add( Ingredient.ofStacks( itemStack ) ) ;
		}
		return list;
	}

	@Override
	public ItemStack craft( SimpleInventory inventory ) {
		return this.getOutput().copy();
	}

	@Override
	public boolean fits( int width, int height ) {
		return true;
	}

	@Override
	public ItemStack getOutput() {
		return output.copy();
	}

	@Override
	public Identifier getId() {
		return id;
	}

	@Override
	public RecipeSerializer< ? > getSerializer() {
		return Serializer.INSTANCE;
	}

	@Override
	public RecipeType< ? > getType() {
		return Type.INSTANCE;
	}

	public static class Type implements RecipeType< TerracottaCauldronRecipe > {
		public static final Type INSTANCE = new Type();
		public static final String ID = "0_cauldron";
	}

	public static class Serializer implements RecipeSerializer< TerracottaCauldronRecipe > {
		public static final Serializer INSTANCE = new Serializer();
		public static final String ID = "0_cauldron";


		@Override
		public TerracottaCauldronRecipe read( Identifier id, JsonObject json ) {
			ItemStack output = ShapedRecipe.outputFromJson( JsonHelper.getObject( json, "output" ) );
			JsonArray ingredients = JsonHelper.getArray( json, "ingredients" );
			DefaultedList< ItemStack > inputs = DefaultedList.ofSize( 7, ItemStack.EMPTY );
			for ( int i = 0; i < ingredients.size(); i++ ) {
				if ( ( i + 1 ) == ingredients.size() && ( i + 1 ) != inputs.size() ) {
					inputs.set( inputs.size() - 1, ShapedRecipe.outputFromJson( ingredients.get( i ).getAsJsonObject() ) );
				} else {
					inputs.set( i, ShapedRecipe.outputFromJson( ingredients.get( i ).getAsJsonObject() ) );
				}
			}
			return new TerracottaCauldronRecipe( id, inputs, output );
		}

		@Override
		public TerracottaCauldronRecipe read( Identifier id, PacketByteBuf buf ) {
			int size = buf.readVarInt();
			DefaultedList< ItemStack > inputs = DefaultedList.ofSize( size, ItemStack.EMPTY );
			inputs.replaceAll( ignored -> buf.readItemStack() );
			ItemStack output = buf.readItemStack();
			return new TerracottaCauldronRecipe( id, inputs, output );
		}

		@Override
		public void write( PacketByteBuf buf, TerracottaCauldronRecipe recipe ) {
			buf.writeVarInt( recipe.recipeItems.size() );
			for ( ItemStack itemStack : recipe.recipeItems ) {
				buf.writeItemStack( itemStack.copy() );
				}
			buf.writeItemStack( recipe.getOutput() );
		}
	}
}

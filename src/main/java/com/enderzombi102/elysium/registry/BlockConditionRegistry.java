package com.enderzombi102.elysium.registry;

import com.enderzombi102.elysium.data.DataTypes;
import io.github.apace100.apoli.power.factory.condition.ConditionFactory;
import io.github.apace100.apoli.registry.ApoliRegistries;
import io.github.apace100.calio.data.SerializableData;
import io.github.apace100.calio.data.SerializableData.Instance;
import net.minecraft.block.pattern.CachedBlockPosition;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.util.function.BiFunction;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.elysium.util.Const.getId;

public class BlockConditionRegistry {

	public static void register() {
		register(
			getId( "material" ),
			new SerializableData()
				.add( "material", DataTypes.MATERIAL ),
			( data, block ) -> block.getBlockState().getMaterial() == data.get( "material" )
		);

		LOGGER.info( "[Elysium] Registered materials" );
	}

	private static void register( Identifier id, SerializableData data, BiFunction< Instance, CachedBlockPosition, Boolean > condition ) {
		Registry.register( ApoliRegistries.BLOCK_CONDITION, id, new ConditionFactory<>( id, data, condition ) );
	}
}

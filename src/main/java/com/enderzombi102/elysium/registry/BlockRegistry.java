package com.enderzombi102.elysium.registry;

import com.enderzombi102.elysium.block.EssenceBlock;
import com.enderzombi102.elysium.crops.*;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.Material;
import net.minecraft.block.SweetBerryBushBlock;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.util.HashMap;
import java.util.Map;

import static com.enderzombi102.elysium.util.Const.getId;

public class BlockRegistry {
	public static final BambooCropBlock BAMBOO_CROP_BLOCK=new BambooCropBlock( FabricBlockSettings.copy( Blocks.WHEAT ) );
	public static final BrownMushroomCropBlock BROWN_MUSHROOM_CROP_BLOCK=new BrownMushroomCropBlock( FabricBlockSettings.copy( Blocks.WHEAT ) );
	public static final CactusCropBlock CACTUS_CROP_BLOCK=new CactusCropBlock( FabricBlockSettings.copy( Blocks.WHEAT ) );
	public static final CornCropBlock CORN_CROP_BLOCK=new CornCropBlock( FabricBlockSettings.copy( Blocks.WHEAT ) );
	public static final KelpCropBlock KELP_CROP_BLOCK=new KelpCropBlock(  );
	public static final MelonCropBlock MELON_CROP_BLOCK=new MelonCropBlock( FabricBlockSettings.copy( Blocks.WHEAT ) );
	public static final PumpkinCropBlock PUMPKIN_CROP_BLOCK=new PumpkinCropBlock( FabricBlockSettings.copy( Blocks.WHEAT ) );
	public static final RedMushroomCropBlock RED_MUSHROOM_CROP_BLOCK=new RedMushroomCropBlock( FabricBlockSettings.copy( Blocks.WHEAT ) );
	public static final SugarCaneCropBlock SUGAR_CANE_CROP_BLOCK=new SugarCaneCropBlock( FabricBlockSettings.copy( Blocks.WHEAT ) );
	public static final RiceCropBlock RICE_CROP_BLOCK=new RiceCropBlock(  );
	public static final FlaxCropBlock FLAX_CROP_BLOCK=new FlaxCropBlock( FabricBlockSettings.copy( Blocks.WHEAT ) );
	public static final CarrotCropBlock CARROT_CROP_BLOCK=new CarrotCropBlock( FabricBlockSettings.copy( Blocks.WHEAT ) );
	public static final CocoaCropBlock COCOA_CROP_BLOCK=new CocoaCropBlock( FabricBlockSettings.copy( Blocks.WHEAT ) );
	public static final PotatoCropBlock POTATO_CROP_BLOCK=new PotatoCropBlock( FabricBlockSettings.copy( Blocks.WHEAT ) );
	public static final SweetBerryCropBlock SWEET_BERRY_CROP_BLOCK=new SweetBerryCropBlock( FabricBlockSettings.copy( Blocks.WHEAT ) );
	public static final Map< String, Block > MINING_BLOCKS = new HashMap<>() {{
		put( "essence_block", new EssenceBlock( FabricBlockSettings.of( Material.STONE ).strength(50.0F, 1200.0F) ) );
		put( "tin_block", new Block( FabricBlockSettings.of( Material.METAL ).hardness( 1.5F ).resistance( 1F ).requiresTool() ) );
	}};

	public static void register() {
		for ( var entry : MINING_BLOCKS.entrySet() ) {
			Registry.register( Registry.BLOCK, getId( entry.getKey() ), entry.getValue() );
			Registry.register( Registry.ITEM, getId( entry.getKey() ), new BlockItem( entry.getValue(), new FabricItemSettings() ) );
		}
		Registry.register( Registry.BLOCK, getId( "bamboo_crop" ), BAMBOO_CROP_BLOCK );
		Registry.register( Registry.BLOCK, getId( "brown_mushroom_crop" ), BROWN_MUSHROOM_CROP_BLOCK );
		Registry.register( Registry.BLOCK, getId( "cactus_crop" ), CACTUS_CROP_BLOCK );
		Registry.register( Registry.BLOCK, getId( "corn_crop" ), CORN_CROP_BLOCK );
		Registry.register( Registry.BLOCK, getId( "melon_crop" ), MELON_CROP_BLOCK );
		Registry.register( Registry.BLOCK, getId( "pumpkin_crop" ), PUMPKIN_CROP_BLOCK );
		Registry.register( Registry.BLOCK, getId( "red_mushroom_crop" ), RED_MUSHROOM_CROP_BLOCK );
		Registry.register( Registry.BLOCK, getId( "sugar_cane_crop" ), SUGAR_CANE_CROP_BLOCK );
		Registry.register( Registry.BLOCK, getId( "kelp_crop" ), KELP_CROP_BLOCK );
		Registry.register( Registry.BLOCK, getId( "rice_crop" ), RICE_CROP_BLOCK );
		Registry.register( Registry.BLOCK, getId( "flax_crop" ), FLAX_CROP_BLOCK );
		Registry.register( Registry.BLOCK, getId( "carrot_crop" ), CARROT_CROP_BLOCK );
		Registry.register( Registry.BLOCK, getId( "cocoa_crop" ), COCOA_CROP_BLOCK );
		Registry.register( Registry.BLOCK, getId( "potato_crop" ), POTATO_CROP_BLOCK );
		Registry.register( Registry.BLOCK, getId( "sweet_berry_crop" ), SWEET_BERRY_CROP_BLOCK );
	}
}

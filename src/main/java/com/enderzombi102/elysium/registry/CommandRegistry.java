package com.enderzombi102.elysium.registry;

import com.enderzombi102.elysium.command.ElysiumCommand;
import com.enderzombi102.elysium.command.SpawnCommand;
import com.enderzombi102.elysium.command.TpxCommand;
import com.mojang.brigadier.CommandDispatcher;
import net.minecraft.command.CommandRegistryAccess;
import net.minecraft.server.command.CommandManager.RegistrationEnvironment;
import net.minecraft.server.command.ServerCommandSource;

import static com.enderzombi102.elysium.Elysium.LOGGER;

public class CommandRegistry {

	/**
	 * Register our commands to the server
	 */
	public static void register( CommandDispatcher< ServerCommandSource > dispatcher, CommandRegistryAccess registryAccess, RegistrationEnvironment environment ) {
		dispatcher.register( ElysiumCommand.command() );
		dispatcher.register( ElysiumCommand.status() );
		dispatcher.register( ElysiumCommand.delNickname() );
		dispatcher.register( ElysiumCommand.removeStatus() );
		dispatcher.register( SpawnCommand.command() );
		dispatcher.register( TpxCommand.command() );

		LOGGER.info( "[Elysium] Registered commands" );
	}
}

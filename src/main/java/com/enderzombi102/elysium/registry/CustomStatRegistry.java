package com.enderzombi102.elysium.registry;

import net.minecraft.stat.StatFormatter;
import net.minecraft.stat.Stats;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;


public class CustomStatRegistry {
	public static final Identifier BOW_DAMAGE = new Identifier( "elysium", "bow_damage");
	public static void register() {
		Registry.register(Registry.CUSTOM_STAT, "bow_damage", BOW_DAMAGE);
		Stats.CUSTOM.getOrCreateStat(BOW_DAMAGE, StatFormatter.DEFAULT);
	}
}

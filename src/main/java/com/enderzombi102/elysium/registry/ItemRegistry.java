package com.enderzombi102.elysium.registry;

import com.enderzombi102.elysium.item.*;
import com.enderzombi102.elysium.util.Const;
import com.google.common.collect.ImmutableMultimap;
import com.magistuarmory.item.ModItemTier;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.block.Blocks;
import net.minecraft.block.cauldron.CauldronBehavior;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.item.*;
import net.minecraft.potion.Potion;
import net.minecraft.recipe.Ingredient;
import net.minecraft.util.Identifier;
import net.minecraft.util.Rarity;
import net.minecraft.util.registry.Registry;
import net.spell_engine.api.item.trinket.SpellBookItem;
import net.spell_engine.api.item.weapon.StaffItem;
import net.spell_engine.api.item.weapon.Weapon;
import net.spell_engine.api.spell.SpellContainer;
import net.spell_engine.internals.SpellRegistry;
import com.ianm1647.expandeddelight.registry.BlockRegistry;
import com.nhoryzon.mc.farmersdelight.registry.BlocksRegistry;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.elysium.util.Const.getId;
import static net.spell_engine.api.item.trinket.SpellBooks.itemIdFor;


public class ItemRegistry {
	public static final AliasedBlockItem BAMBOO_SEED = new AliasedBlockItem( com.enderzombi102.elysium.registry.BlockRegistry.BAMBOO_CROP_BLOCK, new FabricItemSettings() );
	public static final AliasedBlockItem BROWN_MUSHROOM_SEED = new AliasedBlockItem( com.enderzombi102.elysium.registry.BlockRegistry.BROWN_MUSHROOM_CROP_BLOCK, new FabricItemSettings() );
	public static final AliasedBlockItem CACTUS_SEED = new AliasedBlockItem( com.enderzombi102.elysium.registry.BlockRegistry.CACTUS_CROP_BLOCK, new FabricItemSettings() );
	public static final AliasedBlockItem CORN_SEED = new AliasedBlockItem( com.enderzombi102.elysium.registry.BlockRegistry.CORN_CROP_BLOCK, new FabricItemSettings() );
	public static final AliasedBlockItem MELON_SEED = new AliasedBlockItem( com.enderzombi102.elysium.registry.BlockRegistry.MELON_CROP_BLOCK, new FabricItemSettings() );
	public static final AliasedBlockItem PUMPKIN_SEED = new AliasedBlockItem( com.enderzombi102.elysium.registry.BlockRegistry.PUMPKIN_CROP_BLOCK, new FabricItemSettings() );
	public static final AliasedBlockItem RED_MUSHROOM_SEED = new AliasedBlockItem( com.enderzombi102.elysium.registry.BlockRegistry.RED_MUSHROOM_CROP_BLOCK, new FabricItemSettings() );
	public static final AliasedBlockItem SUGAR_CANE_SEED = new AliasedBlockItem( com.enderzombi102.elysium.registry.BlockRegistry.SUGAR_CANE_CROP_BLOCK, new FabricItemSettings() );
	public static final AliasedBlockItem KELP_SEED = new AliasedBlockItem( com.enderzombi102.elysium.registry.BlockRegistry.KELP_CROP_BLOCK, new FabricItemSettings() );
	public static final AliasedBlockItem CARROT_SEED = new AliasedBlockItem( com.enderzombi102.elysium.registry.BlockRegistry.CARROT_CROP_BLOCK, new FabricItemSettings() );
	public static final AliasedBlockItem COCOA_SEED = new AliasedBlockItem( com.enderzombi102.elysium.registry.BlockRegistry.COCOA_CROP_BLOCK, new FabricItemSettings() );
	public static final AliasedBlockItem ONION_SEED = new AliasedBlockItem( BlocksRegistry.ONION_CROP.get(), new FabricItemSettings() );
	public static final AliasedBlockItem PEANUT_SEED = new AliasedBlockItem( BlockRegistry.PEANUT_CROP, new FabricItemSettings() );
	public static final AliasedBlockItem POTATO_SEED = new AliasedBlockItem( com.enderzombi102.elysium.registry.BlockRegistry.POTATO_CROP_BLOCK, new FabricItemSettings() );
	public static final AliasedBlockItem RICE_SEED =  new AliasedBlockItem( com.enderzombi102.elysium.registry.BlockRegistry.RICE_CROP_BLOCK, new FabricItemSettings() );
	public static final AliasedBlockItem SWEET_BERRY_SEED = new AliasedBlockItem( com.enderzombi102.elysium.registry.BlockRegistry.SWEET_BERRY_CROP_BLOCK, new FabricItemSettings() );
	public static final AliasedBlockItem SWEET_POTATO_SEED = new AliasedBlockItem( BlockRegistry.SWEET_POTATO_CROP, new FabricItemSettings() );
	public static final AliasedBlockItem FLAX_SEED = new AliasedBlockItem( com.enderzombi102.elysium.registry.BlockRegistry.FLAX_CROP_BLOCK, new FabricItemSettings() );
	public static final WaterBottle WATER_BOTTLE = new WaterBottle(new Item.Settings().maxCount( 16 ) );

	public static final Map< String, Item > ITEMS = new HashMap<>() {{
		put( "race_dwarf", new Item( new Item.Settings() ) );
		put( "race_elf", new Item( new Item.Settings() ) );
		put( "race_halforc", new Item( new Item.Settings() ) );
		put( "race_human", new Item( new Item.Settings() ) );
		put( "role_admin", new Item( new Item.Settings() ) );
		put( "role_alchemist", new Item( new Item.Settings() ) );
		put( "role_archer", new Item( new Item.Settings() ) );
		put( "role_bard", new Item( new Item.Settings() ) );
		put( "role_blacksmith", new Item( new Item.Settings() ) );
		put( "role_carpenter", new Item( new Item.Settings() ) );
		put( "role_cleric", new Item( new Item.Settings() ) );
		put( "role_cook", new Item( new Item.Settings() ) );
		put( "role_countryman", new Item( new Item.Settings() ) );
		put( "role_nitwit", new Item( new Item.Settings() ) );
		put( "role_thief", new Item( new Item.Settings() ) );
		put( "role_warrior", new Item( new Item.Settings() ) );
		put( "role_wizard", new Item( new Item.Settings() ) );
		put( "role_worker", new Item( new Item.Settings() ) );
		put( "dung_ball", new Item( new Item.Settings() ) );
		put( "claim_contract", new ClaimContract( new Item.Settings().maxCount( 1 ) ) );
		put( "carpenter_contract", new CarpenterContract( new Item.Settings().maxCount( 1 ) ) );
		put( "tower_concession", new TowerConcession( new Item.Settings().maxCount( 1 ) ) );
		put( "ownership_contract", new Contract( new Item.Settings().maxCount( 1 ) ) );

		put( "auxilia_maggiore", new GlowItem( new Item.Settings().rarity( Rarity.EPIC ) ) );
		put( "auxilia_media", new GlowItem( new Item.Settings().rarity( Rarity.RARE ) ) );
		put( "auxilia_minore", new GlowItem( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
		put( "animal_bait", new Item( new Item.Settings() ) );
		put( "fish_bait", new Item( new Item.Settings() ) );
		put( "exotic_bait", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
		put( "artifact_fragment", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
		//put( "frammento_armatura", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
		//put( "frammento_arma", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
		//put( "frammento_uovo", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
		put( "favore", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
		put( "cactus_spines", new Item( new Item.Settings() ) );
		put( "fire_catalyst", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
		put( "frost_catalyst", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
		put( "arcane_catalyst", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
		put( "healing_catalyst", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
		put( "haste_catalyst", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
		put( "amplify_catalyst", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
		put( "volatility_catalyst", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
		put( "archer_catalyst", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
		put( "biscotto_vuoto", new Item( new Item.Settings().maxCount( 64 ) ) );
		put( "biscotto_crudo", new Item( new Item.Settings().maxCount( 16 ) ) );
		put( "bandana", new ArmorClothItem( "elysium:bandana", EquipmentSlot.HEAD ) );
		put( "cappello", new ArmorClothItem( "elysium:cappello", EquipmentSlot.HEAD ) );
		put("alchemist_helmet",new ArmorClothItem("elysium:alchemist_helmet",EquipmentSlot.HEAD));
		put("blacksmith_helmet", new ArmorClothItem("elysium:blacksmith_helmet",EquipmentSlot.HEAD));
		put( "maglia_acqua_corta_alt", new ArmorClothItem( "elysium:maglia_acqua_corta_alt", EquipmentSlot.CHEST ) );
		put( "maglia_acqua_corta", new ArmorClothItem( "elysium:maglia_acqua_corta", EquipmentSlot.CHEST ) );
		put( "maglia_acqua_lunga_alt", new ArmorClothItem( "elysium:maglia_acqua_lunga_alt", EquipmentSlot.CHEST ) );
		put( "maglia_acqua_lunga", new ArmorClothItem( "elysium:maglia_acqua_lunga", EquipmentSlot.CHEST ) );
		put( "maglia_blu_corta_alt", new ArmorClothItem( "elysium:maglia_blu_corta_alt", EquipmentSlot.CHEST ) );
		put( "maglia_blu_corta", new ArmorClothItem( "elysium:maglia_blu_corta", EquipmentSlot.CHEST ) );
		put( "maglia_blu_lunga_alt", new ArmorClothItem( "elysium:maglia_blu_lunga_alt", EquipmentSlot.CHEST ) );
		put( "maglia_blu_lunga", new ArmorClothItem( "elysium:maglia_blu_lunga", EquipmentSlot.CHEST ) );
		put( "maglia_corta", new ArmorClothItem( "elysium:maglia_corta", EquipmentSlot.CHEST ) );
		put( "maglia_lunga", new ArmorClothItem( "elysium:maglia_lunga", EquipmentSlot.CHEST ) );
		put( "maglia_marrone_corta_alt", new ArmorClothItem( "elysium:maglia_marrone_corta_alt", EquipmentSlot.CHEST ) );
		put( "maglia_marrone_corta", new ArmorClothItem( "elysium:maglia_marrone_corta", EquipmentSlot.CHEST ) );
		put( "maglia_marrone_lunga_alt", new ArmorClothItem( "elysium:maglia_marrone_lunga_alt", EquipmentSlot.CHEST ) );
		put( "maglia_marrone_lunga", new ArmorClothItem( "elysium:maglia_marrone_lunga", EquipmentSlot.CHEST ) );
		put( "maglia_rossa_corta_alt", new ArmorClothItem( "elysium:maglia_rossa_corta_alt", EquipmentSlot.CHEST ) );
		put( "maglia_rossa_corta", new ArmorClothItem( "elysium:maglia_rossa_corta", EquipmentSlot.CHEST ) );
		put( "maglia_rossa_lunga_alt", new ArmorClothItem( "elysium:maglia_rossa_lunga_alt", EquipmentSlot.CHEST ) );
		put( "maglia_rossa_lunga", new ArmorClothItem( "elysium:maglia_rossa_lunga", EquipmentSlot.CHEST ) );
		put( "maglia_verde_corta_alt", new ArmorClothItem( "elysium:maglia_verde_corta_alt", EquipmentSlot.CHEST ) );
		put( "maglia_verde_corta", new ArmorClothItem( "elysium:maglia_verde_corta", EquipmentSlot.CHEST ) );
		put( "maglia_verde_lunga_alt", new ArmorClothItem( "elysium:maglia_verde_lunga_alt", EquipmentSlot.CHEST ) );
		put( "maglia_verde_lunga", new ArmorClothItem( "elysium:maglia_verde_lunga", EquipmentSlot.CHEST ) );
		put( "maglia_viola_corta_alt", new ArmorClothItem( "elysium:maglia_viola_corta_alt", EquipmentSlot.CHEST ) );
		put( "maglia_viola_corta", new ArmorClothItem( "elysium:maglia_viola_corta", EquipmentSlot.CHEST ) );
		put( "maglia_viola_lunga_alt", new ArmorClothItem( "elysium:maglia_viola_lunga_alt", EquipmentSlot.CHEST ) );
		put( "maglia_viola_lunga", new ArmorClothItem( "elysium:maglia_viola_lunga", EquipmentSlot.CHEST ) );
		put( "pantalone_acqua", new ArmorClothItem( "elysium:pantalone_acqua", EquipmentSlot.LEGS ) );
		put( "pantalone_blu", new ArmorClothItem( "elysium:pantalone_blu", EquipmentSlot.LEGS ) );
		put( "pantalone_marrone", new ArmorClothItem( "elysium:pantalone_marrone", EquipmentSlot.LEGS ) );
		put( "pantalone_rosso", new ArmorClothItem( "elysium:pantalone_rosso", EquipmentSlot.LEGS ) );
		put( "pantalone_verde", new ArmorClothItem( "elysium:pantalone_verde", EquipmentSlot.LEGS ) );
		put( "pantalone_viola", new ArmorClothItem( "elysium:pantalone_viola", EquipmentSlot.LEGS ) );
		put( "pantalone", new ArmorClothItem( "elysium:pantalone", EquipmentSlot.LEGS ) );
		put( "stivali", new ArmorClothItem( "elysium:stivali", EquipmentSlot.FEET ) );
		put( "stracci", new ArmorClothItem( "elysium:stracci", EquipmentSlot.CHEST ) );
		put( "ban_hammer", new SwordItem( ToolMaterials.NETHERITE, 23, -3, new Item.Settings().rarity( Rarity.EPIC ) ) );
		put( "blacksmith_hammer", new SwordItem( ModItemTier.COPPER, 3, -3F, new Item.Settings() ) );
		put( "saw", new SwordItem( ToolMaterials.IRON, 3, -3, new Item.Settings() ) );
		put( "copper_plate", new Item( new Item.Settings().rarity( Rarity.COMMON ) ) );
		put( "tin_plate", new Item( new Item.Settings().rarity( Rarity.COMMON ) ) );
		put( "iron_plate", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
		put( "bronze_plate", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
		put( "silver_plate", new Item( new Item.Settings().rarity( Rarity.RARE ) ) );
		put( "gold_plate", new Item( new Item.Settings().rarity( Rarity.RARE ) ) );
		put( "raw_tin", new Item( new Item.Settings() ) );
		put( "raw_silver", new Item( new Item.Settings() ) );
		put( "scroll_arcane", new GlowItem( new Item.Settings().maxCount( 1 ).maxDamage( 1 ).rarity( Rarity.RARE ) ) );
		put( "scroll_fire", new GlowItem( new Item.Settings().maxCount( 1 ).maxDamage( 1 ).rarity( Rarity.RARE ) ) );
		put( "scroll_frost", new GlowItem( new Item.Settings().maxCount( 1 ).maxDamage( 1 ).rarity( Rarity.RARE ) ) );
		put( "scroll_heal", new GlowItem( new Item.Settings().maxCount( 1 ).maxDamage( 1 ).rarity( Rarity.RARE ) ) );
		put( "scroll_blank", new Item( new Item.Settings().maxCount( 1 ).maxDamage( 1 ).rarity( Rarity.UNCOMMON ) ) );
		put( "tin_sword", new SwordItem( ModItemTier.TIN, 3, -2.4F, new Item.Settings() ) );
		put( "tin_pickaxe", new PickaxeItem( ModItemTier.TIN, 1, -2.8F, new Item.Settings() ) );
		put( "tin_hoe", new HoeItem( ModItemTier.TIN, 0, -1, new Item.Settings() ) );
		put( "tin_shovel", new ShovelItem( ModItemTier.TIN, 1.5F, -3, new Item.Settings() ) );
		put( "tin_axe", new AxeItem( ModItemTier.TIN, 6, -3.1F, new Item.Settings() ) );
		put( "copper_sword", new SwordItem( ModItemTier.COPPER, 3, -2.4F, new Item.Settings() ) );
		put( "copper_pickaxe", new PickaxeItem( ModItemTier.COPPER, 1, -2.8F, new Item.Settings() ) );
		put( "copper_hoe", new HoeItem( ModItemTier.COPPER, 0, -1, new Item.Settings() ) );
		put( "copper_shovel", new ShovelItem( ModItemTier.COPPER, 1.5F, -3, new Item.Settings() ) );
		put( "copper_axe", new AxeItem( ModItemTier.COPPER, 6, -3.1F, new Item.Settings() ) );
		put( "bronze_sword", new SwordItem( ModItemTier.BRONZE, 3, -2.4F, new Item.Settings() ) );
		put( "bronze_pickaxe", new PickaxeItem( ModItemTier.BRONZE, 1, -2.8F, new Item.Settings() ) );
		put( "bronze_hoe", new HoeItem( ModItemTier.BRONZE, 0, -1, new Item.Settings() ) );
		put( "bronze_shovel", new ShovelItem( ModItemTier.BRONZE, 1.5F, -3, new Item.Settings() ) );
		put( "bronze_axe", new AxeItem( ModItemTier.BRONZE, 6, -3.1F, new Item.Settings() ) );
		put( "steel_sword", new SwordItem( ModItemTier.STEEL, 3, -2.4F, new Item.Settings() ) );
		put( "steel_pickaxe", new PickaxeItem( ModItemTier.STEEL, 1, -2.8F, new Item.Settings() ) );
		put( "steel_hoe", new HoeItem( ModItemTier.STEEL, 0, -1, new Item.Settings() ) );
		put( "steel_shovel", new ShovelItem( ModItemTier.STEEL, 1.5F, -3, new Item.Settings() ) );
		put( "steel_axe", new AxeItem( ModItemTier.STEEL, 6, -3.1F, new Item.Settings() ) );
		put( "silver_sword", new SwordItem( ModItemTier.SILVER, 3, -2.4F, new Item.Settings() ) );
		put( "silver_pickaxe", new PickaxeItem( ModItemTier.SILVER, 1, -2.8F, new Item.Settings() ) );
		put( "silver_hoe", new HoeItem( ModItemTier.SILVER, 0, -1, new Item.Settings() ) );
		put( "silver_shovel", new ShovelItem( ModItemTier.SILVER, 1.5F, -3, new Item.Settings() ) );
		put( "silver_axe", new AxeItem( ModItemTier.SILVER, 6, -3.1F, new Item.Settings() ) );
		put( "core", new Item( new Item.Settings() ) );

		put( "core_fragment", new Item( new Item.Settings() ) );
		put( "end_shard", new Item( new Item.Settings() ) );
		put( "tin_nugget", new Item( new Item.Settings() ) );
		put( "copper_nugget", new Item( new Item.Settings() ) );
		put( "sapling_bag", new Item( new Item.Settings() ) );
		put( "artifact_bag", new Item( new Item.Settings() ) );
		put( "gold_flute", new HornItem( new Item.Settings().maxCount( 1 ).maxDamage( 50 ) ) );
		put( "netherite_flute", new HornItem( new Item.Settings().maxCount( 1 ).maxDamage( 50 ) ) );
		put( "claim_monocle", new ClaimMonocleItem( new Item.Settings().maxCount( 1 ) ) );
		put( "inferior_mending_elisir", new Item( new Item.Settings() ) );
		put( "lesser_mending_elisir", new Item( new Item.Settings() ) );
		put( "medium_mending_elisir", new Item( new Item.Settings() ) );
		put( "greater_mending_elisir", new Item( new Item.Settings() ) );
		put( "superior_mending_elisir", new Item( new Item.Settings() ) );
		put( "patch_copper", new Item( new Item.Settings() ) );
		put( "patch_iron", new Item( new Item.Settings() ) );
		put( "patch_steel", new Item( new Item.Settings() ) );
		put( "patch_silver", new Item( new Item.Settings() ) );
		put( "patch_netherite", new Item( new Item.Settings() ) );
		put( "patch_gold", new Item( new Item.Settings() ) );
		put( "patch_diamond", new Item( new Item.Settings() ) );
		put( "cote_copper", new Item( new Item.Settings() ) );
		put( "cote_iron", new Item( new Item.Settings() ) );
		put( "cote_steel", new Item( new Item.Settings() ) );
		put( "cote_silver", new Item( new Item.Settings() ) );
		put( "cote_netherite", new Item( new Item.Settings() ) );
		put( "cote_gold", new Item( new Item.Settings() ) );
		put( "cote_diamond", new Item( new Item.Settings() ) );
		put( "cote_tin", new Item( new Item.Settings() ) );
		put( "patch_tin", new Item( new Item.Settings() ) );
		put( "diamond_plate", new Item( new Item.Settings() ) );
		put( "netherite_plate", new Item( new Item.Settings() ) );
		put("raw_diamond",new Item( new Item.Settings() ) );
		put("raw_emerald",new Item( new Item.Settings() ) );
		put("carrot_juice", new DrinkItem((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("pumpkin_juice", new DrinkItem((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)));
		put("strawberry_juice", new DrinkItem((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("sugar_cane_juice", new DrinkItem((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("tomato_juice", new DrinkItem((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("baguette_bread", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("bun_bread", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("focaccia_bread", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("rosette_bread", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("grilled_asparagus", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("grilled_beetroot", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("grilled_broccoli", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("grilled_brown_mushroom", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("grilled_corn", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("grilled_eggplant", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("grilled_red_mushroom", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("grilled_tomato", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("smoked_asparagus", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("smoked_beef", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("smoked_beetroot", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("smoked_broccoli", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("smoked_brown_mushroom", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("smoked_chicken", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("smoked_cod", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("smoked_mutton", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("smoked_porkchop", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("smoked_potato", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("smoked_rabbit", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("smoked_red_mushroom", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("smoked_salmon", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("smoked_squid", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("smoked_tropical_fish", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("fish_sandwich", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("jelly_sandwich", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("meat_sandwich", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("sliced_bread", new Item( new Item.Settings() ) );
		put("vegetable_sandwich", new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("apple_jam",new DrinkItem((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("carrot_jam",new DrinkItem((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("cocoa_jam",new DrinkItem((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("grape_jam",new DrinkItem((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)) );
		put("strawberry_jam",new DrinkItem((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE))  );
		put("sweetberry_jam",new DrinkItem((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE))  );
		put("choco_wrap",new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)));
		put("fish_wrap",new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)));
		put("meat_wrap",new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)));
		put("sweet_wrap",new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)));
		put("fish_burrito",new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)));
		put("meat_burrito",new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)));
		put("sweet_burrito",new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)));
		put("veg_burrito",new Item((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)));
		put("fish_stew",new StewItem((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)));
		put("meat_stew",new StewItem((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)));
		put("veg_stew",new StewItem((new Item.Settings()).group(ItemGroup.FOOD).food(FoodComponents.HONEY_BOTTLE)));
		put("melodic_rune",new Item( new Item.Settings() ) );
		put("instrument_strength", new HornItem(new Item.Settings().maxCount( 1 ).maxDamage( 50 )));
		put("instrument_speed", new HornItem(new Item.Settings().maxCount( 1 ).maxDamage( 50 )));
		put("instrument_glowing", new HornItem(new Item.Settings().maxCount( 1 ).maxDamage( 50 )));
		put("instrument_absorption", new HornItem(new Item.Settings().maxCount( 1 ).maxDamage( 50 )));
		put("instrument_cleanse", new HornItem(new Item.Settings().maxCount( 1 ).maxDamage( 50 )));
		put("instrument_haste", new HornItem(new Item.Settings().maxCount( 1 ).maxDamage( 50 )));
		put("instrument_resistance", new HornItem(new Item.Settings().maxCount( 1 ).maxDamage( 50 )));
		put("instrument_night_vision", new HornItem(new Item.Settings().maxCount( 1 ).maxDamage( 50 )));
	}};
	public static final ItemGroup MAGIC_BOOKS = FabricItemGroupBuilder.build(
		new Identifier( Const.ID, "books" ),
		() -> ITEMS.get( "favore" ).getDefaultStack() );

	public static void register() {
		ITEMS.putAll( gems() );
		ITEMS.putAll( classes() );
		for ( var entry : ITEMS.entrySet() )
			Registry.register( Registry.ITEM, getId( entry.getKey() ), entry.getValue() );
		Registry.register(Registry.ITEM,getId("bamboo_seeds"),BAMBOO_SEED);
		Registry.register(Registry.ITEM,getId("water_bottle"),WATER_BOTTLE);
		Registry.register(Registry.ITEM,getId("brown_mushroom_seeds"),BROWN_MUSHROOM_SEED);
		Registry.register(Registry.ITEM,getId("cactus_seeds"),CACTUS_SEED);
		Registry.register(Registry.ITEM,getId("corn_seeds"),CORN_SEED);
		Registry.register(Registry.ITEM,getId("melon_seeds"),MELON_SEED);
		Registry.register(Registry.ITEM,getId("pumpkin_seeds"),PUMPKIN_SEED);
		Registry.register(Registry.ITEM,getId("red_mushroom_seeds"),RED_MUSHROOM_SEED);
		Registry.register(Registry.ITEM,getId("sugar_cane_seeds"),SUGAR_CANE_SEED);
		Registry.register(Registry.ITEM,getId("kelp_seeds"),KELP_SEED);
		Registry.register(Registry.ITEM,getId("carrot_seeds"),CARROT_SEED);
		Registry.register(Registry.ITEM,getId("cocoa_seeds"),COCOA_SEED);
		Registry.register(Registry.ITEM,getId("onion_seeds"),ONION_SEED);
		Registry.register(Registry.ITEM,getId("peanut_seeds"),PEANUT_SEED);
		Registry.register(Registry.ITEM,getId("potato_seeds"),POTATO_SEED);
		Registry.register(Registry.ITEM,getId("rice_seeds"),RICE_SEED);
		Registry.register(Registry.ITEM,getId("flax_seeds"),FLAX_SEED);
		Registry.register(Registry.ITEM,getId("sweet_berry_seeds"),SWEET_BERRY_SEED);
		Registry.register(Registry.ITEM,getId("sweet_potato_seeds"),SWEET_POTATO_SEED);
		Registry.register( Registry.POTION, "elysium:antidote", new Potion( "antidote", new StatusEffectInstance( StatusEffectRegistry.MILK_STATUS_EFFECT ) ) );
		Registry.register( Registry.POTION, "elysium:antidote2", new Potion( "antidote2", new StatusEffectInstance( StatusEffectRegistry.MILK_STATUS_EFFECT, 1, 1 ) ) );
		Registry.register( Registry.POTION, "elysium:spawn_recall", new Potion( "spawn_recall", new StatusEffectInstance( StatusEffectRegistry.WORLDSPAWN, 160 ) ) );
		Registry.register( Registry.POTION, "elysium:home_return", new Potion( "home_return", new StatusEffectInstance( StatusEffectRegistry.BEDSPAWN, 160 ) ) );
		Registry.register( Registry.POTION, "elysium:improved_strength", new Potion( "home_return", new StatusEffectInstance( StatusEffectRegistry.BEDSPAWN, 160 ) ) );
		Registry.register( Registry.POTION, "elysium:fire_endurance_potion_lv1", new Potion( new StatusEffectInstance( StatusEffectRegistry.FIRE_ENDURANCE, 9600 ) ) );
		Registry.register( Registry.POTION, "elysium:fire_endurance_potion_lv2", new Potion( "fire_endurance_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.FIRE_ENDURANCE, 9600, 1 ) ) );
		Registry.register( Registry.POTION, "elysium:fire_endurance_potion_lv3", new Potion( "fire_endurance_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.FIRE_ENDURANCE, 9600, 2 ) ) );
		Registry.register( Registry.POTION, "elysium:improved_strength_potion_lv1", new Potion( new StatusEffectInstance( StatusEffectRegistry.STRENGTH, 9600 ) ) );
		Registry.register( Registry.POTION, "elysium:improved_strength_potion_lv2", new Potion( "improved_strength_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.STRENGTH, 9600, 1 ) ) );
		Registry.register( Registry.POTION, "elysium:improved_strength_potion_lv3", new Potion( "improved_strength_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.STRENGTH, 9600, 2 ) ) );
		Registry.register( Registry.POTION, "elysium:improved_weakness_potion_lv1", new Potion( new StatusEffectInstance( StatusEffectRegistry.WEAKNESS, 9600 ) ) );
		Registry.register( Registry.POTION, "elysium:improved_weakness_potion_lv2", new Potion( "improved_weakness_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.WEAKNESS, 9600, 1 ) ) );
		Registry.register( Registry.POTION, "elysium:improved_weakness_potion_lv3", new Potion( "improved_weakness_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.WEAKNESS, 9600, 2 ) ) );
		Registry.register( Registry.POTION, "elysium:fire_spell_power_potion_lv1", new Potion( new StatusEffectInstance( StatusEffectRegistry.FIRE_SPELL_POWER, 9600 ) ) );
		Registry.register( Registry.POTION, "elysium:fire_spell_power_potion_lv2", new Potion( "fire_spell_power_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.FIRE_SPELL_POWER, 9600, 1 ) ) );
		Registry.register( Registry.POTION, "elysium:fire_spell_power_potion_lv3", new Potion( "fire_spell_power_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.FIRE_SPELL_POWER, 9600, 2 ) ) );
		Registry.register( Registry.POTION, "elysium:frost_spell_power_potion_lv1", new Potion( new StatusEffectInstance( StatusEffectRegistry.FROST_SPELL_POWER, 9600 ) ) );
		Registry.register( Registry.POTION, "elysium:frost_spell_power_potion_lv2", new Potion( "frost_spell_power_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.FROST_SPELL_POWER, 9600, 1 ) ) );
		Registry.register( Registry.POTION, "elysium:frost_spell_power_potion_lv3", new Potion( "frost_spell_power_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.FROST_SPELL_POWER, 9600, 2 ) ) );
		Registry.register( Registry.POTION, "elysium:arcane_spell_power_potion_lv1", new Potion( new StatusEffectInstance( StatusEffectRegistry.ARCANE_SPELL_POWER, 9600 ) ) );
		Registry.register( Registry.POTION, "elysium:arcane_spell_power_potion_lv2", new Potion( "arcane_spell_power_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.ARCANE_SPELL_POWER, 9600, 1 ) ) );
		Registry.register( Registry.POTION, "elysium:arcane_spell_power_potion_lv3", new Potion( "arcane_spell_power_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.ARCANE_SPELL_POWER, 9600, 2 ) ) );
		Registry.register( Registry.POTION, "elysium:holy_spell_power_potion_lv1", new Potion( new StatusEffectInstance( StatusEffectRegistry.HOLY_SPELL_POWER, 9600 ) ) );
		Registry.register( Registry.POTION, "elysium:holy_spell_power_potion_lv2", new Potion( "holy_spell_power_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.HOLY_SPELL_POWER, 9600, 1 ) ) );
		Registry.register( Registry.POTION, "elysium:holy_spell_power_potion_lv3", new Potion( "holy_spell_power_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.HOLY_SPELL_POWER, 9600, 2 ) ) );
		Registry.register( Registry.POTION, "elysium:impact_power_potion_lv1", new Potion( new StatusEffectInstance( StatusEffectRegistry.IMPACT_POWER, 9600 ) ) );
		Registry.register( Registry.POTION, "elysium:impact_power_potion_lv2", new Potion( "impact_power_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.IMPACT_POWER, 9600, 1 ) ) );
		Registry.register( Registry.POTION, "elysium:impact_power_potion_lv3", new Potion( "impact_power_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.IMPACT_POWER, 9600, 2 ) ) );
		Registry.register( Registry.POTION, "elysium:critical_damage_power_potion_lv1", new Potion( new StatusEffectInstance( StatusEffectRegistry.CRITICAL_DAMAGE_POWER, 9600 ) ) );
		Registry.register( Registry.POTION, "elysium:critical_damage_power_potion_lv2", new Potion( "critical_damage_power_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.CRITICAL_DAMAGE_POWER, 9600, 1 ) ) );
		Registry.register( Registry.POTION, "elysium:critical_damage_power_potion_lv3", new Potion( "critical_damage_power_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.CRITICAL_DAMAGE_POWER, 9600, 2 ) ) );
		Registry.register( Registry.POTION, "elysium:critical_chance_power_potion_lv1", new Potion( new StatusEffectInstance( StatusEffectRegistry.CRITICAL_CHANCE_POWER, 9600 ) ) );
		Registry.register( Registry.POTION, "elysium:critical_chance_power_potion_lv2", new Potion( "critical_chance_power_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.CRITICAL_CHANCE_POWER, 9600, 1 ) ) );
		Registry.register( Registry.POTION, "elysium:critical_chance_power_potion_lv3", new Potion( "critical_chance_power_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.CRITICAL_CHANCE_POWER, 9600, 2 ) ) );
		Registry.register( Registry.POTION, "elysium:haste_spell_power_potion_lv1", new Potion( new StatusEffectInstance( StatusEffectRegistry.HASTE_SPELL_POWER, 9600 ) ) );
		Registry.register( Registry.POTION, "elysium:haste_spell_power_potion_lv2", new Potion( "haste_spell_power_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.HASTE_SPELL_POWER, 9600, 1 ) ) );
		Registry.register( Registry.POTION, "elysium:haste_spell_power_potion_lv3", new Potion( "haste_spell_power_potion_lv1", new StatusEffectInstance( StatusEffectRegistry.HASTE_SPELL_POWER, 9600, 2 ) ) );
		Registry.register( Registry.POTION, "elysium:levitation_lv1", new Potion( "levitation_lv1", new StatusEffectInstance( StatusEffects.LEVITATION, 100 ) ) );
		Registry.register( Registry.POTION, "elysium:levitation_lv2", new Potion( "levitation_lv2", new StatusEffectInstance( StatusEffects.LEVITATION, 200,1 ) ) );
		Registry.register( Registry.POTION, "elysium:levitation_lv3", new Potion( "levitation_lv3", new StatusEffectInstance( StatusEffects.LEVITATION, 300,2 ) ) );
		Registry.register( Registry.POTION, "elysium:jump_boost_lv2", new Potion( "jump_boost_lv2", new StatusEffectInstance( StatusEffects.JUMP_BOOST, 9600,3 ) ) );
		Registry.register( Registry.POTION, "elysium:jump_boost", new Potion( "jump_boost", new StatusEffectInstance( StatusEffects.JUMP_BOOST, 9600,1 ) ) );
		Registry.register( Registry.POTION, "elysium:swiftness_lv2", new Potion( "swiftness_lv2", new StatusEffectInstance( StatusEffects.SPEED, 9600,1 ) ) );
		//Registry.register(Registry.POTION, "elysium:death_recall", new Potion("death_recall", new StatusEffectInstance(StatusEffectRegistry.DEATHLOC, 160)));
		Items.LAVA_BUCKET.kjs$setBurnTime( 0 );
		Items.BLAZE_ROD.kjs$setBurnTime( 0 );
		Map< String, StaffItem > map = new HashMap<>() {{
			put( "staff_arcane_t1", new StaffItem( ModItemTier.COPPER, new Item.Settings() ) );
			put( "staff_arcane_t2", new StaffItem( Weapon.CustomMaterial.matching( ToolMaterials.IRON, () -> Ingredient.ofItems( Items.IRON_INGOT ) ), new Item.Settings() ) );
			put( "staff_arcane_t3", new StaffItem( Weapon.CustomMaterial.matching( ToolMaterials.GOLD, () -> Ingredient.ofItems( Items.GOLD_INGOT ) ), new Item.Settings() ) );
			put( "staff_arcane_t4", new StaffItem( Weapon.CustomMaterial.matching( ToolMaterials.DIAMOND, () -> Ingredient.ofItems( Items.DIAMOND ) ), new Item.Settings() ) );
			put( "staff_arcane_t5", new StaffItem( Weapon.CustomMaterial.matching( ToolMaterials.NETHERITE, () -> Ingredient.ofItems( Items.NETHERITE_INGOT ) ), new Item.Settings() ) );
			put( "staff_fire_t1", new StaffItem( ModItemTier.COPPER, new Item.Settings() ) );
			put( "staff_fire_t2", new StaffItem( Weapon.CustomMaterial.matching( ToolMaterials.IRON, () -> Ingredient.ofItems( Items.IRON_INGOT ) ), new Item.Settings() ) );
			put( "staff_fire_t3", new StaffItem( Weapon.CustomMaterial.matching( ToolMaterials.GOLD, () -> Ingredient.ofItems( Items.GOLD_INGOT ) ), new Item.Settings() ) );
			put( "staff_fire_t4", new StaffItem( Weapon.CustomMaterial.matching( ToolMaterials.DIAMOND, () -> Ingredient.ofItems( Items.DIAMOND ) ), new Item.Settings() ) );
			put( "staff_fire_t5", new StaffItem( Weapon.CustomMaterial.matching( ToolMaterials.NETHERITE, () -> Ingredient.ofItems( Items.NETHERITE_INGOT ) ), new Item.Settings() ) );
			put( "staff_frost_t1", new StaffItem( ModItemTier.COPPER, new Item.Settings() ) );
			put( "staff_frost_t2", new StaffItem( Weapon.CustomMaterial.matching( ToolMaterials.IRON, () -> Ingredient.ofItems( Items.IRON_INGOT ) ), new Item.Settings() ) );
			put( "staff_frost_t3", new StaffItem( Weapon.CustomMaterial.matching( ToolMaterials.GOLD, () -> Ingredient.ofItems( Items.GOLD_INGOT ) ), new Item.Settings() ) );
			put( "staff_frost_t4", new StaffItem( Weapon.CustomMaterial.matching( ToolMaterials.DIAMOND, () -> Ingredient.ofItems( Items.DIAMOND ) ), new Item.Settings() ) );
			put( "staff_frost_t5", new StaffItem( Weapon.CustomMaterial.matching( ToolMaterials.NETHERITE, () -> Ingredient.ofItems( Items.NETHERITE_INGOT ) ), new Item.Settings() ) );
			put( "staff_holy_t1", new StaffItem( ModItemTier.COPPER, new Item.Settings() ) );
			put( "staff_holy_t2", new StaffItem( Weapon.CustomMaterial.matching( ToolMaterials.IRON, () -> Ingredient.ofItems( Items.IRON_INGOT ) ), new Item.Settings() ) );
			put( "staff_holy_t3", new StaffItem( Weapon.CustomMaterial.matching( ToolMaterials.GOLD, () -> Ingredient.ofItems( Items.GOLD_INGOT ) ), new Item.Settings() ) );
			put( "staff_holy_t4", new StaffItem( Weapon.CustomMaterial.matching( ToolMaterials.DIAMOND, () -> Ingredient.ofItems( Items.DIAMOND ) ), new Item.Settings() ) );
			put( "staff_holy_t5", new StaffItem( Weapon.CustomMaterial.matching( ToolMaterials.NETHERITE, () -> Ingredient.ofItems( Items.NETHERITE_INGOT ) ), new Item.Settings() ) );

		}};
		ImmutableMultimap.Builder< EntityAttribute, EntityAttributeModifier > attributes = ImmutableMultimap.builder();
		attributes.put( EntityAttributes.GENERIC_ATTACK_SPEED, new EntityAttributeModifier( ItemAccessor.ATTACK_SPEED_MODIFIER_ID(), "Weapon modifier", (double) -3, EntityAttributeModifier.Operation.ADDITION ) );
		attributes.put( EntityAttributes.GENERIC_ATTACK_DAMAGE, new EntityAttributeModifier( ItemAccessor.ATTACK_DAMAGE_MODIFIER_ID(), "Weapon modifier", (double) 4, EntityAttributeModifier.Operation.ADDITION ) );
		for ( Map.Entry< String, StaffItem > staff : map.entrySet() ) {
			staff.getValue().setAttributes( attributes.build() );
			Registry.register( Registry.ITEM, getId( staff.getKey() ), staff.getValue() );
		}
		HashMap< String, String[] > books = new HashMap<>();
		books.put( "arcane_bm1", new String[] { "elysium:arcane_flourish",
			"elysium:arcane_temporal_rift" } );
		books.put( "arcane_bm2", new String[] { "elysium:arcane_flourish",
			"elysium:arcane_temporal_rift",
			"spellbladenext:multislash" } );
		books.put( "arcane_bm3", new String[] { "elysium:arcane_flourish",
			"elysium:arcane_temporal_rift",
			"spellbladenext:multislash",
			"elysium:arcane_smash" } );
		books.put( "arcane_bm4", new String[] { "elysium:arcane_flourish",
			"elysium:arcane_temporal_rift",
			"spellbladenext:multislash",
			"elysium:arcane_smash",
			"spellbladenext:flicker_strike" } );
		books.put( "arcane_bm5", new String[] { "elysium:arcane_flourish",
			"elysium:arcane_temporal_rift",
			"spellbladenext:multislash",
			"elysium:arcane_smash",
			"spellbladenext:flicker_strike",
			"elysium:arcane_reflection" } );
		books.put( "arcane_m1", new String[] { "elysium:arcane_missile",
			"elysium:arcane_blast" } );
		books.put( "arcane_m2", new String[] { "elysium:arcane_missile",
			"elysium:arcane_blast",
			"elysium:arcane_beam" } );
		books.put( "arcane_m3", new String[] { "elysium:arcane_missile",
			"elysium:arcane_blast",
			"elysium:arcane_beam",
			"spellbladenext:magic_missile" } );
		books.put( "arcane_m4", new String[] { "elysium:arcane_missile",
			"elysium:arcane_blast",
			"elysium:arcane_beam",
			"spellbladenext:magic_missile",
			"elysium:arcane_surge" } );
		books.put( "arcane_m5", new String[] { "elysium:arcane_missile",
			"elysium:arcane_blast",
			"elysium:arcane_beam",
			"spellbladenext:magic_missile",
			"elysium:arcane_surge",
			"elysium:arcane_rain" } );
		books.put( "bm0", new String[] { "elysium:arcane_flourish",
			"elysium:fire_flourish",
			"elysium:frost_flourish",
			"elysium:holy_flourish" } );
		books.put( "fire_bm1", new String[] { "elysium:fire_flourish",
			"elysium:fire_shadowslash" } );
		books.put( "fire_bm2", new String[] { "elysium:fire_flourish",
			"elysium:fire_shadowslash",
			"elysium:fire_supernova" } );
		books.put( "fire_bm3", new String[] { "elysium:fire_flourish",
			"elysium:fire_shadowslash",
			"elysium:fire_supernova",
			"elysium:fire_smash" } );
		books.put( "fire_bm4", new String[] { "elysium:fire_flourish",
			"elysium:fire_shadowslash",
			"elysium:fire_supernova",
			"elysium:fire_smash",
			"elysium:fire_fervor" } );
		books.put( "fire_bm5", new String[] { "elysium:fire_flourish",
			"elysium:fire_shadowslash",
			"elysium:fire_supernova",
			"elysium:fire_smash",
			"elysium:fire_fervor",
			"elysium:fire_dive" } );
		books.put( "fire_m1", new String[] { "elysium:fire_ball",
			"elysium:fire_breath" } );
		books.put( "fire_m2", new String[] { "elysium:fire_ball",
			"elysium:fire_breath",
			"elysium:fire_meteor" } );
		books.put( "fire_m3", new String[] { "elysium:fire_ball",
			"elysium:fire_breath",
			"elysium:fire_meteor",
			"elysium:fire_meteor_bolts" } );
		books.put( "fire_m4", new String[] { "elysium:fire_ball",
			"elysium:fire_breath",
			"elysium:fire_meteor",
			"elysium:fire_meteor_bolts",
			"elysium:fire_fervor" } );
		books.put( "fire_m5", new String[] { "elysium:fire_ball",
			"elysium:fire_breath",
			"elysium:fire_meteor",
			"elysium:fire_meteor_bolts",
			"elysium:fire_fervor",
			"elysium:fire_winds" } );
		books.put( "frost_bm1", new String[] { "elysium:frost_flourish",
			"elysium:frost_nova" } );
		books.put( "frost_bm2", new String[] { "elysium:frost_flourish",
			"elysium:frost_nova",
			"elysium:frost_storm" } );
		books.put( "frost_bm3", new String[] { "elysium:frost_flourish",
			"elysium:frost_nova",
			"elysium:frost_storm",
			"elysium:frost_smash"
		} );
		books.put( "frost_bm4", new String[] { "elysium:frost_flourish",
			"elysium:frost_nova",
			"elysium:frost_storm",
			"elysium:frost_smash",
			"elysium:frost_shield" } );
		books.put( "frost_bm5", new String[] { "elysium:frost_flourish",
			"elysium:frost_nova",
			"elysium:frost_storm",
			"elysium:frost_smash",
			"elysium:frost_shield",
			"elysium:frost_statue" } );
		books.put( "frost_m1", new String[] { "elysium:frost_bolt",
			"elysium:frost_wind_artic" } );
		books.put( "frost_m2", new String[] { "elysium:frost_bolt",
			"elysium:frost_wind_artic",
			"elysium:frost_meteor" } );
		books.put( "frost_m3", new String[] { "elysium:frost_bolt",
			"elysium:frost_wind_artic",
			"elysium:frost_meteor",
			"elysium:frost_barrage_glacial" } );
		books.put( "frost_m4", new String[] { "elysium:frost_bolt",
			"elysium:frost_wind_artic",
			"elysium:frost_meteor",
			"elysium:frost_barrage_glacial",
			"elysium:frost_beam" } );
		books.put( "frost_m5", new String[] { "elysium:frost_bolt",
			"elysium:frost_wind_artic",
			"elysium:frost_meteor",
			"elysium:frost_barrage_glacial",
			"elysium:frost_beam",
			"elysium:frost_snowball_bombardment" } );
		books.put( "holy_bm1", new String[] { "elysium:holy_flourish",
			"elysium:holy_heal" } );
		books.put( "holy_bm2", new String[] { "elysium:holy_flourish",
			"elysium:holy_heal",
			"elysium:holy_punishment" } );
		books.put( "holy_bm3", new String[] { "elysium:holy_flourish",
			"elysium:holy_heal",
			"elysium:holy_punishment",
			"elysium:holy_smash" } );
		books.put( "holy_bm4", new String[] { "elysium:holy_flourish",
			"elysium:holy_heal",
			"elysium:holy_punishment",
			"elysium:holy_smash",
			"elysium:holy_protection" } );
		books.put( "holy_bm5", new String[] { "elysium:holy_flourish",
			"elysium:holy_heal",
			"elysium:holy_punishment",
			"elysium:holy_smash",
			"elysium:holy_protection",
			"spellbladenext:whirlwind_polearm" } );
		books.put( "holy_m1", new String[] { "elysium:holy_dard",
			"elysium:holy_heal_beam" } );
		books.put( "holy_m2", new String[] { "elysium:holy_dard",
			"elysium:holy_heal_beam",
			"elysium:holy_area" } );
		books.put( "holy_m3", new String[] { "elysium:holy_dard",
			"elysium:holy_heal_beam",
			"elysium:holy_area",
			"elysium:holy_shower" } );
		books.put( "holy_m4", new String[] { "elysium:holy_dard",
			"elysium:holy_heal_beam",
			"elysium:holy_area",
			"elysium:holy_shower",
			"elysium:holy_circle" } );
		books.put( "holy_m5", new String[] { "elysium:holy_dard",
			"elysium:holy_heal_beam",
			"elysium:holy_area",
			"elysium:holy_shower",
			"elysium:holy_circle",
			"elysium:holy_revive" } );
		books.put( "m0", new String[] { "elysium:frost_bolt",
			"elysium:holy_dard",
			"elysium:fire_ball",
			"elysium:arcane_missile" } );
		for ( var book : books.entrySet() ) {
			Identifier i = new Identifier( Const.ID, book.getKey() );
			SpellContainer container = new SpellContainer( false, i.toString(), 0, List.of( book.getValue() ) );
			SpellRegistry.book_containers.put( itemIdFor( i ), container );
			SpellBookItem b = new SpellBookItem( i, ( new FabricItemSettings() ).maxCount( 1 ).group( MAGIC_BOOKS ) );
			Registry.register( Registry.ITEM, itemIdFor( i ), b );
		}

		LOGGER.info( "[Elysium] Registered items" );
	}


	private static Map< String, ? extends Item > gems() {
		Map< String, Item > g = new HashMap<>() {
		};
		String[] gems = { "amber", "jade", "ruby", "sapphire", "topaz" };
		for ( var gem : gems ) {
			g.put( gem + "_common", new Item( new Item.Settings() ) );
			g.put( gem + "_epic", new Item( new Item.Settings().rarity( Rarity.RARE ) ) );
			g.put( gem + "_legendary", new Item( new Item.Settings().rarity( Rarity.EPIC ) ) );
			g.put( gem + "_rare", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
			g.put( gem + "_raw_common", new Item( new Item.Settings() ) );
			g.put( gem + "_raw_epic", new Item( new Item.Settings().rarity( Rarity.RARE ) ) );
			g.put( gem + "_raw_legendary", new Item( new Item.Settings().rarity( Rarity.EPIC ) ) );
			g.put( gem + "_raw_rare", new Item( new Item.Settings().rarity( Rarity.UNCOMMON ) ) );
			g.put( gem + "_raw_uncommon", new Item( new Item.Settings() ) );
			g.put( gem + "_uncommon", new Item( new Item.Settings() ) );
		}
		return g;
	}

	private static Map< String, ? extends Item > classes() {
		Map< String, Item > c = new HashMap<>() {
		};
		String[] classes = { "alchemist",
			"bard",
			"blacksmith",
			"carpenter",
			"cook",
			"farmer",
			"hunter",
			"mage/fire",
			"mage/frost",
			"mage/arcane",
			"mage/holy",
			"miner",
			"mystic/fire",
			"mystic/frost",
			"mystic/arcane",
			"mystic/holy",
			"shadow",
			"warrior" };
		for ( var classe : classes ) {

			int i = 0;
			if ( classe.contains( "mage/" ) || classe.contains( "mystic/" ) ) {
				i = 1;
			}
			for ( ; i < 6; i++ ) {
				c.put( "class/" + classe + "/" + i, new Item( new Item.Settings() ) );
			}
		}
		c.put( "class/admin", new Item( new Item.Settings() ) );
		c.put( "class/wanderer", new Item( new Item.Settings() ) );
		c.put( "class/mage/0", new Item( new Item.Settings() ) );
		c.put( "class/mystic/0", new Item( new Item.Settings() ) );
		return c;
	}
}

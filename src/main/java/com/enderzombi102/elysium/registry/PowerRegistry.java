package com.enderzombi102.elysium.registry;

import com.enderzombi102.elysium.power.CraftAmountPower;
import com.enderzombi102.elysium.power.RemoveRecipePower;
import io.github.apace100.apoli.data.ApoliDataTypes;
import io.github.apace100.apoli.power.Power;
import io.github.apace100.apoli.power.PowerType;
import io.github.apace100.apoli.power.factory.PowerFactory;
import io.github.apace100.apoli.registry.ApoliRegistries;
import io.github.apace100.apoli.util.modifier.Modifier;
import io.github.apace100.calio.data.SerializableData;
import io.github.apace100.calio.data.SerializableData.Instance;
import io.github.apace100.calio.data.SerializableDataTypes;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.elysium.util.Const.getId;

public class PowerRegistry {
	public static void register() {
		register(
			getId( "craft_amount" ),
			new SerializableData()
				.add( "item_condition", ApoliDataTypes.ITEM_CONDITION, null )
				.add( "modifier", Modifier.DATA_TYPE, null )
				.add( "modifiers", Modifier.LIST_TYPE, null ),
			data ->
				( type, player ) -> {
					var power = new CraftAmountPower( type, player, data.isPresent( "item_condition" ) ? data.get( "item_condition" ) : ( stack -> true ) );
					data.ifPresent( "modifier", power::addModifier );
					data.< List< Modifier > >ifPresent( "modifiers", mods -> mods.forEach( power::addModifier ) );
					return power;
				}
		);
		register(
			getId( "remove_recipes" ),
			new SerializableData().add( "recipes", SerializableDataTypes.IDENTIFIERS ),
			data -> ( type, player ) -> new RemoveRecipePower( type, player, data.get( "recipes" ) )
		);

		LOGGER.info( "[Elysium] Registered powers" );
	}

	private static < P extends Power > void register( Identifier id, SerializableData data, Function< Instance, BiFunction< PowerType< P >, LivingEntity, P > > factoryConstructor ) {
		var factory = new PowerFactory<>( id, data, factoryConstructor );
		Registry.register( ApoliRegistries.POWER_FACTORY, factory.getSerializerId(), factory );
	}
}

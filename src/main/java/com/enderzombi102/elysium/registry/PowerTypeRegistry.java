package com.enderzombi102.elysium.registry;

import io.github.apace100.apoli.power.Power;
import io.github.apace100.apoli.power.PowerType;
import io.github.apace100.apoli.power.PowerTypeReference;

import static com.enderzombi102.elysium.util.Const.getId;

public class PowerTypeRegistry {
	public static final PowerType< Power > NAMETAGS = new PowerTypeReference<>( getId( "class/admin/nametags_state" ) );
	public static final PowerType< Power > FADE_SWITCH = new PowerTypeReference<>( getId( "class/shadow/fade_res-switch" ) );
}

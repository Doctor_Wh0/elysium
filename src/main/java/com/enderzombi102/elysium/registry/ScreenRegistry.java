package com.enderzombi102.elysium.registry;

import com.enderzombi102.elysium.screen.ClaimAnchorGuiDescription;
import com.enderzombi102.elysium.screen.ClaimAnchorScreen;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerType;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.util.registry.Registry;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.elysium.util.Const.getId;

public class ScreenRegistry {
	public static ExtendedScreenHandlerType< ClaimAnchorGuiDescription > CLAIM_SCREEN_HANDLER_TYPE;

	public static void registerClient() {
		HandledScreens.<ClaimAnchorGuiDescription,ClaimAnchorScreen>register(
			CLAIM_SCREEN_HANDLER_TYPE,
			( gui, inventory, title ) -> new ClaimAnchorScreen( gui, inventory.player, title )
		);
	}

	public static void register() {
		CLAIM_SCREEN_HANDLER_TYPE = Registry.register(
			Registry.SCREEN_HANDLER,
			getId( "claim_screen_handler" ),
			new ExtendedScreenHandlerType<>(
				( syncId, inventory, buf ) -> new ClaimAnchorGuiDescription( syncId, inventory, buf, ScreenHandlerContext.EMPTY )
			)
		);

		LOGGER.info( "[Elysium] Registered screens" );
	}
}

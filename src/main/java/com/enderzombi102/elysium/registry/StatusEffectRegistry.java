package com.enderzombi102.elysium.registry;

import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.effect.*;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.effect.AbsorptionStatusEffect;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.registry.Registry;
import net.projectile_damage.api.EntityAttributes_ProjectileDamage;
import net.spell_engine.api.effect.ActionImpairing;
import net.spell_engine.api.effect.EntityActionsAllowed;
import net.spell_engine.api.effect.RemoveOnHit;
import net.spell_engine.api.effect.Synchronized;
import net.spell_power.api.MagicSchool;
import net.spell_power.api.SpellPower;
import net.spell_power.api.attributes.SpellAttributes;

import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.elysium.util.Const.getId;

public class StatusEffectRegistry {
	public static final StatusEffect MILK_STATUS_EFFECT = new MilkStatusEffect( StatusEffectCategory.BENEFICIAL, 16777215 );
	public static final StatusEffect WORLDSPAWN = new WorldSpawnStatusEffect( StatusEffectCategory.BENEFICIAL, 522131 );
	public static final StatusEffect BEDSPAWN = new BedSpawnStatusEffect( StatusEffectCategory.BENEFICIAL, 497655 );
	public static final StatusEffect DEATHLOC = new DeathLocStatusEffect( StatusEffectCategory.BENEFICIAL, 3357854 );
	public static final StatusEffect POSTMORTEM = new PostMortemEffect( StatusEffectCategory.HARMFUL, 11403181 );
	public static final StatusEffect CLUMSY = new ClumsyEffect( StatusEffectCategory.HARMFUL, 16755968 );
	public static final StatusEffect AWAKEN = new AwakenStatusEffect( StatusEffectCategory.HARMFUL, 2233856 );

	public static final StatusEffect SACRIFICE = new SacrificeStatusEffect( StatusEffectCategory.BENEFICIAL, 11337728 );
	public static final StatusEffect MESMERIZED = new MesmerizedEffect( StatusEffectCategory.HARMFUL, 14540253 )
		.addAttributeModifier( EntityAttributes.GENERIC_MOVEMENT_SPEED,
			"052f3166-8ae7-11ed-a1eb-0242ac120002",
			-1F,
			EntityAttributeModifier.Operation.MULTIPLY_TOTAL );
	public static StatusEffect STRENGTH = new CustomStatusEffect( StatusEffectCategory.BENEFICIAL, 16121863 )
		.addAttributeModifier( EntityAttributes.GENERIC_ATTACK_DAMAGE,
			"1ce35aec-6a41-44ff-a537-f03b76f01664",
			Config.getEffect().strength_bonus,
			EntityAttributeModifier.Operation.MULTIPLY_TOTAL );
	public static StatusEffect STRENGTH_SONG = new CustomStatusEffect( StatusEffectCategory.BENEFICIAL, 16121863 )
		.addAttributeModifier( EntityAttributes.GENERIC_ATTACK_DAMAGE,
			"1ce35aec-6a41-44ff-a537-f03b76f01665",
			Config.getEffect().strength_bonus,
			EntityAttributeModifier.Operation.MULTIPLY_TOTAL );
	public static StatusEffect WEAKNESS = new CustomStatusEffect( StatusEffectCategory.HARMFUL, 4797001 )
		.addAttributeModifier( EntityAttributes.GENERIC_ATTACK_DAMAGE,
			"1ce35aeb-6a41-44ff-a537-f03b76f01664",
			Config.getEffect().weakness_malus,
			EntityAttributeModifier.Operation.MULTIPLY_TOTAL );

	public static StatusEffect FIRE_SPELL_POWER = new CustomStatusEffect( StatusEffectCategory.BENEFICIAL, 16754995 )
		.addAttributeModifier( SpellAttributes.POWER.get( MagicSchool.FIRE ).attribute,
			"5835e9c2-4182-4098-b9ef-23670c46cb4d",
			Config.getEffect().fire_spell_power_bonus,
			EntityAttributeModifier.Operation.MULTIPLY_TOTAL );

	public static StatusEffect FROST_SPELL_POWER = new CustomStatusEffect( StatusEffectCategory.BENEFICIAL, 297467 )
		.addAttributeModifier( SpellAttributes.POWER.get( MagicSchool.FROST ).attribute,
			"caa82c97-9874-4f5e-84e4-37380bf756ec",
			Config.getEffect().frost_spell_power_bonus,
			EntityAttributeModifier.Operation.MULTIPLY_TOTAL );

	public static StatusEffect ARCANE_SPELL_POWER = new CustomStatusEffect( StatusEffectCategory.BENEFICIAL, 15435507 )
		.addAttributeModifier( SpellAttributes.POWER.get( MagicSchool.ARCANE ).attribute,
			"8b724548-dbd9-4dbf-8ad5-9c0b7757dec5",
			Config.getEffect().arcane_spell_power_bonus,
			EntityAttributeModifier.Operation.MULTIPLY_TOTAL );

	public static StatusEffect HOLY_SPELL_POWER = new CustomStatusEffect( StatusEffectCategory.BENEFICIAL, 7274046 )
		.addAttributeModifier( SpellAttributes.POWER.get( MagicSchool.HEALING ).attribute,
			"60125c3e-4980-4cc8-b54e-037b47185e2b",
			Config.getEffect().holy_spell_power_bonus,
			EntityAttributeModifier.Operation.MULTIPLY_TOTAL );

	public static StatusEffect IMPACT_POWER = new CustomStatusEffect( StatusEffectCategory.BENEFICIAL, 16777215 )
		.addAttributeModifier( EntityAttributes_ProjectileDamage.GENERIC_PROJECTILE_DAMAGE,
			"45da702a-e40a-4041-bd54-f06e283ad7cb",
			Config.getEffect().impact_power_bonus,
			EntityAttributeModifier.Operation.MULTIPLY_TOTAL );

	public static StatusEffect CRITICAL_DAMAGE_POWER = new CustomStatusEffect( StatusEffectCategory.BENEFICIAL, 3124687 )
		.addAttributeModifier( SpellAttributes.CRITICAL_DAMAGE.attribute,
			"32a5a129-51a6-4a38-b78e-e7afb69f9e17",
			Config.getEffect().critical_damage_power_bonus,
			EntityAttributeModifier.Operation.MULTIPLY_TOTAL );
	public static StatusEffect CRITICAL_CHANCE_POWER = new CustomStatusEffect( StatusEffectCategory.BENEFICIAL, 3124687 )
		.addAttributeModifier( SpellAttributes.CRITICAL_CHANCE.attribute,
			"32a5a129-51a6-4a38-b78e-e7afb69f9e17",
			Config.getEffect().critical_chance_power_bonus,
			EntityAttributeModifier.Operation.MULTIPLY_TOTAL );
	public static StatusEffect HASTE_SPELL_POWER = new CustomStatusEffect( StatusEffectCategory.BENEFICIAL, 3124687 )
		.addAttributeModifier( SpellAttributes.HASTE.attribute,
			"bb6233b1-4759-47d0-9044-d509b4bc6695",
			Config.getEffect().haste_spell_power_bonus,
			EntityAttributeModifier.Operation.MULTIPLY_TOTAL );

	public static StatusEffect RESISTANCE_SONG = new CustomStatusEffect( StatusEffectCategory.BENEFICIAL, 3124687 );

	public static StatusEffect ABSORPTION_SONG = new AbsorptionEffect( StatusEffectCategory.BENEFICIAL, 3124687 );
	public static StatusEffect HASTE_SONG = new CustomStatusEffect( StatusEffectCategory.BENEFICIAL, 16777215 )
		.addAttributeModifier( EntityAttributes.GENERIC_ATTACK_SPEED,
			String.valueOf( UUID.randomUUID() ),
			0.025,
			EntityAttributeModifier.Operation.MULTIPLY_TOTAL );
	public static StatusEffect NIGHT_VISION_SONG = new CustomStatusEffect( StatusEffectCategory.BENEFICIAL, 3124687 );
	public static StatusEffect FIRE_ENDURANCE = new FireEnduranceEffect( StatusEffectCategory.BENEFICIAL, 16754995 ).setVulnerability(
		MagicSchool.FIRE, new SpellPower.Vulnerability(
			Config.getEffect().fire_endurance_bonus, 0, 0 ) );

	public static void register() {

		Registry.register( Registry.STATUS_EFFECT, getId( "cleanser" ), MILK_STATUS_EFFECT );
		Registry.register( Registry.STATUS_EFFECT, getId( "spawn_recall" ), WORLDSPAWN );
		Registry.register( Registry.STATUS_EFFECT, getId( "home_return" ), BEDSPAWN );
		//Registry.register( Registry.STATUS_EFFECT, getId( "death_recall" ), DEATHLOC );
		Registry.register( Registry.STATUS_EFFECT, getId( "postmortem" ), POSTMORTEM );
		Registry.register( Registry.STATUS_EFFECT, getId( "clumsy" ), CLUMSY );
		Registry.register( Registry.STATUS_EFFECT, getId( "mesmerized" ), MESMERIZED );
		Registry.register( Registry.STATUS_EFFECT, getId( "awaken" ), AWAKEN );
		Registry.register( Registry.STATUS_EFFECT, getId( "sacrifice" ), SACRIFICE );
		Registry.register( Registry.STATUS_EFFECT, getId( "fire_endurance" ), FIRE_ENDURANCE );
		Registry.register( Registry.STATUS_EFFECT, getId( "strength" ), STRENGTH );
		Registry.register( Registry.STATUS_EFFECT, getId( "weakness" ), WEAKNESS );
		Registry.register( Registry.STATUS_EFFECT, getId( "fire_spell_power" ), FIRE_SPELL_POWER );
		Registry.register( Registry.STATUS_EFFECT, getId( "frost_spell_power" ), FROST_SPELL_POWER );
		Registry.register( Registry.STATUS_EFFECT, getId( "arcane_spell_power" ), ARCANE_SPELL_POWER );
		Registry.register( Registry.STATUS_EFFECT, getId( "holy_spell_power" ), HOLY_SPELL_POWER );
		Registry.register( Registry.STATUS_EFFECT, getId( "impact_power" ), IMPACT_POWER );
		Registry.register( Registry.STATUS_EFFECT, getId( "critical_damage_power" ), CRITICAL_DAMAGE_POWER );
		Registry.register( Registry.STATUS_EFFECT, getId( "critical_chance_power" ), CRITICAL_CHANCE_POWER );
		Registry.register( Registry.STATUS_EFFECT, getId( "haste_spell_power" ), HASTE_SPELL_POWER );
		Registry.register( Registry.STATUS_EFFECT, getId( "strength_song" ), new CustomStatusEffect( StatusEffectCategory.BENEFICIAL, 16777215 )
			.addAttributeModifier( EntityAttributes.GENERIC_ATTACK_DAMAGE,
				String.valueOf( UUID.randomUUID() ),
				0.025,
				EntityAttributeModifier.Operation.MULTIPLY_TOTAL ) );
		Registry.register( Registry.STATUS_EFFECT, getId( "speed_song" ), new CustomStatusEffect( StatusEffectCategory.BENEFICIAL, 16777215 )
			.addAttributeModifier( EntityAttributes.GENERIC_ATTACK_SPEED,
				String.valueOf( UUID.randomUUID() ),
				0.025,
				EntityAttributeModifier.Operation.MULTIPLY_TOTAL ) );

		Registry.register( Registry.STATUS_EFFECT, getId( "haste_song" ), HASTE_SONG);
		Registry.register( Registry.STATUS_EFFECT, getId( "night_vision_song" ), NIGHT_VISION_SONG );
		Registry.register( Registry.STATUS_EFFECT, getId( "absorption_song" ), ABSORPTION_SONG);
		Registry.register( Registry.STATUS_EFFECT, getId( "resistance_song" ), RESISTANCE_SONG);

		ActionImpairing.configure( MESMERIZED, EntityActionsAllowed.STUN );
		RemoveOnHit.configure( MESMERIZED, true );
		Synchronized.configure( MESMERIZED, true );
		LOGGER.info( "[Elysium] Registered status effects" );

	}
}

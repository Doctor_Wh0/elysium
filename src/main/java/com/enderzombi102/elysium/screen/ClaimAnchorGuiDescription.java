package com.enderzombi102.elysium.screen;

import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.imixin.ElysiumClaimBlockEntity;
import com.enderzombi102.elysium.registry.ScreenRegistry;
import com.enderzombi102.elysium.screen.widget.CyclingItemIcon;
import com.enderzombi102.elysium.screen.widget.WItemLabelWidget;
import com.enderzombi102.elysium.screen.widget.WMoneyShowWidget;
import com.enderzombi102.elysium.screen.widget.WTextureWidget;
import com.glisco.numismaticoverhaul.item.CoinItem;
import com.glisco.numismaticoverhaul.item.CurrencyItem;
import com.glisco.numismaticoverhaul.item.MoneyBagItem;
import com.glisco.numismaticoverhaul.item.NumismaticOverhaulItems;
import io.github.cottonmc.cotton.gui.SyncedGuiDescription;
import io.github.cottonmc.cotton.gui.networking.NetworkSide;
import io.github.cottonmc.cotton.gui.networking.ScreenNetworking;
import io.github.cottonmc.cotton.gui.widget.WDynamicLabel;
import io.github.cottonmc.cotton.gui.widget.WItemSlot;
import io.github.cottonmc.cotton.gui.widget.WLabel;
import io.github.cottonmc.cotton.gui.widget.WPlainPanel;
import io.github.cottonmc.cotton.gui.widget.data.Insets;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static com.enderzombi102.elysium.Elysium.LOGGER;
import static com.enderzombi102.elysium.screen.widget.WMoneyShowWidget.*;
import static com.enderzombi102.elysium.util.Const.getId;
import static com.enderzombi102.enderlib.collections.ListUtil.listOf;

public class ClaimAnchorGuiDescription extends SyncedGuiDescription {
	public static final Identifier SYNC_TIME_ID = getId( "screen/sync/claim_anchor/time" );
	private static final ItemStack COIN_BAG_STACK = new ItemStack( NumismaticOverhaulItems.MONEY_BAG );
	private static final Identifier BOOK_TEXTURE = getId( "textures/gui/widget/new_book.png" );
	private final @Nullable ElysiumClaimBlockEntity entity; // null == we're on client
	private long time; // seconds
	private long oldtime = 0;
	private String formatted;


	public ClaimAnchorGuiDescription( int syncId, PlayerInventory inventory, PacketByteBuf buf, ScreenHandlerContext ctx ) {
		super( ScreenRegistry.CLAIM_SCREEN_HANDLER_TYPE, syncId, inventory, new SimpleInventory( 1 ), getBlockPropertyDelegate( ctx ) );

		var root = new WPlainPanel();
		root.setSize( 400, 300 );
		this.setRootPanel( root );
		root.setInsets( Insets.ROOT_PANEL );

		var book = new WTextureWidget( BOOK_TEXTURE, 10, 10, 390, 290 );
		root.add( book, 0, 0 );

		this.time = buf.readLong();

		root.add( new WLabel( Text.literal( String.valueOf( buf.readInt() ) ) ), 64, 114 ); // reads distance

		root.add( new WDynamicLabel( this::getRemainingTime ), 67, 204 );


		var pup = new Apfloat( buf.readString() );
		Text text;
		var pupPerSecond = pup.precision( 10 ).divide( new Apfloat( 86400 ) );
		if ( pupPerSecond.compareTo( Apfloat.ONE ) <= 0 )
			text = Text.literal( ApfloatMath.roundToInteger( new Apfloat( 86400 ).divide( pup ), RoundingMode.UP ) + "s <- 1" );
		else
			text = Text.literal( "1s -> " + ApfloatMath.roundToInteger( pupPerSecond, RoundingMode.UP ) );

		//root.add( new WItemLabelWidget( text, BRONZE_STACK ), 22, 30 ); // reads pup

		root.add( new WMoneyShowWidget( pup.intValue(), -1 ), 76, 169 );

		var itemSlot = WItemSlot.of( this.blockInventory, 0 );
		itemSlot.setBackgroundPainter( null );
		itemSlot.setIcon( null );
		itemSlot.setFilter( it -> it.getItem() instanceof CoinItem || it.getItem() instanceof MoneyBagItem );
		itemSlot.setIcon( new CyclingItemIcon( listOf( GOLD_STACK, SILVER_STACK, BRONZE_STACK, COIN_BAG_STACK ), Config.getData().claims.taxes.claimScreenInputCycleTime ) );
		itemSlot.addChangeListener( this::onSlotChange );
		root.add( itemSlot, 298, 74 );


		root.add( this.createPlayerInventoryPanel(), 224, 193 );

		if ( this.world.isClient() )
			ScreenNetworking.of( this, NetworkSide.CLIENT )
				.receive( SYNC_TIME_ID, byteBuf -> this.time = byteBuf.readLong() );

		this.entity = ctx.get( ( world, pos ) -> (ElysiumClaimBlockEntity) world.getBlockEntity( pos ), null );

		root.validate( this );
		if ( Config.getData().logging.claimAnchorScreen )
			LOGGER.info( "[Elysium] Panel size: {}x{}", root.getWidth(), root.getHeight() );
	}

	private void onSlotChange( WItemSlot slot, Inventory inventory, int i, ItemStack stack ) {
		if ( this.world.isClient() || stack.isEmpty() )
			return;

		assert this.entity != null : "Why is entity null?";
		var currency = ( (CurrencyItem) stack.getItem() ).getValue( stack );
		this.entity.elysium$addTime( currency );
		this.time = this.entity.elysium$getTime();

		ScreenNetworking.of( this, NetworkSide.SERVER )
			.send( SYNC_TIME_ID, buf -> buf.writeLong( this.time ) );

		inventory.setStack( i, ItemStack.EMPTY );
	}

	private @NotNull String getRemainingTime() {
		if ( this.time < this.world.getTime() )
			return "Not active";
		if ( time != oldtime ) {
			var timeRemaining = Math.abs( this.time - this.world.getTime() );
			var time = System.currentTimeMillis() + ( timeRemaining / 20 ) * 1000;
			Date d = new Date( time );
			DateFormat formatter = new SimpleDateFormat( "dd/MM/yy HH:mm:ss" );
			formatter.setTimeZone( TimeZone.getTimeZone( "CET" ) );
			formatted = formatter.format( d );
			oldtime = time;
			return formatted;
		} else {
			return formatted;
		}
	}
}

package com.enderzombi102.elysium.screen;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.config.Config;
import com.enderzombi102.elysium.screen.widget.WTextureWidget;
import com.enderzombi102.elysium.util.Const;
import com.minecraftserverzone.weaponmaster.ClientInit;
import com.minecraftserverzone.weaponmaster.gui.DetailedSettingsScreen;
import com.minecraftserverzone.weaponmaster.setup.GuiHelper;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.block.entity.MobSpawnerBlockEntity;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

public class FirstLoginScreen extends Screen {
	public static int stage=0;
	public static final Identifier IMG0 =  Const.getId( "textures/gui/first.png");
	public static final Identifier IMG1 =  Const.getId( "textures/gui/second.png");
	public static final Identifier IMG2 =  Const.getId( "textures/gui/third.png");
	public FirstLoginScreen() {
		super( Text.literal( "Welcome to ElysiumCraft" ) );
	}

	@Override
	protected void init() {
		ButtonWidget buttonWidget=new ButtonWidget( (int) (this.width * 0.75), (int) (this.height * 0.90), this.textRenderer.getWidth( "Sei sicuro?"), 20, Text.literal("Prosegui"), ( p_96337_) -> {
			if(p_96337_.getMessage().getString().equalsIgnoreCase( "Prosegui" )){
				p_96337_.setMessage( Text.literal("Sei sicuro?") );
			}else if(p_96337_.getMessage().getString().equalsIgnoreCase( "Sei sicuro?" )) {
				if(stage==0){
					p_96337_.setMessage( Text.literal( "Prosegui" ) );
					stage=1;
				}else if(stage==1) {
					p_96337_.setMessage( Text.literal( "Prosegui" ) );
					stage=2;
				}else{
					MinecraftClient.getInstance().setScreen( new NicknameScreen() );
					stage=0;
				}
			}else{
				this.close();
			}
		});
		//this.addDrawableChild( new WTextureWidget(IMG, ) );
		this.addDrawableChild(buttonWidget);
		this.addDrawableChild( new ButtonWidget( (int) (this.width * 0.10), (int) (this.height *0.90), this.textRenderer.getWidth( "DISCONNETTI" )+4, 20, Text.literal("DISCONNETTI"), ( p_96337_) -> {
			MinecraftClient.getInstance().scheduleStop();
			MinecraftClient.getInstance().stop();
		} ) );
		super.init();
	}

	@Override
	public boolean keyPressed( int keyCode, int scanCode, int modifiers ) {
		return false;
	}

	@Override
	public void render( MatrixStack matrices, int mouseX, int mouseY, float delta ) {
		int screenHeight = this.height;
		int screenWidth = this.width;
		if(stage==0) {
			RenderSystem.setShaderTexture( 0, IMG0 );
			drawTexture( matrices, 0, 0, 0, 0, screenWidth, screenHeight, screenWidth, screenHeight );
		}else if(stage==1) {
			RenderSystem.setShaderTexture( 0, IMG1 );
			drawTexture( matrices, 0, 0, 0, 0, screenWidth, screenHeight, screenWidth, screenHeight );
		}else{
			RenderSystem.setShaderTexture( 0, IMG2 );
			drawTexture( matrices, 0, 0, 0, 0, screenWidth, screenHeight, screenWidth, screenHeight );
		}
		super.render(matrices, mouseX, mouseY, delta);
	}
}

package com.enderzombi102.elysium.screen;

import com.enderzombi102.elysium.client.ElysiumClient;
import com.enderzombi102.elysium.util.Const;
import com.minecraftserverzone.weaponmaster.ClientInit;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

public class ImageScreen  extends Screen {
	public static final Identifier IMG0 =  Const.getId( "textures/gui/guide.png");
	public ImageScreen( ) {
		super( Text.literal("WIP") );
	}

	@Override
	protected void init() {
		super.init();
	}

	@Override
	public boolean keyPressed( int keyCode, int scanCode, int modifiers ) {
		if ( ElysiumClient.SCREEN_IMG.matchesKey(keyCode, scanCode)) {
			this.close();
			return true;
		} else {
			return super.keyPressed(keyCode, scanCode, modifiers);
		}
	}

	@Override
	public void render( MatrixStack matrices, int mouseX, int mouseY, float delta ) {
		int screenHeight = this.height;
		int screenWidth = this.width;
		RenderSystem.setShaderTexture( 0, IMG0 );
		drawTexture( matrices, 0, 0, 0, 0, screenWidth, screenHeight, screenWidth, screenHeight );
		super.render(matrices, mouseX, mouseY, delta);
	}
}

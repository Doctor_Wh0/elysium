package com.enderzombi102.elysium.screen;

import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.util.Const;
import com.minecraftserverzone.weaponmaster.setup.GuiHelper;
import com.mojang.blaze3d.systems.RenderSystem;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NicknameScreen extends Screen {
	public static final Identifier IMG = Const.getId( "textures/gui/nickname.png" );
	public static String output = "";
	public static ButtonWidget confirm;
	private TextFieldWidget name;

	public NicknameScreen() {
		super( Text.literal( "Choose nickname" ) );
	}

	public static void setOutput( String output ) {
		NicknameScreen.output = output;
	}

	public static void setMessage( String message ) {
		confirm.setMessage( Text.literal( message ) );
	}

	protected void init() {
		name = new TextFieldWidget( this.textRenderer, this.width / 2 - 75, this.height / 2 - 30, 150, 20, Text.literal( "" ) );
		confirm = new ButtonWidget( this.width / 2 - 75, this.height / 2 + 10, 150, 20, Text.literal( "Confirm" ), ( p_96337_ ) -> {
			name.setTextFieldFocused( false );
			if ( p_96337_.getMessage().getString().equalsIgnoreCase( "Confirm" ) ) {
				if ( !name.getText().isEmpty() ) {
					Pattern p = Pattern.compile( "[^a-z ]", Pattern.CASE_INSENSITIVE );
					Matcher m = p.matcher( name.getText() );
					if ( m.find() ) {
						setOutput( "No special characters or numbers allowed" );
					} else {
						setOutput( "" );
						p_96337_.setMessage( Text.literal( "Press again to confirm" ) );
					}
				} else {
					setOutput( "Error, your name is too short" );
				}
			} else if ( p_96337_.getMessage().getString().equalsIgnoreCase( "Press again to confirm" ) ) {
				if ( !name.getText().isEmpty() ) {
					Pattern p = Pattern.compile( "[^a-z ]", Pattern.CASE_INSENSITIVE );
					Matcher m = p.matcher( name.getText() );
					if ( m.find() ) {
						setOutput( "No special characters or numbers allowed" );
						p_96337_.setMessage( Text.literal( "Confirm" ) );
					} else if(name.getText().replace( " ", "" ).length()<17){
						PacketByteBuf buf = PacketByteBufs.create();
						buf.writeString( "choose" );
						buf.writeString( name.getText() );
						ClientPlayNetworking.send( Elysium.QUERY_ID, buf );
					}else{
						setOutput( "Name too long" );
						p_96337_.setMessage( Text.literal( "Confirm" ) );
					}
				} else {
					setOutput( "Error, your name is too short" );
					p_96337_.setMessage( Text.literal( "Confirm" ) );
				}

			}
		} );
		this.addDrawableChild( confirm );
		this.addSelectableChild( name );
		super.init();
	}

	@Override
	public void tick() {
		this.name.tick();
	}

	@Override
	public void resize( MinecraftClient client, int width, int height ) {
		Text text = confirm.getMessage();
		String string2 = this.name.getText();
		this.init(client, width, height);
		confirm.setMessage(text);
		this.name.setText(string2);

	}

	public void render( MatrixStack matrices, int mouseX, int mouseY, float delta ) {
		int screenHeight = this.height;
		int screenWidth = this.width;
		TextRenderer fontrenderer = this.textRenderer;
		RenderSystem.setShaderTexture( 0, IMG );
		this.renderBackground( matrices );
		this.drawTexture( matrices, screenWidth / 2 - 119, screenHeight / 2 - 92, 0, 0, 238, 184 );
		GuiHelper.drawString( matrices, fontrenderer, "Choose a name", screenWidth / 2 - fontrenderer.getWidth( "Choose a name" ) / 2, screenHeight / 2 - 80, Integer.parseInt( "ffffff", 16 ), false );
		GuiHelper.drawString( matrices, fontrenderer, output, screenWidth / 2 - fontrenderer.getWidth( output ) / 2, screenHeight / 2 + 40, Integer.parseInt( "ffffff", 16 ), false );
		this.name.render( matrices, mouseX, mouseY, delta );
		super.render( matrices, mouseX, mouseY, delta );
	}




	@Override
	public boolean keyPressed( int keyCode, int scanCode, int modifiers ) {
		if ( name.isActive() ) {
			return super.keyPressed( keyCode, scanCode, modifiers );
		}
		return false;
	}
}

package com.enderzombi102.elysium.screen;
import com.enderzombi102.elysium.Elysium;
import com.enderzombi102.elysium.config.Config;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import it.unimi.dsi.fastutil.longs.LongCollection;
import it.unimi.dsi.fastutil.longs.LongIterator;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import me.shedaniel.rei.RoughlyEnoughItemsCoreClient;
import me.shedaniel.rei.api.client.entry.filtering.*;
import me.shedaniel.rei.api.client.plugins.REIClientPlugin;
import me.shedaniel.rei.api.common.entry.EntryIngredient;
import me.shedaniel.rei.api.common.entry.EntryStack;
import me.shedaniel.rei.api.common.entry.type.VanillaEntryTypes;
import me.shedaniel.rei.api.common.util.EntryStacks;
import me.shedaniel.rei.impl.common.InternalLogger;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.StringNbtReader;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.jetbrains.annotations.ApiStatus;

import java.util.Collection;
import java.util.Iterator;
import java.util.function.Consumer;

@Environment( EnvType.CLIENT)
@ApiStatus.Internal
public class REIPluginHide implements REIClientPlugin {

	static {
		FilteringRuleTypeRegistry.getInstance().register(new Identifier("elysium", "hide_items"), HideTagsFilteringRuleType.INSTANCE);
		RoughlyEnoughItemsCoreClient.POST_UPDATE_TAGS.register(HideTagsFilteringRule.INSTANCE::markDirty);
	}

	private enum HideTagsFilteringRuleType implements FilteringRuleType<HideTagsFilteringRule> {
		INSTANCE;

		@Override
		public NbtCompound saveTo(HideTagsFilteringRule rule, NbtCompound tag) {
			return tag;
		}

		@Override
		public HideTagsFilteringRule readFrom(NbtCompound tag) {
			return HideTagsFilteringRule.INSTANCE;
		}

		@Override
		public HideTagsFilteringRule createNew() {
			return HideTagsFilteringRule.INSTANCE;
		}

		@Override
		public boolean isSingular() {
			return true;
		}
	}

	private enum HideTagsFilteringRule implements FilteringRule<HideTagsFilteringRule.Cache> {
		INSTANCE;

		private Cache cache;

		private record Cache( EntryIngredient ingredient, LongSet hashes) {}

		@Override
		public FilteringRuleType<? extends FilteringRule<HideTagsFilteringRule.Cache>> getType() {
			return HideTagsFilteringRuleType.INSTANCE;
		}

		@Override
		public Cache prepareCache(boolean async) {
			try {
				EntryIngredient.Builder b =EntryIngredient.builder();
				Config.getHideData().items.forEach( new Consumer< String >() {
					@Override
					public void accept( String s ) {
						String[] all=s.split( "\\{",2);
						ItemStack i =  new ItemStack(Registry.ITEM.get(new Identifier(all[0])));
						try {
							if(all[1].length()>1) {
								i.setNbt( StringNbtReader.parse( "{" + all[ 1 ] ) );
							}
						} catch ( CommandSyntaxException ignored ) {
							ignored.printStackTrace();
						}
						b.add(EntryStacks.of(i));
					}
				} );
				EntryIngredient ingredient = b.build();
				LongSet hashes = new LongOpenHashSet();
				for ( EntryStack<?> stack : ingredient) {
					hashes.add(EntryStacks.hashExact(stack));
				}
				return this.cache = new Cache(ingredient, hashes);
			} catch (Throwable e) {
				InternalLogger.getInstance().warn("Failed to load hidden ingredients from tag, falling back to empty cache.", e);
				return this.cache = null;
			}
		}

		@Override
		public FilteringResult processFilteredStacks( FilteringContext context, FilteringResultFactory resultFactory, HideTagsFilteringRule.Cache cache, boolean async) {
			FilteringResult result = resultFactory.create();
			if (cache != null) {
				process(result, context.getShownStacks(), context.getShownExactHashes(), cache);
				process(result, context.getUnsetStacks(), context.getUnsetExactHashes(), cache);
			}
			return result;
		}

		private void process( FilteringResult result, Collection<EntryStack<?>> stacks, LongCollection hashes, HideTagsFilteringRule.Cache cache) {
			Iterator<EntryStack<?>> stackIterator = stacks.iterator();
			LongIterator hashIterator = hashes.iterator();
			while (stackIterator.hasNext()) {
				EntryStack<?> stack = stackIterator.next();
				long hash = hashIterator.nextLong();
				if (cache.hashes().contains(hash) || (stack.getIdentifier()!=null && stack.getIdentifier().toString().contains( "elysium:class/" ))) {
					result.hide(stack);
				}
			}
			try {
				result.show(EntryStacks.of(Registry.ITEM.get( new Identifier("furnish:letter") ).getDefaultStack().kjs$withNBT( StringNbtReader.parse("{Function:\"broadcast\",display:{Name:\\'[{\"text\":\"Howling Letter\",\"italic\":false}]\\', Lore: [\\'[{\"text\":\"Invia un messaggio immediato a tutta la cittadinanza. Si attiva se viene utilizzata dopo la firma.\"}]\\']}}"))));
			} catch ( CommandSyntaxException e ) {
			}
		}

		private void markDirty() {
			InternalLogger.getInstance().debug("Marking hidden ingredients from tag cache as dirty.");
			if (this.cache != null) {
				this.markDirty(this.cache.ingredient(), this.cache.hashes());
			}
			try {
				VanillaEntryTypes.ITEM.getDefinition();
			} catch (NullPointerException ignored) {
				this.cache = null;
				return;
			}
			this.cache = this.prepareCache(false);
			if (this.cache != null) {
				this.markDirty(this.cache.ingredient(), this.cache.hashes());
			}
			InternalLogger.getInstance().debug("Marked %d hidden ingredients from tag cache as dirty.", this.cache == null ? 0 : this.cache.hashes().size());
		}
	}


}

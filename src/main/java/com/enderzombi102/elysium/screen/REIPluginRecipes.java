package com.enderzombi102.elysium.screen;

import com.enderzombi102.elysium.recipe.*;
import com.enderzombi102.elysium.screen.anvil.AnvilCategory;
import com.enderzombi102.elysium.screen.anvil.AnvilDisplay;
import com.enderzombi102.elysium.screen.barbeque.BarbequeCategory;
import com.enderzombi102.elysium.screen.barbeque.BarbequeDisplay;
import com.enderzombi102.elysium.screen.copper_cauldron.CopperCauldronCategory;
import com.enderzombi102.elysium.screen.copper_cauldron.CopperCauldronDisplay;
import com.enderzombi102.elysium.screen.furnish.FurnishCategory;
import com.enderzombi102.elysium.screen.furnish.FurnishDisplay;
import com.enderzombi102.elysium.screen.iron_cauldron.IronCauldronCategory;
import com.enderzombi102.elysium.screen.iron_cauldron.IronCauldronDisplay;
import com.enderzombi102.elysium.screen.oven.OvenCategory;
import com.enderzombi102.elysium.screen.oven.OvenDisplay;
import com.enderzombi102.elysium.screen.refining.RefiningCauldronCategory;
import com.enderzombi102.elysium.screen.refining.RefiningCauldronDisplay;
import com.enderzombi102.elysium.screen.rune_altar.AltarCategory;
import com.enderzombi102.elysium.screen.rune_altar.AltarDisplay;
import com.enderzombi102.elysium.screen.terracotta_cauldron.TerracottaCauldronCategory;
import com.enderzombi102.elysium.screen.terracotta_cauldron.TerracottaCauldronDisplay;
import io.github.wouink.furnish.integration.jei.FurnitureRecipeCategory;
import me.shedaniel.rei.api.client.plugins.REIClientPlugin;
import me.shedaniel.rei.api.client.registry.category.CategoryRegistry;
import me.shedaniel.rei.api.client.registry.display.DisplayRegistry;
import me.shedaniel.rei.api.common.util.EntryStacks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class REIPluginRecipes implements REIClientPlugin {

	@Override
	public void registerCategories( CategoryRegistry registry ) {
		registry.add( new TerracottaCauldronCategory() );
		ItemStack i = Registry.BLOCK.get( new Identifier( "custommachinery:custom_machine_block" ) ).asItem().getDefaultStack();
		NbtCompound nbtCompound = new NbtCompound();
		nbtCompound.putString( "machine", "elysium:0_cauldron" );
		i = i.kjs$withNBT( nbtCompound );
		registry.addWorkstations( TerracottaCauldronCategory.CAULDRON, EntryStacks.of( i ) );
		registry.add( new CopperCauldronCategory() );
		nbtCompound = new NbtCompound();
		nbtCompound.putString( "machine", "elysium:1_cauldron" );
		i = i.kjs$withNBT( nbtCompound );
		registry.addWorkstations( CopperCauldronCategory.CAULDRON, EntryStacks.of( i ) );
		registry.add( new IronCauldronCategory() );
		nbtCompound = new NbtCompound();
		nbtCompound.putString( "machine", "elysium:2_cauldron" );
		i = i.kjs$withNBT( nbtCompound );
		registry.addWorkstations( IronCauldronCategory.CAULDRON, EntryStacks.of( i ) );
		registry.add( new BarbequeCategory() );
		nbtCompound = new NbtCompound();
		nbtCompound.putString( "machine", "elysium:0_barbeque" );
		i = i.kjs$withNBT( nbtCompound );
		registry.addWorkstations( BarbequeCategory.BARBEQUE, EntryStacks.of( i ) );
		registry.add( new OvenCategory() );
		nbtCompound = new NbtCompound();
		nbtCompound.putString( "machine", "elysium:0_oven" );
		i = i.kjs$withNBT( nbtCompound );
		registry.addWorkstations( OvenCategory.OVEN, EntryStacks.of( i ) );
		registry.add( new RefiningCauldronCategory() );
		nbtCompound = new NbtCompound();
		nbtCompound.putString( "machine", "elysium:refining_station" );
		i = i.kjs$withNBT( nbtCompound );
		registry.addWorkstations( RefiningCauldronCategory.REFINING, EntryStacks.of( i ) );
		registry.add(new AltarCategory());
		registry.addWorkstations( AltarCategory.ALTAR, EntryStacks.of( Registry.BLOCK.get( new Identifier( "runes:crafting_altar" ) ).asItem().getDefaultStack() ) );
		registry.add(new AnvilCategory());
		registry.addWorkstations( AnvilCategory.ANVIL, EntryStacks.of( Registry.BLOCK.get( new Identifier( "minecraft:anvil" ) ).asItem().getDefaultStack() ) );
		registry.add(new FurnishCategory());
		registry.addWorkstations( FurnishCategory.FURNISH_CATEGORY, EntryStacks.of( Registry.BLOCK.get( new Identifier( "furnish:furniture_workbench" ) ).asItem().getDefaultStack() ) );
	}

	@Override
	public void registerDisplays( DisplayRegistry registry ) {
		registry.registerRecipeFiller( TerracottaCauldronRecipe.class, TerracottaCauldronRecipe.Type.INSTANCE, TerracottaCauldronDisplay::new );
		registry.registerRecipeFiller( CopperCauldronRecipe.class, CopperCauldronRecipe.Type.INSTANCE, CopperCauldronDisplay::new );
		registry.registerRecipeFiller( IronCauldronRecipe.class, IronCauldronRecipe.Type.INSTANCE, IronCauldronDisplay::new );
		registry.registerRecipeFiller( BarbequeRecipe.class, BarbequeRecipe.Type.INSTANCE, BarbequeDisplay::new );
		registry.registerRecipeFiller( OvenRecipe.class, OvenRecipe.Type.INSTANCE, OvenDisplay::new );
		registry.registerRecipeFiller( AltarRecipe.class, AltarRecipe.Type.INSTANCE, AltarDisplay::new );
		registry.registerRecipeFiller( AnvilRecipe.class, AnvilRecipe.Type.INSTANCE, AnvilDisplay::new );
		registry.registerRecipeFiller( RefiningStationRecipe.class, RefiningStationRecipe.Type.INSTANCE, RefiningCauldronDisplay::new );
		registry.registerRecipeFiller( FurnishRecipe.class, FurnishRecipe.Type.INSTANCE, FurnishDisplay::new );
	}
}

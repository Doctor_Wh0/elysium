package com.enderzombi102.elysium.screen.anvil;

import com.enderzombi102.elysium.util.Const;
import me.shedaniel.math.Point;
import me.shedaniel.math.Rectangle;
import me.shedaniel.rei.api.client.gui.Renderer;
import me.shedaniel.rei.api.client.gui.widgets.Widget;
import me.shedaniel.rei.api.client.gui.widgets.Widgets;
import me.shedaniel.rei.api.client.registry.display.DisplayCategory;
import me.shedaniel.rei.api.common.category.CategoryIdentifier;
import me.shedaniel.rei.api.common.display.basic.BasicDisplay;
import me.shedaniel.rei.api.common.util.EntryStacks;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.util.ArrayList;
import java.util.List;

public class AnvilCategory implements DisplayCategory< BasicDisplay > {
	public static final CategoryIdentifier< AnvilDisplay > ANVIL = CategoryIdentifier.of( Const.ID, "anvil" );

	@Override
	public CategoryIdentifier< ? extends BasicDisplay > getCategoryIdentifier() {
		return ANVIL;
	}

	@Override
	public Text getTitle() {
		return Text.literal( "Anvil Repair" );
	}

	@Override
	public Renderer getIcon() {
		return EntryStacks.of( Registry.BLOCK.get( new Identifier( "minecraft:anvil" ) ).asItem().getDefaultStack() );
	}

	@Override
	public List< Widget > setupDisplay( BasicDisplay display, Rectangle bounds ) {
		List< Widget > widgets = new ArrayList<>();
		Point s = new Point( bounds.getCenterX() - 55, bounds.getCenterY() - 17 );
		widgets.add( Widgets.createRecipeBase( bounds ) );
		widgets.add( Widgets.createSlot( new Point( s.x + 25, s.y + 5 ) )
			.entries( display.getInputEntries().get( 1 ) )  );
		widgets.add( Widgets.createSlot( new Point( s.x + 8, s.y + 5 ) )
			.entries( display.getInputEntries().get( 0 ) ) );
		widgets.add( Widgets.createResultSlotBackground( new Point( s.x + 83, s.y + 5 ) ) );
		widgets.add( Widgets.createSlot( new Point( s.x + 83, s.y + 5 ) )
			.markOutput().disableBackground().entries( display.getOutputEntries().get(0) ) );
		widgets.add( Widgets.createArrow( new Point( s.x + 54, s.y + 5 ) ) );

		return widgets;
	}

}

package com.enderzombi102.elysium.screen.barbeque;

import com.enderzombi102.elysium.recipe.BarbequeRecipe;
import me.shedaniel.rei.api.common.category.CategoryIdentifier;
import me.shedaniel.rei.api.common.display.basic.BasicDisplay;
import me.shedaniel.rei.api.common.entry.EntryIngredient;
import me.shedaniel.rei.api.common.util.EntryIngredients;
import me.shedaniel.rei.api.common.util.EntryStacks;
import net.minecraft.recipe.Ingredient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BarbequeDisplay extends BasicDisplay {

	public BarbequeDisplay( List< EntryIngredient > inputs, List< EntryIngredient > outputs ) {
		super( inputs, outputs );
	}

	public BarbequeDisplay( BarbequeRecipe recipe ) {
		super( getInputList( recipe ), List.of( EntryIngredient.of( EntryStacks.of( recipe.getOutput() ) ) ) );
	}

	private static List< EntryIngredient > getInputList( BarbequeRecipe recipe ) {
		if ( recipe == null ) return Collections.emptyList();
		List< EntryIngredient > list = new ArrayList<>();
		for ( Ingredient ingredient : recipe.getIngredients() ) {
			list.add( EntryIngredients.ofIngredient( ingredient ) );
		}
		return list;
	}

	@Override
	public CategoryIdentifier< ? > getCategoryIdentifier() {
		return BarbequeCategory.BARBEQUE;
	}

}

package com.enderzombi102.elysium.screen.copper_cauldron;

import com.enderzombi102.elysium.util.Const;
import me.shedaniel.math.Point;
import me.shedaniel.math.Rectangle;
import me.shedaniel.rei.api.client.gui.Renderer;
import me.shedaniel.rei.api.client.gui.widgets.Widget;
import me.shedaniel.rei.api.client.gui.widgets.Widgets;
import me.shedaniel.rei.api.client.registry.display.DisplayCategory;
import me.shedaniel.rei.api.common.category.CategoryIdentifier;
import me.shedaniel.rei.api.common.display.basic.BasicDisplay;
import me.shedaniel.rei.api.common.util.EntryStacks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.util.ArrayList;
import java.util.List;

public class CopperCauldronCategory implements DisplayCategory< BasicDisplay > {
	public static final CategoryIdentifier< CopperCauldronDisplay > CAULDRON = CategoryIdentifier.of( Const.ID, "1_cauldron" );

	@Override
	public CategoryIdentifier< ? extends BasicDisplay > getCategoryIdentifier() {
		return CAULDRON;
	}

	@Override
	public Text getTitle() {
		return Text.literal( "Copper Cauldron" );
	}

	@Override
	public Renderer getIcon() {
		ItemStack i = Registry.BLOCK.get( new Identifier( "custommachinery:custom_machine_block" ) ).asItem().getDefaultStack();
		NbtCompound nbtCompound = new NbtCompound();
		nbtCompound.putString( "machine", "elysium:1_cauldron" );
		return EntryStacks.of( i.kjs$withNBT( nbtCompound ) );
	}

	@Override
	public List< Widget > setupDisplay( BasicDisplay display, Rectangle bounds ) {
		List< Widget > widgets = new ArrayList<>();
		Point s = new Point( bounds.getCenterX() - 55, bounds.getCenterY() - 17 );
		widgets.add( Widgets.createRecipeBase( bounds ) );
		widgets.add( Widgets.createSlot( new Point( s.x - 9, s.y ) )
			.entries( display.getInputEntries().get( 0 ) ) );
		widgets.add( Widgets.createSlot( new Point( s.x + 8, s.y ) )
			.entries( display.getInputEntries().get( 1 ) ) );
		widgets.add( Widgets.createSlot( new Point( s.x + 25, s.y ) )
			.entries( display.getInputEntries().get( 2 ) ) );
		widgets.add( Widgets.createSlot( new Point( s.x - 9, s.y + 18 ) )
			.entries( display.getInputEntries().get( 3 ) ) );
		widgets.add( Widgets.createSlot( new Point( s.x + 8, s.y + 18 ) )
			.entries( display.getInputEntries().get( 4 ) ) );
		widgets.add( Widgets.createSlot( new Point( s.x + 25, s.y + 18 ) )
			.entries( display.getInputEntries().get( 5 ) ) );
		widgets.add( Widgets.createSlot( new Point( s.x + 55, s.y + 5 ) )
			.entries( display.getInputEntries().get( 6 ) ) );
		widgets.add( Widgets.createResultSlotBackground( new Point( s.x + 103, s.y + 5 ) ) );
		widgets.add( Widgets.createSlot( new Point( s.x + 103, s.y + 5 ) )
			.markOutput().disableBackground().entries( display.getOutputEntries().get( 0 ) ) );
		widgets.add( Widgets.createArrow( new Point( s.x + 74, s.y + 5 ) ) );

		return widgets;
	}

}

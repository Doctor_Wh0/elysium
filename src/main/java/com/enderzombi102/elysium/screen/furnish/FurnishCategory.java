package com.enderzombi102.elysium.screen.furnish;

import com.enderzombi102.elysium.screen.copper_cauldron.CopperCauldronDisplay;
import com.enderzombi102.elysium.util.Const;
import io.github.wouink.furnish.setup.FurnishBlocks;
import me.shedaniel.math.Point;
import me.shedaniel.math.Rectangle;
import me.shedaniel.rei.api.client.gui.Renderer;
import me.shedaniel.rei.api.client.gui.widgets.Widget;
import me.shedaniel.rei.api.client.gui.widgets.Widgets;
import me.shedaniel.rei.api.client.registry.display.DisplayCategory;
import me.shedaniel.rei.api.common.category.CategoryIdentifier;
import me.shedaniel.rei.api.common.display.basic.BasicDisplay;
import me.shedaniel.rei.api.common.util.EntryStacks;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.util.ArrayList;
import java.util.List;

public class FurnishCategory implements DisplayCategory< BasicDisplay > {
	public static final CategoryIdentifier< CopperCauldronDisplay > FURNISH_CATEGORY = CategoryIdentifier.of( "furnish", "furniture_making" );

	@Override
	public CategoryIdentifier< ? extends BasicDisplay > getCategoryIdentifier() {
		return FURNISH_CATEGORY;
	}

	@Override
	public Text getTitle() {
		return Text.translatable("furnish.furniture_making");
	}

	@Override
	public Renderer getIcon() {
		return EntryStacks.of( new ItemStack((ItemConvertible) FurnishBlocks.Furniture_Workbench.get()) );
	}

	@Override
	public List< Widget > setupDisplay( BasicDisplay display, Rectangle bounds ) {
		List< Widget > widgets = new ArrayList<>();
		Point s = new Point( bounds.getCenterX() - 55, bounds.getCenterY() - 17 );
		widgets.add( Widgets.createRecipeBase( bounds ) );
		widgets.add( Widgets.createSlot( new Point( s.x + 25, s.y + 5 ) )
			.entries( display.getInputEntries().get( 0 ) ) );
		widgets.add( Widgets.createResultSlotBackground( new Point( s.x + 83, s.y + 5 ) ) );
		widgets.add( Widgets.createSlot( new Point( s.x + 83, s.y + 5 ) )
			.markOutput().disableBackground().entries( display.getOutputEntries().get( 0 ) ) );
		widgets.add( Widgets.createArrow( new Point( s.x + 54, s.y + 5 ) ) );

		return widgets;
	}

}

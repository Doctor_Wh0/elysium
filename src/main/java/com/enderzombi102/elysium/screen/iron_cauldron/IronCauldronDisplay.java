package com.enderzombi102.elysium.screen.iron_cauldron;

import com.enderzombi102.elysium.recipe.IronCauldronRecipe;
import me.shedaniel.rei.api.common.category.CategoryIdentifier;
import me.shedaniel.rei.api.common.display.basic.BasicDisplay;
import me.shedaniel.rei.api.common.entry.EntryIngredient;
import me.shedaniel.rei.api.common.util.EntryIngredients;
import me.shedaniel.rei.api.common.util.EntryStacks;
import net.minecraft.recipe.Ingredient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IronCauldronDisplay extends BasicDisplay {

	public IronCauldronDisplay( List< EntryIngredient > inputs, List< EntryIngredient > outputs ) {
		super( inputs, outputs );
	}

	public IronCauldronDisplay( IronCauldronRecipe recipe){
		super(getInputList( recipe ),List.of(EntryIngredient.of( EntryStacks.of(recipe.getOutput()) )));
	}

	private static List< EntryIngredient > getInputList( IronCauldronRecipe recipe ) {
		if ( recipe == null ) return Collections.emptyList();
		List< EntryIngredient > list = new ArrayList<>();
		for( Ingredient ingredient: recipe.getIngredients() ) {
			list.add( EntryIngredients.ofIngredient( ingredient ) );
		}
		return list;
	}

	@Override
	public CategoryIdentifier< ? > getCategoryIdentifier() {
		return IronCauldronCategory.CAULDRON;
	}
}

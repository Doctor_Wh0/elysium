package com.enderzombi102.elysium.screen.widget;

import com.enderzombi102.elysium.util.MatrixStackLayer;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import io.github.cottonmc.cotton.gui.widget.icon.Icon;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.DiffuseLighting;
import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

public class CyclingItemIcon implements Icon {
	private final @NotNull List< ItemStack > stacks;
	private final long cycleMillis;
	private long lastChange = 0;
	private int currentIndex = 0;


	public CyclingItemIcon( @NotNull List< ItemStack > stacks, int cycleSecs ) {
		this.stacks = Objects.requireNonNull( stacks, "stacks" );
		this.cycleMillis = cycleSecs * 1000L;
	}

	@Override
	@Environment( EnvType.CLIENT )
	public void paint( MatrixStack matrices, int x, int y, int size ) {
		var client = MinecraftClient.getInstance();
		var stack = this.stacks.get( this.currentIndex );
		var renderer = client.getItemRenderer();
		var texManager = client.getTextureManager();
		var modelViewStack = RenderSystem.getModelViewStack();

		var model = renderer.getModel( stack, null, null, 0 );
		var unlit = !model.isSideLit();
		texManager.getTexture( SpriteAtlasTexture.BLOCK_ATLAS_TEXTURE ).setFilter( false, false );
		RenderSystem.setShaderTexture( 0, SpriteAtlasTexture.BLOCK_ATLAS_TEXTURE );
		RenderSystem.enableBlend();
		RenderSystem.blendFunc( GlStateManager.SrcFactor.SRC_ALPHA, GlStateManager.DstFactor.ONE_MINUS_SRC_ALPHA );
		RenderSystem.setShaderColor( 1f, 1f, 1f, 1f );

		if ( unlit )
			DiffuseLighting.disableGuiDepthLighting();

		try ( var push = new MatrixStackLayer( modelViewStack ) ) {
			modelViewStack.translate( x, y, 100.0F + renderer.zOffset );
			modelViewStack.translate( 8.0, 8.0, 0.0 );
			modelViewStack.scale( 1.0F, -1.0F, 1.0F );
			modelViewStack.scale( 16.0F, 16.0F, 16.0F );
			RenderSystem.applyModelViewMatrix();
			var immediate = client.getBufferBuilders().getEntityVertexConsumers();

			renderer.renderItem(
				stack,
				ModelTransformation.Mode.GUI,
				false,
				new MatrixStack(),
				immediate,
				0xCC0000,
				OverlayTexture.DEFAULT_UV,
				model
			);
			immediate.draw();
		}
		RenderSystem.applyModelViewMatrix();
		RenderSystem.enableDepthTest();

		if ( unlit )
			DiffuseLighting.enableGuiDepthLighting();


		if ( System.currentTimeMillis() - this.lastChange > this.cycleMillis ) {
			this.lastChange = System.currentTimeMillis();
			this.currentIndex = this.currentIndex + 1;
			if ( this.currentIndex >= this.stacks.size() ) this.currentIndex = 0;
		}
	}
}

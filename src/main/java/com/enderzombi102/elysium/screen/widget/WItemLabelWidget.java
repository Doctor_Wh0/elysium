package com.enderzombi102.elysium.screen.widget;

import io.github.cottonmc.cotton.gui.widget.WWidget;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import org.jetbrains.annotations.NotNull;

import static io.github.cottonmc.cotton.gui.widget.WLabel.DEFAULT_TEXT_COLOR;

public class WItemLabelWidget extends WWidget {
	private final ItemStack stack;
	private final Text label;

	public WItemLabelWidget( @NotNull Text label, ItemStack stack ) {
		this.label = label;
		this.stack = stack;
	}

	@Override
	public void paint( MatrixStack matrices, int x, int y, int mouseX, int mouseY ) {
		// interfaces
		var textRenderer = MinecraftClient.getInstance().textRenderer;
		var itemRenderer = MinecraftClient.getInstance().getItemRenderer();
		// draw
		int size = textRenderer.getWidth( this.label );
		textRenderer.draw( matrices, this.label, x, y + 2, DEFAULT_TEXT_COLOR );
		itemRenderer.renderGuiItemIcon( this.stack, x + size - 3, y - 3 );
	}
}

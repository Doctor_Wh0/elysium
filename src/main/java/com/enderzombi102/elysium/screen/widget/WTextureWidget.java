package com.enderzombi102.elysium.screen.widget;

import com.mojang.blaze3d.systems.RenderSystem;
import io.github.cottonmc.cotton.gui.widget.WWidget;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.NotNull;

public class WTextureWidget extends WWidget {
	private final int texWidth;
	private final int texHeight;
	private final @NotNull Identifier texture;

	public WTextureWidget( @NotNull Identifier texture, int width, int height, int texWidth, int texHeight ) {
		this.texture = texture; // size == texSize
		this.setSize( width, height );
		this.texWidth = texWidth;
		this.texHeight = texHeight;
	}

	@Override
	public void paint( MatrixStack matrices, int x, int y, int mouseX, int mouseY ) {
		RenderSystem.setShader( GameRenderer::getPositionTexShader );
		RenderSystem.setShaderTexture( 0, this.texture );
		RenderSystem.setShaderColor( 1.0F, 1.0F, 1.0F, .0f );
		DrawableHelper.drawTexture(
			matrices,
			x,
			y,
			0, 0,
			this.texWidth, this.texHeight,
			this.texWidth, this.texHeight
		);
	}
}

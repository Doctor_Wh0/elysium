package com.enderzombi102.elysium.util;

import com.enderzombi102.elysium.mixin.playerlist.PlayerListHudAccessor;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.OrderedText;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class PlayerListUtil extends DrawableHelper {
	public static void render( @NotNull PlayerListHudAccessor self, @NotNull MatrixStack matrices, int scaledWindowWidth ) {
		var networkHandler = self.getClient().getNetworkHandler();
		assert networkHandler != null : "Why are we rendering the PlayerList while not connected to a server?";

		var spOrEncrypted = self.getClient().isInSingleplayer() || networkHandler.getConnection().isEncrypted();
		var lines = new ArrayList< OrderedText >();

		if ( self.getHeader() != null )
			lines.addAll( self.getClient().textRenderer.wrapLines( self.getHeader(), scaledWindowWidth - 50 ) );

		if ( self.getFooter() != null )
			lines.addAll( self.getClient().textRenderer.wrapLines( self.getFooter(), scaledWindowWidth - 50 ) );

		if ( !lines.isEmpty() ) {
			var height = 10;
			var textWidth = Math.min( ( spOrEncrypted ? 9 : 0 ) + 13, scaledWindowWidth - 50 );

			for ( OrderedText orderedText : lines )
				textWidth = Math.max( textWidth, self.getClient().textRenderer.getWidth( orderedText ) );

			fill( matrices, scaledWindowWidth / 2 - textWidth / 2 - 1, height - 1, scaledWindowWidth / 2 + textWidth / 2 + 1, height + lines.size() * 9, Integer.MIN_VALUE );

			for ( OrderedText orderedText2 : lines ) {
				var s = self.getClient().textRenderer.getWidth( orderedText2 );
				self.getClient().textRenderer.drawWithShadow( matrices, orderedText2, (float) ( scaledWindowWidth / 2 - s / 2 ), (float) height, -1 );
				height += 9;
			}
		}
	}
}

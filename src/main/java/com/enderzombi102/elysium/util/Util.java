package com.enderzombi102.elysium.util;

import com.jamieswhiteshirt.rtree3i.Entry;
import draylar.goml.api.Claim;
import draylar.goml.api.ClaimBox;
import draylar.goml.api.ClaimUtils;
import draylar.goml.registry.GOMLBlocks;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.tag.TagKey;
import net.minecraft.text.ClickEvent;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkSectionPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Path;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class Util {
	public static final Style TOOLTIP_STYLE = Style.EMPTY.withFormatting( Formatting.DARK_GRAY ).withItalic( true );
	public static final ModContainer CONTAINER = FabricLoader.getInstance().getModContainer( Const.ID ).orElseThrow();
	public static final TagKey< Item > ELYSIUM_DROPS = TagKey.of( Registry.ITEM_KEY, new Identifier( Const.ID, "drops" ) );
	public static final TagKey< Item > FOOD_T0 = TagKey.of( Registry.ITEM_KEY, new Identifier( Const.ID, "food_t0" ) );
	public static final TagKey< Item > FOOD_T1 = TagKey.of( Registry.ITEM_KEY, new Identifier( Const.ID, "food_t1" ) );
	public static final TagKey< Item > FOOD_T2 = TagKey.of( Registry.ITEM_KEY, new Identifier( Const.ID, "food_t2" ) );
	public static final TagKey< Item > FOOD_T3 = TagKey.of( Registry.ITEM_KEY, new Identifier( Const.ID, "food_t3" ) );
	public static final TagKey< Item > FOOD_T4 = TagKey.of( Registry.ITEM_KEY, new Identifier( Const.ID, "food_t4" ) );
	public static final TagKey< Item > FOOD_T5 = TagKey.of( Registry.ITEM_KEY, new Identifier( Const.ID, "food_t5" ) );
	public static final TagKey< Item > BEER = TagKey.of( Registry.ITEM_KEY, new Identifier( Const.ID, "beer" ) );

	public static int toColor( int red, int green, int blue, int alpha ) {
		return ( ( alpha & 0xFF ) << 24 ) | ( ( red & 0xFF ) << 16 ) | ( ( green & 0xFF ) << 8 ) | ( blue & 0xFF );
	}

	public static List< Entry< ClaimBox, Claim > > sonoAttaccato( World world, BlockPos origin, int val, UUID player ) {
		ChunkSectionPos center = ChunkSectionPos.from( origin );
		ChunkSectionPos start = ChunkSectionPos.from( new BlockPos( center.getMinX() - val * 16, -70, center.getMinZ() ) );
		ChunkSectionPos end = ChunkSectionPos.from( new BlockPos( center.getMaxX() + val * 16, 255, center.getMaxZ() ) );
		List< Entry< ClaimBox, Claim > > claims = ClaimUtils.getClaimsInBox( world, start.getCenterPos(), end.getCenterPos() ).filter( claimBoxClaimEntry -> claimBoxClaimEntry.getValue().getOwners().contains( player ) ).collect( Collectors.toList() );
		start = ChunkSectionPos.from( new BlockPos( center.getMinX(), center.getCenterPos().getY(), center.getMinZ() - val * 16 ) );
		end = ChunkSectionPos.from( new BlockPos( center.getMaxX(), center.getCenterPos().getY(), center.getMaxZ() + val * 16 ) );
		claims.addAll( ClaimUtils.getClaimsInBox( world, start.getCenterPos(), end.getCenterPos() ).filter( claimBoxClaimEntry -> claimBoxClaimEntry.getValue().getOwners().contains( player ) ).collect( Collectors.toList() ) );
		return claims;
	}

	public static @NotNull Text position( @NotNull BlockPos pos ) {
		return Text.literal( pos.toShortString().replace( ", ", " " ) )
			.setStyle(
				Style.EMPTY
					.withClickEvent(
						new ClickEvent(
							ClickEvent.Action.RUN_COMMAND,
							"/tp @s %s %s %s".formatted( pos.getX(), pos.getY(), pos.getZ() )
						)
					)
					.withFormatting( Formatting.GREEN )
			);
	}

	public static Path getJarPath() {
		var root = CONTAINER.getRoot();
		var path = root.toUri().toString();

		if ( path.startsWith( "jar:file" ) )
			root = Path.of( path.substring( 12, path.length() - 2 ) );

		return root;
	}

	public static @Nullable QuiverItem.Data getQuiverData( @NotNull PlayerEntity player ) {
		for ( var stack : player.getInventory().main )
			if ( stack.getItem() instanceof QuiverItem )
				return QuiverItem.getQuiverData( stack );

		for ( var stack : player.getInventory().offHand )
			if ( stack.getItem() instanceof QuiverItem )
				return QuiverItem.getQuiverData( stack );

		return null;
	}

	public static boolean isOverworld( @NotNull ServerWorld world ) {
		return world == world.getServer().getWorld( ServerWorld.OVERWORLD );
	}

	public static boolean isAdminClaim( @NotNull BlockState claim ) {
		return claim.getBlock() == GOMLBlocks.ADMIN_CLAIM_ANCHOR.getFirst();
	}

	public static boolean isAdminClaim( @NotNull Claim claim ) {
		return claim.getType() == GOMLBlocks.ADMIN_CLAIM_ANCHOR.getFirst();
	}
}

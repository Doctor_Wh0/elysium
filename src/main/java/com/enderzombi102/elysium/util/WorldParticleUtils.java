package com.enderzombi102.elysium.util;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;

import java.util.ArrayList;

// TODO: Move to library?
@Environment( EnvType.CLIENT )
public class WorldParticleUtils {
	public static void spawnCube( MinecraftClient client, ClientWorld world, BlockPos min, BlockPos max, ParticleEffect effect ) {
		var maxInterval = 5;
		var maxCount = 40;

		for ( var edge : edges( min, max ) ) {
			var length = edge.length();
			var interval = 1.0;
			if ( length > 0 )
				interval = MathHelper.clamp( length / Math.min( maxCount, length ), 1, maxInterval );

			var steps = ( (double) length + interval - 1.0 ) / interval;

			for ( double i = 0.0; i < steps; ++i ) {
				var m = i * interval / (double) length;
				spawnParticleIfVisible( client, world, effect, edge.projX( m ) + 0.5, edge.projY( m ) + 0.5, edge.projZ( m ) + 0.5 );
			}
		}
	}

	private static void spawnParticleIfVisible( MinecraftClient client, ClientWorld world, ParticleEffect effect, double x, double y, double z ) {
		var player = client.player;
		assert player != null : "Why are we rendering particles without a player?";
		var delta = player.getPos().subtract( x, y, z );

		if ( !( delta.lengthSquared() > 262144.0 ) )
			world.addParticle( effect, true, x, y, z, .0, .0, .0 );
	}

	private static Edge[] edges( BlockPos min, BlockPos max ) {
		var list = new ArrayList< Edge >();
		var minX = min.getX();
		var minY = min.getY();
		var minZ = min.getZ();
		var maxX = max.getX();
		var maxY = max.getY();
		var maxZ = max.getZ();
		list.add( new Edge( minX, minY, minZ, minX, maxY, minZ ) );
		list.add( new Edge( maxX, minY, minZ, maxX, maxY, minZ ) );
		list.add( new Edge( minX, minY, maxZ, minX, maxY, maxZ ) );
		list.add( new Edge( maxX, minY, maxZ, maxX, maxY, maxZ ) );
		list.add( new Edge( minX, minY, minZ, maxX, minY, minZ ) );
		list.add( new Edge( minX, minY, maxZ, maxX, minY, maxZ ) );
		list.add( new Edge( maxX, minY, minZ, maxX, minY, maxZ ) );
		list.add( new Edge( minX, minY, minZ, minX, minY, maxZ ) );
		list.add( new Edge( minX, maxY, minZ, minX, maxY, maxZ ) );
		list.add( new Edge( maxX, maxY, minZ, maxX, maxY, maxZ ) );
		list.add( new Edge( minX, maxY, minZ, maxX, maxY, minZ ) );
		list.add( new Edge( minX, maxY, maxZ, maxX, maxY, maxZ ) );
		var height = max.getY() - min.getY();
		var count = MathHelper.ceil( (double) height / 64.0 );
		var delta = height / count;

		for ( var i = 1; i < count; ++i ) {
			list.add( new Edge( minX, minY + i * delta, minZ, maxX, minY + i * delta, minZ ) );
			list.add( new Edge( minX, minY + i * delta, maxZ, maxX, minY + i * delta, maxZ ) );
			list.add( new Edge( maxX, minY + i * delta, minZ, maxX, minY + i * delta, maxZ ) );
			list.add( new Edge( minX, minY + i * delta, minZ, minX, minY + i * delta, maxZ ) );
		}

		return list.toArray( new Edge[ 0 ] );
	}

	private record Edge( int startX, int startY, int startZ, int endX, int endY, int endZ ) {
		double projX( double m ) {
			return (double) this.startX + (double) ( this.endX - this.startX ) * m;
		}

		double projY( double m ) {
			return (double) this.startY + (double) ( this.endY - this.startY ) * m;
		}

		double projZ( double m ) {
			return (double) this.startZ + (double) ( this.endZ - this.startZ ) * m;
		}

		int length() {
			var dx = this.endX - this.startX;
			var dy = this.endY - this.startY;
			var dz = this.endZ - this.startZ;
			return MathHelper.ceil( Math.sqrt( dx * dx + dy * dy + dz * dz ) );
		}
	}
}


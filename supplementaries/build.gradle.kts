plugins {
	id( "org.quiltmc.loom" ) version "1.2.3"
//	id( "org.jetbrains.kotlin.jvm" ) version "1.9.0"
}

group = "com.enderzombi102"
version = "1.19.2-2.3.20-fabric"
val minecraftVersion = rootProject.ext[ "minecraftVersion" ]

dependencies {
	minecraft( "com.mojang:minecraft:$minecraftVersion" )
	mappings( "net.fabricmc:yarn:$minecraftVersion+build.21:v2" )
}

//tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
//	kotlinOptions.jvmTarget = "17"
//	kotlinOptions.languageVersion = "1.9"
//}

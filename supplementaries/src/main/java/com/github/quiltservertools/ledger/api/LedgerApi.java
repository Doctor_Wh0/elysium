package com.github.quiltservertools.ledger.api;

import com.github.quiltservertools.ledger.actions.ActionType;

public interface LedgerApi {

	/**
	 * Logs an action to the database
	 * @param action The action to log
	 */
	void logAction(ActionType action);
}

package net.mehvahdjukaar.supplementaries.common.items;


import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class QuiverItem extends Item {
	public QuiverItem( Settings settings ) {
		super( settings );
	}

	public static QuiverItem.Data getQuiverData( ItemStack stack ) {
		return null;
	}

	public static ItemStack getQuiver( LivingEntity entity ) {
		return null;
	}

	public interface Data {
		void consumeArrow();
	}
}


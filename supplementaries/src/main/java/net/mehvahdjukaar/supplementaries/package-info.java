/**
 * This package contains a subset of Supplementaries's classes, which are used to botch some sort of "compatability" with it.<p>
 * Everything in this package was taken from MehVahdJukaar's repo, which is ARR ( ofc it is, the code is horrible so what else could it be ).
 * <p>
 * repo: https://github.com/MehVahdJukaar/Supplementaries/tree/1.19 <p>
 * license: All Rights Reserved
 */
package net.mehvahdjukaar.supplementaries;
